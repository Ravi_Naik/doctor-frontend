
import axios from 'axios'

export const register = newUser => {
  return axios
    .post('users/register', {
      first_name: newUser.first_name,
      last_name: newUser.last_name,
      email: newUser.email,
      password: newUser.password
    })
    .then(response => {
      console.log('Registered')
    })
}





export const login = user => {
  return axios
    .post('userlogin', {
      email: user.email,
      password: user.password
    })
    .then(response => {
      localStorage.setItem('usertoken',JSON.stringify(response.data))
    //   const a=JSON.parse(localStorage.getItem('usertoken'));
    //   console.log(a.user.id)
      return response.data
      
    

    })
    .catch(err => {
      console.log(err)
    })
}

export const getProfile = user => {
  return axios
    .get('users/profile', {
      headers: { Authorization: ` ${this.getToken()}` }
    })
    .then(response => {
      console.log(response)
      return response.data
    })
    .catch(err => {
      console.log(err)
    })
}