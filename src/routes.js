/*!

=========================================================
* Light Bootstrap Dashboard React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Dashboard from "views/Dashboard.js";
import UserProfile from './views/UserProfile'
import UserManagement from "views/UserManagement.js";
// import UserManagement from "views/usermanagement1.js";

import PatientManagement from "views/PatientManagement.js";
import Icons from "views/Icons.js";
import FollowupManagement from "views/FollowupManagement.js";
import UserForm from "views/UserForm.js";
import PatientForm from "views/PatientForm.js";
import Main from './views/UpdatePatient/Main.js'
import bpatientupdate from './views/BpatientUpdate.js'
import Bpatient_Attachment from './views/Bpatient_Attachment.js'

import Examination from './views/RheumatologyForm/Examination/Examination'
import Examinationupdate from './views/RheumatologyForm/Examination/ExaminationUpdate'


import Investigation_Main from './views/RheumatologyForm/Investigation/Investigation'
import Investigationupdate from './views/RheumatologyForm/Investigation/InvestigationUpdate'


import Followup from './views/RheumatologyForm/Followup/Followup'
import FollowupUpdate from './views/RheumatologyForm/Followup/FollowupUpdate'


import rpatientupdate from './views/RheumatologyForm/Basic_details/Rpatientupdate'
import Attchments from './views/Bpatient_Attachment'
import bpatientpic from './views/Attachment/dbpic'

import bpatientprint from './views/Print_Details/bpatient'
import rpatientprint from './views/Print_Details/rpatient'

import rfpatientprint from './views/Print_Details/rfpatient'



import rapatient from './views/Attachment/rpatient_Attach'
import rfpatient from './views/Attachment/rfollow_Attach'

// import bvpatient from './views/Attachment/video_battach'





import Upgrade from "views/Upgrade.js";

const dashboardRoutes = [
    
  // {
  //   upgrade: true,
  //   path: "/upgrade",
  //   name: "Upgrade to PRO",
  //   icon: "nc-icon nc-alien-33",
  //   component: Upgrade,
  //   layout: "/admin",
  // },
  {

    path: "/dashboard",
    name: "Dashboard",
    icon: "nc-icon nc-chart-pie-35",
    component: Dashboard,
    layout: "/admin",
  },


  {
    path: "/umanagement",
    name: "UserManagement",
    icon: "nc-icon nc-alien-33",
    component:UserManagement,
    layout: "/admin",
  },
  

  {
    path: "/pmanagement",
    name: "PatientManagement",
    icon: "nc-icon nc-alien-33",
    component:PatientManagement,
    layout: "/admin",
  },

 
  // {
  //   path: "/icons",
  //   name: "Icons",
  //   icon: "nc-icon nc-atom",
  //   component: Icons,
  //   layout: "/admin",
  // },
  {
    path: "/followup",
    name: "Fallow-up View",
    icon: "nc-icon nc-vector",
    component:FollowupManagement,
    layout: "/admin",
  },

  
 



  {
    path: "/UserForm",
    // name: "UserForm",
    // icon: "nc-icon nc-bell-55",
    component: UserForm,
    layout: "/admin",
  },
  {
    path: "/user/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component: UserProfile,
    layout: "/admin",
  },

 

  

  // {
  //   path: "/Addattchment",
  //   // name: "UserForm",
  //   // icon: "nc-icon nc-bell-55",
  //   component: Bpatient_Attachment,
  //   layout: "/admin",
  // },
  
  {
  
    path:  "/bpatient/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component: bpatientupdate,
    layout: "/admin",
  },



  {
  
    path:  "/bpatientprint/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component: bpatientprint,
    layout: "/admin",
  },


  {
  
    path:  "/bpatientpic/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component:bpatientpic,
    layout: "/admin",
  },


  


  {
  
    path:  "/bapatient/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component: Attchments,
    layout: "/admin",
  },





  {
  
    path:  "/rapatient/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component: rapatient,
    layout: "/admin",
  },


  {
  
    path:  "/rpatientprint/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component:rpatientprint,
    layout: "/admin",
  },


  {
  
    path:  "/rfpatientprint/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component:rfpatientprint,
    layout: "/admin",
  },


  {
    path:  "/addrexamination/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component:Examination,
    layout: "/admin",
  },



  {
    path:  "/addrinvestigation/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component:Investigation_Main,
    layout: "/admin",
  },



  {
    path:  "/addrfollowup/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component:Followup,
    layout: "/admin",
  },



  {
    path:  "/rpatient/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component: rpatientupdate,
    layout: "/admin",
  },



  {
    path:  "/rfpatient/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component: rfpatient,
    layout: "/admin",
  },



  {
    path:  "/rexampatient/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component: Examinationupdate,
    layout: "/admin",
  },


  {
    path:  "/rinvespatient/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component: Investigationupdate,
    layout: "/admin",
  },

  {
    path:  "/rfollowpatient/edit/:id/",
    // name: "User Profile",
    // icon: "nc-icon nc-circle-09",
    component: FollowupUpdate,
    layout: "/admin",
  }


];

export default dashboardRoutes;
