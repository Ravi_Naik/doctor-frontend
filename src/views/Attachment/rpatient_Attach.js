

import axios from 'axios';

import React,{Component} from 'react';
import {
	Badge,
	Button,
	Card,
	Form,
	Navbar,
	Nav,
	Container,
	Row,
	Col,
  } from "react-bootstrap";
  import './upload.css'

class App extends Component {

	state = {

	// Initially, no file is selected
	selectedFile: null
	};
	
	// On file select (from the pop up)
	onFileChange = event => {
	
	// Update the state
	this.setState({ selectedFile: event.target.files[0] });
	
	};
	



    opensweetalert3()
    
    {
      Swal.fire({
        title:"Added Attachment",
        position: 'top-end',
     icon: 'success', 
    showConfirmButton: false,
    timer: 1200
        
      }).then(function() {
		window.location = "/admin/dashboard";
	});
    }


	// On file upload (click the upload button)
	onFileUpload = () => {
	
	// Create an object of formData
	const formData = new FormData();
	
	// Update the formData object
	formData.append(
		"file",
		this.state.selectedFile,
		this.state.selectedFile.name
	);
	
	// Details of the uploaded file
	console.log(this.state.selectedFile);
	
	// Request made to the backend api
	// Send formData object
	axios.post(`https://abcapi.vidaria.in/rfiles?X-AUTH=abc123&pid=${this.props.match.params.id}`, formData)
	.then(
		this.opensweetalert3()
	);
	};
	
	// File content to be displayed after
	// file upload is complete
	fileData = () => {
	
	if (this.state.selectedFile) {
		
		return (
		<div>
			<h2>File Details:</h2>
			
<p>File Name: {this.state.selectedFile.name}</p>

			
<p>File Type: {this.state.selectedFile.type}</p>

			
<p>
			Last Modified:{" "}
			{this.state.selectedFile.lastModifiedDate.toDateString()}
			</p>

		</div>
		);
	} else {
		return (
		<div>
			<br />
			<h4>Choosen file before Pressing the Upload button</h4>
		</div>
		);
	}
	};
	
	render() {
 

	return (
		<Container  style={{
			backgroundColor:'#BDC3C7',
			alignItems:"center",
			display:"flex"
			
		  }}>
			  <Row>
				  <Col md="12">
		<div >

		
		
			<h3  style={{
			alignItems:"center",
			
			
		  }}>
			 Upload Attachments!
			</h3>
			<div>
				<input type="file" name="file" onChange={this.onFileChange} />
				<Button  className="btn-fill pull-right"
                    type="submit"
                    name=""
                    variant="light" onClick={this.onFileUpload}>
				Upload!
				</Button>
			</div>
		{this.fileData()}
		</div>
		</Col>
		</Row>

		
		</Container>

	)
}
}

export default App;
