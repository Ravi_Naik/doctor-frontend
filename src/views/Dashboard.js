import React from "react";
import ChartistGraph from "react-chartist";
// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
  Form,
  OverlayTrigger,
  Tooltip,
} from "react-bootstrap";
import { Line } from 'react-chartjs-2';  
import { Bar } from 'react-chartjs-2';  



import PatientManagement from './PatientManagement'


import axios from 'axios';
import { data } from "jquery";

export default class Dashboard extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      users:{},
      deshboard:{},
      rhdashboard:{},
      bargraph:{}

    }
  }



  opensweetalert()
  {
    Swal.fire({
      title: 'Server Down Please restart server',
      text: "fail",
      type: 'fail',
      icon: 'error',
   
      
    }).then(function() {
      window.location = "/admin/dashboard";
  });
  }

  componentDidMount(){

    // var config ={
    //   headers : {
    //     'X-AUTH':'abc123'
    //   }
    // }
    // axios.defaults.headers.common['X-AUTH'] = 'abc123' // for all requests

    axios.get(`https://abcapi.vidaria.in/dashboardcomponents?X-AUTH=abc123`)
    .then((res)=>{
      // const a=JSON.parse(localStorage.getItem('token'));
      // console.log(a.user.id)
      console.log(res.data.components)
      this.setState({
        users:res.data.components
      })
    }).catch(error => {
      if (!error.response) {
        // network error
        this.errorStatus = this.opensweetalert();
    } else {
        this.errorStatus = error.response.data.message;
    }

    })

    axios.get(`https://abcapi.vidaria.in/dashboardyearmap?X-AUTH=abc123`,

    )
    .then((res)=>{
      // const a=JSON.parse(localStorage.getItem('token'));
      // console.log(a.user.id)
      console.log(res.data.yearmapcomponents)
      this.setState({
        // users:res.data.yearmapcomponents
       
      
        
      })
    })







    axios.get(`https://abcapi.vidaria.in/dashboardmap?X-AUTH=abc123`)
    .then(res => {
      // const a=JSON.parse(localStorage.getItem('token'));
      // console.log(a.user.id)
      // console.log(`.patientcount`,res.data.mapcomponents.breastpatient.patientcount);
      // console.log(`.bpdates`,res.data.mapcomponents.breastpatient.bpdates);
      const list=res.data.mapcomponents;
      // const list1=res.data.mapcomponents.breastpatient.bpdates;

      console.log(`list`,list)
      // console.log(`list1`,list1)


      // const p=res.data.mapcomponents.breastpatient.patientcount
      
      let bpdates =list.breastpatient.bpdates;
      let patientcount=list.breastpatient.patientcount;


      let rpdates =list.rheumapatient.rpdates;
      let patientcount1=list.rheumapatient.patientcount;

      // let 
      // list.forEach(record => {
      // bpdates.push(record.patientcount);
      // console.log(record.patientcount)
    

      // // patientcount.push(record.patientcount);
        
      // });

console.log(bpdates)
console.log(patientcount)
      this.setState({
        deshboard:{
          labels:bpdates,
          datasets:[
            {
              fill: false,
              lineTension: 10,
              backgroundColor: 'rgba(75,192,192,1)',
              borderColor: 'green',
              borderWidth: 2,


              label: 'Patients',
             data:patientcount,
              backgroundColor: [  
                "#3cb371",  
                "#0000FF",  
                "#9966FF",  
                "#4C4CFF",  
                "#00FFFF",  
                "#f990a7",  
                "#aad2ed",  
                "#FF00FF",  
                "Blue",  
                "Red"  
        ] 

            }
          ]
        }
       
      



        
      })



      this.setState({
        rhdashboard:{
          labels:rpdates,
          datasets:[
            {
              fill: false,
              lineTension: 0.5,
              backgroundColor: 'rgba(75,192,192,1)',
              borderColor: '#00BFFF',
              borderWidth: 2,


              label: 'Patients',
             data:patientcount1,
              backgroundColor: [  
                "#3cb371",  
                "#0000FF",  
                "#9966FF",  
                "#4C4CFF",  
                "#00FFFF",  
                "#f990a7",  
                "#aad2ed",  
                "#FF00FF",  
                "Blue",  
                "Red"  
        ] 

            }
          ]
        }
       
      



        
      })
    })



    axios.get(`https://abcapi.vidaria.in/dashboardyearmap?X-AUTH=abc123`)
    .then((res)=>{
      const list=res.data.yearmapcomponents;
      // const list1=res.data.mapcomponents.breastpatient.bpdates;

      console.log(`list`,list)
   
      let bp=list.breastpatient;
      let rp=list.rheumapatient;
      let fp=list.followrheumapatient;
      // let bp=[];
      // let rp=[];
      // let fp=[];

//       list.forEach(record => {  
//         bp.push(record.breastpatient);  
//         rp.push(record.rheumapatient); 
//         fp.push(record.followrheumapatient);  
//         console.log(record)
// });  


      this.setState({
        bargraph:{
          labels: ['January', 'February', 'March',
          'April', 'May','June','July','August','September','October','November','December'],
          datasets:[
            {
              fill: false,
              lineTension: 0.5,
              backgroundColor: '#FFFF00',
              borderColor: '##FFFF00',
              borderWidth: 2,


              label: 'Followup-Rheumatology Patients',
             data:fp,
              backgroundColor:"FFFF00"
   

            },




            {
              fill: false,
              lineTension: 0.5,
              backgroundColor: 'yellow',
              borderColor: 'yellow',
              borderWidth: 2,


              label: 'Breastpatient Patients',
             data:bp,
              backgroundColor:"yellow",

            },



            {
              fill: false,
              lineTension: 0.5,
              backgroundColor: 'red',
              borderColor: 'red',
              borderWidth: 2,


              label: 'Rheumatology Patients',
             data:rp,
              backgroundColor:"#DC143C"

            }
          ]
        }
       
      
        
      })
    })









  }


render(){

  return (
    <>

      <Container fluid>

     
  
      <Row>
       
   
          <Col lg="3" sm="6">
            <Card className="card-stats">
              <Card.Body>
                <Row>
                  <Col xs="5">
                    <div className="icon-big text-center icon-warning">
                      <i className="nc-icon nc-circle-09 text-warning"></i>
                    </div>
                  </Col>
                  <Col xs="7">
                    <div className="numbers">
                      <p className="card-category">Rheumatology Patients</p>
                      <Card.Title as="h4">{this.state.users.rpc}</Card.Title>
                    </div>
                  </Col>
                </Row>
              </Card.Body>
              <Card.Footer>
                <hr></hr>
                <div className="stats">
                  <i className="fas fa-redo mr-1"></i>
                  Rheumatology Patients
                  
                </div>
              </Card.Footer>
            </Card>
          </Col>
          <Col lg="3" sm="6">
            <Card className="card-stats">
              <Card.Body>
                <Row>
                  <Col xs="5">
                    <div className="icon-big text-center icon-warning">
                      <i className="nc-icon nc-circle-09 text-success"></i>
                    </div>
                  </Col>
                  <Col xs="7">
                    <div className="numbers">
                      <p className="card-category">Breast Cancer Patients</p>
                      <Card.Title as="h4">{this.state.users.bpc}</Card.Title>
                    </div>
                  </Col>
                </Row>
              </Card.Body>
              <Card.Footer>
                <hr></hr>
                <div className="stats">
                  <i className="far fa-calendar-alt mr-1"></i>
                  Breast Cancer Patients
                </div>
              </Card.Footer>
            </Card>
          </Col>
          <Col lg="3" sm="6">
            <Card className="card-stats">
              <Card.Body>
                <Row>
                  <Col xs="5">
                    <div className="icon-big text-center icon-warning">
                      <i className="nc-icon nc-vector text-danger"></i>
                    </div>
                  </Col>
                  <Col xs="7">
                    <div className="numbers">
                      <p className="card-category">Doctor's Available</p>
                      <Card.Title as="h4">{this.state.users.uc}</Card.Title>
                    </div>
                  </Col>
                </Row>
              </Card.Body>
              <Card.Footer>
                <hr></hr>
                <div className="stats">
                  <i className="far fa-clock-o mr-1"></i>
                  Doctor's Available Today
                </div>
              </Card.Footer>
            </Card>
          </Col>
          <Col lg="3" sm="6">
            <Card className="card-stats">
              <Card.Body>
                <Row>
                  <Col xs="5">
                    <div className="icon-big text-center icon-warning">
                      <i className="nc-icon nc-badge text-primary"></i>
                    </div>
                  </Col>
                  <Col xs="7">
                    <div className="numbers">
                      <p className="card-category">Total Patients</p>
                      <Card.Title as="h4">{this.state.users.tp}</Card.Title>
                    </div>
                  </Col>
                </Row>
              </Card.Body>
              <Card.Footer>
                <hr></hr>
                <div className="stats">
                  <i className="fas fa-redo mr-1"></i>
                  Total Patients as of now
                </div>
              </Card.Footer>
            </Card>
          </Col>
    
        </Row>
     
      <Row>
          <Col md="12">
            <Card>


            
            <PatientManagement />



            </Card>
          </Col>
          </Row>


       




        <Row>
          <Col md="6">
            <Card>
              <Card.Header>
                <Card.Title as="h4">Patient Graph</Card.Title>
                <p className="card-category">last 10 days record</p>
              </Card.Header>
              <Card.Body>
                <div className="ct-chart" id="chartHours">

              
                <Line
                      data={this.state.deshboard}  
                      options={{
                   title:{
                     display:true,
                     text:'Patients per day',
                     fontSize:20
                   },
                   
                   legend:{
                     display:true,
                     position:'right'
                   }
                  }}
                  options={{ maintainAspectRatio: false }}
                /> 

{/*               
                  <ChartistGraph
                    data={{

                     labels:                
                    [],
                      series: [
                        [287, 385, 490, 492, 554, 586, 698, 695],
                        [67, 152, 143, 240, 287, 335, 435, 437],
                      
                      ],
                      
                    }}
                    type="Line"
                    options={{
                      low: 0,
                      high: 800,
                      showArea: false,
                      height: "245px",
                      axisX: {
                        showGrid: false,
                      },
                      lineSmooth: true,
                      showLine: true,
                      showPoint: true,
                      fullWidth: true,
                      chartPadding: {
                        right: 50,
                      },
                    }}
                    responsiveOptions={[
                      [
                        "screen and (max-width: 640px)",
                        {
                          axisX: {
                            labelInterpolationFnc: function (value) {
                              return value[0];
                            },
                          },
                        },
                      ],
                    ]}
                  /> */}
               
                 
                </div>
              </Card.Body>
              <Card.Footer>
                <div className="legend">
                 
                <h4 style={{"color":"green"}}>Breast Cancer </h4>  
                  
                
                </div>
                <hr></hr>
                <div className="stats">
                  <i className="fas fa-history"></i>
                  Updated 24 Hours ago
                </div>
              </Card.Footer>
            </Card>
          </Col>
          
       
          <Col md="6">
            <Card>
              <Card.Header>
                <Card.Title as="h4">Patient Graph</Card.Title>
                <p className="card-category">last 10 days records</p>
              </Card.Header>
              <Card.Body>
                <div className="ct-chart" id="chartHours">
        
              
                <Line
                      data={this.state.rhdashboard}  
                      options={{
                   title:{
                     display:true,
                     text:'Patients per day',
                     fontSize:20
                   },
                   
                   legend:{
                     display:true,
                     position:'right'
                   }
                  }}
                  options={{ maintainAspectRatio: false }}
                />  
                  {/* <ChartistGraph
                    data={{
                      labels: [
                        "9:00AM",
                        "12:00AM",
                        "3:00PM",
                        "6:00PM",
                        "9:00PM",
                        "12:00PM",
                        "3:00AM",
                        "6:00AM",
                      ],
                      series: [
                        [287, 385, 490, 492, 554, 586, 698, 695],
                        [67, 152, 143, 240, 287, 335, 435, 437],
                      
                      ],
                    }}
                    type="Line"
                    options={{
                      low: 0,
                      high: 800,
                      showArea: false,
                      height: "245px",
                      axisX: {
                        showGrid: false,
                      },
                      lineSmooth: true,
                      showLine: true,
                      showPoint: true,
                      fullWidth: true,
                      chartPadding: {
                        right: 50,
                      },
                    }}
                    responsiveOptions={[
                      [
                        "screen and (max-width: 640px)",
                        {
                          axisX: {
                            labelInterpolationFnc: function (value) {
                              return value[0];
                            },
                          },
                        },
                      ],
                    ]}
                  /> */}
                </div>
              </Card.Body>
              <Card.Footer>
                <div className="legend">
     

                  <h4 style={{"color":"#00BFFF"}}>Rheumatology</h4>  

                </div>
                <hr></hr>
                <div className="stats">
                  <i className="fas fa-history"></i>
                  Updated 24 Hours ago
                </div>
              </Card.Footer>
            </Card>
          </Col>
          
        </Row>

















        <Row>
          <Col md="12">
            <Card>
              <Card.Header>
                <Card.Title as="h4">All Patient Treatment</Card.Title>
                <p className="card-category">All Patient records per year Treatment</p>
              </Card.Header>
              <Card.Body>
                <div className="ct-chart" id="chartActivity">
                    
                <Bar
                      data={this.state.bargraph}  
                      options={{
                   title:{
                     display:true,
                     text:'Patients per year',
                     fontSize:20
                   },
                   
                   legend:{
                     display:true,
                     position:'right'
                   }
                  }}
                  options={{ maintainAspectRatio: false }}
                /> 
                  {/* <ChartistGraph
                    data={{
                      labels: [
                        "Jan",
                        "Feb",
                        "Mar",
                        "Apr",
                        "Mai",
                        "Jun",
                        "Jul",
                        "Aug",
                        "Sep",
                        "Oct",
                        "Nov",
                        "Dec",
                      ],
                      series: [
                        [
                          542,
                          443,
                          320,
                          780,
                          553,
                          453,
                          326,
                          434,
                          568,
                          610,
                          756,
                          895,
                        ],
                        [
                          412,
                          243,
                          280,
                          580,
                          453,
                          353,
                          300,
                          364,
                          368,
                          410,
                          636,
                          695,
                        ],
                      ],
                    }}
                    type="Bar"
                    options={{
                      seriesBarDistance: 10,
                      axisX: {
                        showGrid: false,
                      },
                      height: "245px",
                    }}
                    responsiveOptions={[
                      [
                        "screen and (max-width: 640px)",
                        {
                          seriesBarDistance: 5,
                          axisX: {
                            labelInterpolationFnc: function (value) {
                              return value[0];
                            },
                          },
                        },
                      ],
                    ]}
                  /> */}
                </div>
              </Card.Body>
              <Card.Footer>
                <div className="legend">
                <h5  style={{"color":"tomato"}}> Rheumatology</h5>
                  <h5 style={{"color":"yellow"}}> Breast Cancer </h5>
                 <h5  style={{"color":"gray"}}>  Followup-Rheumatology</h5>
                 
                </div>
                <hr></hr>
                <div className="stats">
                  <i className="fas fa-check"></i>
                  Patient Treatment Information for year
                </div>
              </Card.Footer>
            </Card>
          </Col>
         
            


{/* 

          <Col md="6">
            <Card>
              <Card.Header>
                <Card.Title as="h4">2020 Treatment</Card.Title>
                <p className="card-category">All Patient Treatment</p>
              </Card.Header>
              <Card.Body>
                <div className="ct-chart" id="chartActivity">
                <Bar
                                        data={this.state.deshboard}  
                                        options={{ maintainAspectRatio: false }} />  */}
                  {/* <ChartistGraph
                    data={{
                      labels: [
                        "Jan",
                        "Feb",
                        "Mar",
                        "Apr",
                        "Mai",
                        "Jun",
                        "Jul",
                        "Aug",
                        "Sep",
                        "Oct",
                        "Nov",
                        "Dec",
                      ],
                      series: [
                        [
                          542,
                          443,
                          320,
                          780,
                          553,
                          453,
                          326,
                          434,
                          568,
                          610,
                          756,
                          895,
                        ],
                        [
                          412,
                          243,
                          280,
                          580,
                          453,
                          353,
                          300,
                          364,
                          368,
                          410,
                          636,
                          695,
                        ],
                      ],
                    }}
                    type="Bar"
                    options={{
                      seriesBarDistance: 10,
                      axisX: {
                        showGrid: false,
                      },
                      height: "245px",
                    }}
                    responsiveOptions={[
                      [
                        "screen and (max-width: 640px)",
                        {
                          seriesBarDistance: 5,
                          axisX: {
                            labelInterpolationFnc: function (value) {
                              return value[0];
                            },
                          },
                        },
                      ],
                    ]}
                  /> */}
                {/* </div> */}
              {/* </Card.Body>
              <Card.Footer>
                <div className="legend">
                  <i className="fas fa-circle text-info"></i>
                  Breast Cancer <i className="fas fa-circle text-danger"></i>
                  Rheumatology
                </div>
                <hr></hr>
                <div className="stats">
                  <i className="fas fa-check"></i>
                  Patient Treatment Information for year
                </div>
              </Card.Footer>
            </Card>
          </Col> */}
          </Row>
            
      </Container>
      
    </>
  );
}
}

