
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class BasicInfo extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}

  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Information</h2>

        <Row>

        <Form>
  
        <Row>
          <Col md="6">
          <Form.Group>
        <Form.Label>Patient Id</Form.Label><br></br>
          <Form.Control type="number" onChange={this.props.handleChange('pid')}
						defaultValue={values.pid}/>
          </Form.Group>


          <Form.Group>
          <Form.Label>code</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('code')}  defaultValue={values.code}/>
          </Form.Group>


         
        </Col>

        
        <Col md="6">
        <Form.Group>
          <Form.Label>Date</Form.Label><br></br>
          <Form.Control type="date"  onChange={this.props.handleChange('date')} defaultValue={values.date}  />
          </Form.Group>



          <Form.Group>
          <Form.Label>Patient Name</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('pname')} defaultValue={values.pname}/>
          </Form.Group>


          
          </Col>
          </Row>


          <Row>

          <Col md="6">

<Form.Group>
          <Form.Label>Age</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('age')} defaultValue={values.age}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>Gender</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('sex')} defaultValue={values.sex}/>
          </Form.Group>

</Col>

<Col md="6">

<Form.Group>

          <Form.Label>Address</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('address')} defaultValue={values.address}/>
                    </Form.Group>

                    <Form.Group>

<Form.Label>Phone</Form.Label><br></br>
<Form.Control type="number"  onChange={this.props.handleChange('phone')} defaultValue={values.phone}/>
          </Form.Group>

</Col>
</Row>

<Row>
  <Col md="6">
  <Form.Group>

            <Form.Label>Firstvisit</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('firstvisit')} defaultValue={values.firstvisit}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>Lastvisit</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('lastvisit')} defaultValue={values.lastvisit}/>
                    </Form.Group>


</Col>
<Col md="6">
<Form.Group>

          <Form.Label>Timelapsed</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('timelapsed')} defaultValue={values.timelapsed}/>
                    </Form.Group>



          <Form.Group>

          <Form.Label>Bp</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('district')} defaultValue={values.district}/>
                    </Form.Group>


</Col>
</Row> 

<Row>
  <Col md="6">
  <Form.Group>

          <Form.Label>Weight</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('wt')} defaultValue={values.wt}/>
                    </Form.Group>


          <Form.Group>


          <Form.Label>Followup</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('followup')}  defaultValue={values.followup}/>
                    </Form.Group>


          </Col>
          <Col md="6">
          <Form.Group>

          <Form.Label>Course_result</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('course_result')}  defaultValue={values.course_result}/>
                    </Form.Group>

          <Form.Group>

          <Form.Label>Improved_percent</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('improved_percent')}  defaultValue={values.improved_percent}/>
                    </Form.Group>


</Col>
</Row>

<Row>
  <Col md="6">
  <Form.Group>

          <Form.Label>Deteriorate_percent</Form.Label><br></br>
          <Form.Control type="number"  onChange={this.props.handleChange('deteriorate_percent')}  defaultValue={values.deteriorate_percent}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>History</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('history')} defaultValue={values.history}/>
                    </Form.Group>


         </Col> 
         <Col md="6">
         <Form.Group>

          <Form.Label>Diagnosis</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('diagnosis')}  defaultValue={values.diagnosis}/>
                    </Form.Group>



          <Form.Group>

          <Form.Label>Addnewdiagnosis</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('addnewdiagnosis')} defaultValue={values.addnewdiagnosis}/>
                    </Form.Group>

                    <Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

          </Col>
          </Row>














        </Form>
   
      </Row>
      </Container>
    )
  }
}

export default BasicInfo;







