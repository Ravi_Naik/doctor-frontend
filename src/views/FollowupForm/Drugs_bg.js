
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
  Table
} from "react-bootstrap";

class Drug_on extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}

  
    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>DRUG BEGUN ON</h2>

        <Row>

        <Form>
  
        <Row>
          <Col md="6">
          <Form.Group>
        <Form.Label>HCQ</Form.Label><br></br>
          <Form.Control type="date" onChange={this.props.handleChange('hcq')}
						defaultValue={values.hcq}/>
          </Form.Group>


          <Form.Group>
          <Form.Label>MTX</Form.Label><br></br>
          <Form.Control type="date"  onChange={this.props.handleChange('mtx')}  defaultValue={values.mtx}/>
          </Form.Group>


         
        </Col>

        
        <Col md="6">
        <Form.Group>
          <Form.Label>SSZ</Form.Label><br></br>
          <Form.Control type="date"  onChange={this.props.handleChange('ssz')} defaultValue={values.ssz}  />
          </Form.Group>



          <Form.Group>
          <Form.Label>LEP</Form.Label><br></br>
          <Form.Control type="date"  onChange={this.props.handleChange('lef')} defaultValue={values.lef}/>
          </Form.Group>


          
          </Col>
          </Row>


          <Row>

          <Col md="6">

<Form.Group>
          <Form.Label>AZT</Form.Label><br></br>
          <Form.Control type="date"  onChange={this.props.handleChange('azt')} defaultValue={values.azt}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>MMF</Form.Label><br></br>
          <Form.Control type="date"  onChange={this.props.handleChange('mmf')} defaultValue={values.mmf}/>
          </Form.Group>

</Col>

<Col md="6">

<Form.Group>

          <Form.Label>PRED</Form.Label><br></br>
          <Form.Control type="date"  onChange={this.props.handleChange('pred')} defaultValue={values.pred}/>
                    </Form.Group>

                    <Form.Group>

<Form.Label>NSAID</Form.Label><br></br>
<Form.Control type="date"  onChange={this.props.handleChange('nsaid')} defaultValue={values.nsaid}/>
          </Form.Group>

</Col>
</Row>



<Table striped bordered hover>
  <thead>
    <tr>
    <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td>ESR</td>

      <td><input type="text" 	onChange={this.props.handleChange('esr')}
defaultValue={values.esr} maxlength="50" size="6" /></td>
     


<td>BUN</td>

      <td><input type="text" 	onChange={this.props.handleChange('bun')}
defaultValue={values.bun} maxlength="50" size="6" /></td>



<td>U.A</td>

      <td><input type="text" 	onChange={this.props.handleChange('ua')}
defaultValue={values.ua} maxlength="50" size="6" /></td>

    </tr>



    <tr>
      <td>HB</td>

      <td><input type="text" 	onChange={this.props.handleChange('hb')}
defaultValue={values.hb} maxlength="50" size="6" /></td>
     


<td>CR</td>

      <td><input type="text" 	onChange={this.props.handleChange('cr')}
defaultValue={values.cr} maxlength="50" size="6" /></td>


<td>BSL</td>

      <td><input type="text" 	onChange={this.props.handleChange('bsl')}
defaultValue={values.bsl} maxlength="50" size="6" /></td>


    </tr>

    <tr>
      <td>TLC</td>

      <td><input type="text" 	onChange={this.props.handleChange('tlc')}
defaultValue={values.tlc} maxlength="50" size="6" /></td>
     


<td>SGOT</td>

      <td><input type="text" 	onChange={this.props.handleChange('sgot')}
defaultValue={values.sgot} maxlength="50" size="6" /></td>



<td>RF</td>

      <td><input type="text" 	onChange={this.props.handleChange('rf')}
defaultValue={values.rf} maxlength="50" size="6" /></td>

    </tr>

    <tr>
      <td>DLC-P/L/E/M</td>

      <td><input type="text" 	onChange={this.props.handleChange('dlc')}
defaultValue={values.dlc} maxlength="50" size="6" /></td>
     


<td>SGPT</td>

      <td><input type="text" 	onChange={this.props.handleChange('sgpt')}
defaultValue={values.sgpt} maxlength="50" size="6" /></td>


<td>CRP</td>

      <td><input type="text" 	onChange={this.props.handleChange('crp')}
defaultValue={values.crp} maxlength="50" size="6" /></td>


    </tr>



    <tr>
      <td>PLATELET</td>

      <td><input type="text" 	onChange={this.props.handleChange('platelet')}
defaultValue={values.platelet} maxlength="50" size="6" /></td>
     


<td>ALB</td>

      <td><input type="text" 	onChange={this.props.handleChange('alb')}
defaultValue={values.alb} maxlength="50" size="6" /></td>



<td>BILI(T)</td>

      <td><input type="text" 	onChange={this.props.handleChange('bili')}
defaultValue={values.bili} maxlength="50" size="6" /></td>

    </tr>



    <tr>
      <td>URINE</td>

      <td><input type="text" 	onChange={this.props.handleChange('urine')}
defaultValue={values.urine} maxlength="50" size="6" /></td>
     


<td>GLOB</td>

      <td><input type="text" 	onChange={this.props.handleChange('glob')}
defaultValue={values.glob} maxlength="50" size="6" /></td>




<td>OTHERS</td>

      <td><input type="text" 	onChange={this.props.handleChange('other')}
defaultValue={values.other} maxlength="50" size="6" /></td>

    </tr>
  </tbody>
</Table>





<Row>

<Col md="6">

<Form.Group>
<Form.Label>X-ray</Form.Label><br></br>
<Form.Control type="text"  onChange={this.props.handleChange('xray')} defaultValue={values.xray}/>
</Form.Group>


</Col>

<Col md="6">

<Form.Group>


<Form.Label>Treatment Plan</Form.Label><br></br>
<Form.Control type="text" as="textarea" rows={3}  onChange={this.props.handleChange('treatment_plan')} defaultValue={values.treatment_plan}/>
</Form.Group>

</Col>
</Row>
<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">
<Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>
</Row>


        </Form>
   
      </Row>
      </Container>
    )
  }
}

export default Drug_on;







