
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Table,
  Col,
} from "react-bootstrap";

class Soft_tis_examination extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}


    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient MUSCULOSKELETAL_EXAMINATIONS</h2>
         <h5>Soft Tissue RHEUMATISM: Information</h5> 

        <Row>

        <Form>
  
        <Table striped bordered hover>
  <thead>
    <tr>
      <th></th>
      <th>RT</th>
      <th>LT</th>
      <th></th>
      <th>RT</th>
      <th>LT</th>
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td>OPT</td>
      <td><input type="text" 	onChange={this.props.handleChange('opt_rt')}
defaultValue={values.opt_rt} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('opt_lt')}
defaultValue={values.opt_lt} maxlength="50" size="4" /></td>

      <td>GLUT</td>
            <td><input type="text" 						onChange={this.props.handleChange('glut_rt')}
defaultValue={values.glut_rt} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('glut_lt')}
defaultValue={values.glut_lt} maxlength="50" size="4" /></td>

    </tr>
    <tr>
      <td>LCER</td>
      <td><input type="text" 	onChange={this.props.handleChange('lcer_rt')}
defaultValue={values.lcer_rt} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('lcer_lt')}
defaultValue={values.lcer_lt} maxlength="50" size="4" /></td>

      <td>TRCR</td>
            <td><input type="text" 						onChange={this.props.handleChange('trcr_rt')}
defaultValue={values.trcr_rt} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('trcr_lt')}
defaultValue={values.trcr_lt} maxlength="50" size="4" /></td>

    </tr>


    <tr>
      <td>TRPZ</td>
      <td><input type="text" 	onChange={this.props.handleChange('trpz_rt')}
defaultValue={values.trpz_rt} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('trpz_lt')}
defaultValue={values.trpz_lt} maxlength="50" size="4" /></td>

      <td>KNEE</td>
            <td><input type="text" 						onChange={this.props.handleChange('knee_rt')}
defaultValue={values.knee_rt} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('knee_lt')}
defaultValue={values.knee_lt} maxlength="50" size="4" /></td>

    </tr>

    <tr>
      <td>SCAP</td>
      <td><input type="text" 	onChange={this.props.handleChange('scap_rt')}
defaultValue={values.scap_rt} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('scap_lt')}
defaultValue={values.scap_lt} maxlength="50" size="4" /></td>

      <td>TA</td>
            <td><input type="text" 						onChange={this.props.handleChange('ta_rt')}
defaultValue={values.ta_rt} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('ta_lt')}
defaultValue={values.ta_lt} maxlength="50" size="4" /></td>

    </tr>
    <tr>
      <td>Z CST</td>
      <td><input type="text" 	onChange={this.props.handleChange('zcst_rt')}
defaultValue={values.zcst_rt} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('zcst_lt')}
defaultValue={values.zcst_lt} maxlength="50" size="4" /></td>

      <td>CALF</td>
            <td><input type="text" 						onChange={this.props.handleChange('calf_rt')}
defaultValue={values.calf_rt} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('calf_lt')}
defaultValue={values.calf_lt} maxlength="50" size="4" /></td>

    </tr>
    <tr>
      <td>EPDL</td>
      <td><input type="text" 	onChange={this.props.handleChange('epdl_rt')}
defaultValue={values.epdl_rt} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('epdl_lt')}
defaultValue={values.epdl_lt} maxlength="50" size="4" /></td>

      <td>SOLE</td>
            <td><input type="text" 						onChange={this.props.handleChange('sole_rt')}
defaultValue={values.sole_rt} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('sole_lt')}
defaultValue={values.sole_lt} maxlength="50" size="4" /></td>

    </tr>



   
  </tbody>
</Table>

<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">
<Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>
</Row>
        </Form>
   
      </Row>

      
      </Container>
    )
  }
}

export default Soft_tis_examination;







