
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Table,
  Col,
} from "react-bootstrap";

class Toxicity extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}


    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Toxicity</h2>

        <Row>

        <Form>
  
        <Table striped bordered hover>
  <thead>
    <tr>
    <th>Adverse Effects</th>
      <th>YES/NO</th>
      <th>Duration</th>
      <th>Severity (mild/moderate/severe)</th>
      <th>Cause</th>
    
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td>ANOREXIA</td>

      <td><input type="text" 	onChange={this.props.handleChange('anorexia')}
defaultValue={values.anorexia} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('anorexia_duration')}
defaultValue={values.anorexia_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={this.props.handleChange('anorexia_severity')}
defaultValue={values.anorexia_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('anorexia_cause')}
defaultValue={values.anorexia_cause} maxlength="50" size="6" /></td>


    </tr>


    <tr>
      <td>NAUSEA</td>

      <td><input type="text" 	onChange={this.props.handleChange('nausea')}
defaultValue={values.nausea} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('nausea_duration')}
defaultValue={values.nausea_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={this.props.handleChange('nausea_severity')}
defaultValue={values.nausea_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('nausea_cause')}
defaultValue={values.nausea_cause} maxlength="50" size="6" /></td>


    </tr>





    <tr>
      <td>VOMITING</td>

      <td><input type="text" 	onChange={this.props.handleChange('vomiting')}
defaultValue={values.vomiting} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('vomiting_duration')}
defaultValue={values.vomiting_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={this.props.handleChange('vomiting_severity')}
defaultValue={values.vomiting_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('vomiting_cause')}
defaultValue={values.vomiting_cause} maxlength="50" size="6" /></td>


    </tr>




    <tr>
      <td>ACIDITY</td>

      <td><input type="text" 	onChange={this.props.handleChange('bio_lft4')}
defaultValue={values.bio_lft4} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('bio_rft4')}
defaultValue={values.bio_rft4} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={this.props.handleChange('bio_crp4')}
defaultValue={values.bio_crp4} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('bio_bsl4')}
defaultValue={values.bio_bsl4} maxlength="50" size="6" /></td>


    </tr>
  


    <tr>
      <td>PAIN ABDOMEN</td>

      <td><input type="text" 	onChange={this.props.handleChange('pain_abdomen')}
defaultValue={values.pain_abdomen} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('pain_duration')}
defaultValue={values.pain_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={this.props.handleChange('pain_severity')}
defaultValue={values.pain_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('pain_cause')}
defaultValue={values.pain_cause} maxlength="50" size="6" /></td>

    </tr>









    <tr>
      <td>DIARRHOEA</td>

      <td><input type="text" 	onChange={this.props.handleChange('diarrhoea')}
defaultValue={values.diarrhoea} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('diarrhoea_duration')}
defaultValue={values.diarrhoea_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={this.props.handleChange('diarrhoea_severity')}
defaultValue={values.diarrhoea_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('diarrhoea_cause')}
defaultValue={values.diarrhoea_cause} maxlength="50" size="6" /></td>


    </tr>


    <tr>
      <td>ORAL ULCERS</td>

      <td><input type="text" 	onChange={this.props.handleChange('oral_ulcers')}
defaultValue={values.oral_ulcers} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('ulcers_duration')}
defaultValue={values.ulcers_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={this.props.handleChange('ulcers_severity')}
defaultValue={values.ulcers_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('ulcers_cause')}
defaultValue={values.ulcers_cause} maxlength="50" size="6" /></td>


    </tr>





    <tr>
      <td>CONSTIPATION</td>

      <td><input type="text" 	onChange={this.props.handleChange('constipation')}
defaultValue={values.constipation} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('constipation_duration')}
defaultValue={values.constipation_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={this.props.handleChange('constipation_severity')}
defaultValue={values.constipation_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('constipation_cause')}
defaultValue={values.constipation_cause} maxlength="50" size="6" /></td>


    </tr>




    <tr>
      <td>SKIN RASH</td>

      <td><input type="text" 	onChange={this.props.handleChange('skinrash')}
defaultValue={values.skinrash} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('skinrash_duration')}
defaultValue={values.skinrash_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={this.props.handleChange('skinrash_severity')}
defaultValue={values.skinrash_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('skinrash_cause')}
defaultValue={values.skinrash_cause} maxlength="50" size="6" /></td>


    </tr>
  


    <tr>
      <td>HAIR FALL</td>

      <td><input type="text" 	onChange={this.props.handleChange('hairfall')}
defaultValue={values.hairfall} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('hairfall_duration')}
defaultValue={values.hairfall_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={this.props.handleChange('hairfall_severity')}
defaultValue={values.hairfall_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('hairfall_cause')}
defaultValue={values.hairfall_cause} maxlength="50" size="6" /></td>

    </tr>
  </tbody>
</Table>

<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">
<Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>
</Row>
        </Form>
   
      </Row>

      
      </Container>
    )
  }
}

export default Toxicity;