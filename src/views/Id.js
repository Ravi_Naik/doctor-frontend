class Parent extends React.Component {
    render() {
        return (
            <Child example="foo" />
        )
    }
}


import React from "react";

import axios from "axios";
import Modal from "react-bootstrap/Modal";
import Main from "../views/FollowupForm/Main2"

// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
} from "react-bootstrap";
import userdetails from './admin'

class FollowupManagement extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      users:[],
      pid:userdetails()

    }

  }

  handleModalShowHide() {
    this.setState({ showHide: !this.state.showHide })
}  
  componentDidMount(){

    axios.get(`https://abcapi.vidaria.in/allrpatientdetails`)
    .then((res)=>{
      console.log(res)
      this.setState({
pid:res.data.rheumatology_patient.pid
   
      
        
      })
    })
    // axios.get( `https://abcapi.vidaria.in/allrpatientdetails`)
    // .then((res)=>{
    //   console.log(res)
    //   this.setState({
    //     user:res.data.rheumatology_patient,
      
        
    //   })
    // })



    axios.get( `https://abcapi.vidaria.in/allfollowuprheumadetails?pid=2`)
    .then((res)=>{
      console.log(res)
      this.setState({
        users:res.data.RheumatologyFollowup,
      





        
      })
    })
  }
  delete(id){
    axios.delete(`https://abcapi.vidaria.in/userdelete/${this.props.match.params.id}`)
    .then(()=>{
      this.componentDidMount();
    })
  }



  render()

 {
  return (
    <>


<Modal 
                
                size="lg"
                
                show={this.state.showHide}>
                    <Modal.Header closeButton onClick={() => this.handleModalShowHide()}>
                 </Modal.Header>
                    <Modal.Body>


<Main />

                    </Modal.Body>
                    <Modal.Footer>
                   <p>Please Fill the Details</p>
                    </Modal.Footer>
                </Modal>


      <Container fluid>
        <div className="container">
          <div className="row" >
           
      <Button variant="info" className="btn-fill" onClick={() => this.handleModalShowHide()} type="submit" name="action" style={{ float: "right" }}>
                Add Patient Followup
                </Button> 
                </div>
                </div>
                <br></br>
        <Row>
          <Col md="12">
            <Card className="strpied-tabled-with-hover">
              <Card.Header>
                <Card.Title as="h4">PatientManagement</Card.Title>
                <p className="card-category">
                  Here is a Patient data
                </p>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover table-striped">
                  <thead>
                    <tr>
                    <th className="border-0">User ID</th>
                    <th className="border-0">Code</th>

                     <th className="border-0">Patient Name</th>
                     <th className="border-0">Patient ID</th>
                      <th className="border-0">Phone</th>
                      <th className="border-0">Address</th>
                      <th className="border-0">Date of Subscribe</th>
                      <th className="border-0">View</th>
                      <th className="border-0">Delete</th>
                      <th className="border-0">Add-attachments</th>






            
                    </tr>
                  </thead>
                  <tbody>
                  {
            this.state.users.map(user=>
              <tr key={user.id}>
               <td>{user.user.id}</td>

                <td>{user.code}</td>
                <td>{user.pname}</td>
                <td>{user.pid}</td>

                <td>{user.phone}</td>
                <td>{user.address}</td>
                <td>{user.date}</td>
                


                <td>
                <Button onClick={(e)=>this.edit(user.user.id)} variant="primary" className="btn-fill" type="submit" name="action">
                  {/* <i class="material-icons">edit</i> */}
                  Edit
                </Button>
                </td>
                <td>
                <Button onClick={(e)=>this.delete(user.user.id)} variant="danger" className="btn-fill" type="submit" name="action">
                  {/* <i class="material-icons">delete</i> */}
                  Delete
                </Button>
                </td>
                <td>
                <Button onClick={(e)=>this.add-attchment(user.user.id)} variant="warning" className="btn-fill" type="submit" name="action">
                  {/* <i class="material-icons">delete</i> */}
                  Add Attchments
                </Button>
                </td>
              </tr>
              )
          }

                  </tbody>
                </Table>
              </Card.Body>
            </Card>
          </Col>
          {/* <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Header>
                <Card.Title as="h4">Table on Plain Background</Card.Title>
                <p className="card-category">
                  Here is a subtitle for this table
                </p>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover">
                  <thead>
                    <tr>
                      <th className="border-0">ID</th>
                      <th className="border-0">Name</th>
                      <th className="border-0">Salary</th>
                      <th className="border-0">Country</th>
                      <th className="border-0">City</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Dakota Rice</td>
                      <td>$36,738</td>
                      <td>Niger</td>
                      <td>Oud-Turnhout</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Minerva Hooper</td>
                      <td>$23,789</td>
                      <td>Curaçao</td>
                      <td>Sinaai-Waas</td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Sage Rodriguez</td>
                      <td>$56,142</td>
                      <td>Netherlands</td>
                      <td>Baileux</td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>Philip Chaney</td>
                      <td>$38,735</td>
                      <td>Korea, South</td>
                      <td>Overland Park</td>
                    </tr>
                    <tr>
                      <td>5</td>
                      <td>Doris Greene</td>
                      <td>$63,542</td>
                      <td>Malawi</td>
                      <td>Feldkirchen in Kärnten</td>
                    </tr>
                    <tr>
                      <td>6</td>
                      <td>Mason Porter</td>
                      <td>$78,615</td>
                      <td>Chile</td>
                      <td>Gloucester</td>
                    </tr>
                  </tbody>
                </Table>
              </Card.Body>
            </Card>
          </Col> */}
        </Row>
      </Container>
    </>
  );
}
}

export default FollowupManagement;



export default Parent;

class Child extends React.component {
    render() {
        return (
            <h1>{this.props.example}</h1>
        )
    }
}