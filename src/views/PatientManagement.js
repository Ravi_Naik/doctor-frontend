import React from "react";

import axios from "axios";
import { Link } from "react-router-dom";
import Modal from "react-bootstrap/Modal";
import Main from '../views/Patientform/Main'
import Main1 from '../views/RheumatologyForm/Main1'
import Main23 from '../views/RheumatologyForm/Rheuma_model'

import Examination_Main from '../views/RheumatologyForm/Examination_Main'
import datasend from '../views/RheumatologyForm/Examination_Main'
import Investigation_Main from '../views/RheumatologyForm/Investigation_Main'
import Follow from '../views/RheumatologyForm/Followup/Followup'
import step6 from '../views/Patientform/step6'
import UserForm from './UserForm'
import Attchments from './Bpatient_Attachment'

//Bootstrap and jQuery libraries
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.min.js';
//Datatable Modules
import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"
import $ from 'jquery';


// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
  Form,
  Dropdown,
} from "react-bootstrap";


class PatientManagement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      rusers: [],

      id: 0,
      username: "",
      email: "",
      phone: "",
      password: "",
      role: "",
      speciality: "",
      pid: "",
      showHide: false,
      open: false,
      modal1: false,
      modal2: false





    }


  }
  // openModal (){
  //   this.setState({open: true})}


  // closeModal (){
  //   this.setState({open: false}) }





  closeModal() {
    this.setState({ open: false })
  }

  // handleModalChange1 () {
  //   this.setState({ ModalContent : '<h1>Modal1 Content</h1>' })
  //   }

  // handleModalChange2  (){
  //   this.setState({ ModalContent : '<h1>Modal2 Content</h1>' })
  // }

  clearTable = () => {
    this.setState({ users: [] })
  }


  clearTable = () => {
    this.setState({ rusers: [] })
  }


  componentDidMount() {
    axios.get(`https://abcapi.vidaria.in/allbpatientdetails?X-AUTH=abc123`)
      .then((res) => {
        console.log(res)
        $('#example1').DataTable().destroy();
        this.setState({
          users: res.data.breast_patient
        }, () => {
          $('#example1').DataTable();
        })
      })

    axios.get(`https://abcapi.vidaria.in/allrpatientdetails?X-AUTH=abc123`)
      .then((pes) => {
        console.log(pes)
        $('#example2').DataTable().destroy();

        this.setState({
          rusers: pes.data.rheumatology_patient,
        }, () => {
          $('#example2').DataTable();
        })
      })


      $(document).ready(function () {
        $('#example1').DataTable(
  
        );
      });


      $(document).ready(function () {
        $('#example2').DataTable(
  
        );
      });

  }







  handleModalShowHide() {
    this.setState({ showHide: !this.state.showHide })
  }


  secondhandleModalShowHide() {
    this.setState({ open: !this.state.open })
  }


  thirdhandleModalShowHide() {
    this.setState({ modal1: !this.state.modal1 })
  }



  fourthhandleModalShowHide(pid) {
    // console.log(pid)
    // var client = new FBClient();
    // client.loadFromConfig(config);
    var apple = new datasend(pid)
    apple.datasend(pid)
    this.setState({ modal2: !this.state.modal2 })
  }


  // Updatebpatient = () => {
  //   axios.get(`https://abcapi.vidaria.in/bpatientdetails/1`)
  //   .then((res)=>{
  //     console.log(res)
  //     this.setState({
  //       // users:res.data.bpatient,

  //       // is this correct way?


  //     })
  //   })
  // }


  // submit(event,id){
  //   event.preventDefault();
  //   if(id === 0){
  //     axios.post("https://abcapi.vidaria.in/adduser",{
  //       username:this.state.username,
  //       email:this.state.email,
  //       phone:this.state.phone,
  //       password:this.state.password,
  //       role:this.state.role,
  //       speciality:this.state.speciality,
  //     })
  //     .then((res)=>{
  //       console.log(res)
  //     })
  //   }else{
  //     axios.put(`https://abcapi.vidaria.in/userupdate/${this.props.match.params.id}`,{
  //       id:this.state.id,
  //       username:this.state.username,
  //       email:this.state.email,
  //       phone:this.state.phone,
  //       password:this.state.password,
  //       role:this.state.role,
  //       speciality:this.state.speciality,
  //     }).then(()=>{
  //       this.componentDidMount();
  //     })

  //   }

  // }



  deleter(id, e) {

    const b = JSON.parse(localStorage.getItem('token'));
    console.log(b.user.id)

    var del = "deleted Rheumatology Patient "
    e.preventDefault()
    Swal.fire({
      title: 'Do you want to delete the patient?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: `Delete`,
      denyButtonText: `Cancel`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('Patient deleted!', '', 'success')
        axios.delete(`https://abcapi.vidaria.in/rheumapatientdelete?X-AUTH=abc123&pid=${id}`)
        axios.post(`https://abcapi.vidaria.in/addactivity?X-AUTH=abc123`, {

          activity: del,
          user_id: b.user.id,

        })

          .then(function () {
            window.location = "/admin/dashboard";
          });
      } else if (result.isDenied) {
        Swal.fire('patient record is safe !', '', 'info')
      }
    })
  }




  deleteb(id, e) {


    const b = JSON.parse(localStorage.getItem('token'));
    console.log(b.user.id)
    var del = "deleted breast cancer Patient "

    e.preventDefault()
    Swal.fire({
      title: 'Do you want to delete the patient?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: `Delete`,
      denyButtonText: `Cancel`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('Patient deleted!', '', 'success')
        axios.delete(`https://abcapi.vidaria.in/bpatientdelete?X-AUTH=abc123&pid=${id}?X-AUTH=abc123`)
        axios.post(`https://abcapi.vidaria.in/addactivity?X-AUTH=abc123`, {

          activity: del,
          user_id: b.user.id,

        })
          .then(function () {
            window.location = "/admin/dashboard";
          });
      } else if (result.isDenied) {
        Swal.fire('patient record is safe !', '', 'info')
      }
    })
  }











  // deleter(id){
  //   axios.delete(`https://abcapi.vidaria.in/rheumapatientdelete?pid=${id}`)
  //   .then(()=>{
  //     this.componentDidMount();
  //   })
  // }

  // deleteb(id){
  //   axios.delete(`https://abcapi.vidaria.in/bpatientdelete?pid=${id}`)
  //   .then(()=>{
  //     this.componentDidMount();
  //   })
  // }


  render() {
    return (
      <>

        <div>


          <Modal

            size="lg"

            show={this.state.modal1}>
            <Modal.Header closeButton onClick={() => this.thirdhandleModalShowHide()}>
            </Modal.Header>
            <Modal.Body>
              <Investigation_Main />



            </Modal.Body>
            {/* <Modal.Footer>
                   <p>Please Fill the Details</p>
                    </Modal.Footer> */}
          </Modal>












          <Modal

            size="lg"

            show={this.state.showHide}>
            <Modal.Header closeButton onClick={() => this.handleModalShowHide()}>
            </Modal.Header>
            <Modal.Body>


              <Main />



            </Modal.Body>
            {/* <Modal.Footer>
                   <p>Please Fill the Details</p>
                    </Modal.Footer> */}
          </Modal>





          {/* modal 2 */}
          <Modal


            size="lg"

            show={this.state.open}>
            <Modal.Header closeButton onClick={() => this.secondhandleModalShowHide()}>
            </Modal.Header>
            <Modal.Body>


              <Main1 />

            </Modal.Body>

          </Modal>



          <Modal

            size="lg"

            show={this.state.modal2}>
            <Modal.Header closeButton onClick={() => this.fourthhandleModalShowHide()}>
            </Modal.Header>
            <Modal.Body>


              <Examination_Main />



            </Modal.Body>
            {/* <Modal.Footer>
                   <p>Please Fill the Details</p>
                    </Modal.Footer> */}
          </Modal>

        </div>
        <Container fluid>
          <div className="container">
            <div className="row" >

              <Col md="6" style={{ float: "left" }}>


                <Button variant="info" className="btn-fill" onClick={() => this.secondhandleModalShowHide()} type="submit" name="action" style={{ float: "right" }}>

                  Add Rheumatology Patient
                 </Button>
                {/* <Dropdown>
  <Dropdown.Toggle variant="info"  className="btn-fill" id="dropdown-basic">
  Add Rheumatology Patient
  </Dropdown.Toggle>

  <Dropdown.Menu>
    <Dropdown.Item  onClick={() => this.secondhandleModalShowHide()}>Basic patient details</Dropdown.Item>
    <Dropdown.Item  onClick={() => this.fourthhandleModalShowHide()}>Add examination </Dropdown.Item>
    <Dropdown.Item  onClick={() => this.thirdhandleModalShowHide()}>Add investigation </Dropdown.Item>

  </Dropdown.Menu>
</Dropdown> */}

              </Col>
              <Col md="6">

                <Button variant="info" className="btn-fill" onClick={() => this.handleModalShowHide()} type="submit" name="action" style={{ float: "right" }}>

                  Add Breast Patient
                </Button>
              </Col>
            </div>
          </div>
          <br></br>
          <Row>
            <Col md="12">
              <Card className="strpied-tabled-with-hover">
                <Card.Header>
                  <Card.Title as="h4">Breast Cancer PatientManagement</Card.Title>
                  <p className="card-category">
                    Here is a Patient data
                </p>
                </Card.Header>
                <Card.Body className="table-full-width table-responsive px-0">
                  <table id="example1" className="table table-striped table-bordered table-sm row-border hover mb-5">
                    <thead>
                      <tr>
                        <th>User ID</th>
                        <th>Code</th>

                        <th>Patient Name</th>
                        <th>Patient ID</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>Date of Subscribe</th>
                        <th>Edit</th>

                        <th>Delete</th>
                        <th>Add Details</th>
                        <th>Add Attachments</th>
                        <th>prescription</th>


                      </tr>
                    </thead>
                    <tbody>
                      {
                        this.state.users.map(user =>
                          <tr key={user.id}>
                            <td>{user.user.id}</td>

                            <td>{user.code}</td>
                            <td>{user.pname}</td>
                            <td>{user.pid}</td>

                            <td>{user.phone}</td>
                            <td>{user.address}</td>
                            <td>{user.date}</td>



                            <td>
                              <Link to={`bpatient/edit/${user.pid}`}>
                                <Button variant="primary" className="btn-fill" type="submit" name="action">Edit</Button>
                              </Link>
                            </td>






                            <td>
                              <Button onClick={(e) => this.deleteb(user.pid, e)} variant="danger" className="btn-fill" type="submit" name="action">
                                {/* <i class="material-icons">delete</i> */}
                  Delete
                </Button>
                            </td>
                            <td>
                            <Dropdown>
                                <Dropdown.Toggle variant="secondary" className="btn-fill" style={{ width: "160px" }} id="dropdown-basic">
                                  Add Details
  </Dropdown.Toggle>

                                <Dropdown.Menu>

                                  <Dropdown.Item href="" >Add examination </Dropdown.Item>
                                  <Dropdown.Item href="" >Add investigation </Dropdown.Item>
                                  <Dropdown.Item href="" >Add Follow-up</Dropdown.Item>

                                </Dropdown.Menu>
                              </Dropdown>
                            </td>
                            <td>
                              <Link to={`bapatient/edit/${user.pid}`}>
                                <Button variant="warning" className="btn-fill" type="submit" name="action">
                                  {/* <i class="material-icons">delete</i> */}
                  Attachments
                </Button>
                              </Link>
                            </td>


                            <td>
                <Link to={`bpatientprint/edit/${user.pid}`}>
                <Button  variant="success" className="btn-fill" type="submit" name="action">
               print
                </Button>
                </Link>
                </td>
                          </tr>
                        )
                      }

                    </tbody>
                  </table>
                </Card.Body>
              </Card>
            </Col>

          </Row>



          <Row>
            <Col md="12">
              <Card className="strpied-tabled-with-hover">
                <Card.Header>
                  <Card.Title as="h4">Rheumatology PatientManagement</Card.Title>
                  <p className="card-category">
                    Here is a Patient data
                </p>
                </Card.Header>
                <Card.Body className="table-full-width table-responsive px-0">
                <table id="example2" className="table table-striped table-bordered table-sm row-border hover mb-5">
                    <thead>
                      <tr>
                        <th>User ID</th>
                        <th>Code</th>

                        <th>Patient Name</th>
                        <th>Patient ID</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>Date of Subscribe</th>
                        <th>Edit</th>
                        <th>View</th>

                        <th>Delete</th>
                        <th>Add Details</th>
                        <th>Add Attchments</th>
                        <th>prescription</th>







                      </tr>
                    </thead>
                    <tbody>
                      {
                        this.state.rusers.map(user =>
                          <tr key={user.id}>
                            <td>{user.user.id}</td>

                            <td>{user.code}</td>
                            <td>{user.pname}</td>
                            <td>{user.pid}</td>

                            <td>{user.phone}</td>
                            <td>{user.address}</td>
                            <td>{user.date}</td>



                            <td>
                              <Link to={`rpatient/edit/${user.pid}`}>
                                <Button variant="primary" className="btn-fill" type="submit" name="action">Edit</Button>
                              </Link>
                            </td>



                            <td>
                              <Dropdown>
                                <Dropdown.Toggle variant="info" className="btn-fill" style={{ width: "160px" }} id="dropdown-basic">
                                  View Details
  </Dropdown.Toggle>

                                <Dropdown.Menu>

                                  <Dropdown.Item href={`rexampatient/edit/${user.pid}`}  >View examination </Dropdown.Item>
                                  <Dropdown.Item href={`rinvespatient/edit/${user.pid}`} >View investigation </Dropdown.Item>
                                  <Dropdown.Item href={`rfollowpatient/edit/${user.pid}`}>View Follow-up</Dropdown.Item>

                                </Dropdown.Menu>
                              </Dropdown>
                            </td>


                            <td>
                              <Button onClick={(e) => this.deleter(user.pid, e)} variant="danger" className="btn-fill" type="submit" name="action">
                                {/* <i class="material-icons">delete</i> */}
                  Delete
                </Button>
                            </td>
                            <td>
                              <Dropdown>
                                <Dropdown.Toggle variant="secondary" className="btn-fill" style={{ width: "160px" }} id="dropdown-basic">
                                  Add Details
  </Dropdown.Toggle>

                                <Dropdown.Menu>

                                  <Dropdown.Item href={`addrexamination/edit/${user.pid}`}  >Add examination </Dropdown.Item>
                                  <Dropdown.Item href={`addrinvestigation/edit/${user.pid}`} >Add investigation </Dropdown.Item>
                                  <Dropdown.Item href={`addrfollowup/edit/${user.pid}`}>Add Follow-up</Dropdown.Item>

                                </Dropdown.Menu>
                              </Dropdown>
                            </td>
                            <td>
                              <Link to={`rapatient/edit/${user.pid}`}>
                                <Button variant="warning" className="btn-fill" type="submit" name="action">
                                  {/* <i class="material-icons">delete</i> */}
                  Attachments
                </Button>
                              </Link>
                            </td>
                            <td>
                            <Link to={`rpatientprint/edit/${user.pid}`}>
                <Button  variant="success" className="btn-fill" type="submit" name="action">
                  print
                </Button>
                </Link>
                </td>
                          </tr>
                        )
                      }

                    </tbody>
                  </table>
                </Card.Body>
              </Card>
            </Col>
            {/* <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Header>
                <Card.Title as="h4">Table on Plain Background</Card.Title>
                <p className="card-category">
                  Here is a subtitle for this table
                </p>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th className="border-0">Name</th>
                      <th className="border-0">Salary</th>
                      <th className="border-0">Country</th>
                      <th className="border-0">City</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Dakota Rice</td>
                      <td>$36,738</td>
                      <td>Niger</td>
                      <td>Oud-Turnhout</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Minerva Hooper</td>
                      <td>$23,789</td>
                      <td>Curaçao</td>
                      <td>Sinaai-Waas</td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Sage Rodriguez</td>
                      <td>$56,142</td>
                      <td>Netherlands</td>
                      <td>Baileux</td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>Philip Chaney</td>
                      <td>$38,735</td>
                      <td>Korea, South</td>
                      <td>Overland Park</td>
                    </tr>
                    <tr>
                      <td>5</td>
                      <td>Doris Greene</td>
                      <td>$63,542</td>
                      <td>Malawi</td>
                      <td>Feldkirchen in Kärnten</td>
                    </tr>
                    <tr>
                      <td>6</td>
                      <td>Mason Porter</td>
                      <td>$78,615</td>
                      <td>Chile</td>
                      <td>Gloucester</td>
                    </tr>
                  </tbody>
                </Table>
              </Card.Body>
            </Card>
          </Col> */}
          </Row>
        </Container>
      </>
    );
  }
}

export default PatientManagement;




// class App extends React.Component {
//   constructor(props){
//     super(props);
//     this.state ={
//       users:[],
//       id:0,
//       username: "",
//       email: "",  
//       phone: "",
//       password: "",
//       role:"",
//       speciality:""
//     }

//   }
//   componentDidMount(){
//     axios.get("https://abcapi.vidaria.in/alluserdetails")
//     .then((res)=>{
//       this.setState({
//         users:res.data.user,
//         id:0,
//         username:'',
//         email:'',
//         phone:'',
//         password:'',
//         role:'',
//         speciality:'',

//       })
//     })
//   }
//   submit(event,id){
//     event.preventDefault();
//     if(id === 0){
//       axios.post("https://abcapi.vidaria.in/adduser",{
//         username:this.state.username,
//         email:this.state.email,
//         phone:this.state.phone,
//         password:this.state.password,
//         role:this.state.role,
//         speciality:this.state.speciality,
//       })
//       .then((res)=>{
//         this.componentDidMount();
//       })
//     }else{
//       axios.put(`https://abcapi.vidaria.in/userupdate/${id}`,{
//         id:this.state.id,
//         username:this.state.username,
//         email:this.state.email,
//         phone:this.state.phone,
//         password:this.state.password,
//         role:this.state.role,
//         speciality:this.state.speciality,
//       }).then(()=>{
//         this.componentDidMount();
//       })

//     }

//   }
//   delete(id){
//     axios.delete(`https://abcapi.vidaria.in/userdelete/${id}`)
//     .then(()=>{
//       this.componentDidMount();
//     })
//   }
//   edit(id){
//     axios.get(`https://abcapi.vidaria.in/userdetails/${id}`)
//     .then((res)=>{
//       console.log(res.data.user);
//       this.setState({
//         id:res.data.user.id,
//         username:res.data.user.username,
//         email:res.data.user.email,
//         phone:res.data.user.phone,
//         password:res.data.user.password,
//         role:res.data.user.role,
//         speciality:res.data.user.speciality,
//       })
//     })
//   }
//   render(){
//   return (
//     <div className="container" >

//     <div className="row">

//         <form onSubmit={(e)=>this.submit(e,this.state.id)}>

//         <label>Name</label>
//           <input onChange={(e)=>this.setState({username:e.target.value})} name="uname" value={this.state.username} type="text"   />



//           <label>email</label>
//           <input onChange={(e)=>this.setState({email:e.target.value})} name="email" value={this.state.email}  type="email"   />





//           <label>phone</label>
//           <input onChange={(e)=>this.setState({phone:e.target.value})} name="phone" value={this.state.phone} type="number"   />




//           <label>password</label>
//           <input onChange={(e)=>this.setState({password:e.target.value})} name="password" value={this.state.password}  type="password"   />




//           <label>role</label>
//           <input onChange={(e)=>this.setState({role:e.target.value})} name="role" value={this.state.role}  type="text"   />








//           <label>speciality</label>
//           <input onChange={(e)=>this.setState({speciality:e.target.value})} name="speciality" value={this.state.speciality} type="text"   />








//         <button className="btn waves-effect waves-light right" type="submit" name="action">Submit
//           <i className="material-icons right">send</i>
//         </button>
//         </form>
//       </div>


//       <table>
//         <thead>
//           <tr>
//               <th>Name</th>
//               <th>Email</th>
//               <th>Phone</th>
//               <th>Password</th>
//               <th>Role</th>
//               <th>Speciality</th>
//               <th>Date of Subscribe</th>
//               <th>Edit</th>
//               <th>Delete</th>
//           </tr>
//         </thead>

//         <tbody>
//           {
//             this.state.users.map(user=>
//               <tr key={user.id}>
//                 <td>{user.username}</td>
//                 <td>{user.email}</td>
//                 <td>{user.phone}</td>
//                 <td>{user.password}</td>
//                 <td>{user.role}</td>
//                 <td>{user.speciality}</td>
//                 <td>{user.dateofsub}</td>


//                 <td>
//                 <button onClick={(e)=>this.edit(user.id)} class="btn waves-effect waves-light" type="submit" name="action">
//                   <i class="material-icons">edit</i>
//                 </button>
//                 </td>
//                 <td>
//                 <button onClick={(e)=>this.delete(user.id)} class="btn waves-effect waves-light" type="submit" name="action">
//                   <i class="material-icons">delete</i>
//                 </button>
//                 </td>
//               </tr>
//               )
//           }

//         </tbody>
//       </table>



//     </div>
//   );
//   }
// }

