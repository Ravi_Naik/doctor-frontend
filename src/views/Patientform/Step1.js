


// import React, { Component } from 'react';
// import { Form, Button } from 'semantic-ui-react';

// class Step1 extends Component {

// 	saveAndContinue = (e) => {
// 		e.preventDefault()
// 		this.props.nextStep()
// 	}

// 	render() {
// 		const { values } = this.props;
// 		return (
// 			<Form >
// 				<h1 className="ui centered">Enter User Details</h1>
// 				<Form.Field>
// 					<label>First Name</label>
// 					<input
// 						placeholder='First Name'
// 						onChange={this.props.handleChange('username')}
// 						defaultValue={values.username}
// 					/>
// 				</Form.Field>
				
// 				<Form.Field>
// 					<label>Email Address</label>
// 					<input
// 						type='email'
// 						placeholder='Email Address'
// 						onChange={this.props.handleChange('email')}
// 						defaultValue={values.email}
// 					/>
// 				</Form.Field>

//                 <Form.Field>
// 					<label>Phone</label>
// 					<input
// 						type='number'
// 						placeholder='phone'
// 						onChange={this.props.handleChange('phone')}
// 						defaultValue={values.phone}
// 					/>
// 				</Form.Field>
// 				<Button onClick={this.saveAndContinue}>Save And Continue </Button>
// 			</Form>
// 		)
// 	}
// }

// export default Step1;


import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class Step1 extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}
	
back = (e) => {
		e.preventDefault();
		this.props.prevStep();
}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Information</h2>

        {/* <Row> */}

        <Form>


<Row>
  <Col md="6">
  <Form.Group>
            <Form.Label>Education</Form.Label><br></br>
          <Form.Control  type="text"  onChange={this.props.handleChange('education')} defaultValue={values.education}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>Address</Form.Label><br></br>
          <Form.Control  type="text"  onChange={this.props.handleChange('address')} defaultValue={values.address}/>
          </Form.Group>

</Col>
<Col md="6">
<Form.Group>
          <Form.Label>Phone</Form.Label><br></br>
          <Form.Control  type="number"  onChange={this.props.handleChange('phone')} defaultValue={values.phone}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>District</Form.Label><br></br>
          <Form.Control  type="text"  onChange={this.props.handleChange('district')} defaultValue={values.district}/>
          </Form.Group>

</Col>
</Row> 

<Row>
  <Col md="6">
  <Form.Group>
          <Form.Label>State</Form.Label><br></br>
          <Form.Control  type="text"  onChange={this.props.handleChange('state')} defaultValue={values.state}/>
          </Form.Group>


          <Form.Group>
          <Form.Label>Area</Form.Label><br></br>
          <Form.Control  type="text"  onChange={this.props.handleChange('area')}  defaultValue={values.area}/>
          </Form.Group>

          </Col>
          <Col md="6">
          <Form.Group>

          <Form.Label>Referred_by</Form.Label><br></br>
          <Form.Control  type="text"  onChange={this.props.handleChange('referred_by')}  defaultValue={values.referred_by}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>Occupation</Form.Label><br></br>
          <Form.Control  type="text"  onChange={this.props.handleChange('occupation')}  defaultValue={values.occupation}/>
          </Form.Group>

</Col>
</Row>

{/* 

<Row>
  <Col md="6">
          <Form.Label>Ethnic</Form.Label><br></br>
          <Form.Control  type="text"  onChange={this.props.handleChange('ethnic')}  defaultValue={values.ethnic}/>
          <br/>


          <label>Marital_status</label><br></br>
          <input type="text"  onChange={this.props.handleChange('marital_status')} defaultValue={values.marital_status}/>
          <br/>

         </Col> 
         <Col md="6">
          <label>marital_status_years</label><br></br>
          <input type="number"  onChange={this.props.handleChange('marital_status_years')}  defaultValue={values.marital_status_years}/>
          <br/>
          <label>menses_frequency</label><br></br>
          <input type="text"  onChange={this.props.handleChange('menses_frequency')} defaultValue={values.menses_frequency}/>
          <br/>

          </Col>
          </Row>


          <Row>
  <Col md="6">   
          <label>menses_loss</label><br></br>
          <input type="text"  onChange={this.props.handleChange('menses_loss')} defaultValue={values.menses_loss}/>
          <br/>
          <label>menarche_years</label><br></br>
          <input type="number"  onChange={this.props.handleChange('menarche_years')} defaultValue={values.menarche_years}/>
          <br/>
          </Col>
          <Col md="6">   

          <label>hystrectomy</label><br></br>
          <input type="text"  onChange={this.props.handleChange('hystrectomy')} defaultValue={values.hystrectomy}/>
          <br/>
          <label>hystrectomy_years</label><br></br>
          <input type="number"  onChange={this.props.handleChange('hystrectomy_years')} defaultValue={values.hystrectomy_years}/>
          <br/><br></br>

          <Button onClick={this.saveAndContinue}>Save And Continue </Button>



       </Col>
       </Row> */}


<Row>
	   <Col md="6">
	   <Button className="btn-fill" onClick={this.back}>Back</Button>
	 

			   </Col>
			   <Col md="6">
			   <Button className="btn-fill" variant="warning" onClick={this.saveAndContinue}>Save And Continue </Button>

		   </Col>
	   </Row>
		</Form>
   
      {/* </Row> */}
      </Container>
	  )
	  }
	}

	export default Step1;