

// import React, { Component } from 'react';
// import { Button, List } from 'semantic-ui-react';

// class Step3 extends Component {
// 	saveAndContinue = (e) => {
// 		e.preventDefault();
// 		this.props.nextStep();
// 	}

// 	back = (e) => {
// 		e.preventDefault();
// 		this.props.prevStep();
// 	}

// 	render() {
// 		const { values: { username,email,phone,password,role,speciality} } = this.props;

// 		return (
// 			<div>
// 				<h1 className="ui centered">Confirm your Details</h1>
// 				<p>Click Confirm if the following details have been correctly entered</p>
// 				<List>
// 					<List.Item>
// 						<List.Icon name='users' />
// 						<List.Content>Name: {username}</List.Content>
// 					</List.Item>
					
// 					<List.Item>
// 						<List.Icon name='mail' />
// 						<List.Content>
// 							<a href='mailto:jack@semantic-ui.com'>{email}</a>
// 						</List.Content>
// 					</List.Item>
// 					<List.Item>
// 						<List.Icon name='phone' />
// 						<List.Content>{phone}phone</List.Content>
// 					</List.Item>
// 					<List.Item>
// 						<List.Icon name='role' />
// 						<List.Content> {role}</List.Content>
// 					</List.Item>
//                     <List.Item>
// 						<List.Icon name='speciality' />
// 						<List.Content> {speciality}</List.Content>
// 					</List.Item>
// 				</List>

// 				<Button onClick={this.back}>Back</Button>
//                 <button onClick={this.props.saveAll}>Submit</button>
// 			</div>
// 		)
// 	}
// }

// export default Step3;


import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { Button } from 'react-bootstrap';


class Summary extends Component {

  saveAndContinue = (e) => {
		e.preventDefault();
		this.props.nextStep();
	}

	back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}

  render(){
const { values: { pid,date,code,place,pname,fhname,age,sex,education,address,phone,district,state,area,referred_by,occupation,ethnic,marital_status,marital_status_years,menses_frequency,menses_loss,menarche_years ,hystrectomy ,
      hystrectomy_years,
      menopause,
      menopause_years,
      children,
      children_male,
      children_female,
      abortions,
      abortions_number,
      abortions_cause,
      breastfed,
      breastfed_years,
      current_lactation,
      contraception_methods,
      contraception_methods_type,
      hormone_treatment,
      addiction,
      tobacco,
      tobacco_years,
      smoking,
      smoking_years,
      alcohol,
      alcohol_years,
      family_history,
      comorbidities,
      breastlump,
      breastlump_location,
      breastlump_size,
      overlying_skin,
      axillarylump,
      axillarylump_side,
      matted,
      axillarylump_size,
      nipple_discharge,
      nipple_discharge_frequency,
      nipple_discharge_colour,
      mastalgia,
      mastitis,
      ulcer,
      nipple_inversion,
      others,
      duration,
      pasthistory,
      surgical_history,
      drug_history,
      drug_allergy,
      drug_allergy_type,
      bowelhabit,
      bladderhabit,
      sleep,
      appetite,
      weight,
      height,
      bmi,
      bp,
      pulse,
      temp,
      respiratory_rate,
      health_condition,
      examination_remarks,
      bra_size,
      usg,
      mmg,
      mri,
      fnac,
      core_biopsy,
      incision_biopsy,
      investigation_remarks,
      blood_investigation,
      diagnosis,
      treatment_plan,
      user_id
    } } = this.props;

    return (

    <>
    <h2>Summary</h2>
    <Row>
      <Row>
      <Col md="6">
      
        
            <li>
              pid: {pid}
            </li>
            <li>
              date: {date}
            </li>
            <li>
              code: {code}
            </li>
            <li>
              place: {place}
            </li>
            <li>
              pname: {pname}
            </li>

            <li>
              fhname: {fhname}
            </li>
</Col>

<Col md="6">
            <li>
              age: {age}
            </li>
            <li>
              sex: {sex}
            </li>
            <li>
              education: {education}
            </li>
            <li>
              address: {address}
            </li>
            <li>
              phone: {phone}
            </li>

            <li>
              district: {district}
            </li>


            </Col>
            <Col md="6">
           

            <li>
              state: {state}
            </li>
            <li>
              area: {area}
            </li>
            <li>
              referred_by: {referred_by}
            </li>
            <li>
              occupation: {occupation}
            </li>
            <li>
              ethinic: {ethnic}
            </li>

            <li>
              marital_status: {marital_status}
            </li>
</Col>
<Col md="6">
            <li>
              marital_status_years: {marital_status_years}
            </li>
            <li>
              menses_frequency: {menses_frequency}
            </li>

            <li>
              menses_loss: {menses_loss}
            </li>
            <li>
              menarche_years: {menarche_years}
            </li>

            <li>
              hystrectomy: {hystrectomy}
            </li>


            <li>
              hystrectomy_years: {hystrectomy_years}
            </li>
            </Col>
            </Row>

            <Row>
              <Col md="6">
         
            <li>
              menopause: {menopause}
            </li>
            <li>
              menopause_years: {menopause_years}
            </li>
            <li>
              children: {children}
            </li>
            <li>
              children_male: {children_male}
            </li>
            <li>
              children_female: {children_female}
            </li>
            <li>
            abortions: {abortions}
            </li>
            </Col>
            <Col md="6">

            <li>
              abortions_number: {abortions_number}
            </li>
            <li>
              abortions_cause: {abortions_cause}
            </li>
            <li>
              breastfed: {breastfed}
            </li>
            <li>
              breastfed_years: {breastfed_years}
            </li>
            <li>
              current_lactation: {current_lactation}
            </li>
            <li>
            contraception_methods: {contraception_methods}
            </li>

</Col>


<Col md="6">
            <li>
              contraception_methods_type: {contraception_methods_type}
            </li>
            <li>
              hormone_treatment: {hormone_treatment}
            </li>
            <li>
              addiction: {addiction}
            </li>
            <li>
              tobacco: {tobacco}
            </li>
            <li>
              tobacco_years: {tobacco_years}
            </li>
            <li>
            smoking: {smoking}
            </li>

            </Col>
            

           
              <Col md="6">
            
          
        <li>
              smoking_years: {smoking_years}
            </li>
            <li>
              alcohol: {alcohol}
            </li>
            <li>
              alcohol_years: {alcohol_years}
            </li>
            <li>
              family_history: {family_history}
            </li>
            <li>
              comorbidities: {comorbidities}
            </li>
            <li>
            breastlump: {breastlump}
            </li>

</Col>
</Row>

<Row>
<Col md="6">

        <li>
              breastlump_location: {breastlump_location}
            </li>
            <li>
              breastlump_size: {breastlump_size}
            </li>

            <li>
              overlying_skin: {overlying_skin}
            </li>
            <li>
              axillarylump: {axillarylump}
            </li>
            <li>
              axillarylump_side: {axillarylump_side}
            </li>
            <li>
            matted: {matted}
            </li>
            </Col>


            <Col md="6">
           

        <li>
              axillarylump_size: {axillarylump_size}
            </li>
            <li>
              nipple_discharge: {nipple_discharge}
            </li>
            <li>
              nipple_discharge_frequency: {nipple_discharge_frequency}
            </li>
            <li>
              nipple_discharge_colour: {nipple_discharge_colour}
            </li>
            <li>
              mastalgia: {mastalgia}
            </li>
            <li>
            mastitis: {mastitis}
            </li>
        </Col>
      



  <Col md="6">

        


            <li>
            ulcer: {ulcer}
            </li>
            <li>
              nipple_inversion: {nipple_inversion}
            </li>
            <li>
              others: {others}
            </li>
            <li>
              duration: {duration}
            </li>
            <li>
              pasthistory: {pasthistory}
            </li>
            <li>
              surgical_history: {surgical_history}
            </li>


</Col>
            

            <Col md="6">


        <li>
        drug_history: {drug_history}
            </li>
            <li>
              drug_allergy: {drug_allergy}
            </li>
            <li>
              drug_allergy_type: {drug_allergy_type}
            </li>
            <li>
              bowelhabit: {bowelhabit}
            </li>

            <li>
              bladderhabit: {bladderhabit}
            </li>
            <li>
              sleep: {sleep}
            </li>
            </Col>
            </Row>



            <Row>

<Col md="6">
        <li>
        appetite: {appetite}
            </li>
            <li>
              weight: {weight}
            </li>
            <li>
              height: {height}
            </li>
            <li>
              bmi: {bmi}
            </li>
            <li>
              bp: {bp}
            </li>
            <li>
              pulse: {pulse}
            </li>
</Col>

<Col md="6">
        <li>
        temp: {temp}
            </li>
            <li>
              respiratory_rate: {respiratory_rate}
            </li>
            <li>
              health_condition: {health_condition}
            </li>
            <li>
              examination_remarks: {examination_remarks}
            </li>
            <li>
              bra_size: {bra_size}
            </li>
            <li>
              usg: {usg}
            </li>

            </Col>
          
           
            
          
              <Col md="6">

           

        <li>
            mmg: {mmg}
            </li>
            <li>
              mri: {mri}
            </li>
            <li>
              fnac: {fnac}
            </li>
            <li>
              core_biopsy: {core_biopsy}
            </li>
            <li>
              incision_biopsy: {incision_biopsy}
            </li>
            <li>
              investigation_remarks: {investigation_remarks}
            </li>
            </Col>
            <Col md="6">
            <li>
              blood_investigation: {blood_investigation}
            </li>
            <li>
              diagnosis: {diagnosis}
            </li>
            <li>
              treatment_plan: {treatment_plan}
            </li>


            <li>
            User_id: {user_id}
            </li><br></br>
            {/* onClick={(e) => {this.submit(e,0);this.handleModalShowHide()}} */}

            <Button className="btn-fill" variant="success" onClick={this.props.saveAll}>Submit</Button>


          </Col>
          <Col md="6">
            
              
         
         <Button className="btn-fill"  onClick={this.back}>Back</Button>

            </Col>
          
        

</Row>
      
        </Row>
      </>
 
    )
  }
}
export default Summary;
//   nextStep(e) {
//     e.preventDefault()
//     var data = {
//       name: this.refs.name.value,
//       sport: this.refs.sport.value,
//       nationality: this.refs.nationality.value,
//       gender: this.refs.gender.value,
//       dob: this.refs.dob.value,
//     }
//     this.props.saveValues(data);
//     this.props.nextStep();
//   }


