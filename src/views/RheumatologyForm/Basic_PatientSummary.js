

// import React, { Component } from 'react';
// import { Button, List } from 'semantic-ui-react';

// class Step3 extends Component {
// 	saveAndContinue = (e) => {
// 		e.preventDefault();
// 		this.props.nextStep();
// 	}

// 	back = (e) => {
// 		e.preventDefault();
// 		this.props.prevStep();
// 	}

// 	render() {
// 		const { values: { username,email,phone,password,role,speciality} } = this.props;

// 		return (
// 			<div>
// 				<h1 className="ui centered">Confirm your Details</h1>
// 				<p>Click Confirm if the following details have been correctly entered</p>
// 				<List>
// 					<List.Item>
// 						<List.Icon name='users' />
// 						<List.Content>Name: {username}</List.Content>
// 					</List.Item>
					
// 					<List.Item>
// 						<List.Icon name='mail' />
// 						<List.Content>
// 							<a href='mailto:jack@semantic-ui.com'>{email}</a>
// 						</List.Content>
// 					</List.Item>
// 					<List.Item>
// 						<List.Icon name='phone' />
// 						<List.Content>{phone}phone</List.Content>
// 					</List.Item>
// 					<List.Item>
// 						<List.Icon name='role' />
// 						<List.Content> {role}</List.Content>
// 					</List.Item>
//                     <List.Item>
// 						<List.Icon name='speciality' />
// 						<List.Content> {speciality}</List.Content>
// 					</List.Item>
// 				</List>

// 				<Button onClick={this.back}>Back</Button>
//                 <button onClick={this.props.saveAll}>Submit</button>
// 			</div>
// 		)
// 	}
// }

// export default Step3;


import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import { Button } from 'react-bootstrap';


class Summary extends Component {

  saveAndContinue = (e) => {
		e.preventDefault();
		this.props.nextStep();
	}

	back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}

  render(){
const { values: {
    pid,date,code,place,pname,fhname,age,sex,education,address,phone,district,state,area,referred_by,occupation,ethnic,marital_status,marital_status_years,menses_frequency,menses_loss,menarche_years,hysterectomy,hysterectomy_years,menopause,menopause_years,children,children_male,children_female,abortions,abortions_number,abortions_cause,current_lactation,contraception_methods,contraception_methods_type,hormone_treatment,addiction,tobacco,tobacco_years,smoking,smoking_years,alcohol,alcohol_years,family_history,health_condition,remarks,comorbidities,onset,onset_duration,onset_duration_type,first_symptom,initial_site_joint,soft_tissue,others,course1,course2,pattern1,pattern2,current_relapse,current_relapse_type,detailed_history,pasthistory,surgical_history,drug_history,drug_allergy,drug_allergy_type,bowelhabit,sleep,user_id

    
    
    } } = this.props;

    return (

    <>
    <h2>Summary</h2>
   
     
     
      
        
            <li>
              pid: {pid}
            </li>
            <li>
              {/* date: {date} */}
            </li>
            <li>
              code: {code}
            </li>
            <li>
              place: {place}
            </li>
            <li>
              pname: {pname}
            </li>

            <li>
              fhname: {fhname}
            </li>



            <li>
              age: {age}
            </li>
            <li>
              sex: {sex}
            </li>
            <li>
              education: {education}
            </li>
            <li>
              address: {address}
            </li>
            <li>
              phone: {phone}
            </li>

            <li>
              district: {district}
            </li>


            
           
           

            <li>
              state: {state}
            </li>
            <li>
              area: {area}
            </li>
            <li>
              referred_by: {referred_by}
            </li>
            <li>
              occupation: {occupation}
            </li>
            <li>
              ethinic: {ethnic}
            </li>

            <li>
              marital_status: {marital_status}
            </li>


            <li>
              marital_status_years: {marital_status_years}
            </li>
            <li>
              menses_frequency: {menses_frequency}
            </li>

            <li>
              menses_loss: {menses_loss}
            </li>
            <li>
              menarche_years: {menarche_years}
            </li>
        
            <li>
              hystrectomy: {hysterectomy}
            </li>


            <li>
              hystrectomy_years: {hysterectomy_years}
            </li>
            
          
           
             
         
            <li>
              menopause: {menopause}
            </li>
            <li>
              menopause_years: {menopause_years}
            </li>
            <li>
              children: {children}
            </li>
            <li>
              children_male: {children_male}
            </li>
            <li>
              children_female: {children_female}
            </li>
            <li>
            abortions: {abortions}
            </li>
            
           

            <li>
              abortions_number: {abortions_number}
            </li>
            <li>
              abortions_cause: {abortions_cause}
            </li>


          
            <li>
              current_lactation: {current_lactation}
            </li>
            <li>
            contraception_methods: {contraception_methods}
            </li>





            <li>
              contraception_methods_type: {contraception_methods_type}
            </li>
            <li>
              hormone_treatment: {hormone_treatment}
            </li>
            <li>
              addiction: {addiction}
            </li>
            <li>
              tobacco: {tobacco}
            </li>
            <li>
              tobacco_years: {tobacco_years}
            </li>
            <li>
            smoking: {smoking}
            </li>

            
            

           
             
            
          
        <li>
              smoking_years: {smoking_years}
            </li>
            <li>
              alcohol: {alcohol}
            </li>
            <li>
              alcohol_years: {alcohol_years}
            </li>
            <li>
              family_history: {family_history}
            </li>
          

         
            <li>
            health_condition: {health_condition}
            </li>
            <li>
            remarks: {remarks}
            </li>
            <li>
              comorbidities: {comorbidities}
            </li>
            



        <li>
              onset: {onset}
            </li>
            <li>
              onset_duration: {onset_duration}
            </li>

            <li>
              onset_duration_type: {onset_duration_type}
            </li>
            <li>
              first_symptom: {first_symptom}
            </li>
            <li>
              initial_site_joint: {initial_site_joint}
            </li>
            <li>
            soft_tissue: {soft_tissue}
            </li>
            


           
           

        <li>
              others: {others}
            </li>

            <li>
              course1: {course1}
            </li>
            <li>
              course2: {course2}
            </li>
            <li>
              pattern1: {pattern1}
            </li>
            <li>
              pattern2: {pattern2}
            </li>
            <li>
            current_relapse: {current_relapse}
            </li>



            <li>
              current_relapse_type: {current_relapse_type}
            </li>
            <li>
            detailed_history: {detailed_history}
            </li>
        
      



 

        

            <li>
            pasthistory: {pasthistory}
            </li>
            <li>
              surgical_history: {surgical_history}
            </li>
            <li>
              drug_history: {drug_history}
            </li>
            <li>
              drug_allergy: {drug_allergy}
            </li>
            <li>
              drug_allergy_type: {drug_allergy_type}
            </li>
            <li>
              bowelhabit: {bowelhabit}
            </li>



            

           


        <li>
        sleep: {sleep}
            </li>
            <li>
              user_id: {user_id}
            </li>
            
         


           


<Button className="btn-fill"  onClick={this.back}>Back</Button>


          
         
            
              
         
         <Button className="btn-fill" variant="success" onClick={this.props.saveAll}>Submit</Button>

            
          
        

      </>
 
    )
  }
}
export default Summary;
//   nextStep(e) {
//     e.preventDefault()
//     var data = {
//       name: this.refs.name.value,
//       sport: this.refs.sport.value,
//       nationality: this.refs.nationality.value,
//       gender: this.refs.gender.value,
//       dob: this.refs.dob.value,
//     }
//     this.props.saveValues(data);
//     this.props.nextStep();
//   }


