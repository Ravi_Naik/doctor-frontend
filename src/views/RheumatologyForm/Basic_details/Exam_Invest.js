import React from "react";

import axios from "axios";
import { Link } from "react-router-dom";
import Modal from "react-bootstrap/Modal";
import Main from '../views/Patientform/Main'
import Main1 from '../views/RheumatologyForm/Main1'
import Main23 from '../views/RheumatologyForm/Rheuma_model'

import Examination_Main from '../views/RheumatologyForm/Examination_Main'
import datasend from '../views/RheumatologyForm/Examination_Main'
import Investigation_Main from '../views/RheumatologyForm/Investigation_Main'
import Follow from '../views/RheumatologyForm/Followup/Followup'
import step6 from '../views/Patientform/step6'
import UserForm  from './UserForm'


// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
  Form,
  Dropdown,
} from "react-bootstrap";


class PatientManagement extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      users:[],
      rusers:[],
      
      id:0,
      username: "",
      email: "",  
      phone: "",
      password: "",
      role:"",
      speciality:"",
      pid:"",
      showHide : false,
      open: false,
      modal1:false,
      modal2:false
    


      

    }


  }






  closeModal (){
    this.setState({open: false}) }



  componentDidMount(){
    axios.get("https://abcapi.vidaria.in/allbpatientdetails")
    .then((res)=>{
      console.log(res)
      this.setState({
        users:res.data.breast_patient,
        
      
        
      })
    })


    axios.get("https://abcapi.vidaria.in/allrpatientdetails")
    .then((pes)=>{
      console.log(pes)
      this.setState({
        rusers:pes.data.rheumatology_patient,
        
      
        
      })
    })



  

  }

  handleModalShowHide() {
    this.setState({ showHide: !this.state.showHide })
}


secondhandleModalShowHide() {
  this.setState({ open: !this.state.open })
}


thirdhandleModalShowHide() {
  this.setState({ modal1: !this.state.modal1 })
}



fourthhandleModalShowHide(pid) {
  // console.log(pid)
  // var client = new FBClient();
  // client.loadFromConfig(config);
  var apple = new datasend(pid)
  apple.datasend(pid)
  this.setState({ modal2: !this.state.modal2 })
}


// Updatebpatient = () => {
//   axios.get(`https://abcapi.vidaria.in/bpatientdetails/1`)
//   .then((res)=>{
//     console.log(res)
//     this.setState({
//       // users:res.data.bpatient,
   
//       // is this correct way?
    
      
//     })
//   })
// }


// submit(event,id){
//   event.preventDefault();
//   if(id === 0){
//     axios.post("https://abcapi.vidaria.in/adduser",{
//       username:this.state.username,
//       email:this.state.email,
//       phone:this.state.phone,
//       password:this.state.password,
//       role:this.state.role,
//       speciality:this.state.speciality,
//     })
//     .then((res)=>{
//       console.log(res)
//     })
//   }else{
//     axios.put(`https://abcapi.vidaria.in/userupdate/${this.props.match.params.id}`,{
//       id:this.state.id,
//       username:this.state.username,
//       email:this.state.email,
//       phone:this.state.phone,
//       password:this.state.password,
//       role:this.state.role,
//       speciality:this.state.speciality,
//     }).then(()=>{
//       this.componentDidMount();
//     })

//   }
  
// }
deleter(id){
  axios.delete(`https://abcapi.vidaria.in/rheumapatientdelete?pid=${id}`)
  .then(()=>{
    this.componentDidMount();
  })
}

deleteb(id){
  axios.delete(`https://abcapi.vidaria.in/bpatientdelete?pid=${id}`)
  .then(()=>{
    this.componentDidMount();
  })
}


  render()

 {
  return (
    <>

<div>


<Modal 
                
                size="lg"
                
                show={this.state.modal1}>
                    <Modal.Header closeButton onClick={() => this.thirdhandleModalShowHide()}>
                 </Modal.Header>
                    <Modal.Body>
<Investigation_Main />



                    </Modal.Body>
                    <Modal.Footer>
                   <p>Please Fill the Details</p>
                    </Modal.Footer>
                </Modal>












<Modal 
                
                size="lg"
                
                show={this.state.showHide}>
                    <Modal.Header closeButton onClick={() => this.handleModalShowHide()}>
                 </Modal.Header>
                    <Modal.Body>


<Main />



                    </Modal.Body>
                    <Modal.Footer>
                   <p>Please Fill the Details</p>
                    </Modal.Footer>
                </Modal>


       


{/* modal 2 */}
                <Modal 
                
                size="lg"
                
                show={this.state.open}>
                    <Modal.Header closeButton onClick={() => this.secondhandleModalShowHide()}> 
                 </Modal.Header>
                    <Modal.Body>


<Main1 />

                    </Modal.Body>
                 
                </Modal>


                         
           <Modal 
                
                size="lg"
                
                show={this.state.modal2}>
                    <Modal.Header closeButton onClick={() => this.fourthhandleModalShowHide()}>
                 </Modal.Header>
                    <Modal.Body>


          <Examination_Main />



                    </Modal.Body>
                    <Modal.Footer>
                   <p>Please Fill the Details</p>
                    </Modal.Footer>
                </Modal>

            </div>
      <Container fluid>
        <div className="container">
          <div className="row" >
            
            <Col md="6" style={{ float: "left" }}>


            <Button variant="info" className="btn-fill" onClick={() => this.secondhandleModalShowHide()} type="submit" name="action" style={{ float: "right" }}>
                 
            Add Rheumatology Patient
                 </Button> 
          {/* <Dropdown>
  <Dropdown.Toggle variant="info"  className="btn-fill" id="dropdown-basic">
  Add Rheumatology Patient
  </Dropdown.Toggle>

  <Dropdown.Menu>
    <Dropdown.Item  onClick={() => this.secondhandleModalShowHide()}>Basic patient details</Dropdown.Item>
    <Dropdown.Item  onClick={() => this.fourthhandleModalShowHide()}>Add examination </Dropdown.Item>
    <Dropdown.Item  onClick={() => this.thirdhandleModalShowHide()}>Add investigation </Dropdown.Item>

  </Dropdown.Menu>
</Dropdown> */}

</Col>
<Col md="6">
           
      <Button variant="info" className="btn-fill" onClick={() => this.handleModalShowHide()} type="submit" name="action" style={{ float: "right" }}>
                 
                Add Breast Patient
                </Button> 
                </Col>
                </div>
                </div>
                <br></br>
        <Row>
          <Col md="12">
            <Card className="strpied-tabled-with-hover">
              <Card.Header>
                <Card.Title as="h4">Breast Cancer PatientManagement</Card.Title>
                <p className="card-category">
                  Here is a Patient data
                </p>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover table-striped">
                  <thead>
                    <tr>
                    <th className="border-0">User ID</th>
                    <th className="border-0">Code</th>

                     <th className="border-0">Patient Name</th>
                     <th className="border-0">Patient ID</th>
                      <th className="border-0">Phone</th>
                      <th className="border-0">Address</th>
                      <th className="border-0">Date of Subscribe</th>
                      <th className="border-0">View</th>
                      <th className="border-0">Delete</th>
                      <th className="border-0">Add Details</th>
                      <th className="border-0">Add Attchments</th>





            
                    </tr>
                  </thead>
                  <tbody>
                  {
            this.state.users.map(user=>
              <tr key={user.id}>
               <td>{user.user.id}</td>

                <td>{user.code}</td>
                <td>{user.pname}</td>
                <td>{user.pid}</td>

                <td>{user.phone}</td>
                <td>{user.address}</td>
                <td>{user.date}</td>
                

            
                <td>
                <Link to={`bpatient/edit/${user.pid}`}>
             <Button  variant="primary" className="btn-fill" type="submit" name="action">Edit</Button>
             </Link>
                </td>
                <td>
                <Button onClick={(e)=>this.deleteb(user.pid)} variant="danger" className="btn-fill" type="submit" name="action">
                  {/* <i class="material-icons">delete</i> */}
                  Delete
                </Button>
                </td>
                <td>
                     <Dropdown>
  <Dropdown.Toggle variant="secondary" className="btn-fill" style={{ width: "160px" }} id="dropdown-basic">
  Add Details
  </Dropdown.Toggle>

  <Dropdown.Menu>
  
    <Dropdown.Item href={`addrexamination/edit/${user.pid}`}  >Add examination </Dropdown.Item>
    <Dropdown.Item  href={`addrinvestigation/edit/${user.pid}`} >Add investigation </Dropdown.Item>
    <Dropdown.Item  href={`addrfollowup/edit/${user.pid}`}>Follow-up</Dropdown.Item>

  </Dropdown.Menu>
</Dropdown>
                </td>
                <td>
                <Button  variant="warning" className="btn-fill" type="submit" name="action">
                  {/* <i class="material-icons">delete</i> */}
                  Add Attchments
                </Button>
                </td>
              </tr>
              )
          }

                  </tbody>
                </Table>
              </Card.Body>
            </Card>
          </Col>
  
        </Row>



        <Row>
          <Col md="12">
            <Card className="strpied-tabled-with-hover">
              <Card.Header>
                <Card.Title as="h4">Rheumatology PatientManagement</Card.Title>
                <p className="card-category">
                  Here is a Patient data
                </p>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover table-striped">
                  <thead>
                    <tr>
                    <th className="border-0">User ID</th>
                    <th className="border-0">Code</th>

                     <th className="border-0">Patient Name</th>
                     <th className="border-0">Patient ID</th>
                      <th className="border-0">Phone</th>
                      <th className="border-0">Address</th>
                      <th className="border-0">Date of Subscribe</th>
                      <th className="border-0">View</th>
                      <th className="border-0">Delete</th>
                      <th className="border-0">Add Details</th>
                      <th className="border-0">Add Attchments</th>





            
                    </tr>
                  </thead>
                  <tbody>
                  {
            this.state.rusers.map(user=>
              <tr key={user.id}>
               <td>{user.user.id}</td>

                <td>{user.code}</td>
                <td>{user.pname}</td>
                <td>{user.pid}</td>

                <td>{user.phone}</td>
                <td>{user.address}</td>
                <td>{user.date}</td>
                

            
                <td>
                <Link to={`rpatient/edit/${user.pid}`}>
             <Button  variant="primary" className="btn-fill" type="submit" name="action">View</Button>
             </Link>
                </td>
                <td>
                <Button onClick={(e)=>this.deleter(user.id)} variant="danger" className="btn-fill" type="submit" name="action">
                  {/* <i class="material-icons">delete</i> */}
                  Delete
                </Button>
                </td>
                <td>
                     <Dropdown>
  <Dropdown.Toggle variant="secondary" className="btn-fill" style={{ width: "160px" }} id="dropdown-basic">
  Add Details
  </Dropdown.Toggle>

  <Dropdown.Menu>
 
    <Dropdown.Item href={`addrexamination/edit/${user.pid}`}  >Add examination </Dropdown.Item>
    <Dropdown.Item  href={`addrinvestigation/edit/${user.pid}`} >Add investigation </Dropdown.Item>
    <Dropdown.Item   href={`addrfollowup/edit/${user.pid}`}>Follow-up</Dropdown.Item>

  </Dropdown.Menu>
</Dropdown>
                </td>
                <td>
                <Button onClick={(e)=>this.delete(user.user.id)} variant="warning" className="btn-fill" type="submit" name="action">
                  {/* <i class="material-icons">delete</i> */}
                  Add Attchments
                </Button>
                </td>
              </tr>
              )
          }

                  </tbody>
                </Table>
              </Card.Body>
            </Card>
          </Col>
       
        </Row>
      </Container>
    </>
  );
}
}

export default PatientManagement;


