import React from "react";
import axios from 'axios'
// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Form,
  Navbar,
  Nav,
  Container,
  Row,
  Col,
  Image
} from "react-bootstrap";
import Swal from 'sweetalert2'


class User extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      products: [],
      users:[],
      id:0,
      pid:''
      ,date:''
      ,code:''
      ,place:''
      ,pname:''
      ,fhname:''
      ,age:''
      ,sex:''
      ,education:''
      ,address:''
      ,phone:''
      ,district:''
      ,state:''
      ,area:''
      ,referred_by:''
      ,occupation:''
      ,ethnic:''
      ,marital_status:''
      ,marital_status_years:''
      ,menses_frequency:''
      ,menses_loss:''
      ,menarche_years:''
      ,hysterectomy:''
      ,hysterectomy_years:''
      ,menopause:''
      ,menopause_years:''
      ,children:''
      ,children_male:''
      ,children_female:''
      ,abortions:''
      ,abortions_number:''
      ,abortions_cause:''
      ,current_lactation:''
      ,contraception_methods:''
      ,contraception_methods_type:''
      ,hormone_treatment:''
      ,addiction:''
      ,tobacco:''
      ,tobacco_years:''
      ,smoking:''
      ,smoking_years:''
      ,alcohol:''
      ,alcohol_years:''
      ,family_history:''
      ,health_condition:''
      ,remarks:''
      ,comorbidities:''
      ,onset:''
      ,onset_duration:''
      ,onset_duration_type:''
      ,first_symptom:''
      ,initial_site_joint:''
      ,soft_tissue:''
      ,others:''
      ,course1:''
      ,course2:''
      ,pattern1:''
      ,pattern2:''
      ,current_relapse:''
      ,current_relapse_type:''
      ,detailed_history:''
      ,pasthistory:''
      ,surgical_history:''
      ,drug_history:''
      ,drug_allergy:''
      ,drug_allergy_type:''
      ,bowelhabit:''
      ,sleep:''
      ,user_id:''

    }

  }


  opensweetalert()
  {
    Swal.fire({
      title: 'Patient Updated',
      text: "success",
      type: 'success',
      
    }).then(function() {
      window.location = "/admin/dashboard";
  });
  }


  componentDidMount(){
    axios.get(`https://abcapi.vidaria.in/rheumapatientdetails?X-AUTH=abc123&pid=${this.props.match.params.id}`)
    .then((res)=>{
      console.log(res)
      this.setState({
        id:res.data.RheumatologyPatientHistory.id,

        pid:res.data.RheumatologyPatientHistory.pid,
        date:res.data.RheumatologyPatientHistory.date,
        code:res.data.RheumatologyPatientHistory.code,
        place:res.data.RheumatologyPatientHistory.place,
        pname:res.data.RheumatologyPatientHistory.pname,
        fhname:res.data.RheumatologyPatientHistory.fhname,
        age:res.data.RheumatologyPatientHistory.age,
        sex:res.data.RheumatologyPatientHistory.sex,
        education:res.data.RheumatologyPatientHistory.education,
        address:res.data.RheumatologyPatientHistory.address,
        phone:res.data.RheumatologyPatientHistory.phone,
        district:res.data.RheumatologyPatientHistory.district,
        state:res.data.RheumatologyPatientHistory.state,
        area:res.data.RheumatologyPatientHistory.area,
        referred_by:res.data.RheumatologyPatientHistory.referred_by,
        occupation:res.data.RheumatologyPatientHistory.occupation,
        ethnic:res.data.RheumatologyPatientHistory.ethnic,
        marital_status:res.data.RheumatologyPatientHistory.marital_status,
        marital_status_years :res.data.RheumatologyPatientHistory.marital_status_years,
        menses_frequency:res.data.RheumatologyPatientHistory.menses_frequency,
        menses_loss:res.data.RheumatologyPatientHistory.menses_loss,
        menarche_years :res.data.RheumatologyPatientHistory.menarche_years,
        hysterectomy :res.data.RheumatologyPatientHistory.hysterectomy,
        hysterectomy_years:res.data.RheumatologyPatientHistory.hysterectomy_years,
        menopause:res.data.RheumatologyPatientHistory.menopause,
        menopause_years:res.data.RheumatologyPatientHistory.menarche_years,
        children:res.data.RheumatologyPatientHistory.children,
        children_male:res.data.RheumatologyPatientHistory.children_male,
        children_female:res.data.RheumatologyPatientHistory.children_female,
        abortions:res.data.RheumatologyPatientHistory.abortions,
        abortions_number:res.data.RheumatologyPatientHistory.abortions_number,
        abortions_cause:res.data.RheumatologyPatientHistory.abortions_cause,

        current_lactation:res.data.RheumatologyPatientHistory.current_lactation,
        contraception_methods:res.data.RheumatologyPatientHistory.contraception_methods,
        contraception_methods_type:res.data.RheumatologyPatientHistory.contraception_methods_type,
        hormone_treatment:res.data.RheumatologyPatientHistory.hormone_treatment,
        addiction:res.data.RheumatologyPatientHistory.addiction,
        tobacco:res.data.RheumatologyPatientHistory.tobacco,
        tobacco_years:res.data.RheumatologyPatientHistory.tobacco_years,
        smoking:res.data.RheumatologyPatientHistory.smoking,
        smoking_years:res.data.RheumatologyPatientHistory.smoking_years,
        alcohol:res.data.RheumatologyPatientHistory.alcohol,
        alcohol_years:res.data.RheumatologyPatientHistory.alcohol_years,
        family_history:res.data.RheumatologyPatientHistory.family_history,


        health_condition:res.data.RheumatologyPatientHistory.health_condition,
        remarks:res.data.RheumatologyPatientHistory.remarks,

        


        comorbidities:res.data.RheumatologyPatientHistory.comorbidities,


        onset:res.data.RheumatologyPatientHistory.onset,
        onset_duration:res.data.RheumatologyPatientHistory.onset_duration,
        onset_duration_type:res.data.RheumatologyPatientHistory.onset_duration_type,
        first_symptom:res.data.RheumatologyPatientHistory.first_symptom,
        initial_site_joint:res.data.RheumatologyPatientHistory.initial_site_joint,

        soft_tissue:res.data.RheumatologyPatientHistory.soft_tissue,
        others:res.data.RheumatologyPatientHistory.others,
        course1:res.data.RheumatologyPatientHistory.course1,
        course2:res.data.RheumatologyPatientHistory.course2,
        pattern1:res.data.RheumatologyPatientHistory.pattern1,
        pattern2:res.data.RheumatologyPatientHistory.pattern2,
        current_relapse:res.data.RheumatologyPatientHistory.current_relapse,
        current_relapse_type:res.data.RheumatologyPatientHistory.current_relapse_type,
        detailed_history:res.data.RheumatologyPatientHistory.detailed_history,




        pasthistory:res.data.RheumatologyPatientHistory.pasthistory,
        surgical_history:res.data.RheumatologyPatientHistory.surgical_history,
        drug_history:res.data.RheumatologyPatientHistory.drug_history,
        drug_allergy:res.data.RheumatologyPatientHistory.drug_allergy,
        drug_allergy_type:res.data.RheumatologyPatientHistory.drug_allergy_type,
        bowelhabit:res.data.RheumatologyPatientHistory.bowelhabit,
        sleep:res.data.RheumatologyPatientHistory.sleep,
        user_id:res.data.RheumatologyPatientHistory.user.id
      
        
      })
    })  
  

      
    axios.get(`https://abcapi.vidaria.in/rattachmentslist?X-AUTH=abc123&pid=${this.props.match.params.id}`)
    .then(res => {
      const products = res.data.Rattachments;
      console.log(products)
      for (let i = 0; i < products.length; i++) {
        this.setState({ products });
     
      }
    });
  }



// componentDidMount(){
//     axios.get("https://abcapi.vidaria.in/allbpatientdetails")
//     .then((res)=>{
//       console.log(res)
//       this.setState({
//         users:res.data.breast_patient,
        
      
        
//       })
//     })
//   }

  submit = e => {
   
    e.preventDefault()
    axios.post(`https://abcapi.vidaria.in/addactivity?X-AUTH=abc123`,{

      activity :" Rheumatology  patient updated",
      user_id:this.state.user_id,

    })

      axios.put(`https://abcapi.vidaria.in/updaterheumapatient?X-AUTH=abc123`,{

    
      pid:this.state.pid
      ,date:this.state.date
      ,code:this.state.code
      ,place:this.state.place
      ,pname:this.state.pname
      ,fhname:this.state.fhname
      ,age:this.state.age
      ,sex:this.state.sex
      ,education:this.state.education
      ,address:this.state.address
      ,phone:this.state.phone
      ,district:this.state.district
      ,state:this.state.state
      ,area:this.state.area
      ,referred_by:this.state.referred_by
      ,occupation:this.state.occupation
      ,ethnic:this.state.ethnic
      ,marital_status:this.state.marital_status
      ,marital_status_years:this.state.marital_status_years
      ,menses_frequency:this.state.menses_frequency
      ,menses_loss:this.state.menses_loss
      ,menarche_years:this.state.menarche_years
      ,hysterectomy:this.state.hysterectomy
      ,hysterectomy_years:this.state.hysterectomy_years
      ,menopause:this.state.menopause
      ,menopause_years:this.state.menopause_years
      ,children:this.state.children
      ,children_male:this.state.children_male
      ,children_female:this.state.children_female
      ,abortions:this.state.abortions
      ,abortions_number:this.state.abortions_number
      ,abortions_cause:this.state.abortions_cause
      ,current_lactation:this.state.current_lactation
      ,contraception_methods:this.state.contraception_methods
      ,contraception_methods_type:this.state.contraception_methods_type
      ,hormone_treatment:this.state.hormone_treatment
      ,addiction:this.state.addiction
      ,tobacco:this.state.tobacco
      ,tobacco_years:this.state.tobacco_years
      ,smoking:this.state.smoking
      ,smoking_years:this.state.smoking_years
      ,alcohol:this.state.alcohol
      ,alcohol_years:this.state.alcohol_years
      ,family_history:this.state.family_history
      ,health_condition:this.state.health_condition
      ,remarks:this.state.remarks
      ,comorbidities:this.state.comorbidities
      ,onset:this.state.onset
      ,onset_duration:this.state.onset_duration
      ,onset_duration_type:this.state.onset_duration_type
      ,first_symptom:this.state.first_symptom
      ,initial_site_joint:this.state.initial_site_joint
      ,soft_tissue:this.state.soft_tissue
      ,others:this.state.others
      ,course1:this.state.course1
      ,course2:this.state.course2
      ,pattern1:this.state.pattern1
      ,pattern2:this.state.pattern2
      ,current_relapse:this.state.current_relapse
      ,current_relapse_type:this.state.current_relapse_type
      ,detailed_history:this.state.detailed_history
      ,pasthistory:this.state.pasthistory
      ,surgical_history:this.state.surgical_history
      ,drug_history:this.state.drug_history
      ,drug_allergy:this.state.drug_allergy
      ,drug_allergy_type:this.state.drug_allergy_type
      ,bowelhabit:this.state.bowelhabit
      ,sleep:this.state.sleep
      ,user_id:this.state.user_id
    })
   .then((res)=>{
    console.log(res.data.success)
    if(res.data.success == "false"){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: res.data.message,
        
      })
    }else{
   this.opensweetalert()
    }
      // this.opensweetalert()
      // this.componentDidMount();

    })

  }


  render()


{
  return (
    <>
      <Container fluid>
        <Row>
          <Col md="8">
            <Card>
              <Card.Header>
                <Card.Title as="h4">Rheumatology Updation Form</Card.Title>
              </Card.Header>
              <Card.Body>
                <Form onSubmit={this.submit}>


                <Row>
          {/* <Col md="12"> */}
         
{/* 
          <Form.Group>
        <Form.Label>Patient Id</Form.Label><br></br>
          <Form.Control
           type="number"
           readOnly
           name="pid"                          
           placeholder="patient id"
           value={this.state.pid}
           onChange={(e)=>this.setState({pid:e.target.value})}></Form.Control>
          </Form.Group> */}

{/* </Col> */}

<Col md="6">
          <Form.Group>
          <Form.Label>Date</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="date"
          placeholder="Date"
          value={this.state.date}
          onChange={(e)=>this.setState({date:e.target.value})}  />
          </Form.Group>
        </Col>

        <Col md="6">
        
        <Form.Group>
          <Form.Label>code</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="code"
          placeholder="Code"
          value={this.state.code}
          onChange={(e)=>this.setState({code:e.target.value})}/>
          </Form.Group>
          
</Col>
</Row>
<Col md="12">



          <Form.Group>
          <Form.Label>place</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="place"
          placeholder="place"
          value={this.state.place}
          onChange={(e)=>this.setState({place:e.target.value})}/>
          </Form.Group>
          </Col>
          


          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>Patient Name</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="pname"
          placeholder="pname"
          value={this.state.pname}
          onChange={(e)=>this.setState({pname:e.target.value})}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>Father or Husband name</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="fhname"
          placeholder="fhname"
          value={this.state.fhname}
          onChange={(e)=>this.setState({fhname:e.target.value})}/>
          </Form.Group>
</Col>
<Col md="6">

<Form.Group>
          <Form.Label>Age</Form.Label><br></br>
          <Form.Control 
          type="number"
          name="age"  
          placeholder="Age"
          value={this.state.age}
          onChange={(e)=>this.setState({age:e.target.value})}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>Gender</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="sex"
          placeholder="Gender"
          value={this.state.sex}
          onChange={(e)=>this.setState({sex:e.target.value})}/>
          </Form.Group>

</Col>
</Row>



<Row>
  <Col md="6">
  <Form.Group>
            <Form.Label>Education</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="education"
          placeholder="Education"
          value={this.state.education}
          onChange={(e)=>this.setState({education:e.target.value})}
          />
          </Form.Group>

          <Form.Group>
          <Form.Label>Address</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="address"
          placeholder="Address"
          value={this.state.address}
          onChange={(e)=>this.setState({address:e.target.value})}
          />
          </Form.Group>

</Col>
<Col md="6">
<Form.Group>
          <Form.Label>Phone</Form.Label><br></br>
          <Form.Control  
          type="number"
          name="phone"  
          placeholder="Phone"
          value={this.state.phone}
          onChange={(e)=>this.setState({phone:e.target.value})}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>District</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="district"
          placeholder="District"
          value={this.state.district}
          onChange={(e)=>this.setState({district:e.target.value})}
          />
          </Form.Group>

</Col>
</Row> 

<Row>
  <Col md="6">
  <Form.Group>
          <Form.Label>State</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="state"
          placeholder="State"
          value={this.state.state}
          onChange={(e)=>this.setState({state:e.target.value})}
          />
          </Form.Group>


          <Form.Group>
          <Form.Label>Area</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="area"
          placeholder="Area"
          value={this.state.area}
          onChange={(e)=>this.setState({area:e.target.value})}
          />
          </Form.Group>

          </Col>
          <Col md="6">
          <Form.Group>

          <Form.Label>Referred_by</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="referred_by"
          placeholder="Referred by"
          value={this.state.referred_by}
          onChange={(e)=>this.setState({referred_by:e.target.value})}
          />
          </Form.Group>

          <Form.Group>
          <Form.Label>Occupation</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="occupation"
          placeholder="Occupation"
          value={this.state.occupation}
          onChange={(e)=>this.setState({occupation:e.target.value})}
          />
          </Form.Group>


</Col>
</Row>
          

            <Row>
  <Col md="6">
  <Form.Group>
          <Form.Label>Ethnic</Form.Label><br></br>
           <Form.Control  
           type="text"  
           name="ethnic"
            placeholder="ethnic"
          value={this.state.ethnic}
          onChange={(e)=>this.setState({ethnic:e.target.value})}
          
           />
          </Form.Group>

          <Form.Group>
          <Form.Label>Marital_status</Form.Label><br></br>
           <Form.Control  
           type="text"  
           name="marital_status"
            placeholder="Marital status"
          value={this.state.marital_status}
          onChange={(e)=>this.setState({marital_status:e.target.value})}
          
           />
          </Form.Group>

         </Col> 
      
         <Col md="6">
         <Form.Group>
          <Form.Label>marital_status_years</Form.Label><br></br>
           <Form.Control  
           type="number"
           name="marital_status_years"
            placeholder="marital status years"
          value={this.state.marital_status_years}
          onChange={(e)=>this.setState({marital_status_years:e.target.value})}  
         />
          </Form.Group>

          <Form.Group>
          <Form.Label>menses_frequency</Form.Label><br></br>
           <Form.Control  
           type="text"  
           name="menses_frequency"
            placeholder="menses frequency"
          value={this.state.menses_frequency}
          onChange={(e)=>this.setState({menses_frequency:e.target.value})}
           />
          </Form.Group>

          </Col>
          </Row>



          <Row>

			  
  <Col md="6">   
  <Form.Group>
          <Form.Label>menses_loss</Form.Label><br></br>
           <Form.Control  
           type="text"  
           name="menses_loss"
            placeholder="menses loss"
          value={this.state.menses_loss}
          onChange={(e)=>this.setState({menses_loss:e.target.value})}
    />
          </Form.Group>
          <Form.Group>
          <Form.Label>menarche_years</Form.Label><br></br>
           <Form.Control  
           type="number"
           name="menarche_years"
            placeholder="menarche year"
          value={this.state.menarche_years}
          onChange={(e)=>this.setState({menarche_years:e.target.value})}  
          />
          </Form.Group>
          </Col>
          <Col md="6"> 
          <Form.Group>  

          <Form.Label>Hysterectomy</Form.Label><br></br>
           <Form.Control  
           type="text"  
           name="hysterectomy"
            placeholder="hysterectomy"
          value={this.state.hysterectomy}
          onChange={(e)=>this.setState({hysterectomy:e.target.value})}
        />
          </Form.Group>
          <Form.Group>
          <Form.Label>hysterectomy_years</Form.Label><br></br>
           <Form.Control  
           type="number"
           name="hysterectomy_years"
            placeholder="hysterectomy year"
          value={this.state.hysterectomy_years}
          onChange={(e)=>this.setState({hysterectomy_years:e.target.value})}  
           />
          </Form.Group>

</Col>
</Row>




<Row>
          <Col md="6">
       
          <Form.Group>
          <Form.Label>menopause</Form.Label><br></br>
          <Form.Control 
          type="text" 	
          name="menopause"
             placeholder="menopause"
          value={this.state.menopause}
          onChange={(e)=>this.setState({menopause:e.target.value})}  					
  
/>
           </Form.Group>
          <Form.Group>
          <Form.Label>menopause_years</Form.Label><br></br>
           <Form.Control 
           type="number"
           name="menopause_years"
              placeholder="menopause year"
          value={this.state.menopause_years}
          onChange={(e)=>this.setState({menopause_years:e.target.value})}   						
/>
           </Form.Group>
          </Col>


          <Col md="6">
          <Form.Group>

          <Form.Label>no of children</Form.Label><br></br>
           <Form.Control 
           type="number"
           name="children"
              placeholder="no of children"
          value={this.state.children}
          onChange={(e)=>this.setState({children:e.target.value})}   						
/>
           </Form.Group>
          <Form.Group>
          <Form.Label>children_male</Form.Label><br></br>
           <Form.Control 
           type="number"
           name="children_male"
              placeholder="children_male"
          value={this.state.children_male}
          onChange={(e)=>this.setState({children_male:e.target.value})}   						
     />
           </Form.Group>

</Col>
</Row>

<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>children_female</Form.Label><br></br>
           <Form.Control 
           type="number"
           name="children_female"
              placeholder="children_female "
          value={this.state.children_female}
          onChange={(e)=>this.setState({children_female:e.target.value})}   						
 />
           </Form.Group>
          <Form.Group>
          <Form.Label>abortions</Form.Label><br></br>
           <Form.Control 
           type="text" 	
           name="abortions"
              placeholder="abortions"
          value={this.state.abortions}
          onChange={(e)=>this.setState({abortions:e.target.value})}  					
/>
           </Form.Group>
</Col>
<Col md="6">
<Form.Group>
          <Form.Label>abortions_number</Form.Label><br></br>
           <Form.Control 
           type="number"
           name="abortions_number"
              placeholder="abortions_number"
          value={this.state.abortions_number}
          onChange={(e)=>this.setState({abortions_number:e.target.value})}   						
    />
           </Form.Group>
          <Form.Group>
          <Form.Label>abortions_cause</Form.Label><br></br>
           <Form.Control 
           type="text" 	
           name="abortions_cause"
              placeholder="abortions_cause"
          value={this.state.abortions_cause}
          onChange={(e)=>this.setState({abortions_cause:e.target.value})}  					
/>
           </Form.Group>
         </Col>
         </Row>




         <Row>
  

<Col md="6">

<Form.Group>
           <Form.Label>current_lactation</Form.Label><br></br>
           <Form.Control  
           type="text" 	
           name="current_lactation"	
               placeholder="current_lactation"
          value={this.state.current_lactation}
          onChange={(e)=>this.setState({current_lactation:e.target.value})}  					
  />
          </Form.Group>
</Col>

<Col md="6">
          <Form.Group>
           <Form.Label>contraception_methods</Form.Label><br></br>
           <Form.Control  
           type="text" 	
           name="contraception_methods"	
               placeholder="contraception_methods"
          value={this.state.contraception_methods}
          onChange={(e)=>this.setState({contraception_methods:e.target.value})}  					
/>
          </Form.Group>
         </Col>
         </Row>



       <Row>
          <Col md="6">
          <Form.Group>
         <Form.Label>contraception_methods_type</Form.Label><br></br>
           <Form.Control  
           type="text" 	
           name="contraception_methods_type"	
               placeholder="contraception_methods_type"
          value={this.state.contraception_methods_type}
          onChange={(e)=>this.setState({contraception_methods_type:e.target.value})}  					
/>
          </Form.Group>
          <Form.Group>
           <Form.Label>hormone_treatment</Form.Label><br></br>
           <Form.Control  
           type="text" 	
           name="hormone_treatment"	
               placeholder="hormone_treatment"
          value={this.state.hormone_treatment}
          onChange={(e)=>this.setState({hormone_treatment:e.target.value})}  					
/>
          </Form.Group>
          </Col>

          <Col md="6">
          <Form.Group>
           <Form.Label>addiction</Form.Label><br></br>
           <Form.Control  
           type="text" 	
           name="addiction"	
               placeholder="addiction"
          value={this.state.addiction}
          onChange={(e)=>this.setState({addiction:e.target.value})}  					
/>
          </Form.Group>
          <Form.Group>
           <Form.Label>tobacco</Form.Label><br></br>
           <Form.Control  
           type="text" 	
           name="tobacco"	
               placeholder="tobacco"
          value={this.state.tobacco}
          onChange={(e)=>this.setState({tobacco:e.target.value})}  					
/>
          </Form.Group>

          </Col>
          </Row>


          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>tobacco_years</Form.Label><br></br>
          <Form.Control 
          type="number"
          name="tobacco_years" 
             placeholder="tobacco_years"
          value={this.state.tobacco_years}
          onChange={(e)=>this.setState({tobacco_years:e.target.value})}  						
/>
          </Form.Group>
          <Form.Group>
          <Form.Label>smoking</Form.Label><br></br>
          <Form.Control 
          type="text" 	
          name="smoking"	
             placeholder="smoking"
          value={this.state.smoking}
          onChange={(e)=>this.setState({smoking:e.target.value})}  				
/>
          </Form.Group>
          </Col>

          <Col md="6">
          <Form.Group>
          <Form.Label>smoking_years</Form.Label><br></br>
          <Form.Control 
          type="number"
          name="smoking_years" 
             placeholder="smoking_years"
          value={this.state.smoking_years}
          onChange={(e)=>this.setState({smoking_years:e.target.value})}  						
 />
          </Form.Group>
          <Form.Group>
          <Form.Label>alcohol</Form.Label><br></br>
          <Form.Control 
          type="text" 	
          name="alcohol"	
             placeholder="alcohol"
          value={this.state.alcohol}
          onChange={(e)=>this.setState({alcohol:e.target.value})}  				
 />
          </Form.Group>
          </Col>

          </Row>

          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>alcohol_years</Form.Label><br></br>
          <Form.Control 
          type="number"
          name="alcohol_years" 
             placeholder="alcohol_years"
          value={this.state.alcohol_years}
          onChange={(e)=>this.setState({alcohol_years:e.target.value})}  						
   />
          </Form.Group>
          <Form.Group>
          <Form.Label>family_history</Form.Label><br></br>
          <Form.Control 
          type="text" 	
          name="family_history"	
             placeholder="family_history"
          value={this.state.family_history}
          onChange={(e)=>this.setState({family_history:e.target.value})}  				
/>
          </Form.Group>
          </Col>


          <Col md="6">
          <Form.Group>
          <Form.Label>comorbidities</Form.Label><br></br>
          <Form.Control 
          type="text" 	
          name="comorbidities"	
             placeholder="comorbidities"
          value={this.state.comorbidities}
          onChange={(e)=>this.setState({comorbidities:e.target.value})}  				
/>
          </Form.Group>

          
          <Form.Group>
          <Form.Label>Health condition</Form.Label><br></br>
          <Form.Control 
          type="text" 	
          name="health_condition"	
             placeholder="health_condition"
          value={this.state.health_condition}
          onChange={(e)=>this.setState({health_condition:e.target.value})}  				
/>




</Form.Group>
          </Col>
          </Row>


<Row>
          <Col md="6">
     
          <Form.Group>
          <Form.Label>Remarks</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="remarks"        placeholder="remarks"
          value={this.state.remarks}
          onChange={(e)=>this.setState({remarks:e.target.value})}   						
/>
           </Form.Group>
</Col>





<Col md="6">
<Form.Group>
          <Form.Label>Onset</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="onset"        placeholder="onset"
          value={this.state.onset}
          onChange={(e)=>this.setState({onset:e.target.value})}   						
  />
           </Form.Group>

          <Form.Group>
          <Form.Label>onset_duration</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="onset_duration"        placeholder="onset_duration"
          value={this.state.onset_duration}
          onChange={(e)=>this.setState({onset_duration:e.target.value})}   						
/>
           </Form.Group>
</Col>
</Row>


<Row>
          <Col md="6">
          <Form.Group>

          <Form.Label>onset_duration_type</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="onset_duration_type"        placeholder="onset_duration_type"
          value={this.state.onset_duration_type}
          onChange={(e)=>this.setState({onset_duration_type:e.target.value})}   						
/>
           </Form.Group>
          <Form.Group>
          <Form.Label>first_symptom</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="first_symptom"        placeholder="first_symptom"
          value={this.state.first_symptom}
          onChange={(e)=>this.setState({first_symptom:e.target.value})}   						
/>
           </Form.Group>
</Col>

<Col md="6">
<Form.Group>
          <Form.Label>initial_site_joint</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="initial_site_joint"        placeholder="initial_site_joint"
          value={this.state.initial_site_joint}
          onChange={(e)=>this.setState({initial_site_joint:e.target.value})}   						
/>
           </Form.Group>
          <Form.Group>
          <Form.Label>soft_tissue</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="soft_tissue"        placeholder="soft_tissue"
          value={this.state.soft_tissue}
          onChange={(e)=>this.setState({soft_tissue:e.target.value})}   						
 />
           </Form.Group>

          </Col>
          </Row>
        
          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>others</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="others"        placeholder="others"
          value={this.state.others}
          onChange={(e)=>this.setState({others:e.target.value})}   						
/>
           </Form.Group>
          <Form.Group>
          <Form.Label>course1</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="course1"        placeholder="course1"
          value={this.state.course1}
          onChange={(e)=>this.setState({course1:e.target.value})}   						
    />
           </Form.Group>
</Col>

<Col md="6">
<Form.Group>
          <Form.Label>course2</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="course2"        placeholder="course2"
          value={this.state.course2}
          onChange={(e)=>this.setState({course2:e.target.value})}   						
/>
           </Form.Group>

          <Form.Group>
          <Form.Label>pattern1</Form.Label><br></br>
         <Form.Control 
         type="text"
 
 name="pattern1"        placeholder="pattern1"
          value={this.state.pattern1}
          onChange={(e)=>this.setState({pattern1:e.target.value})}   						
/>
           </Form.Group>
 </Col>
</Row>


<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>pattern2</Form.Label>
           <Form.Control 
           type="text" 	
           name="pattern2"	
           placeholder="pattern2"
          value={this.state.pattern2}
          onChange={(e)=>this.setState({pattern2:e.target.value})}  				
/>
          </Form.Group>


          <Form.Group>
          <Form.Label>current_relapse</Form.Label>
           <Form.Control 
           type="text" 	
           name="current_relapse"	
           placeholder="current_relapse"
          value={this.state.current_relapse}
          onChange={(e)=>this.setState({current_relapse:e.target.value})}  				
/>
          </Form.Group>


</Col>
<Col md="6">
<Form.Group>
          <Form.Label>current_relapse_type</Form.Label>
           <Form.Control 
           type="text" 	
           name="current_relapse_type"	
           placeholder="current_relapse_type"
          value={this.state.current_relapse_type}
          onChange={(e)=>this.setState({current_relapse_type:e.target.value})}  				
/>
          </Form.Group>

          <Form.Group>
          <Form.Label>detailed_history</Form.Label>
           <Form.Control 
           type="text" 	
           name="detailed_history"	
           placeholder="detailed_history"
          value={this.state.detailed_history}
          onChange={(e)=>this.setState({detailed_history:e.target.value})}  				
   />
          </Form.Group>
</Col>
</Row>


<Row>

          <Col md="6">
          <Form.Group>
          <Form.Label>pasthistory</Form.Label>
           <Form.Control 
           type="text" 	
           name="pasthistory"	
           placeholder="pasthistory"
          value={this.state.pasthistory}
          onChange={(e)=>this.setState({pasthistory:e.target.value})}  				
/>
          </Form.Group>

          <Form.Group>
          <Form.Label>surgical_history</Form.Label>
           <Form.Control 
           type="text" 	
           name="surgical_history"	
           placeholder="surgical_history"
          value={this.state.surgical_history}
          onChange={(e)=>this.setState({surgical_history:e.target.value})}  				
/>
          </Form.Group>
          </Col>


<Col md="6">
<Form.Group>
        <Form.Label>drug_history</Form.Label>
           <Form.Control 
           type="text" 	
           name="drug_history"	
           placeholder="drug_history"
          value={this.state.drug_history}
          onChange={(e)=>this.setState({drug_history:e.target.value})}  				
 />
          </Form.Group>


          <Form.Group>
          <Form.Label>drug_allergy</Form.Label>
           <Form.Control 
           type="text" 	
           name="drug_allergy"	
           placeholder="drug_allergy"
          value={this.state.drug_allergy}
          onChange={(e)=>this.setState({drug_allergy:e.target.value})}  				
/>
          </Form.Group>
</Col>

</Row>



<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>drug_allergy_type</Form.Label>
          <Form.Control 
          type="text" 	
          name="drug_allergy_type"	
          placeholder="drug_allergy_type"
          value={this.state.drug_allergy_type}
          onChange={(e)=>this.setState({drug_allergy_type:e.target.value})}  				
    />
          </Form.Group>

          <Form.Group>
          <Form.Label>bowelhabit</Form.Label>
          <Form.Control 
          type="text" 	
          name="bowelhabit"	
          placeholder="bowelhabit"
          value={this.state.bowelhabit}
          onChange={(e)=>this.setState({bowelhabit:e.target.value})}  				
/>
          </Form.Group>

</Col>

<Col md="6">

          <Form.Group>
          <Form.Label>sleep</Form.Label>
          <Form.Control 
          type="text" 	
          name="sleep"	
          placeholder="sleep"
          value={this.state.sleep}
          onChange={(e)=>this.setState({sleep:e.target.value})}  				
/>
          </Form.Group>

   </Col>
   </Row>








        <Row>
        
        
          <Col md="6">
          <Form.Group>
          <Form.Label>User Id</Form.Label>
         <Form.Control 
         type="number"
         name="user_id" 
         placeholder="User Id"
          value={this.state.user_id}
          onChange={(e)=>this.setState({user_id:e.target.value})}  	
          disabled					
/>
          </Form.Group>

</Col>
</Row>



<Button
                    className="btn-fill pull-right"
                    type="submit"
                    name=""
                    variant="info"
                  >
                    Update User
                  </Button>

        



                       </Form>
                       </Card.Body>
                       </Card>
                       </Col>
                       </Row>
                       
                       <div>
                        <h4>Attachments</h4>



        {
          this.state.products.map((i, index) => {
            console.log(i.attachment)

          var image= i.attachment.toString()

            var ext = image.split(".").pop()
            console.log(ext)
            if (ext == "jpeg" || ext == "png" || ext == "jpg"){
              console.log(i.attachment)

              return(
                <div>
                   <Row>

    <Col xs={4} md={4}>
      <Image src={`https://abcapi.vidaria.in/files/${i.attachment}?X-AUTH=abc123`}   fluid /><br></br><br></br>

      </Col>
      </Row>
                  </div>
                  
              )

            }else{
            console.log("im also coming")
              return(
                <video width="750" height="500" controls >
                <source src={`https://abcapi.vidaria.in/files/${i.attachment}?X-AUTH=abc123`} type="video/mp4"/>
          </video>
              )
              
   

            }
  
    })
  }
  </div>



      </Container>
    </>
  );
}
}

export default User;
