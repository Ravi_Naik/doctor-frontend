
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class Complaints extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}

    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Complaint  Information</h2>

        {/* <Row> */}

        <Form>
  












        {/* <Row>
          <Col md="6">
          <Form.Group>
        <Form.Label>Onset</Form.Label><br></br>
          <Form.Control type="text" onChange={this.props.handleChange('onset')}
						defaultValue={values.onset}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>onset_duration</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('onset_duration')} defaultValue={values.onset_duration}  />
          </Form.Group>
        </Col>

        
        <Col md="6">
        <Form.Group>
          <Form.Label>onset_duration_type</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('onset_duration_type')}  defaultValue={values.onset_duration_type}/>
          </Form.Group>


          <Form.Group>
          <Form.Label>first_symptom</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('first_symptom')} defaultValue={values.first_symptom}/>
          </Form.Group>
          </Col>
          </Row>





          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>initial_site_joint</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('initial_site_joint')} defaultValue={values.initial_site_joint}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>soft_tissue</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('soft_tissue')} defaultValue={values.soft_tissue}/>
          </Form.Group>
</Col>
<Col md="6">

<Form.Group>
          <Form.Label>others</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('others')} defaultValue={values.others}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>course1</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('course1')} defaultValue={values.course1}/>
          </Form.Group>

</Col>
</Row>

<Row>
  <Col md="6">
  <Form.Group>

            <Form.Label>course2</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('course2')} defaultValue={values.course2}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>pattern1</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('pattern1')} defaultValue={values.pattern1}/>
                    </Form.Group>


</Col>
<Col md="6">
<Form.Group>

          <Form.Label>pattern2</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('pattern2')} defaultValue={values.pattern2}/>
                    </Form.Group>



          <Form.Group>

          <Form.Label>current_relapse</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('current_relapse')} defaultValue={values.current_relapse}/>
                    </Form.Group>


</Col>
</Row> 

<Row>
  <Col md="6">
  <Form.Group>

          <Form.Label>current_relapse_type</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('current_relapse_type')} defaultValue={values.current_relapse_type}/>
                    </Form.Group>


          <Form.Group>


          <Form.Label>detailed_history</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('detailed_history')}  defaultValue={values.detailed_history}/>
                    </Form.Group>


          </Col>
          <Col md="6">
          <Form.Group>

          <Form.Label>pasthistory</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('pasthistory')}  defaultValue={values.pasthistory}/>
                    </Form.Group>

          <Form.Group>

          <Form.Label>surgical_history</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('surgical_history')}  defaultValue={values.surgical_history}/>
                    </Form.Group>


</Col>
</Row> */}

<Row>
  {/* <Col md="6">
  <Form.Group>

          <Form.Label>drug_history</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('drug_history')}  defaultValue={values.drug_history}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>drug_allergy</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('drug_allergy')} defaultValue={values.drug_allergy}/>
                    </Form.Group>


         </Col>  */}
         <Col md="6">
         <Form.Group>

          <Form.Label>drug_allergy_type</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('drug_allergy_type')}  defaultValue={values.drug_allergy_type}/>
                    </Form.Group>


                    </Col>
        
          <Col md="6">
          <Form.Group>

          <Form.Label>bowelhabit</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('bowelhabit')} defaultValue={values.bowelhabit}/>
                    </Form.Group>


          </Col>
          </Row>


          <Row>
  <Col md="6">   
  <Form.Group>

          <Form.Label>sleep</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('sleep')} defaultValue={values.sleep}/>
                    </Form.Group>


</Col>


<Col md="6">
  {/* <Form.Group>

<Form.Label>User ID</Form.Label><br></br>
<Form.Control type="number" readOnly  onChange={this.props.handleChange('user_id')} defaultValue={values.user_id}/>
          </Form.Group> */}
  </Col>
{/* <Col md="6">
<Form.Group>

<Form.Label>pid ID</Form.Label><br></br>
<Form.Control type="number"  onChange={this.props.handleChange('pid')} defaultValue={values.pid}/>
          </Form.Group>
</Col> */}
</Row>

<Row>






<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">   
           {/* <Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button> */}
           <Button className="btn-fill" variant="success" onClick={this.props.saveAll}>Submit</Button>


</Col>

</Row>









        </Form>
   
      {/* </Row> */}
      </Container>
    )
  }
}

export default Complaints;







