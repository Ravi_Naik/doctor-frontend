
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Table,
  Col,
} from "react-bootstrap";

class Deformities_examination extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}


    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient MUSCULOSKELETAL_EXAMINATIONS</h2>
         <h5>DEFORMITIES: Information</h5> 

        <Row>

        <Form>
  
        <Table striped bordered hover>
  <thead>
    <tr>
      <th>JOINT</th>
      <th>FL</th>
      <th>EX</th>
      <th>AB</th>
      <th>ADD</th>
      <th>SLX</th>
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td>HAND(R)</td>
      <td><input type="text" 	onChange={this.props.handleChange('rhand_fl')}
defaultValue={values.rhand_fl} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('rhand_ex')}
defaultValue={values.rhand_ex} maxlength="50" size="4" /></td>

      
            <td><input type="text" 						onChange={this.props.handleChange('rhand_ab')}
defaultValue={values.rhand_ab} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('rhand_add')}
defaultValue={values.rhand_add} maxlength="50" size="4" /></td>

<td><input type="text" 						onChange={this.props.handleChange('rhand_slx')}
defaultValue={values.rhand_slx} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td>HAND(L)</td>
      <td><input type="text" 	onChange={this.props.handleChange('lhand_fl')}
defaultValue={values.lhand_fl} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('lhand_ex')}
defaultValue={values.lhand_ex} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('lhand_ab')}
defaultValue={values.lhand_ab} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('lhand_add')}
defaultValue={values.lhand_add} maxlength="50" size="4" /></td>

<td><input type="text" 						onChange={this.props.handleChange('lhand_slx')}
defaultValue={values.lhand_slx} maxlength="50" size="4" /></td>

    </tr>


    <tr>
    <td>WRIST(R)</td>
      <td><input type="text" 	onChange={this.props.handleChange('rwrist_fl')}
defaultValue={values.rwrist_fl} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('rwrist_ex')}
defaultValue={values.rwrist_ex} maxlength="50" size="4" /></td>

     
            <td><input type="text" 						onChange={this.props.handleChange('rwrist_ab')}
defaultValue={values.rwrist_ab} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('rwrist_add')}
defaultValue={values.rwrist_add} maxlength="50" size="4" /></td>


<td><input type="text" 						onChange={this.props.handleChange('rwrist_slx')}
defaultValue={values.rwrist_slx} maxlength="50" size="4" /></td>

    </tr>

    <tr>
    <td>WRIST(L)</td>
      <td><input type="text" 	onChange={this.props.handleChange('lwrist_fl')}
defaultValue={values.lwrist_fl} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('lwrist_ex')}
defaultValue={values.lwrist_ex} maxlength="50" size="4" /></td>

      
            <td><input type="text" 						onChange={this.props.handleChange('lwrist_ab')}
defaultValue={values.lwrist_ab} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('lwrist_add')}
defaultValue={values.lwrist_add} maxlength="50" size="4" /></td>

<td><input type="text" 						onChange={this.props.handleChange('lwrist_slx')}
defaultValue={values.lwrist_slx} maxlength="50" size="4" /></td>

    </tr>


    <tr>
    <td>ELB(R)</td>
      <td><input type="text" 	onChange={this.props.handleChange('relb_fl')}
defaultValue={values.relb_fl} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('relb_ex')}
defaultValue={values.relb_ex} maxlength="50" size="4" /></td>

      
            <td><input type="text" 						onChange={this.props.handleChange('relb_ab')}
defaultValue={values.relb_ab} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('relb_add')}
defaultValue={values.relb_add} maxlength="50" size="4" /></td>

<td><input type="text" 						onChange={this.props.handleChange('relb_slx')}
defaultValue={values.relb_slx} maxlength="50" size="4" /></td>

    </tr>


    <tr>
    <td>ELB(L)</td>
      <td><input type="text" 	onChange={this.props.handleChange('lelb_fl')}
defaultValue={values.lelb_fl} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('lelb_ex')}
defaultValue={values.lelb_ex} maxlength="50" size="4" /></td>

     
            <td><input type="text" 						onChange={this.props.handleChange('lelb_ab')}
defaultValue={values.lelb_ab} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('lelb_add')}
defaultValue={values.lelb_add} maxlength="50" size="4" /></td>

<td><input type="text" 						onChange={this.props.handleChange('lelb_slx')}
defaultValue={values.lelb_slx} maxlength="50" size="4" /></td>

    </tr>


    <tr>
    <td>SH (RT)</td>
      <td><input type="text" 	onChange={this.props.handleChange('shrt_fl')}
defaultValue={values.shrt_fl} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('shrt_ex')}
defaultValue={values.shrt_ex} maxlength="50" size="4" /></td>

     
            <td><input type="text" 						onChange={this.props.handleChange('shrt_ab')}
defaultValue={values.shrt_ab} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('shrt_add')}
defaultValue={values.shrt_add} maxlength="50" size="4" /></td>

<td><input type="text" 						onChange={this.props.handleChange('shrt_slx')}
defaultValue={values.shrt_slx} maxlength="50" size="4" /></td>

    </tr>


<tr>
    <td>SH (LT)</td>
      <td><input type="text" 	onChange={this.props.handleChange('shlt_fl')}
defaultValue={values.shlt_fl} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('shlt_ex')}
defaultValue={values.shlt_ex} maxlength="50" size="4" /></td>

     
            <td><input type="text" 						onChange={this.props.handleChange('shlt_ab')}
defaultValue={values.shlt_ab} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('shlt_add')}
defaultValue={values.shlt_add} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('shlt_slx')}
defaultValue={values.shlt_slx} maxlength="50" size="4" /></td>

    </tr>

    <tr>
    <td>KNEE (RL)</td>
      <td><input type="text" 	onChange={this.props.handleChange('kneert_fl')}
defaultValue={values.kneert_fl} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('kneert_ex')}
defaultValue={values.kneert_ex} maxlength="50" size="4" /></td>

     
            <td><input type="text" 						onChange={this.props.handleChange('kneert_ab')}
defaultValue={values.kneert_ab} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('kneert_add')}
defaultValue={values.kneert_add} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('kneert_slx')}
defaultValue={values.kneert_slx} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td>KNEE (LT)</td>
      <td><input type="text" 	onChange={this.props.handleChange('kneelt_fl')}
defaultValue={values.kneelt_fl} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('kneelt_ex')}
defaultValue={values.kneelt_ex} maxlength="50" size="4" /></td>

     
            <td><input type="text" 						onChange={this.props.handleChange('kneelt_ab')}
defaultValue={values.kneelt_ab} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('kneelt_add')}
defaultValue={values.kneelt_add} maxlength="50" size="4" /></td>


<td><input type="text" 						onChange={this.props.handleChange('kneelt_slx')}
defaultValue={values.kneelt_slx} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td>FOOT (RL)</td>
      <td><input type="text" 	onChange={this.props.handleChange('footrt_fl')}
defaultValue={values.footrt_fl} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('footrt_ex')}
defaultValue={values.footrt_ex} maxlength="50" size="4" /></td>

     
            <td><input type="text" 						onChange={this.props.handleChange('footrt_ab')}
defaultValue={values.footrt_ab} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('footrt_add')}
defaultValue={values.footrt_add} maxlength="50" size="4" /></td>


<td><input type="text" 						onChange={this.props.handleChange('footrt_slx')}
defaultValue={values.footrt_slx} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td>FOOT (LT)</td>
      <td><input type="text" 	onChange={this.props.handleChange('footlt_fl')}
defaultValue={values.footlt_fl} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('footlt_ex')}
defaultValue={values.footlt_ex} maxlength="50" size="4" /></td>

     
            <td><input type="text" 						onChange={this.props.handleChange('footlt_ab')}
defaultValue={values.footlt_ab} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('footlt_add')}
defaultValue={values.footlt_add} maxlength="50" size="4" /></td>

<td><input type="text" 						onChange={this.props.handleChange('footlt_slx')}
defaultValue={values.footlt_slx} maxlength="50" size="4" /></td>

    </tr>

 

    
   
   
  </tbody>
</Table>

<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">
<Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>
</Row>
        </Form>
   
      </Row>

      
      </Container>
    )
  }
}

export default Deformities_examination;







