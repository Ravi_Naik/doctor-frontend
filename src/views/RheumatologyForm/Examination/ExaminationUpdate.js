

import React from "react";
import axios from 'axios'
// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Form,
  Navbar,
  Nav,
  Container,
  Row,
  Col,
  Table
} from "react-bootstrap";
import userdetails from '../../admin';
import { Redirect } from "react-router-dom"


import Swal from 'sweetalert2'

class Examination_Main extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      pid: this.props.match.params.id,
      user_id: '',
      general_condition: '',
      weight: '',
      height: '',
      bmi: '',
      bp: '',
      pulse: '',
      temp: '',
      respiratory_rate: '',
      ex_health_condition: '',
      ex_remarks: '',
      eyes: '',
      skin_morphology: '',
      skin_pattern: '',
      skin_color: '',
      distribution: '',
      other_affects: '',
      mucosa: '',
      hair_loss: '',
      dandruff: '',
      nail: '',
      lymphnodes: '',
      lymphnodes_number: '',
      tender: '',
      tender_type: '',
      lymphnodes_tender_others: '',
      vascular: '',
      cns_examination: '',
      cvs_examination: '',
      chest_examination: '',
      abdomen_examination: '',
      tm_t1: '',
      tm_s1: '',
      tm_t2: '',
      tm_s2: '',
      scl_t1: '',
      scl_s1: '',
      scl_t2: '',
      scl_s2: '',
      acl_t1: '',
      acl_s1: '',
      acl_t2: '',
      acl_s2: '',
      sh_t1: '',
      sh_s1: '',
      sh_t2: '',
      sh_s2: '',
      elbow_t1: '',
      elbow_s1: '',
      elbow_t2: '',
      elbow_s2: '',
      infru_t1: '',
      infru_s1: '',
      infru_t2: '',
      infru_s2: '',
      cmc1_t1: '',
      cmc1_s1: '',
      cmc1_t2: '',
      cmc1_s2: '',
      wrist_t1: '',
      wrist_s1: '',
      wrist_t2: '',
      wrist_s2: '',
      dip2_t1: '',
      dip2_s1: '',
      dip2_t2: '',
      dip2_s2: '',
      dip3_t1: '',
      dip3_s1: '',
      dip3_t2: '',
      dip3_s2: '',
      dip4_t1: '',
      dip4_s1: '',
      dip4_t2: '',
      dip4_s2: '',
      dip5_t1: '',
      dip5_s1: '',
      dip5_t2: '',
      dip5_s2: '',
      ip1_t1: '',
      ip1_s1: '',
      ip1_t2: '',
      ip1_s2: '',
      ip2_t1: '',
      ip2_s1: '',
      ip2_t2: '',
      ip2_s2: '',
      pip3_t1: '',
      pip3_s1: '',
      pip3_t2: '',
      pip3_s2: '',
      pip4_t1: '',
      pip4_s1: '',
      pip4_t2: '',
      pip4_s2: '',
      pip5_t1: '',
      pip5_s1: '',
      pip5_t2: '',
      pip5_s2: '',
      mcp1_t1: '',
      mcp1_s1: '',
      mcp1_t2: '',
      mcp1_s2: '',
      mcp2_t1: '',
      mcp2_s1: '',
      mcp2_t2: '',
      mcp2_s2: '',
      mcp3_t1: '',
      mcp3_s1: '',
      mcp3_t2: '',
      mcp3_s2: '',
      mcp4_t1: '',
      mcp4_s1: '',
      mcp4_t2: '',
      mcp4_s2: '',
      mcp5_t1: '',
      mcp5_s1: '',
      mcp5_t2: '',
      mcp5_s2: '',
      hip_t1: '',
      hip_s1: '',
      hip_t2: '',
      hip_s2: '',
      knee_t1: '',
      knee_s1: '',
      knee_t2: '',
      knee_s2: '',
      a_tt_t1: '',
      a_tt_s1: '',
      a_tt_t2: '',
      a_tt_s2: '',
      a_tc_t1: '',
      a_tc_s1: '',
      a_tc_t2: '',
      a_tc_s2: '',
      mtl_t1: '',
      mtl_s1: '',
      mtl_t2: '',
      mtl_s2: '',
      mtp1_t1: '',
      mtp1_s1: '',
      mtp1_t2: '',
      mtp1_s2: '',
      mtp2_t1: '',
      mtp2_s1: '',
      mtp2_t2: '',
      mtp2_s2: '',
      mtp3_t1: '',
      mtp3_s1: '',
      mtp3_t2: '',
      mtp3_s2: '',
      mtp4_t1: '',
      mtp4_s1: '',
      mtp4_t2: '',
      mtp4_s2: '',
      mtp5_t1: '',
      mtp5_s1: '',
      mtp5_t2: '',
      mtp5_s2: '',
      ip1b_t1: '',
      ip1b_s1: '',
      ip1b_t2: '',
      ip1b_s2: '',
      ip2b_t1: '',
      ip2b_s1: '',
      ip2b_t2: '',
      ip2b_s2: '',
      ip3b_t1: '',
      ip3b_s1: '',
      ip3b_t2: '',
      ip3b_s2: '',
      ip4b_t1: '',
      ip4b_s1: '',
      ip4b_t2: '',
      ip4b_s2: '',
      ip5b_t1: '',
      ip5b_s1: '',
      ip5b_t2: '',
      ip5b_s2: '',
      s1_t1: '',
      s1_s1: '',
      s1_t2: '',
      s1_s2: '',
      cervical_anky: '',
      cervical_flex: '',
      cervical_ext: '',
      cervical_rtrot: '',
      cervical_ltrot: '',
      cervical_rtfl: '',
      cervical_ltfl: '',
      cervical_pt: '',
      thorasic_anky: '',
      thorasic_flex: '',
      thorasic_ext: '',
      thorasic_rtrot: '',
      thorasic_ltrot: '',
      thorasic_rtfl: '',
      thorasic_ltfl: '',
      thorasic_pt: '',
      lumbar_anky: '',
      lumbar_flex: '',
      lumbar_ext: '',
      lumbar_rtrot: '',
      lumbar_ltrot: '',
      lumbar_rtfl: '',
      lumbar_ltfl: '',
      lumbar_pt: '',
      opt_rt: '',
      opt_lt: '',
      lcer_rt: '',
      lcer_lt: '',
      trpz_rt: '',
      trpz_lt: '',
      scap_rt: '',
      scap_lt: '',
      zcst_rt: '',
      zcst_lt: '',
      epdl_rt: '',
      epdl_lt: '',
      glut_rt: '',
      glut_lt: '',
      trcr_rt: '',
      trcr_lt: '',
      knee_rt: '',
      knee_lt: '',
      ta_rt: '',
      ta_lt: '',
      calf_rt: '',
      calf_lt: '',
      sole_rt: '',
      sole_lt: '',
      rhand_fl: '',
      rhand_ex: '',
      rhand_ab: '',
      rhand_add: '',
      rhand_slx: '',

      lhand_fl: '',
      lhand_ex: '',
      lhand_ab: '',
      lhand_add: '',
      lhand_slx: '',


      rwrist_fl: '',
      rwrist_ex: '',
      rwrist_ab: '',
      rwrist_add: '',
      rwrist_slx: '',


      lwrist_fl: '',
      lwrist_ex: '',
      lwrist_ab: '',
      lwrist_add: '',
      lwrist_slx: '',

      relb_fl: '',
      relb_ex: '',
      relb_ab: '',
      relb_add: '',
      relb_slx: '',

      lelb_fl: '',
      lelb_ex: '',
      lelb_ab: '',
      lelb_add: '',
      lelb_slx: '',


      shrt_fl: '',
      shrt_ex: '',
      shrt_ab: '',
      shrt_add: '',
      shrt_slx: '',

      shlt_fl: '',
      shlt_ex: '',
      shlt_ab: '',
      shlt_add: '',
      shlt_slx: '',

      kneert_fl: '',
      kneert_ex: '',
      kneert_ab: '',
      kneert_add: '',
      kneert_slx: '',

      kneelt_fl: '',
      kneelt_ex: '',
      kneelt_ab: '',
      kneelt_add: '',
      kneelt_slx: '',

      footrt_fl: '',
      footrt_ex: '',
      footrt_ab: '',
      footrt_add: '',
      footrt_slx: '',

      footlt_fl: '',
      footlt_ex: '',
      footlt_ab: '',
      footlt_add: '',
      footlt_slx: '',

      thumb_rt: '',
      thumb_lt: '',
      finger_rt: '',
      finger_lt: '',
      palm_rt: '',
      palm_lt: '',
      elbow_rt: '',
      elbow_lt: '',
      hy_knee_rt: '',
      hy_knee_lt: '',
      ankle_rt: '',
      ankle_lt: '',
      other_rt: '',
      other_lt: '',
      spine_rt: '',
      spine_lt: '',



    }

  }

  opensweetalert() {
    Swal.fire({
      title: 'Patient record updated',
      position: 'top-end',
      icon: 'success',
      showConfirmButton: false,
      timer: 1200

    }).then(function () {
      window.location = "/admin/dashboard";
    });
  }




  norecordsweetalert() {
    Swal.fire({
      title: 'No record Found',
      position: 'top-end',
      icon: 'success',
      showConfirmButton: false,
      timer: 1200

    }).then(function () {
      window.location = "/admin/dashboard";
    });
  }

  // changeHandler=(e) => {
  //   this.setState({[e.target.name]:e.target.value})
  // }

  delete(pid, e) {
    var del = "Rheumatology Patient Examination deleted"

    e.preventDefault()
    Swal.fire({
      title: 'Do you want to delete the changes?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: `Delete`,
      denyButtonText: `Cancel`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('user deleted!', '', 'success')
        axios.delete(`https://abcapi.vidaria.in/rheumapatientexaminationdelete?X-AUTH=abc123&pid=${pid}`)
        axios.post(`https://abcapi.vidaria.in/addactivity?X-AUTH=abc123`, {

          activity: del,
          user_id: this.state.user_id,

        })
          .then(function () {
            window.location = "/admin/dashboard";
          });


      } else if (result.isDenied) {
        Swal.fire('user record is safe !', '', 'info')
      }
    })
  }

  componentDidMount() {
    this.setState({ isLoading: true })
    axios.get(`https://abcapi.vidaria.in/rheumapatientexaminationdetails?X-AUTH=abc123&pid=${this.state.pid}`)

      .then((res) => {
        console.log("value", res.data.RheumatologyPatientExamination.pid)
        if (res.data.RheumatologyPatientExamination.pid != undefined )
         {
          this.setState({
            id: res.data.RheumatologyPatientExamination.id,
            user_id: res.data.RheumatologyPatientExamination.user_id,
            pid: res.data.RheumatologyPatientExamination.pid,
            general_condition: res.data.RheumatologyPatientExamination.general_condition,
            weight: res.data.RheumatologyPatientExamination.weight,
            height: res.data.RheumatologyPatientExamination.height,
            bmi: res.data.RheumatologyPatientExamination.bmi,
            bp: res.data.RheumatologyPatientExamination.bp,
            pulse: res.data.RheumatologyPatientExamination.pulse,
            temp: res.data.RheumatologyPatientExamination.temp,
            respiratory_rate: res.data.RheumatologyPatientExamination.respiratory_rate,
            ex_health_condition: res.data.RheumatologyPatientExamination.ex_health_condition,
            ex_remarks: res.data.RheumatologyPatientExamination.ex_remarks,
            eyes: res.data.RheumatologyPatientExamination.eyes,
            skin_morphology: res.data.RheumatologyPatientExamination.skin_morphology,



            skin_pattern: res.data.RheumatologyPatientExamination.skin_pattern,
            skin_color: res.data.RheumatologyPatientExamination.skin_color,
            distribution: res.data.RheumatologyPatientExamination.distribution,
            other_affects: res.data.RheumatologyPatientExamination.other_affects,
            mucosa: res.data.RheumatologyPatientExamination.mucosa,



            hair_loss: res.data.RheumatologyPatientExamination.hair_loss,
            dandruff: res.data.RheumatologyPatientExamination.dandruff,
            nail: res.data.RheumatologyPatientExamination.nail,
            lymphnodes: res.data.RheumatologyPatientExamination.lymphnodes,
            lymphnodes_number: res.data.RheumatologyPatientExamination.lymphnodes_number,




            tender: res.data.RheumatologyPatientExamination.tender,
            tender_type: res.data.RheumatologyPatientExamination.tender_type,
            lymphnodes_tender_others: res.data.RheumatologyPatientExamination.lymphnodes_tender_others,
            vascular: res.data.RheumatologyPatientExamination.vascular,
            cns_examination: res.data.RheumatologyPatientExamination.cns_examination,
            cvs_examination: res.data.RheumatologyPatientExamination.cvs_examination,
            chest_examination: res.data.RheumatologyPatientExamination.chest_examination,
            abdomen_examination: res.data.RheumatologyPatientExamination.abdomen_examination,


            tm_t1: res.data.RheumatologyPatientExamination.tm_t1,
            tm_s1: res.data.RheumatologyPatientExamination.tm_s1,
            tm_t2: res.data.RheumatologyPatientExamination.tm_t2,
            tm_s2: res.data.RheumatologyPatientExamination.tm_s2,

            scl_t1: res.data.RheumatologyPatientExamination.scl_t1,
            scl_s1: res.data.RheumatologyPatientExamination.scl_s1,
            scl_t2: res.data.RheumatologyPatientExamination.scl_t2,
            scl_s2: res.data.RheumatologyPatientExamination.scl_s2,



            acl_t1: res.data.RheumatologyPatientExamination.acl_t1,
            acl_s1: res.data.RheumatologyPatientExamination.acl_s1,
            acl_t2: res.data.RheumatologyPatientExamination.acl_t2,
            acl_s2: res.data.RheumatologyPatientExamination.acl_s2,


            sh_t1: res.data.RheumatologyPatientExamination.sh_t1,
            sh_s1: res.data.RheumatologyPatientExamination.sh_s1,
            sh_t2: res.data.RheumatologyPatientExamination.sh_t2,
            sh_s2: res.data.RheumatologyPatientExamination.sh_s2,

            elbow_t1: res.data.RheumatologyPatientExamination.elbow_t1,
            elbow_s1: res.data.RheumatologyPatientExamination.elbow_s1,
            elbow_t2: res.data.RheumatologyPatientExamination.elbow_t2,
            elbow_s2: res.data.RheumatologyPatientExamination.elbow_s2,


            infru_t1: res.data.RheumatologyPatientExamination.infru_t1,
            infru_s1: res.data.RheumatologyPatientExamination.infru_s1,
            infru_t2: res.data.RheumatologyPatientExamination.infru_t2,
            infru_s2: res.data.RheumatologyPatientExamination.infru_s2,


            cmc1_t1: res.data.RheumatologyPatientExamination.cmc1_t1,
            cmc1_s1: res.data.RheumatologyPatientExamination.cmc1_s1,
            cmc1_t2: res.data.RheumatologyPatientExamination.cmc1_t2,
            cmc1_s2: res.data.RheumatologyPatientExamination.cmc1_s2,



            wrist_t1: res.data.RheumatologyPatientExamination.wrist_t1,
            wrist_s1: res.data.RheumatologyPatientExamination.wrist_s1,
            wrist_t2: res.data.RheumatologyPatientExamination.wrist_t2,
            wrist_s2: res.data.RheumatologyPatientExamination.wrist_s2,


            dip2_t1: res.data.RheumatologyPatientExamination.dip2_t1,
            dip2_s1: res.data.RheumatologyPatientExamination.dip2_s1,
            dip2_t2: res.data.RheumatologyPatientExamination.dip2_t2,
            dip2_s2: res.data.RheumatologyPatientExamination.dip2_s2,


            dip3_t1: res.data.RheumatologyPatientExamination.dip3_t1,
            dip3_s1: res.data.RheumatologyPatientExamination.dip3_s1,
            dip3_t2: res.data.RheumatologyPatientExamination.dip3_t2,
            dip3_s2: res.data.RheumatologyPatientExamination.dip3_s2,


            dip4_t1: res.data.RheumatologyPatientExamination.dip4_t1,
            dip4_s1: res.data.RheumatologyPatientExamination.dip4_s1,
            dip4_t2: res.data.RheumatologyPatientExamination.dip4_t2,
            dip4_s2: res.data.RheumatologyPatientExamination.dip4_s2,



            dip5_t1: res.data.RheumatologyPatientExamination.dip5_t1,
            dip5_s1: res.data.RheumatologyPatientExamination.dip5_s1,
            dip5_t2: res.data.RheumatologyPatientExamination.dip5_t2,
            dip5_s2: res.data.RheumatologyPatientExamination.dip5_s2,


            ip1_t1: res.data.RheumatologyPatientExamination.ip1_t1,
            ip1_s1: res.data.RheumatologyPatientExamination.ip1_s1,
            ip1_t2: res.data.RheumatologyPatientExamination.ip1_t2,
            ip1_s2: res.data.RheumatologyPatientExamination.ip1_s2,


            ip2_t1: res.data.RheumatologyPatientExamination.ip2_t1,
            ip2_s1: res.data.RheumatologyPatientExamination.ip2_s1,
            ip2_t2: res.data.RheumatologyPatientExamination.ip2_t2,
            ip2_s2: res.data.RheumatologyPatientExamination.ip2_s2,


            pip3_t1: res.data.RheumatologyPatientExamination.pip3_t1,
            pip3_s1: res.data.RheumatologyPatientExamination.pip3_s1,
            pip3_t2: res.data.RheumatologyPatientExamination.pip3_t2,
            pip3_s2: res.data.RheumatologyPatientExamination.pip3_s2,


            pip4_t1: res.data.RheumatologyPatientExamination.pip4_t1,
            pip4_s1: res.data.RheumatologyPatientExamination.pip4_s1,
            pip4_t2: res.data.RheumatologyPatientExamination.pip4_t2,
            pip4_s2: res.data.RheumatologyPatientExamination.pip4_s2,

            pip5_t1: res.data.RheumatologyPatientExamination.pip5_t1,
            pip5_s1: res.data.RheumatologyPatientExamination.pip5_s1,
            pip5_t2: res.data.RheumatologyPatientExamination.pip5_t2,
            pip5_s2: res.data.RheumatologyPatientExamination.pip5_s2,

            mcp1_t1: res.data.RheumatologyPatientExamination.mcp1_t1,
            mcp1_s1: res.data.RheumatologyPatientExamination.mcp1_s1,
            mcp1_t2: res.data.RheumatologyPatientExamination.mcp1_t2,
            mcp1_s2: res.data.RheumatologyPatientExamination.mcp1_s2,


            mcp2_t1: res.data.RheumatologyPatientExamination.mcp2_t1,
            mcp2_s1: res.data.RheumatologyPatientExamination.mcp2_s1,
            mcp2_t2: res.data.RheumatologyPatientExamination.mcp2_t2,
            mcp2_s2: res.data.RheumatologyPatientExamination.mcp2_s2,

            mcp3_t1: res.data.RheumatologyPatientExamination.mcp3_t1,
            mcp3_s1: res.data.RheumatologyPatientExamination.mcp3_s1,
            mcp3_t2: res.data.RheumatologyPatientExamination.mcp3_t2,
            mcp3_s2: res.data.RheumatologyPatientExamination.mcp3_s2,


            mcp4_t1: res.data.RheumatologyPatientExamination.mcp4_t1,
            mcp4_s1: res.data.RheumatologyPatientExamination.mcp4_s1,
            mcp4_t2: res.data.RheumatologyPatientExamination.mcp4_t2,
            mcp4_s2: res.data.RheumatologyPatientExamination.mcp4_s2,

            mcp5_t1: res.data.RheumatologyPatientExamination.mcp5_t1,
            mcp5_s1: res.data.RheumatologyPatientExamination.mcp5_s1,
            mcp5_t2: res.data.RheumatologyPatientExamination.mcp5_t2,
            mcp5_s2: res.data.RheumatologyPatientExamination.mcp5_s2,

            hip_t1: res.data.RheumatologyPatientExamination.hip_t1,
            hip_s1: res.data.RheumatologyPatientExamination.hip_s1,
            hip_t2: res.data.RheumatologyPatientExamination.hip_t2,
            hip_s2: res.data.RheumatologyPatientExamination.hip_s2,


            knee_t1: res.data.RheumatologyPatientExamination.knee_t1,
            knee_s1: res.data.RheumatologyPatientExamination.knee_s1,
            knee_t2: res.data.RheumatologyPatientExamination.knee_t2,
            knee_s2: res.data.RheumatologyPatientExamination.knee_s2,


            a_tt_t1: res.data.RheumatologyPatientExamination.a_tt_t1,
            a_tt_s1: res.data.RheumatologyPatientExamination.a_tt_s1,
            a_tt_t2: res.data.RheumatologyPatientExamination.a_tt_t2,
            a_tt_s2: res.data.RheumatologyPatientExamination.a_tt_s2,

            a_tc_t1: res.data.RheumatologyPatientExamination.a_tc_t1,
            a_tc_s1: res.data.RheumatologyPatientExamination.a_tc_s1,
            a_tc_t2: res.data.RheumatologyPatientExamination.a_tc_t2,
            a_tc_s2: res.data.RheumatologyPatientExamination.a_tc_s2,

            mtl_t1: res.data.RheumatologyPatientExamination.mtl_t1,
            mtl_s1: res.data.RheumatologyPatientExamination.mtl_s1,
            mtl_t2: res.data.RheumatologyPatientExamination.mtl_t2,
            mtl_s2: res.data.RheumatologyPatientExamination.mtl_s2,


            mtp1_t1: res.data.RheumatologyPatientExamination.mtp1_t1,
            mtp1_s1: res.data.RheumatologyPatientExamination.mtp1_s1,
            mtp1_t2: res.data.RheumatologyPatientExamination.mtp1_t2,
            mtp1_s2: res.data.RheumatologyPatientExamination.mtp1_s2,


            mtp2_t1: res.data.RheumatologyPatientExamination.mtp2_t1,
            mtp2_s1: res.data.RheumatologyPatientExamination.mtp2_s1,
            mtp2_t2: res.data.RheumatologyPatientExamination.mtp2_t2,
            mtp2_s2: res.data.RheumatologyPatientExamination.mtp2_s2,


            mtp3_t1: res.data.RheumatologyPatientExamination.mtp3_t1,
            mtp3_s1: res.data.RheumatologyPatientExamination.mtp3_s1,
            mtp3_t2: res.data.RheumatologyPatientExamination.mtp3_t2,
            mtp3_s2: res.data.RheumatologyPatientExamination.mtp3_s2,


            mtp4_t1: res.data.RheumatologyPatientExamination.mtp4_t1,
            mtp4_s1: res.data.RheumatologyPatientExamination.mtp4_s1,
            mtp4_t2: res.data.RheumatologyPatientExamination.mtp4_t2,
            mtp4_s2: res.data.RheumatologyPatientExamination.mtp4_s2,


            mtp5_t1: res.data.RheumatologyPatientExamination.mtp5_t1,
            mtp5_s1: res.data.RheumatologyPatientExamination.mtp5_s1,
            mtp5_t2: res.data.RheumatologyPatientExamination.mtp5_t2,
            mtp5_s2: res.data.RheumatologyPatientExamination.mtp5_s2,


            ip1b_t1: res.data.RheumatologyPatientExamination.ip1b_t1,
            ip1b_s1: res.data.RheumatologyPatientExamination.ip1b_s1,
            ip1b_t2: res.data.RheumatologyPatientExamination.ip1b_t2,
            ip1b_s2: res.data.RheumatologyPatientExamination.ip1b_s2,


            ip2b_t1: res.data.RheumatologyPatientExamination.ip2b_t1,
            ip2b_s1: res.data.RheumatologyPatientExamination.ip2b_s1,
            ip2b_t2: res.data.RheumatologyPatientExamination.ip2b_t2,
            ip2b_s2: res.data.RheumatologyPatientExamination.ip2b_s2,


            ip3b_t1: res.data.RheumatologyPatientExamination.ip3b_t1,
            ip3b_s1: res.data.RheumatologyPatientExamination.ip3b_s1,
            ip3b_t2: res.data.RheumatologyPatientExamination.ip3b_t2,
            ip3b_s2: res.data.RheumatologyPatientExamination.ip3b_s2,

            ip4b_t1: res.data.RheumatologyPatientExamination.ip4b_t1,
            ip4b_s1: res.data.RheumatologyPatientExamination.ip4b_s1,
            ip4b_t2: res.data.RheumatologyPatientExamination.ip4b_t2,
            ip4b_s2: res.data.RheumatologyPatientExamination.ip4b_s2,


            ip5b_t1: res.data.RheumatologyPatientExamination.ip5b_t1,
            ip5b_s1: res.data.RheumatologyPatientExamination.ip5b_s1,
            ip5b_t2: res.data.RheumatologyPatientExamination.ip5b_t2,
            ip5b_s2: res.data.RheumatologyPatientExamination.ip5b_s2,


            s1_t1: res.data.RheumatologyPatientExamination.s1_t1,
            s1_s1: res.data.RheumatologyPatientExamination.s1_s1,
            s1_t2: res.data.RheumatologyPatientExamination.s1_t2,
            s1_s2: res.data.RheumatologyPatientExamination.s1_s2,


            cervical_anky: res.data.RheumatologyPatientExamination.cervical_anky,
            cervical_flex: res.data.RheumatologyPatientExamination.cervical_flex,
            cervical_ext: res.data.RheumatologyPatientExamination.cervical_ext,
            cervical_rtrot: res.data.RheumatologyPatientExamination.cervical_rtrot,

            cervical_ltrot: res.data.RheumatologyPatientExamination.cervical_ltrot,
            cervical_rtfl: res.data.RheumatologyPatientExamination.cervical_rtfl,
            cervical_ltfl: res.data.RheumatologyPatientExamination.cervical_ltfl,
            cervical_pt: res.data.RheumatologyPatientExamination.cervical_pt,


            thorasic_anky: res.data.RheumatologyPatientExamination.thorasic_anky,
            thorasic_flex: res.data.RheumatologyPatientExamination.thorasic_flex,
            thorasic_ext: res.data.RheumatologyPatientExamination.thorasic_ext,
            thorasic_rtrot: res.data.RheumatologyPatientExamination.thorasic_rtrot,

            thorasic_ltrot: res.data.RheumatologyPatientExamination.thorasic_ltrot,
            thorasic_rtfl: res.data.RheumatologyPatientExamination.thorasic_rtfl,
            thorasic_ltfl: res.data.RheumatologyPatientExamination.thorasic_ltfl,
            thorasic_pt: res.data.RheumatologyPatientExamination.thorasic_pt,





            lumbar_anky: res.data.RheumatologyPatientExamination.lumbar_anky,
            lumbar_flex: res.data.RheumatologyPatientExamination.lumbar_flex,
            lumbar_ext: res.data.RheumatologyPatientExamination.lumbar_ext,
            lumbar_rtrot: res.data.RheumatologyPatientExamination.lumbar_rtrot,
            lumbar_ltrot: res.data.RheumatologyPatientExamination.lumbar_ltrot,



            lumbar_rtfl: res.data.RheumatologyPatientExamination.lumbar_rtfl,
            lumbar_ltfl: res.data.RheumatologyPatientExamination.lumbar_ltfl,
            lumbar_pt: res.data.RheumatologyPatientExamination.lumbar_pt,


            opt_rt: res.data.RheumatologyPatientExamination.opt_rt,
            opt_lt: res.data.RheumatologyPatientExamination.opt_lt,

            lcer_rt: res.data.RheumatologyPatientExamination.lcer_rt,
            lcer_lt: res.data.RheumatologyPatientExamination.lcer_lt,
            trpz_rt: res.data.RheumatologyPatientExamination.trpz_rt,


            trpz_lt: res.data.RheumatologyPatientExamination.trpz_lt,
            scap_rt: res.data.RheumatologyPatientExamination.scap_rt,
            scap_lt: res.data.RheumatologyPatientExamination.scap_lt,

            zcst_rt: res.data.RheumatologyPatientExamination.zcst_rt,
            zcst_lt: res.data.RheumatologyPatientExamination.zcst_lt,
            epdl_rt: res.data.RheumatologyPatientExamination.epdl_rt,


            epdl_lt: res.data.RheumatologyPatientExamination.epdl_lt,
            glut_rt: res.data.RheumatologyPatientExamination.glut_rt,
            glut_lt: res.data.RheumatologyPatientExamination.glut_lt,


            trcr_rt: res.data.RheumatologyPatientExamination.trcr_rt,
            trcr_lt: res.data.RheumatologyPatientExamination.trcr_lt,
            knee_rt: res.data.RheumatologyPatientExamination.knee_rt,




            knee_lt: res.data.RheumatologyPatientExamination.knee_lt,
            ta_rt: res.data.RheumatologyPatientExamination.ta_rt,
            ta_lt: res.data.RheumatologyPatientExamination.ta_lt,


            calf_rt: res.data.RheumatologyPatientExamination.calf_rt,
            calf_lt: res.data.RheumatologyPatientExamination.calf_lt,
            sole_rt: res.data.RheumatologyPatientExamination.sole_rt,


            sole_lt: res.data.RheumatologyPatientExamination.sole_lt,


            rhand_fl: res.data.RheumatologyPatientExamination.rhand_fl,
            rhand_ex: res.data.RheumatologyPatientExamination.rhand_ex,
            rhand_ab: res.data.RheumatologyPatientExamination.rhand_ab,
            rhand_add: res.data.RheumatologyPatientExamination.rhand_add,
            rhand_slx: res.data.RheumatologyPatientExamination.rhand_slx,



            lhand_fl: res.data.RheumatologyPatientExamination.lhand_fl,
            lhand_ex: res.data.RheumatologyPatientExamination.lhand_ex,
            lhand_ab: res.data.RheumatologyPatientExamination.lhand_ab,
            lhand_add: res.data.RheumatologyPatientExamination.lhand_add,
            lhand_slx: res.data.RheumatologyPatientExamination.lhand_slx,




            rwrist_fl: res.data.RheumatologyPatientExamination.rwrist_fl,
            rwrist_ex: res.data.RheumatologyPatientExamination.rwrist_ex,
            rwrist_ab: res.data.RheumatologyPatientExamination.rwrist_ab,
            rwrist_add: res.data.RheumatologyPatientExamination.rwrist_add,
            rwrist_slx: res.data.RheumatologyPatientExamination.rwrist_slx,




            lwrist_fl: res.data.RheumatologyPatientExamination.lwrist_fl,
            lwrist_ex: res.data.RheumatologyPatientExamination.lwrist_ex,
            lwrist_ab: res.data.RheumatologyPatientExamination.lwrist_ab,
            lwrist_add: res.data.RheumatologyPatientExamination.lwrist_add,
            lwrist_slx: res.data.RheumatologyPatientExamination.lwrist_slx,



            relb_fl: res.data.RheumatologyPatientExamination.relb_fl,
            relb_ex: res.data.RheumatologyPatientExamination.relb_ex,
            relb_ab: res.data.RheumatologyPatientExamination.relb_ab,
            relb_add: res.data.RheumatologyPatientExamination.relb_add,
            relb_slx: res.data.RheumatologyPatientExamination.relb_slx,


            lelb_fl: res.data.RheumatologyPatientExamination.lelb_fl,
            lelb_ex: res.data.RheumatologyPatientExamination.lelb_ex,
            lelb_ab: res.data.RheumatologyPatientExamination.lelb_ab,
            lelb_add: res.data.RheumatologyPatientExamination.lelb_add,
            lelb_slx: res.data.RheumatologyPatientExamination.lelb_slx,



            shrt_fl: res.data.RheumatologyPatientExamination.shrt_fl,
            shrt_ex: res.data.RheumatologyPatientExamination.shrt_ex,
            shrt_ab: res.data.RheumatologyPatientExamination.shrt_ab,
            shrt_add: res.data.RheumatologyPatientExamination.shrt_add,
            shrt_slx: res.data.RheumatologyPatientExamination.shrt_slx,


            shlt_fl: res.data.RheumatologyPatientExamination.shlt_fl,
            shlt_ex: res.data.RheumatologyPatientExamination.shlt_ex,
            shlt_ab: res.data.RheumatologyPatientExamination.shlt_ab,
            shlt_add: res.data.RheumatologyPatientExamination.shlt_add,
            shlt_slx: res.data.RheumatologyPatientExamination.shlt_slx,



            kneert_fl: res.data.RheumatologyPatientExamination.kneert_fl,
            kneert_ex: res.data.RheumatologyPatientExamination.kneert_ex,
            kneert_ab: res.data.RheumatologyPatientExamination.kneert_ab,
            kneert_add: res.data.RheumatologyPatientExamination.kneert_add,
            kneert_slx: res.data.RheumatologyPatientExamination.kneert_slx,





            kneelt_fl: res.data.RheumatologyPatientExamination.kneelt_fl,
            kneelt_ex: res.data.RheumatologyPatientExamination.kneelt_ex,
            kneelt_ab: res.data.RheumatologyPatientExamination.kneelt_ab,
            kneelt_add: res.data.RheumatologyPatientExamination.kneelt_add,
            kneelt_slx: res.data.RheumatologyPatientExamination.kneelt_slx,


            footrt_fl: res.data.RheumatologyPatientExamination.footrt_fl,
            footrt_ex: res.data.RheumatologyPatientExamination.footrt_ex,
            footrt_ab: res.data.RheumatologyPatientExamination.footrt_ab,
            footrt_add: res.data.RheumatologyPatientExamination.footrt_add,
            footrt_slx: res.data.RheumatologyPatientExamination.footrt_slx,



            footlt_fl: res.data.RheumatologyPatientExamination.footlt_fl,
            footlt_ex: res.data.RheumatologyPatientExamination.footlt_ex,
            footlt_ab: res.data.RheumatologyPatientExamination.footlt_ab,
            footlt_add: res.data.RheumatologyPatientExamination.footlt_add,
            footlt_slx: res.data.RheumatologyPatientExamination.footlt_slx,



            thumb_rt: res.data.RheumatologyPatientExamination.thumb_rt,
            thumb_lt: res.data.RheumatologyPatientExamination.thumb_lt,
            finger_rt: res.data.RheumatologyPatientExamination.finger_rt,


            finger_lt: res.data.RheumatologyPatientExamination.finger_lt,
            palm_rt: res.data.RheumatologyPatientExamination.palm_rt,
            palm_lt: res.data.RheumatologyPatientExamination.palm_lt,




            elbow_rt: res.data.RheumatologyPatientExamination.elbow_rt,
            elbow_lt: res.data.RheumatologyPatientExamination.elbow_lt,
            hy_knee_rt: res.data.RheumatologyPatientExamination.hy_knee_rt,



            hy_knee_lt: res.data.RheumatologyPatientExamination.hy_knee_lt,
            ankle_rt: res.data.RheumatologyPatientExamination.ankle_rt,
            ankle_lt: res.data.RheumatologyPatientExamination.ankle_lt,


            other_rt: res.data.RheumatologyPatientExamination.other_rt,
            other_lt: res.data.RheumatologyPatientExamination.other_lt,
            spine_rt: res.data.RheumatologyPatientExamination.spine_rt,







            spine_lt: res.data.RheumatologyPatientExamination.spine_lt,



          })
        } else {
          this.setState({ pid: 0 })
        }
        
     
      })
  }




  submit = e => {

    e.preventDefault()
    axios.post(`https://abcapi.vidaria.in/addactivity?X-AUTH=abc123`, {

      activity: "Updated Examination details of Rheumatology  patient",
      user_id: this.state.user_id,

    })

    axios.put(`https://abcapi.vidaria.in/updaterheumapatientexamination?X-AUTH=abc123`, {


      pid: this.state.pid,



      user_id: this.state.user_id,

      general_condition: this.state.general_condition,
      weight: this.state.weight,
      height: this.state.height,
      bmi: this.state.bmi,
      bp: this.state.bp,
      pulse: this.state.pulse,
      temp: this.state.temp,
      respiratory_rate: this.state.respiratory_rate,
      ex_health_condition: this.state.ex_health_condition,
      ex_remarks: this.state.ex_remarks,
      eyes: this.state.eyes,
      skin_morphology: this.state.skin_morphology,



      skin_pattern: this.state.skin_pattern,
      skin_color: this.state.skin_color,
      distribution: this.state.distribution,
      other_affects: this.state.other_affects,
      mucosa: this.state.mucosa,



      hair_loss: this.state.hair_loss,
      dandruff: this.state.dandruff,
      nail: this.state.nail,
      lymphnodes: this.state.lymphnodes,
      lymphnodes_number: this.state.lymphnodes_number,




      tender: this.state.tender,
      tender_type: this.state.tender_type,
      lymphnodes_tender_others: this.state.lymphnodes_tender_others,
      vascular: this.state.vascular,
      cns_examination: this.state.cns_examination,
      cvs_examination: this.state.cvs_examination,
      chest_examination: this.state.chest_examination,
      abdomen_examination: this.state.abdomen_examination,


      tm_t1: this.state.tm_t1,
      tm_s1: this.state.tm_s1,
      tm_t2: this.state.tm_t2,
      tm_s2: this.state.tm_s2,

      scl_t1: this.state.scl_t1,
      scl_s1: this.state.scl_s1,
      scl_t2: this.state.scl_t2,
      scl_s2: this.state.scl_s2,



      acl_t1: this.state.acl_t1,
      acl_s1: this.state.acl_s1,
      acl_t2: this.state.acl_t2,
      acl_s2: this.state.acl_s2,


      sh_t1: this.state.sh_t1,
      sh_s1: this.state.sh_s1,
      sh_t2: this.state.sh_t2,
      sh_s2: this.state.sh_s2,

      elbow_t1: this.state.elbow_t1,
      elbow_s1: this.state.elbow_s1,
      elbow_t2: this.state.elbow_t2,
      elbow_s2: this.state.elbow_s2,


      infru_t1: this.state.infru_t1,
      infru_s1: this.state.infru_s1,
      infru_t2: this.state.infru_t2,
      infru_s2: this.state.infru_s2,


      cmc1_t1: this.state.cmc1_t1,
      cmc1_s1: this.state.cmc1_s1,
      cmc1_t2: this.state.cmc1_t2,
      cmc1_s2: this.state.cmc1_s2,



      wrist_t1: this.state.wrist_t1,
      wrist_s1: this.state.wrist_s1,
      wrist_t2: this.state.wrist_t2,
      wrist_s2: this.state.wrist_s2,


      dip2_t1: this.state.dip2_t1,
      dip2_s1: this.state.dip2_s1,
      dip2_t2: this.state.dip2_t2,
      dip2_s2: this.state.dip2_s2,


      dip3_t1: this.state.dip3_t1,
      dip3_s1: this.state.dip3_s1,
      dip3_t2: this.state.dip3_t2,
      dip3_s2: this.state.dip3_s2,


      dip4_t1: this.state.dip4_t1,
      dip4_s1: this.state.dip4_s1,
      dip4_t2: this.state.dip4_t2,
      dip4_s2: this.state.dip4_s2,



      dip5_t1: this.state.dip5_t1,
      dip5_s1: this.state.dip5_s1,
      dip5_t2: this.state.dip5_t2,
      dip5_s2: this.state.dip5_s2,


      ip1_t1: this.state.ip1_t1,
      ip1_s1: this.state.ip1_s1,
      ip1_t2: this.state.ip1_t2,
      ip1_s2: this.state.ip1_s2,


      ip2_t1: this.state.ip2_t1,
      ip2_s1: this.state.ip2_s1,
      ip2_t2: this.state.ip2_t2,
      ip2_s2: this.state.ip2_s2,


      pip3_t1: this.state.pip3_t1,
      pip3_s1: this.state.pip3_s1,
      pip3_t2: this.state.pip3_t2,
      pip3_s2: this.state.pip3_s2,


      pip4_t1: this.state.pip4_t1,
      pip4_s1: this.state.pip4_s1,
      pip4_t2: this.state.pip4_t2,
      pip4_s2: this.state.pip4_s2,

      pip5_t1: this.state.pip5_t1,
      pip5_s1: this.state.pip5_s1,
      pip5_t2: this.state.pip5_t2,
      pip5_s2: this.state.pip5_s2,

      mcp1_t1: this.state.mcp1_t1,
      mcp1_s1: this.state.mcp1_s1,
      mcp1_t2: this.state.mcp1_t2,
      mcp1_s2: this.state.mcp1_s2,


      mcp2_t1: this.state.mcp2_t1,
      mcp2_s1: this.state.mcp2_s1,
      mcp2_t2: this.state.mcp2_t2,
      mcp2_s2: this.state.mcp2_s2,

      mcp3_t1: this.state.mcp3_t1,
      mcp3_s1: this.state.mcp3_s1,
      mcp3_t2: this.state.mcp3_t2,
      mcp3_s2: this.state.mcp3_s2,


      mcp4_t1: this.state.mcp4_t1,
      mcp4_s1: this.state.mcp4_s1,
      mcp4_t2: this.state.mcp4_t2,
      mcp4_s2: this.state.mcp4_s2,

      mcp5_t1: this.state.mcp5_t1,
      mcp5_s1: this.state.mcp5_s1,
      mcp5_t2: this.state.mcp5_t2,
      mcp5_s2: this.state.mcp5_s2,

      hip_t1: this.state.hip_t1,
      hip_s1: this.state.hip_s1,
      hip_t2: this.state.hip_t2,
      hip_s2: this.state.hip_s2,


      knee_t1: this.state.knee_t1,
      knee_s1: this.state.knee_s1,
      knee_t2: this.state.knee_t2,
      knee_s2: this.state.knee_s2,


      a_tt_t1: this.state.a_tt_t1,
      a_tt_s1: this.state.a_tt_s1,
      a_tt_t2: this.state.a_tt_t2,
      a_tt_s2: this.state.a_tt_s2,

      a_tc_t1: this.state.a_tc_t1,
      a_tc_s1: this.state.a_tc_s1,
      a_tc_t2: this.state.a_tc_t2,
      a_tc_s2: this.state.a_tc_s2,

      mtl_t1: this.state.mtl_t1,
      mtl_s1: this.state.mtl_s1,
      mtl_t2: this.state.mtl_t2,
      mtl_s2: this.state.mtl_s2,


      mtp1_t1: this.state.mtp1_t1,
      mtp1_s1: this.state.mtp1_s1,
      mtp1_t2: this.state.mtp1_t2,
      mtp1_s2: this.state.mtp1_s2,


      mtp2_t1: this.state.mtp2_t1,
      mtp2_s1: this.state.mtp2_s1,
      mtp2_t2: this.state.mtp2_t2,
      mtp2_s2: this.state.mtp2_s2,


      mtp3_t1: this.state.mtp3_t1,
      mtp3_s1: this.state.mtp3_s1,
      mtp3_t2: this.state.mtp3_t2,
      mtp3_s2: this.state.mtp3_s2,


      mtp4_t1: this.state.mtp4_t1,
      mtp4_s1: this.state.mtp4_s1,
      mtp4_t2: this.state.mtp4_t2,
      mtp4_s2: this.state.mtp4_s2,


      mtp5_t1: this.state.mtp5_t1,
      mtp5_s1: this.state.mtp5_s1,
      mtp5_t2: this.state.mtp5_t2,
      mtp5_s2: this.state.mtp5_s2,


      ip1b_t1: this.state.ip1b_t1,
      ip1b_s1: this.state.ip1b_s1,
      ip1b_t2: this.state.ip1b_t2,
      ip1b_s2: this.state.ip1b_s2,


      ip2b_t1: this.state.ip2b_t1,
      ip2b_s1: this.state.ip2b_s1,
      ip2b_t2: this.state.ip2b_t2,
      ip2b_s2: this.state.ip2b_s2,


      ip3b_t1: this.state.ip3b_t1,
      ip3b_s1: this.state.ip3b_s1,
      ip3b_t2: this.state.ip3b_t2,
      ip3b_s2: this.state.ip3b_s2,

      ip4b_t1: this.state.ip4b_t1,
      ip4b_s1: this.state.ip4b_s1,
      ip4b_t2: this.state.ip4b_t2,
      ip4b_s2: this.state.ip4b_s2,


      ip5b_t1: this.state.ip5b_t1,
      ip5b_s1: this.state.ip5b_s1,
      ip5b_t2: this.state.ip5b_t2,
      ip5b_s2: this.state.ip5b_s2,


      s1_t1: this.state.s1_t1,
      s1_s1: this.state.s1_s1,
      s1_t2: this.state.s1_t2,
      s1_s2: this.state.s1_s2,


      cervical_anky: this.state.cervical_anky,
      cervical_flex: this.state.cervical_flex,
      cervical_ext: this.state.cervical_ext,
      cervical_rtrot: this.state.cervical_rtrot,

      cervical_ltrot: this.state.cervical_ltrot,
      cervical_rtfl: this.state.cervical_rtfl,
      cervical_ltfl: this.state.cervical_ltfl,
      cervical_pt: this.state.cervical_pt,


      thorasic_anky: this.state.thorasic_anky,
      thorasic_flex: this.state.thorasic_flex,
      thorasic_ext: this.state.thorasic_ext,
      thorasic_rtrot: this.state.thorasic_rtrot,

      thorasic_ltrot: this.state.thorasic_ltrot,
      thorasic_rtfl: this.state.thorasic_rtfl,
      thorasic_ltfl: this.state.thorasic_ltfl,
      thorasic_pt: this.state.thorasic_pt,





      lumbar_anky: this.state.lumbar_anky,
      lumbar_flex: this.state.lumbar_flex,
      lumbar_ext: this.state.lumbar_ext,
      lumbar_rtrot: this.state.lumbar_rtrot,
      lumbar_ltrot: this.state.lumbar_ltrot,



      lumbar_rtfl: this.state.lumbar_rtfl,
      lumbar_ltfl: this.state.lumbar_ltfl,
      lumbar_pt: this.state.lumbar_pt,


      opt_rt: this.state.opt_rt,
      opt_lt: this.state.opt_lt,

      lcer_rt: this.state.lcer_rt,
      lcer_lt: this.state.lcer_lt,
      trpz_rt: this.state.trpz_rt,


      trpz_lt: this.state.trpz_lt,
      scap_rt: this.state.scap_rt,
      scap_lt: this.state.scap_lt,

      zcst_rt: this.state.zcst_rt,
      zcst_lt: this.state.zcst_lt,
      epdl_rt: this.state.epdl_rt,


      epdl_lt: this.state.epdl_lt,
      glut_rt: this.state.glut_rt,
      glut_lt: this.state.glut_lt,


      trcr_rt: this.state.trcr_rt,
      trcr_lt: this.state.trcr_lt,
      knee_rt: this.state.knee_rt,




      knee_lt: this.state.knee_lt,
      ta_rt: this.state.ta_rt,
      ta_lt: this.state.ta_lt,


      calf_rt: this.state.calf_rt,
      calf_lt: this.state.calf_lt,
      sole_rt: this.state.sole_rt,


      sole_lt: this.state.sole_lt,


      rhand_fl: this.state.rhand_fl,
      rhand_ex: this.state.rhand_ex,
      rhand_ab: this.state.rhand_ab,
      rhand_add: this.state.rhand_add,
      rhand_slx: this.state.rhand_slx,



      lhand_fl: this.state.lhand_fl,
      lhand_ex: this.state.lhand_ex,
      lhand_ab: this.state.lhand_ab,
      lhand_add: this.state.lhand_add,
      lhand_slx: this.state.lhand_slx,




      rwrist_fl: this.state.rwrist_fl,
      rwrist_ex: this.state.rwrist_ex,
      rwrist_ab: this.state.rwrist_ab,
      rwrist_add: this.state.rwrist_add,
      rwrist_slx: this.state.rwrist_slx,




      lwrist_fl: this.state.lwrist_fl,
      lwrist_ex: this.state.lwrist_ex,
      lwrist_ab: this.state.lwrist_ab,
      lwrist_add: this.state.lwrist_add,
      lwrist_slx: this.state.lwrist_slx,



      relb_fl: this.state.relb_fl,
      relb_ex: this.state.relb_ex,
      relb_ab: this.state.relb_ab,
      relb_add: this.state.relb_add,
      relb_slx: this.state.relb_slx,

      lelb_fl: this.state.lelb_fl,
      lelb_ex: this.state.lelb_ex,
      lelb_ab: this.state.lelb_ab,
      lelb_add: this.state.lelb_add,
      lelb_slx: this.state.lelb_slx,



      shrt_fl: this.state.shrt_fl,
      shrt_ex: this.state.shrt_ex,
      shrt_ab: this.state.shrt_ab,
      shrt_add: this.state.shrt_add,
      shrt_slx: this.state.shrt_slx,


      shlt_fl: this.state.shlt_fl,
      shlt_ex: this.state.shlt_ex,
      shlt_ab: this.state.shlt_ab,
      shlt_add: this.state.shlt_add,
      shlt_slx: this.state.shlt_slx,



      kneert_fl: this.state.kneert_fl,
      kneert_ex: this.state.kneert_ex,
      kneert_ab: this.state.kneert_ab,
      kneert_add: this.state.kneert_add,
      kneert_slx: this.state.kneert_slx,





      kneelt_fl: this.state.kneelt_fl,
      kneelt_ex: this.state.kneelt_ex,
      kneelt_ab: this.state.kneelt_ab,
      kneelt_add: this.state.kneelt_add,
      kneelt_slx: this.state.kneelt_slx,


      footrt_fl: this.state.footrt_fl,
      footrt_ex: this.state.footrt_ex,
      footrt_ab: this.state.footrt_ab,
      footrt_add: this.state.footrt_add,
      footrt_slx: this.state.footrt_slx,



      footlt_fl: this.state.footlt_fl,
      footlt_ex: this.state.footlt_ex,
      footlt_ab: this.state.footlt_ab,
      footlt_add: this.state.footlt_add,
      footlt_slx: this.state.footlt_slx,



      thumb_rt: this.state.thumb_rt,
      thumb_lt: this.state.thumb_lt,
      finger_rt: this.state.finger_rt,


      finger_lt: this.state.finger_lt,
      palm_rt: this.state.palm_rt,
      palm_lt: this.state.palm_lt,




      elbow_rt: this.state.elbow_rt,
      elbow_lt: this.state.elbow_lt,
      hy_knee_rt: this.state.hy_knee_rt,



      hy_knee_lt: this.state.hy_knee_lt,
      ankle_rt: this.state.ankle_rt,
      ankle_lt: this.state.ankle_lt,


      other_rt: this.state.other_rt,
      other_lt: this.state.other_lt,
      spine_rt: this.state.spine_rt,







      spine_lt: this.state.spine_lt,

    }).then((res) => {
      console.log(res.data.success)
      if(res.data.success == "false"){
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: res.data.message,
          
        })
      }else{
     this.opensweetalert()
      }
        // this.opensweetalert()
        // this.componentDidMount();

      })

  }


  // delete(id){
  //   axios.delete(`https://abcapi.vidaria.in/userdelete/${id}`)
  //   .then(()=>{
  //     this.componentDidMount();
  //   })
  // }
  // edit(id){
  //   axios.get(`https://abcapi.vidaria.in/userdetails/1`)
  //   .then((res)=>{
  //     console.log(res.data.user);
  //     this.setState({
  //       id:res.data.user.id,
  //       username:res.data.user.username,
  //       email:res.data.user.email,
  //       phone:res.data.user.phone,
  //       password:res.data.user.password,
  //       role:res.data.user.role,
  //       speciality:res.data.user.speciality,
  //     })
  //   })
  // }





  render() {
    const p = this.state.pid;
    console.log("render ",p)

    return (
      <>

          <>
            {this.state.pid == 0 ?
            <h1 className="text-center">{this.norecordsweetalert()}</h1>
              
              : 
              <Container fluid>

                <Row>
                  <Col md="8">
                    <Card>
                      <Card.Header>
                        <Card.Title as="h4">Rheumatology Examination</Card.Title>
                      </Card.Header>
                      <Card.Body>
                        <Form onSubmit={this.submit}>


                          <Row>
                            <Col md="6">

                              <Form.Group>
                                <Form.Label>general_condition</Form.Label><br></br>
                                <Form.Control
                                  placeholder="general condition"
                                  type="text"
                                  onChange={(e) => this.setState({ general_condition: e.target.value })}
                                  value={this.state.general_condition}
                                  placeholder="general condition"
                                />
                              </Form.Group>


                              {/* <Form.Group>
      <Form.Label>Patient Id</Form.Label><br></br>
        <Form.Control
         type="number"
         readOnly
         name="pid"                          
         placeholder="patient id"
         value={this.state.pid}
         onChange={(e)=>this.setState({pid:e.target.value})}></Form.Control>
        </Form.Group>



        <Form.Group>
        <Form.Label>User Id</Form.Label><br></br>
        <Form.Control 
        type="number"  
        readOnly
        name="user_id"
        placeholder="user id"
        value={this.state.user_id}
        onChange={(e)=>this.setState({user_id:e.target.value})}  />
        </Form.Group> */}
                            </Col>


                            <Col md="6">
                              {/* <Form.Group>
      <Form.Label>general_condition</Form.Label><br></br>
        <Form.Control 
        placeholder="general condition"
        type="text" 
        onChange={(e)=>this.setState({general_condition:e.target.value})} 						
        value={this.state.general_condition}
        placeholder="general condition"
        />
        </Form.Group> */}

                              <Form.Group>
                                <Form.Label>weight</Form.Label><br></br>
                                <Form.Control
                                  type="number"
                                  onChange={(e) => this.setState({ weight: e.target.value })}
                                  value={this.state.weight}
                                  placeholder="weight"
                                />
                              </Form.Group>
                            </Col>
                          </Row>


                          <Row>
                            <Col md="6">
                              <Form.Group>
                                <Form.Label>height</Form.Label><br></br>
                                <Form.Control
                                  type="number"
                                  onChange={(e) => this.setState({ height: e.target.value })}
                                  value={this.state.height}
                                  placeholder="height"
                                />
                              </Form.Group>


                              <Form.Group>
                                <Form.Label>bmi</Form.Label><br></br>
                                <Form.Control
                                  type="text"
                                  onChange={(e) => this.setState({ bmi: e.target.value })}
                                  value={this.state.bmi}
                                  placeholder="bmi"
                                />
                              </Form.Group>
                            </Col>
                            <Col md="6">

                              <Form.Group>
                                <Form.Label>bp</Form.Label><br></br>
                                <Form.Control
                                  type="number"
                                  onChange={(e) => this.setState({ bp: e.target.value })}
                                  value={this.state.bp}
                                  placeholder="bp" />
                              </Form.Group>

                              <Form.Group>
                                <Form.Label>pulse</Form.Label><br></br>
                                <Form.Control
                                  type="number"
                                  onChange={(e) => this.setState({ pulse: e.target.value })}
                                  value={this.state.pulse}
                                  placeholder="pulse"
                                />
                              </Form.Group>

                            </Col>
                          </Row>
                          <Row>
                            <Col md="6">
                              <Form.Group>
                                <Form.Label>temp</Form.Label>
                                <Form.Control
                                  type="text"
                                  name="temp"
                                  placeholder="temp"
                                  value={this.state.temp}
                                  onChange={(e) => this.setState({ temp: e.target.value })}
                                />

                              </Form.Group>
                            </Col>
                            <Col md="6">
                              <Form.Group>
                                <Form.Label>respiratory_rate</Form.Label>
                                <Form.Control
                                  type="number"
                                  name="respiratory_rate"
                                  placeholder="respiratory_rate"
                                  value={this.state.respiratory_rate}
                                  onChange={(e) => this.setState({ respiratory_rate: e.target.value })}
                                />
                              </Form.Group>

                            </Col>

                          </Row>

                          <Row>
                            <Col md="6">
                              <Form.Group>

                                <Form.Label>examination_health_condition</Form.Label><br></br>
                                <Form.Control
                                  type="text"


                                  name="ex_health_condition"
                                  placeholder="examination_health_condition"
                                  value={this.state.ex_health_condition}
                                  onChange={(e) => this.setState({ ex_health_condition: e.target.value })}
                                />
                              </Form.Group>


                              <Form.Group>

                                <Form.Label>examination_remarks</Form.Label><br></br>
                                <Form.Control
                                  type="text"
                                  name="ex_remarks"
                                  placeholder="examination_remarks"
                                  value={this.state.ex_remarks}
                                  onChange={(e) => this.setState({ ex_remarks: e.target.value })}
                                />
                              </Form.Group>
                            </Col>



                            <Col md="6">
                              <Form.Group>

                                <Form.Label>eyes</Form.Label><br></br>
                                <Form.Control
                                  type="text"
                                  name="eyes"
                                  placeholder="eyes"
                                  value={this.state.eyes}
                                  onChange={(e) => this.setState({ eyes: e.target.value })}
                                />
                              </Form.Group>



                              <Form.Group>

                                <Form.Label>skin_morphology</Form.Label><br></br>
                                <Form.Control
                                  type="text"
                                  name="skin_morphology"
                                  placeholder="skin_morphology"
                                  value={this.state.skin_morphology}
                                  onChange={(e) => this.setState({ skin_morphology: e.target.value })}
                                />
                              </Form.Group>
                            </Col>
                          </Row>




                          <Row>
                            <Col md="6">
                              <Form.Group>

                                <Form.Label>skin_pattern</Form.Label><br></br>
                                <Form.Control
                                  type="text"
                                  name="skin_pattern"
                                  placeholder="skin_pattern"
                                  value={this.state.skin_pattern}
                                  onChange={(e) => this.setState({ skin_pattern: e.target.value })}

                                />
                              </Form.Group>


                              <Form.Group>


                                <Form.Label>skin_color</Form.Label><br></br>
                                <Form.Control
                                  type="text"
                                  name="skin_color"
                                  placeholder="skin_color"
                                  value={this.state.skin_color}
                                  onChange={(e) => this.setState({ skin_color: e.target.value })}

                                />
                              </Form.Group>

                            </Col>
                            <Col md="6">
                              <Form.Group>

                                <Form.Label>distribution</Form.Label><br></br>
                                <Form.Control
                                  type="text"
                                  name="distribution"
                                  placeholder="distribution"
                                  value={this.state.distribution}
                                  onChange={(e) => this.setState({ distribution: e.target.value })}

                                />
                              </Form.Group>

                              <Form.Group>

                                <Form.Label>other_affects</Form.Label><br></br>
                                <Form.Control
                                  type="text"
                                  name="other_affects"
                                  placeholder="other_affects"
                                  value={this.state.other_affects}
                                  onChange={(e) => this.setState({ other_affects: e.target.value })}

                                />
                              </Form.Group>
                            </Col>
                          </Row>

                          <Row>
                            <Col md="6">
                              <Form.Group>

                                <Form.Label>mucosa</Form.Label><br></br>
                                <Form.Control
                                  type="text"
                                  name="mucosa"
                                  placeholder="mucosa"
                                  value={this.state.mucosa}
                                  onChange={(e) => this.setState({ mucosa: e.target.value })}
                                />
                              </Form.Group>


                              <Form.Group>

                                <Form.Label>hair_loss</Form.Label><br></br>
                                <Form.Control
                                  type="text"
                                  name="hair_loss"
                                  placeholder="hair_loss"
                                  value={this.state.hair_loss}
                                  onChange={(e) => this.setState({ hair_loss: e.target.value })}
                                />
                              </Form.Group>

                            </Col>

                            <Col md="6">
                              <Form.Group>

                                <Form.Label>dandruff</Form.Label><br></br>
                                <Form.Control
                                  type="text"
                                  name="dandruff"
                                  placeholder="dandruff"
                                  value={this.state.dandruff}
                                  onChange={(e) => this.setState({ dandruff: e.target.value })}
                                />
                              </Form.Group>



                              <Form.Group>

                                <Form.Label>nail</Form.Label><br></br>
                                <Form.Control
                                  type="text"
                                  name="nail"
                                  placeholder="nail"
                                  value={this.state.nail}
                                  onChange={(e) => this.setState({ nail: e.target.value })}
                                />
                              </Form.Group>
                            </Col>
                          </Row>




                          <Row>

                            <Col md="6">
                              <Form.Group>

                                <Form.Label>lymphnodes</Form.Label><br></br>
                                <Form.Control
                                  type="text"
                                  name="lymphnodes"
                                  placeholder="lymphnodes"
                                  value={this.state.lymphnodes}
                                  onChange={(e) => this.setState({ lymphnodes: e.target.value })}

                                />
                              </Form.Group>


                              <Form.Group>

                                <Form.Label>lymphnodes_number</Form.Label><br></br>
                                <Form.Control
                                  type="text"

                                  name="lymphnodes_number"
                                  placeholder="lymphnodes_number"
                                  value={this.state.lymphnodes_number}
                                  onChange={(e) => this.setState({ lymphnodes_number: e.target.value })}
                                />
                              </Form.Group>

                            </Col>




                            <Col md="6">

                              <Form.Group>

                                <Form.Label>tender</Form.Label><br></br>
                                <Form.Control
                                  type="text"

                                  name="tender"
                                  placeholder="tender"
                                  value={this.state.tender}
                                  onChange={(e) => this.setState({ tender: e.target.value })}
                                />
                              </Form.Group>


                              <Form.Group>

                                <Form.Label>tender_type</Form.Label><br></br>
                                <Form.Control
                                  type="text"

                                  name="tender_type"
                                  placeholder="tender_type"
                                  value={this.state.tender_type}
                                  onChange={(e) => this.setState({ tender_type: e.target.value })}
                                />
                              </Form.Group>
                            </Col>
                          </Row>
















                          <Row>

                            <Col md="6">
                              <Form.Group>

                                <Form.Label>lymphnodes_tender_others</Form.Label><br></br>
                                <Form.Control
                                  type="text"
                                  name="lymphnodes_tender_others"
                                  placeholder="lymphnodes_tender_others"
                                  value={this.state.lymphnodes_tender_others}
                                  onChange={(e) => this.setState({ lymphnodes_tender_others: e.target.value })}

                                />
                              </Form.Group>


                              <Form.Group>

                                <Form.Label>vascular</Form.Label><br></br>
                                <Form.Control
                                  type="text"

                                  name="vascular"
                                  placeholder="vascular"
                                  value={this.state.vascular}
                                  onChange={(e) => this.setState({ vascular: e.target.value })}
                                />
                              </Form.Group>

                            </Col>




                            <Col md="6">

                              <Form.Group>

                                <Form.Label>cns_examination</Form.Label><br></br>
                                <Form.Control
                                  type="text"

                                  name="cns_examination"
                                  placeholder="cns_examination"
                                  value={this.state.cns_examination}
                                  onChange={(e) => this.setState({ cns_examination: e.target.value })}
                                />
                              </Form.Group>


                              <Form.Group>

                                <Form.Label>cvs_examination</Form.Label><br></br>
                                <Form.Control
                                  type="text"

                                  name="cvs_examination"
                                  placeholder="cvs_examination"
                                  value={this.state.cvs_examination}
                                  onChange={(e) => this.setState({ cvs_examination: e.target.value })}
                                />
                              </Form.Group>
                            </Col>
                          </Row>


                          <Row>
                            <Col md="6">
                              <Form.Group>

                                <Form.Label>chest_examination</Form.Label><br></br>
                                <Form.Control
                                  type="text"

                                  name="chest_examination"
                                  placeholder="chest_examination"
                                  value={this.state.chest_examination}
                                  onChange={(e) => this.setState({ chest_examination: e.target.value })}
                                />
                              </Form.Group>
                            </Col>
                            <Col md="6">
                              <Form.Group>

                                <Form.Label>abdomen_examination</Form.Label><br></br>
                                <Form.Control
                                  type="text"

                                  name="abdomen_examination"
                                  placeholder="abdomen_examination"
                                  value={this.state.abdomen_examination}
                                  onChange={(e) => this.setState({ abdomen_examination: e.target.value })}
                                />
                              </Form.Group>
                            </Col>
                          </Row>


                          <h2>Patient MUSCULOSKELETAL_EXAMINATIONS</h2>
                          <h5>joint_evaluation: Information</h5>

                          <Row>


                            <Table striped bordered hover>
                              <thead>
                                <tr>
                                  <th>T</th>
                                  <th>S</th>
                                  <th>RT LT</th>
                                  <th>T</th>
                                  <th>S</th>
                                  <th>T</th>
                                  <th>S</th>
                                  <th>RT  LT</th>
                                  <th>T</th>
                                  <th>S</th>

                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>
                                    <input
                                      type="text"
                                      name="tm_t1"
                                      placeholder="tm_t1"
                                      value={this.state.tm_t1}
                                      onChange={(e) => this.setState({ tm_t1: e.target.value })}
                                      maxlength="50" size="4"
                                    />

                                  </td>


                                  <td><input type="text"




                                    name="tm_s1"
                                    placeholder="tm_s1"
                                    value={this.state.tm_s1}
                                    onChange={(e) => this.setState({ tm_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>TM</td>
                                  <td><input type="text"




                                    name="tm_t2"
                                    placeholder="tm_t2"
                                    value={this.state.tm_t2}
                                    onChange={(e) => this.setState({ tm_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="tm_s2"
                                    placeholder="tm_s2"
                                    value={this.state.tm_s2}
                                    onChange={(e) => this.setState({ tm_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mcp3_t1"
                                    placeholder="mcp3_t1"
                                    value={this.state.mcp3_t1}
                                    onChange={(e) => this.setState({ mcp3_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mcp3_s1"
                                    placeholder="mcp3_s1"
                                    value={this.state.mcp3_s1}
                                    onChange={(e) => this.setState({ mcp3_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>MCP3</td>
                                  <td><input type="text"




                                    name="mcp3_t2"
                                    placeholder="mcp3_t2"
                                    value={this.state.mcp3_t2}
                                    onChange={(e) => this.setState({ mcp3_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mcp3_s2"
                                    placeholder="mcp3_s2"
                                    value={this.state.mcp3_s2}
                                    onChange={(e) => this.setState({ mcp3_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="scl_t1"
                                    placeholder="scl_t1"
                                    value={this.state.scl_t1}
                                    onChange={(e) => this.setState({ scl_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="scl_s1"
                                    placeholder="scl_s1"
                                    value={this.state.scl_s1}
                                    onChange={(e) => this.setState({ scl_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>SCL</td>
                                  <td><input type="text"




                                    name="scl_t2"
                                    placeholder="scl_t2"
                                    value={this.state.scl_t2}
                                    onChange={(e) => this.setState({ scl_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="scl_s2"
                                    placeholder="scl_s2"
                                    value={this.state.scl_s2}
                                    onChange={(e) => this.setState({ scl_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mcp4_t1"
                                    placeholder="mcp4_t1"
                                    value={this.state.mcp4_t1}
                                    onChange={(e) => this.setState({ mcp4_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mcp4_s1"
                                    placeholder="mcp4_s1"
                                    value={this.state.mcp4_s1}
                                    onChange={(e) => this.setState({ mcp4_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>MCP 4</td>
                                  <td><input type="text"




                                    name="mcp4_t2"
                                    placeholder="mcp4_t2"
                                    value={this.state.mcp4_t2}
                                    onChange={(e) => this.setState({ mcp4_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mcp4_s2"
                                    placeholder="mcp4_s2"
                                    value={this.state.mcp4_s2}
                                    onChange={(e) => this.setState({ mcp4_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>


                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="acl_t1"
                                    placeholder="acl_t1"
                                    value={this.state.acl_t1}
                                    onChange={(e) => this.setState({ acl_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="acl_s1"
                                    placeholder="acl_s1"
                                    value={this.state.acl_s1}
                                    onChange={(e) => this.setState({ acl_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>ACL</td>
                                  <td><input type="text"




                                    name="acl_t2"
                                    placeholder="acl_t2"
                                    value={this.state.acl_t2}
                                    onChange={(e) => this.setState({ acl_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="acl_s2"
                                    placeholder="acl_s2"
                                    value={this.state.acl_s2}
                                    onChange={(e) => this.setState({ acl_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mcp5_t1"
                                    placeholder="mcp5_t1"
                                    value={this.state.mcp5_t1}
                                    onChange={(e) => this.setState({ mcp5_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mcp5_s1"
                                    placeholder="mcp5_s1"
                                    value={this.state.mcp5_s1}
                                    onChange={(e) => this.setState({ mcp5_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>MCP 5</td>
                                  <td><input type="text"




                                    name="mcp5_t2"
                                    placeholder="mcp5_t2"
                                    value={this.state.mcp5_t2}
                                    onChange={(e) => this.setState({ mcp5_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mcp5_s2"
                                    placeholder="mcp5_s2"
                                    value={this.state.mcp5_s2}
                                    onChange={(e) => this.setState({ mcp5_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>

                                <tr>
                                  <td><input type="text"




                                    name="sh_t1"
                                    placeholder="sh_t1"
                                    value={this.state.sh_t1}
                                    onChange={(e) => this.setState({ sh_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="sh_s1"
                                    placeholder="sh_s1"
                                    value={this.state.sh_s1}
                                    onChange={(e) => this.setState({ sh_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>SH</td>
                                  <td><input type="text"




                                    name="sh_t2"
                                    placeholder="sh_t2"
                                    value={this.state.sh_t2}
                                    onChange={(e) => this.setState({ sh_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="sh_s2"
                                    placeholder="sh_s2"
                                    value={this.state.sh_s2}
                                    onChange={(e) => this.setState({ sh_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="hip_t1"
                                    placeholder="hip_t1"
                                    value={this.state.hip_t1}
                                    onChange={(e) => this.setState({ hip_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="hip_s1"
                                    placeholder="hip_s1"
                                    value={this.state.hip_s1}
                                    onChange={(e) => this.setState({ hip_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>HIP</td>
                                  <td><input type="text"




                                    name="hip_t2"
                                    placeholder="hip_t2"
                                    value={this.state.hip_t2}
                                    onChange={(e) => this.setState({ hip_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="hip_s2"
                                    placeholder="hip_s2"
                                    value={this.state.hip_s2}
                                    onChange={(e) => this.setState({ hip_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="elbow_t1"
                                    placeholder="elbow_t1"
                                    value={this.state.elbow_t1}
                                    onChange={(e) => this.setState({ elbow_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="elbow_s1"
                                    placeholder="elbow_s1"
                                    value={this.state.elbow_s1}
                                    onChange={(e) => this.setState({ elbow_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>ELBOW</td>
                                  <td><input type="text"




                                    name="elbow_t2"
                                    placeholder="elbow_t2"
                                    value={this.state.elbow_t2}
                                    onChange={(e) => this.setState({ elbow_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="elbow_s2"
                                    placeholder="elbow_s2"
                                    value={this.state.elbow_s2}
                                    onChange={(e) => this.setState({ elbow_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="knee_t1"
                                    placeholder="knee_t1"
                                    value={this.state.knee_t1}
                                    onChange={(e) => this.setState({ knee_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="knee_s1"
                                    placeholder="knee_s1"
                                    value={this.state.knee_s1}
                                    onChange={(e) => this.setState({ knee_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>KNEE</td>
                                  <td><input type="text"




                                    name="knee_t2"
                                    placeholder="knee_t2"
                                    value={this.state.knee_t2}
                                    onChange={(e) => this.setState({ knee_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="knee_s2"
                                    placeholder="knee_s2"
                                    value={this.state.knee_s2}
                                    onChange={(e) => this.setState({ knee_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="infru_t1"
                                    placeholder="infru_t1"
                                    value={this.state.infru_t1}
                                    onChange={(e) => this.setState({ infru_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="infru_s1"
                                    placeholder="infru_s1"
                                    value={this.state.infru_s1}
                                    onChange={(e) => this.setState({ infru_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>INF RU</td>
                                  <td><input type="text"




                                    name="infru_t2"
                                    placeholder="infru_t2"
                                    value={this.state.infru_t2}
                                    onChange={(e) => this.setState({ infru_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="infru_s2"
                                    placeholder="infru_s2"
                                    value={this.state.infru_s2}
                                    onChange={(e) => this.setState({ infru_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="a_tt_t1"
                                    placeholder="a_tt_t1"
                                    value={this.state.a_tt_t1}
                                    onChange={(e) => this.setState({ a_tt_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="a_tt_s1"
                                    placeholder="a_tt_s1"
                                    value={this.state.a_tt_s1}
                                    onChange={(e) => this.setState({ a_tt_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>A(TT)</td>
                                  <td><input type="text"




                                    name="a_tt_t2"
                                    placeholder="a_tt_t2"
                                    value={this.state.a_tt_t2}
                                    onChange={(e) => this.setState({ a_tt_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="a_tt_s2"
                                    placeholder="a_tt_s2"
                                    value={this.state.a_tt_s2}
                                    onChange={(e) => this.setState({ a_tt_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="cmc1_t1"
                                    placeholder="cmc1_t1"
                                    value={this.state.cmc1_t1}
                                    onChange={(e) => this.setState({ cmc1_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="cmc1_s1"
                                    placeholder="cmc1_s1"
                                    value={this.state.cmc1_s1}
                                    onChange={(e) => this.setState({ cmc1_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>CMC 1</td>
                                  <td><input type="text"




                                    name="cmc1_t2"
                                    placeholder="cmc1_t2"
                                    value={this.state.cmc1_t2}
                                    onChange={(e) => this.setState({ cmc1_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="cmc1_s2"
                                    placeholder="cmc1_s2"
                                    value={this.state.cmc1_s2}
                                    onChange={(e) => this.setState({ cmc1_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="a_tc_t1"
                                    placeholder="a_tc_t1"
                                    value={this.state.a_tc_t1}
                                    onChange={(e) => this.setState({ a_tc_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="a_tc_s1"
                                    placeholder="a_tc_s1"
                                    value={this.state.a_tc_s1}
                                    onChange={(e) => this.setState({ a_tc_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>A(TC)</td>
                                  <td><input type="text"




                                    name="a_tc_t2"
                                    placeholder="a_tc_t2"
                                    value={this.state.a_tc_t2}
                                    onChange={(e) => this.setState({ a_tc_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="a_tc_s2"
                                    placeholder="a_tc_s2"
                                    value={this.state.a_tc_s2}
                                    onChange={(e) => this.setState({ a_tc_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="wrist_t1"
                                    placeholder="wrist_t1"
                                    value={this.state.wrist_t1}
                                    onChange={(e) => this.setState({ wrist_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="wrist_s1"
                                    placeholder="wrist_s1"
                                    value={this.state.wrist_s1}
                                    onChange={(e) => this.setState({ wrist_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>WRIST</td>
                                  <td><input type="text"




                                    name="wrist_t2"
                                    placeholder="wrist_t2"
                                    value={this.state.wrist_t2}
                                    onChange={(e) => this.setState({ wrist_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="wrist_s2"
                                    placeholder="wrist_s2"
                                    value={this.state.wrist_s2}
                                    onChange={(e) => this.setState({ wrist_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtl_t1"
                                    placeholder="mtl_t1"
                                    value={this.state.mtl_t1}
                                    onChange={(e) => this.setState({ mtl_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtl_s1"
                                    placeholder="mtl_s1"
                                    value={this.state.mtl_s1}
                                    onChange={(e) => this.setState({ mtl_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>MTL</td>
                                  <td><input type="text"




                                    name="mtl_t2"
                                    placeholder="mtl_t2"
                                    value={this.state.mtl_t2}
                                    onChange={(e) => this.setState({ mtl_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtl_s2"
                                    placeholder="mtl_s2"
                                    value={this.state.mtl_s2}
                                    onChange={(e) => this.setState({ mtl_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="dip2_t1"
                                    placeholder="dip2_t1"
                                    value={this.state.dip2_t1}
                                    onChange={(e) => this.setState({ dip2_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="dip2_s1"
                                    placeholder="dip2_s1"
                                    value={this.state.dip2_s1}
                                    onChange={(e) => this.setState({ dip2_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>DIP2</td>
                                  <td><input type="text"




                                    name="dip2_t2"
                                    placeholder="dip2_t2"
                                    value={this.state.dip2_t2}
                                    onChange={(e) => this.setState({ dip2_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="dip2_s2"
                                    placeholder="dip2_s2"
                                    value={this.state.dip2_s2}
                                    onChange={(e) => this.setState({ dip2_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtp1_t1"
                                    placeholder="mtp1_t1"
                                    value={this.state.mtp1_t1}
                                    onChange={(e) => this.setState({ mtp1_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtp1_s1"
                                    placeholder="mtp1_s1"
                                    value={this.state.mtp1_s1}
                                    onChange={(e) => this.setState({ mtp1_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>MTP 1</td>
                                  <td><input type="text"




                                    name="mtp1_t2"
                                    placeholder="mtp1_t2"
                                    value={this.state.mtp1_t2}
                                    onChange={(e) => this.setState({ mtp1_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtp1_s2"
                                    placeholder="mtp1_s2"
                                    value={this.state.mtp1_s2}
                                    onChange={(e) => this.setState({ mtp1_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="dip3_t1"
                                    placeholder="dip3_t1"
                                    value={this.state.dip3_t1}
                                    onChange={(e) => this.setState({ dip3_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="dip3_s1"
                                    placeholder="dip3_s1"
                                    value={this.state.dip3_s1}
                                    onChange={(e) => this.setState({ dip3_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>DIP3</td>
                                  <td><input type="text"




                                    name="dip3_t2"
                                    placeholder="dip3_t2"
                                    value={this.state.dip3_t2}
                                    onChange={(e) => this.setState({ dip3_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="dip3_s2"
                                    placeholder="dip3_s2"
                                    value={this.state.dip3_s2}
                                    onChange={(e) => this.setState({ dip3_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtp2_t1"
                                    placeholder="mtp2_t1"
                                    value={this.state.mtp2_t1}
                                    onChange={(e) => this.setState({ mtp2_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtp2_s1"
                                    placeholder="mtp2_s1"
                                    value={this.state.mtp2_s1}
                                    onChange={(e) => this.setState({ mtp2_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>MTP 2</td>
                                  <td><input type="text"




                                    name="mtp2_t2"
                                    placeholder="mtp2_t2"
                                    value={this.state.mtp2_t2}
                                    onChange={(e) => this.setState({ mtp2_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtp2_s2"
                                    placeholder="mtp2_s2"
                                    value={this.state.mtp2_s2}
                                    onChange={(e) => this.setState({ mtp2_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="dip4_t1"
                                    placeholder="dip4_t1"
                                    value={this.state.dip4_t1}
                                    onChange={(e) => this.setState({ dip4_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="dip4_s1"
                                    placeholder="dip4_s1"
                                    value={this.state.dip4_s1}
                                    onChange={(e) => this.setState({ dip4_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>DIP4</td>
                                  <td><input type="text"




                                    name="dip4_t2"
                                    placeholder="dip4_t2"
                                    value={this.state.dip4_t2}
                                    onChange={(e) => this.setState({ dip4_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="dip4_s2"
                                    placeholder="dip4_s2"
                                    value={this.state.dip4_s2}
                                    onChange={(e) => this.setState({ dip4_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtp3_t1"
                                    placeholder="mtp3_t1"
                                    value={this.state.mtp3_t1}
                                    onChange={(e) => this.setState({ mtp3_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtp3_s1"
                                    placeholder="mtp3_s1"
                                    value={this.state.mtp3_s1}
                                    onChange={(e) => this.setState({ mtp3_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>MTP 3</td>
                                  <td><input type="text"




                                    name="mtp3_t2"
                                    placeholder="mtp3_t2"
                                    value={this.state.mtp3_t2}
                                    onChange={(e) => this.setState({ mtp3_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtp3_s2"
                                    placeholder="mtp3_s2"
                                    value={this.state.mtp3_s2}
                                    onChange={(e) => this.setState({ mtp3_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="dip5_t1"
                                    placeholder="dip5_t1"
                                    value={this.state.dip5_t1}
                                    onChange={(e) => this.setState({ dip5_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="dip5_s1"
                                    placeholder="dip5_s1"
                                    value={this.state.dip5_s1}
                                    onChange={(e) => this.setState({ dip5_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>DIP5</td>
                                  <td><input type="text"




                                    name="dip5_t2"
                                    placeholder="dip5_t2"
                                    value={this.state.dip5_t2}
                                    onChange={(e) => this.setState({ dip5_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="dip5_s2"
                                    placeholder="dip5_s2"
                                    value={this.state.dip5_s2}
                                    onChange={(e) => this.setState({ dip5_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtp4_t1"
                                    placeholder="mtp4_t1"
                                    value={this.state.mtp4_t1}
                                    onChange={(e) => this.setState({ mtp4_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtp4_s1"
                                    placeholder="mtp4_s1"
                                    value={this.state.mtp4_s1}
                                    onChange={(e) => this.setState({ mtp4_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>MTP 4</td>
                                  <td><input type="text"




                                    name="mtp4_t2"
                                    placeholder="mtp4_t2"
                                    value={this.state.mtp4_t2}
                                    onChange={(e) => this.setState({ mtp4_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtp4_s2"
                                    placeholder="mtp4_s2"
                                    value={this.state.mtp4_s2}
                                    onChange={(e) => this.setState({ mtp4_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="ip1b_t1"
                                    placeholder="ip1b_t1"
                                    value={this.state.ip1b_t1}
                                    onChange={(e) => this.setState({ ip1b_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="ip1b_s1"
                                    placeholder="ip1b_s1"
                                    value={this.state.ip1b_s1}
                                    onChange={(e) => this.setState({ ip1b_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>IP 1</td>
                                  <td><input type="text"




                                    name="ip1b_t2"
                                    placeholder="ip1b_t2"
                                    value={this.state.ip1b_t2}
                                    onChange={(e) => this.setState({ ip1b_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="ip1b_s2"
                                    placeholder="ip1b_s2"
                                    value={this.state.ip1b_s2}
                                    onChange={(e) => this.setState({ ip1b_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>



                                  <td><input type="text"




                                    name="mtp5_t1"
                                    placeholder="mtp5_t1"
                                    value={this.state.mtp5_t1}
                                    onChange={(e) => this.setState({ mtp5_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtp5_s1"
                                    placeholder="mtp5_s1"
                                    value={this.state.mtp5_s1}
                                    onChange={(e) => this.setState({ mtp5_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>MTP 5</td>
                                  <td><input type="text"




                                    name="mtp5_t2"
                                    placeholder="mtp5_t2"
                                    value={this.state.mtp5_t2}
                                    onChange={(e) => this.setState({ mtp5_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="mtp5_s2"
                                    placeholder="mtp5_s2"
                                    value={this.state.mtp5_s2}
                                    onChange={(e) => this.setState({ mtp5_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="ip2_t1"
                                    placeholder="ip2_t1"
                                    value={this.state.ip2_t1}
                                    onChange={(e) => this.setState({ ip2_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="ip2_s1"
                                    placeholder="ip2_s1"
                                    value={this.state.ip2_s1}
                                    onChange={(e) => this.setState({ ip2_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>IP 2</td>
                                  <td><input type="text"




                                    name="ip2_t2"
                                    placeholder="ip2_t2"
                                    value={this.state.ip2_t2}
                                    onChange={(e) => this.setState({ ip2_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="ip2_s2"
                                    placeholder="ip2_s2"
                                    value={this.state.ip2_s2}
                                    onChange={(e) => this.setState({ ip2_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>



                                  <td><input type="text"




                                    name="ip1b_t1"
                                    placeholder="ip1b_t1"
                                    value={this.state.ip1b_t1}
                                    onChange={(e) => this.setState({ ip1b_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="ip1b_s1"
                                    placeholder="ip1b_s1"
                                    value={this.state.ip1b_s1}
                                    onChange={(e) => this.setState({ ip1b_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>IP 1</td>
                                  <td><input type="text"




                                    name="ip1b_t2"
                                    placeholder="ip1b_t2"
                                    value={this.state.ip1b_t2}
                                    onChange={(e) => this.setState({ ip1b_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="ip1b_s2"
                                    placeholder="ip1b_s2"
                                    value={this.state.ip1b_s2}
                                    onChange={(e) => this.setState({ ip1b_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="pip3_t1"
                                    placeholder="pip3_t1"
                                    value={this.state.pip3_t1}
                                    onChange={(e) => this.setState({ pip3_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="pip3_s1"
                                    placeholder="pip3_s1"
                                    value={this.state.pip3_s1}
                                    onChange={(e) => this.setState({ pip3_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>PIP 3</td>
                                  <td><input type="text"




                                    name="pip3_t2"
                                    placeholder="pip3_t2"
                                    value={this.state.pip3_t2}
                                    onChange={(e) => this.setState({ pip3_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="pip3_s2"
                                    placeholder="pip3_s2"
                                    value={this.state.pip3_s2}
                                    onChange={(e) => this.setState({ pip3_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>



                                  <td><input type="text"




                                    name="ip2b_t1"
                                    placeholder="ip2b_t1"
                                    value={this.state.ip2b_t1}
                                    onChange={(e) => this.setState({ ip2b_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="ip2b_s1"
                                    placeholder="ip2b_s1"
                                    value={this.state.ip2b_s1}
                                    onChange={(e) => this.setState({ ip2b_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>IP 2</td>
                                  <td><input type="text"




                                    name="ip2b_t2"
                                    placeholder="ip2b_t2"
                                    value={this.state.ip2b_t2}
                                    onChange={(e) => this.setState({ ip2b_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="ip2b_s2"
                                    placeholder="ip2b_s2"
                                    value={this.state.ip2b_s2}
                                    onChange={(e) => this.setState({ ip2b_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="pip4_t1"
                                    placeholder="pip4_t1"
                                    value={this.state.pip4_t1}
                                    onChange={(e) => this.setState({ pip4_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>


                                  <td><input type="text"




                                    name="pip4_s1"
                                    placeholder="pip4_s1"
                                    value={this.state.pip4_s1}
                                    onChange={(e) => this.setState({ pip4_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>PIP 4</td>
                                  <td><input type="text"




                                    name="pip4_t2"
                                    placeholder="pip4_t2"
                                    value={this.state.pip4_t2}
                                    onChange={(e) => this.setState({ pip4_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="pip4_s2"
                                    placeholder="pip4_s2"
                                    value={this.state.pip4_s2}
                                    onChange={(e) => this.setState({ pip4_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="ip3b_t1"
                                    placeholder="ip3b_t1"
                                    value={this.state.ip3b_t1}
                                    onChange={(e) => this.setState({ ip3b_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="ip3b_s1"
                                    placeholder="ip3b_s1"
                                    value={this.state.ip3b_s1}
                                    onChange={(e) => this.setState({ ip3b_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>IP 3</td>
                                  <td><input type="text"




                                    name="ip3b_t2"
                                    placeholder="ip3b_t2"
                                    value={this.state.ip3b_t2}
                                    onChange={(e) => this.setState({ ip3b_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="ip3b_s2"
                                    placeholder="ip3b_s2"
                                    value={this.state.ip3b_s2}
                                    onChange={(e) => this.setState({ ip3b_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="pip5_t1"
                                    placeholder="pip5_t1"
                                    value={this.state.pip5_t1}
                                    onChange={(e) => this.setState({ pip5_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="pip5_s1"
                                    placeholder="pip5_s1"
                                    value={this.state.pip5_s1}
                                    onChange={(e) => this.setState({ pip5_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>PIP 5</td>
                                  <td><input type="text"




                                    name="pip5_t2"
                                    placeholder="pip5_t2"
                                    value={this.state.pip5_t2}
                                    onChange={(e) => this.setState({ pip5_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="pip5_s2"
                                    placeholder="pip5_s2"
                                    value={this.state.pip5_s2}
                                    onChange={(e) => this.setState({ pip5_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="ip4b_t1"
                                    placeholder="ip4b_t1"
                                    value={this.state.ip4b_t1}
                                    onChange={(e) => this.setState({ ip4b_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="ip4b_s1"
                                    placeholder="ip4b_s1"
                                    value={this.state.ip4b_s1}
                                    onChange={(e) => this.setState({ ip4b_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>IP 4</td>
                                  <td><input type="text"




                                    name="ip4b_t2"
                                    placeholder="ip4b_t2"
                                    value={this.state.ip4b_t2}
                                    onChange={(e) => this.setState({ ip4b_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="ip4b_s2"
                                    placeholder="ip4b_s2"
                                    value={this.state.ip4b_s2}
                                    onChange={(e) => this.setState({ ip4b_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="mcp1_t1"
                                    placeholder="mcp1_t1"
                                    value={this.state.mcp1_t1}
                                    onChange={(e) => this.setState({ mcp1_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="mcp1_s1"
                                    placeholder="mcp1_s1"
                                    value={this.state.mcp1_s1}
                                    onChange={(e) => this.setState({ mcp1_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>MCP 1</td>
                                  <td><input type="text"




                                    name="mcp1_t2"
                                    placeholder="mcp1_t2"
                                    value={this.state.mcp1_t2}
                                    onChange={(e) => this.setState({ mcp1_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="mcp1_s2"
                                    placeholder="mcp1_s2"
                                    value={this.state.mcp1_s2}
                                    onChange={(e) => this.setState({ mcp1_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="ip5b_t1"
                                    placeholder="ip5b_t1"
                                    value={this.state.ip5b_t1}
                                    onChange={(e) => this.setState({ ip5b_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="ip5b_s1"
                                    placeholder="ip5b_s1"
                                    value={this.state.ip5b_s1}
                                    onChange={(e) => this.setState({ ip5b_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>IP 5</td>
                                  <td><input type="text"




                                    name="ip5b_t2"
                                    placeholder="ip5b_t2"
                                    value={this.state.ip5b_t2}
                                    onChange={(e) => this.setState({ ip5b_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="ip5b_s2"
                                    placeholder="ip5b_s2"
                                    value={this.state.ip5b_s2}
                                    onChange={(e) => this.setState({ ip5b_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>
                                <tr>
                                  <td><input type="text"




                                    name="mcp2_t1"
                                    placeholder="mcp2_t1"
                                    value={this.state.mcp2_t1}
                                    onChange={(e) => this.setState({ mcp2_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="mcp2_s1"
                                    placeholder="mcp2_s1"
                                    value={this.state.mcp2_s1}
                                    onChange={(e) => this.setState({ mcp2_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td>MCP 2</td>
                                  <td><input type="text"




                                    name="mcp2_t2"
                                    placeholder="mcp2_t2"
                                    value={this.state.mcp2_t2}
                                    onChange={(e) => this.setState({ mcp2_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>
                                  <td><input type="text"




                                    name="mcp2_s2"
                                    placeholder="mcp2_s2"
                                    value={this.state.mcp2_s2}
                                    onChange={(e) => this.setState({ mcp2_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="s1_t1"
                                    placeholder="s1_t1"
                                    value={this.state.s1_t1}
                                    onChange={(e) => this.setState({ s1_t1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="s1_s1"
                                    placeholder="s1_s1"
                                    value={this.state.s1_s1}
                                    onChange={(e) => this.setState({ s1_s1: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td>S1</td>
                                  <td><input type="text"




                                    name="s1_t2"
                                    placeholder="s1_t2"
                                    value={this.state.s1_t2}
                                    onChange={(e) => this.setState({ s1_t2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                  <td><input type="text"




                                    name="s1_s2"
                                    placeholder="s1_s2"
                                    value={this.state.s1_s2}
                                    onChange={(e) => this.setState({ s1_s2: e.target.value })}
                                    maxlength="50" size="4" /></td>

                                </tr>

                              </tbody>
                            </Table>
                          </Row>



                          <Container fluid>
                            <h2>Patient MUSCULOSKELETAL_EXAMINATIONS</h2>
                            <h5>Rom: Information</h5>

                            <Row>

                              <Table striped bordered hover>
                                <thead>
                                  <tr>
                                    <th></th>
                                    <th>ANKY</th>
                                    <th>FLEX</th>
                                    <th>EXT</th>
                                    <th>RT-ROT</th>
                                    <th>LT-ROT</th>
                                    <th>RT-FL</th>
                                    <th>LT-FL</th>
                                    <th>P/T</th>


                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>CERVICAL</td>
                                    <td><input
                                      type="text"



                                      name="cervical_anky"
                                      placeholder="cervical_anky"
                                      value={this.state.cervical_anky}
                                      onChange={(e) => this.setState({ cervical_anky: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"



                                      name="cervical_flex"
                                      placeholder="cervical_flex"
                                      value={this.state.cervical_flex}
                                      onChange={(e) => this.setState({ cervical_flex: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"



                                      name="cervical_ext"
                                      placeholder="cervical_ext"
                                      value={this.state.cervical_ext}
                                      onChange={(e) => this.setState({ cervical_ext: e.target.value })}
                                      maxlength="50" size="4" /></td>
                                    <td><input
                                      type="text"



                                      name="cervical_rtrot"
                                      placeholder="cervical_rtrot"
                                      value={this.state.cervical_rtrot}
                                      onChange={(e) => this.setState({ cervical_rtrot: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"



                                      name="cervical_ltrot"
                                      placeholder="cervical_ltrot"
                                      value={this.state.cervical_ltrot}
                                      onChange={(e) => this.setState({ cervical_ltrot: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"



                                      name="cervical_rtfl"
                                      placeholder="cervical_rtfl"
                                      value={this.state.cervical_rtfl}
                                      onChange={(e) => this.setState({ cervical_rtfl: e.target.value })}
                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"



                                      name="cervical_ltfl"
                                      placeholder="cervical_ltfl"
                                      value={this.state.cervical_ltfl}
                                      onChange={(e) => this.setState({ cervical_ltfl: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"



                                      name="cervical_pt"
                                      placeholder="cervical_pt"
                                      value={this.state.cervical_pt}
                                      onChange={(e) => this.setState({ cervical_pt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                  </tr>
                                  <tr>
                                    <td>THORASIC</td>
                                    <td><input
                                      type="text"



                                      name="thorasic_anky"
                                      placeholder="thorasic_anky"
                                      value={this.state.thorasic_anky}
                                      onChange={(e) => this.setState({ thorasic_anky: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"



                                      name="thorasic_flex"
                                      placeholder="thorasic_flex"
                                      value={this.state.thorasic_flex}
                                      onChange={(e) => this.setState({ thorasic_flex: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"



                                      name="thorasic_ext"
                                      placeholder="thorasic_ext"
                                      value={this.state.thorasic_ext}
                                      onChange={(e) => this.setState({ thorasic_ext: e.target.value })}
                                      maxlength="50" size="4" /></td>
                                    <td><input
                                      type="text"



                                      name="thorasic_rtrot"
                                      placeholder="thorasic_rtrot"
                                      value={this.state.thorasic_rtrot}
                                      onChange={(e) => this.setState({ thorasic_rtrot: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"



                                      name="thorasic_ltrot"
                                      placeholder="thorasic_ltrot"
                                      value={this.state.thorasic_ltrot}
                                      onChange={(e) => this.setState({ thorasic_ltrot: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"



                                      name="thorasic_rtfl"
                                      placeholder="thorasic_rtfl"
                                      value={this.state.thorasic_rtfl}
                                      onChange={(e) => this.setState({ thorasic_rtfl: e.target.value })}
                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"



                                      name="thorasic_ltfl"
                                      placeholder="thorasic_ltfl"
                                      value={this.state.thorasic_ltfl}
                                      onChange={(e) => this.setState({ thorasic_ltfl: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"



                                      name="thorasic_pt"
                                      placeholder="thorasic_pt"
                                      value={this.state.thorasic_pt}
                                      onChange={(e) => this.setState({ thorasic_pt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                  </tr>
                                  <tr>
                                    <td>LUMBAR</td>
                                    <td><input
                                      type="text"



                                      name="lumbar_anky"
                                      placeholder="lumbar_anky"
                                      value={this.state.lumbar_anky}
                                      onChange={(e) => this.setState({ lumbar_anky: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"



                                      name="lumbar_flex"
                                      placeholder="lumbar_flex"
                                      value={this.state.lumbar_flex}
                                      onChange={(e) => this.setState({ lumbar_flex: e.target.value })}
                                      maxlength="50" size="4" /></td>
                                    <td><input
                                      type="text"



                                      name="lumbar_ext"
                                      placeholder="lumbar_ext"
                                      value={this.state.lumbar_ext}
                                      onChange={(e) => this.setState({ lumbar_ext: e.target.value })}
                                      maxlength="50" size="4" /></td>
                                    <td><input
                                      type="text"



                                      name="lumbar_rtrot"
                                      placeholder="lumbar_rtrot"
                                      value={this.state.lumbar_rtrot}
                                      onChange={(e) => this.setState({ lumbar_rtrot: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"



                                      name="lumbar_ltrot"
                                      placeholder="lumbar_ltrot"
                                      value={this.state.lumbar_ltrot}
                                      onChange={(e) => this.setState({ lumbar_ltrot: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"



                                      name="lumbar_rtfl"
                                      placeholder="lumbar_rtfl"
                                      value={this.state.lumbar_rtfl}
                                      onChange={(e) => this.setState({ lumbar_rtfl: e.target.value })}
                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"



                                      name="lumbar_ltfl"
                                      placeholder="lumbar_ltfl"
                                      value={this.state.lumbar_ltfl}
                                      onChange={(e) => this.setState({ lumbar_ltfl: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"



                                      name="lumbar_pt"
                                      placeholder="lumbar_pt"
                                      value={this.state.lumbar_pt}
                                      onChange={(e) => this.setState({ lumbar_pt: e.target.value })}
                                      maxlength="50" size="4" /></td>


                                  </tr>


                                </tbody>
                              </Table>




                            </Row>


                          </Container>



                          <Container fluid>
                            <h2>Patient MUSCULOSKELETAL_EXAMINATIONS</h2>
                            <h5>Soft Tissue RHEUMATISM: Information</h5>

                            <Row>



                              <Table striped bordered hover>
                                <thead>
                                  <tr>
                                    <th></th>
                                    <th>RT</th>
                                    <th>LT</th>
                                    <th></th>
                                    <th>RT</th>
                                    <th>LT</th>



                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>OPT</td>
                                    <td><input
                                      type="text"


                                      name="opt_rt"
                                      placeholder="opt_rt"
                                      value={this.state.opt_rt}
                                      onChange={(e) => this.setState({ opt_rt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="opt_lt"
                                      placeholder="opt_lt"
                                      value={this.state.opt_lt}
                                      onChange={(e) => this.setState({ opt_lt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td>GLUT</td>
                                    <td><input
                                      type="text"


                                      name="glut_rt"
                                      placeholder="glut_rt"
                                      value={this.state.glut_rt}
                                      onChange={(e) => this.setState({ glut_rt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="glut_lt"
                                      placeholder="glut_lt"
                                      value={this.state.glut_lt}
                                      onChange={(e) => this.setState({ glut_lt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                  </tr>
                                  <tr>
                                    <td>LCER</td>
                                    <td><input
                                      type="text"


                                      name="lcer_rt"
                                      placeholder="lcer_rt"
                                      value={this.state.lcer_rt}
                                      onChange={(e) => this.setState({ lcer_rt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="lcer_lt"
                                      placeholder="lcer_lt"
                                      value={this.state.lcer_lt}
                                      onChange={(e) => this.setState({ lcer_lt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td>TRCR</td>
                                    <td><input
                                      type="text"


                                      name="trcr_rt"
                                      placeholder="trcr_rt"
                                      value={this.state.trcr_rt}
                                      onChange={(e) => this.setState({ trcr_rt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="trcr_lt"
                                      placeholder="trcr_lt"
                                      value={this.state.trcr_lt}
                                      onChange={(e) => this.setState({ trcr_lt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                  </tr>


                                  <tr>
                                    <td>TRPZ</td>
                                    <td><input
                                      type="text"


                                      name="trpz_rt"
                                      placeholder="trpz_rt"
                                      value={this.state.trpz_rt}
                                      onChange={(e) => this.setState({ trpz_rt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="trpz_lt"
                                      placeholder="trpz_lt"
                                      value={this.state.trpz_lt}
                                      onChange={(e) => this.setState({ trpz_lt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td>KNEE</td>
                                    <td><input
                                      type="text"


                                      name="knee_rt"
                                      placeholder="knee_rt"
                                      value={this.state.knee_rt}
                                      onChange={(e) => this.setState({ knee_rt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="knee_lt"
                                      placeholder="knee_lt"
                                      value={this.state.knee_lt}
                                      onChange={(e) => this.setState({ knee_lt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                  </tr>

                                  <tr>
                                    <td>SCAP</td>
                                    <td><input
                                      type="text"


                                      name="scap_rt"
                                      placeholder="scap_rt"
                                      value={this.state.scap_rt}
                                      onChange={(e) => this.setState({ scap_rt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="scap_lt"
                                      placeholder="scap_lt"
                                      value={this.state.scap_lt}
                                      onChange={(e) => this.setState({ scap_lt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td>TA</td>
                                    <td><input
                                      type="text"


                                      name="ta_rt"
                                      placeholder="ta_rt"
                                      value={this.state.ta_rt}
                                      onChange={(e) => this.setState({ ta_rt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="ta_lt"
                                      placeholder="ta_lt"
                                      value={this.state.ta_lt}
                                      onChange={(e) => this.setState({ ta_lt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                  </tr>
                                  <tr>
                                    <td>Z CST</td>
                                    <td><input
                                      type="text"


                                      name="zcst_rt"
                                      placeholder="zcst_rt"
                                      value={this.state.zcst_rt}
                                      onChange={(e) => this.setState({ zcst_rt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="zcst_lt"
                                      placeholder="zcst_lt"
                                      value={this.state.zcst_lt}
                                      onChange={(e) => this.setState({ zcst_lt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td>CALF</td>
                                    <td><input
                                      type="text"


                                      name="calf_rt"
                                      placeholder="calf_rt"
                                      value={this.state.calf_rt}
                                      onChange={(e) => this.setState({ calf_rt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="calf_lt"
                                      placeholder="calf_lt"
                                      value={this.state.calf_lt}
                                      onChange={(e) => this.setState({ calf_lt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                  </tr>
                                  <tr>
                                    <td>EPDL</td>
                                    <td><input
                                      type="text"


                                      name="epdl_rt"
                                      placeholder="epdl_rt"
                                      value={this.state.epdl_rt}
                                      onChange={(e) => this.setState({ epdl_rt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="epdl_lt"
                                      placeholder="epdl_lt"
                                      value={this.state.epdl_lt}
                                      onChange={(e) => this.setState({ epdl_lt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td>SOLE</td>
                                    <td><input
                                      type="text"


                                      name="sole_rt"
                                      placeholder="sole_rt"
                                      value={this.state.sole_rt}
                                      onChange={(e) => this.setState({ sole_rt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="sole_lt"
                                      placeholder="sole_lt"
                                      value={this.state.sole_lt}
                                      onChange={(e) => this.setState({ sole_lt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                  </tr>




                                </tbody>
                              </Table>



                            </Row>


                          </Container>


                          <Container fluid>
                            <h2>Patient MUSCULOSKELETAL_EXAMINATIONS</h2>
                            <h5>DEFORMITIES: Information</h5>

                            <Row>


                              <Table striped bordered hover>
                                <thead>
                                  <tr>
                                    <th>JOINT</th>
                                    <th>FL</th>
                                    <th>EX</th>
                                    <th>AB</th>
                                    <th>ADD</th>
                                    <th>SLX</th>



                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>HAND(R)</td>
                                    <td><input
                                      type="text"

                                      name="rhand_fl"
                                      placeholder="rhand_fl"
                                      value={this.state.rhand_fl}
                                      onChange={(e) => this.setState({ rhand_fl: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="rhand_ex"
                                      placeholder="rhand_ex"
                                      value={this.state.rhand_ex}
                                      onChange={(e) => this.setState({ rhand_ex: e.target.value })}
                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"

                                      name="rhand_ab"
                                      placeholder="rhand_ab"
                                      value={this.state.rhand_ab}
                                      onChange={(e) => this.setState({ rhand_ab: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="rhand_add"
                                      placeholder="rhand_add"
                                      value={this.state.rhand_add}
                                      onChange={(e) => this.setState({ rhand_add: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="rhand_slx"
                                      placeholder="rhand_slx"
                                      value={this.state.rhand_slx}
                                      onChange={(e) => this.setState({ rhand_slx: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                  </tr>
                                  <tr>
                                    <td>HAND(L)</td>
                                    <td><input
                                      type="text"

                                      name="lhand_fl"
                                      placeholder="lhand_fl"
                                      value={this.state.lhand_fl}
                                      onChange={(e) => this.setState({ lhand_fl: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="lhand_ex"
                                      placeholder="lhand_ex"
                                      value={this.state.lhand_ex}
                                      onChange={(e) => this.setState({ lhand_ex: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="lhand_ab"
                                      placeholder="lhand_ab"
                                      value={this.state.lhand_ab}
                                      onChange={(e) => this.setState({ lhand_ab: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="lhand_add"
                                      placeholder="lhand_add"
                                      value={this.state.lhand_add}
                                      onChange={(e) => this.setState({ lhand_add: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="lhand_slx"
                                      placeholder="lhand_slx"
                                      value={this.state.lhand_slx}
                                      onChange={(e) => this.setState({ lhand_slx: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                  </tr>


                                  <tr>
                                    <td>WRIST(R)</td>
                                    <td><input
                                      type="text"

                                      name="rwrist_fl"
                                      placeholder="rwrist_fl"
                                      value={this.state.rwrist_fl}
                                      onChange={(e) => this.setState({ rwrist_fl: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="rwrist_ex"
                                      placeholder="rwrist_ex"
                                      value={this.state.rwrist_ex}
                                      onChange={(e) => this.setState({ rwrist_ex: e.target.value })}
                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"

                                      name="rwrist_ab"
                                      placeholder="rwrist_ab"
                                      value={this.state.rwrist_ab}
                                      onChange={(e) => this.setState({ rwrist_ab: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="rwrist_add"
                                      placeholder="rwrist_add"
                                      value={this.state.rwrist_add}
                                      onChange={(e) => this.setState({ rwrist_add: e.target.value })}

                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"

                                      name="rwrist_slx"
                                      placeholder="rwrist_slx"
                                      value={this.state.rwrist_slx}
                                      onChange={(e) => this.setState({ rwrist_slx: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                  </tr>

                                  <tr>
                                    <td>WRIST(L)</td>
                                    <td><input
                                      type="text"

                                      name="lwrist_fl"
                                      placeholder="lwrist_fl"
                                      value={this.state.lwrist_fl}
                                      onChange={(e) => this.setState({ lwrist_fl: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="lwrist_ex"
                                      placeholder="lwrist_ex"
                                      value={this.state.lwrist_ex}
                                      onChange={(e) => this.setState({ lwrist_ex: e.target.value })}

                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"

                                      name="lwrist_ab"
                                      placeholder="lwrist_ab"
                                      value={this.state.lwrist_ab}
                                      onChange={(e) => this.setState({ lwrist_ab: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="lwrist_add"
                                      placeholder="lwrist_add"
                                      value={this.state.lwrist_add}
                                      onChange={(e) => this.setState({ lwrist_add: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="lwrist_slx"
                                      placeholder="lwrist_slx"
                                      value={this.state.lwrist_slx}
                                      onChange={(e) => this.setState({ lwrist_slx: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                  </tr>


                                  <tr>
                                    <td>ELB(R)</td>
                                    <td><input
                                      type="text"

                                      name="relb_fl"
                                      placeholder="relb_fl"
                                      value={this.state.relb_fl}
                                      onChange={(e) => this.setState({ relb_fl: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="relb_ex"
                                      placeholder="relb_ex"
                                      value={this.state.relb_ex}
                                      onChange={(e) => this.setState({ relb_ex: e.target.value })}

                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"

                                      name="relb_ab"
                                      placeholder="relb_ab"
                                      value={this.state.relb_ab}
                                      onChange={(e) => this.setState({ relb_ab: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="relb_add"
                                      placeholder="relb_add"
                                      value={this.state.relb_add}
                                      onChange={(e) => this.setState({ relb_add: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="relb_slx"
                                      placeholder="relb_slx"
                                      value={this.state.relb_slx}
                                      onChange={(e) => this.setState({ relb_slx: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                  </tr>


                                  <tr>
                                    <td>ELB(L)</td>
                                    <td><input
                                      type="text"

                                      name="lelb_fl"
                                      placeholder="lelb_fl"
                                      value={this.state.lelb_fl}
                                      onChange={(e) => this.setState({ lelb_fl: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="lelb_ex"
                                      placeholder="lelb_ex"
                                      value={this.state.lelb_ex}
                                      onChange={(e) => this.setState({ lelb_ex: e.target.value })}

                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"

                                      name="lelb_ab"
                                      placeholder="lelb_ab"
                                      value={this.state.lelb_ab}
                                      onChange={(e) => this.setState({ lelb_ab: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="lelb_add"
                                      placeholder="lelb_add"
                                      value={this.state.lelb_add}
                                      onChange={(e) => this.setState({ lelb_add: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="lelb_slx"
                                      placeholder="lelb_slx"
                                      value={this.state.lelb_slx}
                                      onChange={(e) => this.setState({ lelb_slx: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                  </tr>


                                  <tr>
                                    <td>SH (RT)</td>
                                    <td><input
                                      type="text"

                                      name="shrt_fl"
                                      placeholder="shrt_fl"
                                      value={this.state.shrt_fl}
                                      onChange={(e) => this.setState({ shrt_fl: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="shrt_ex"
                                      placeholder="shrt_ex"
                                      value={this.state.shrt_ex}
                                      onChange={(e) => this.setState({ shrt_ex: e.target.value })}

                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"

                                      name="shrt_ab"
                                      placeholder="shrt_ab"
                                      value={this.state.shrt_ab}
                                      onChange={(e) => this.setState({ shrt_ab: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="shrt_add"
                                      placeholder="shrt_add"
                                      value={this.state.shrt_add}
                                      onChange={(e) => this.setState({ shrt_add: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="shrt_slx"
                                      placeholder="shrt_slx"
                                      value={this.state.shrt_slx}
                                      onChange={(e) => this.setState({ shrt_slx: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                  </tr>


                                  <tr>
                                    <td>SH (LT)</td>
                                    <td><input
                                      type="text"

                                      name="shlt_fl"
                                      placeholder="shlt_fl"
                                      value={this.state.shlt_fl}
                                      onChange={(e) => this.setState({ shlt_fl: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="shlt_ex"
                                      placeholder="shlt_ex"
                                      value={this.state.shlt_ex}
                                      onChange={(e) => this.setState({ shlt_ex: e.target.value })}
                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"

                                      name="shlt_ab"
                                      placeholder="shlt_ab"
                                      value={this.state.shlt_ab}
                                      onChange={(e) => this.setState({ shlt_ab: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="shlt_add"
                                      placeholder="shlt_add"
                                      value={this.state.shlt_add}
                                      onChange={(e) => this.setState({ shlt_add: e.target.value })}

                                      maxlength="50" size="4" /></td>
                                    <td><input
                                      type="text"

                                      name="shlt_slx"
                                      placeholder="shlt_slx"
                                      value={this.state.shlt_slx}
                                      onChange={(e) => this.setState({ shlt_slx: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                  </tr>

                                  <tr>
                                    <td>KNEE (RL)</td>
                                    <td><input
                                      type="text"

                                      name="kneert_fl"
                                      placeholder="kneert_fl"
                                      value={this.state.kneert_fl}
                                      onChange={(e) => this.setState({ kneert_fl: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="kneert_ex"
                                      placeholder="kneert_ex"
                                      value={this.state.kneert_ex}
                                      onChange={(e) => this.setState({ kneert_ex: e.target.value })}
                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"

                                      name="kneert_ab"
                                      placeholder="kneert_ab"
                                      value={this.state.kneert_ab}
                                      onChange={(e) => this.setState({ kneert_ab: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="kneert_add"
                                      placeholder="kneert_add"
                                      value={this.state.kneert_add}
                                      onChange={(e) => this.setState({ kneert_add: e.target.value })}

                                      maxlength="50" size="4" /></td>
                                    <td><input
                                      type="text"

                                      name="kneert_slx"
                                      placeholder="kneert_slx"
                                      value={this.state.kneert_slx}
                                      onChange={(e) => this.setState({ kneert_slx: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                  </tr>
                                  <tr>
                                    <td>KNEE (LT)</td>
                                    <td><input
                                      type="text"

                                      name="kneelt_fl"
                                      placeholder="kneelt_fl"
                                      value={this.state.kneelt_fl}
                                      onChange={(e) => this.setState({ kneelt_fl: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="kneelt_ex"
                                      placeholder="kneelt_ex"
                                      value={this.state.kneelt_ex}
                                      onChange={(e) => this.setState({ kneelt_ex: e.target.value })}

                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"

                                      name="kneelt_ab"
                                      placeholder="kneelt_ab"
                                      value={this.state.kneelt_ab}
                                      onChange={(e) => this.setState({ kneelt_ab: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="kneelt_add"
                                      placeholder="kneelt_add"
                                      value={this.state.kneelt_add}
                                      onChange={(e) => this.setState({ kneelt_add: e.target.value })}

                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"

                                      name="kneelt_slx"
                                      placeholder="kneelt_slx"
                                      value={this.state.kneelt_slx}
                                      onChange={(e) => this.setState({ kneelt_slx: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                  </tr>
                                  <tr>
                                    <td>FOOT (RL)</td>
                                    <td><input
                                      type="text"

                                      name="footrt_fl"
                                      placeholder="footrt_fl"
                                      value={this.state.footrt_fl}
                                      onChange={(e) => this.setState({ footrt_fl: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="footrt_ex"
                                      placeholder="footrt_ex"
                                      value={this.state.footrt_ex}
                                      onChange={(e) => this.setState({ footrt_ex: e.target.value })}

                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"

                                      name="footrt_ab"
                                      placeholder="footrt_ab"
                                      value={this.state.footrt_ab}
                                      onChange={(e) => this.setState({ footrt_ab: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="footrt_add"
                                      placeholder="footrt_add"
                                      value={this.state.footrt_add}
                                      onChange={(e) => this.setState({ footrt_add: e.target.value })}
                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"

                                      name="footrt_slx"
                                      placeholder="footrt_slx"
                                      value={this.state.footrt_slx}
                                      onChange={(e) => this.setState({ footrt_slx: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                  </tr>
                                  <tr>
                                    <td>FOOT (LT)</td>
                                    <td><input
                                      type="text"

                                      name="footlt_fl"
                                      placeholder="footlt_fl"
                                      value={this.state.footlt_fl}
                                      onChange={(e) => this.setState({ footlt_fl: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="footlt_ex"
                                      placeholder="footlt_ex"
                                      value={this.state.footlt_ex}
                                      onChange={(e) => this.setState({ footlt_ex: e.target.value })}
                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"

                                      name="footlt_ab"
                                      placeholder="footlt_ab"
                                      value={this.state.footlt_ab}
                                      onChange={(e) => this.setState({ footlt_ab: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="footlt_add"
                                      placeholder="footlt_add"
                                      value={this.state.footlt_add}
                                      onChange={(e) => this.setState({ footlt_add: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"

                                      name="footlt_slx"
                                      placeholder="footlt_slx"
                                      value={this.state.footlt_slx}
                                      onChange={(e) => this.setState({ footlt_slx: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                  </tr>






                                </tbody>
                              </Table>



                            </Row>


                          </Container>



                          <Container fluid>
                            <h2>Patient MUSCULOSKELETAL_EXAMINATIONS</h2>
                            <h5>Hypermobility: Information</h5>

                            <Row>

                              <Table striped bordered hover>
                                <thead>
                                  <tr>
                                    <th></th>
                                    <th>RT</th>
                                    <th>LT</th>
                                    <th></th>
                                    <th>RT</th>
                                    <th>LT</th>



                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>THUMB</td>
                                    <td><input
                                      type="text"


                                      name="thumb_rt"
                                      placeholder="thumb_rt"
                                      value={this.state.thumb_rt}
                                      onChange={(e) => this.setState({ thumb_rt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="thumb_lt"
                                      placeholder="thumb_lt"
                                      value={this.state.thumb_lt}
                                      onChange={(e) => this.setState({ thumb_lt: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td>KNEE</td>
                                    <td><input
                                      type="text"


                                      name="hy_knee_rt"
                                      placeholder="hy_knee_rt"
                                      value={this.state.hy_knee_rt}
                                      onChange={(e) => this.setState({ hy_knee_rt: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="hy_knee_lt"
                                      placeholder="hy_knee_lt"
                                      value={this.state.hy_knee_lt}
                                      onChange={(e) => this.setState({ hy_knee_lt: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                  </tr>
                                  <tr>
                                    <td>FINGER</td>
                                    <td><input
                                      type="text"


                                      name="finger_rt"
                                      placeholder="finger_rt"
                                      value={this.state.finger_rt}
                                      onChange={(e) => this.setState({ finger_rt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="finger_lt"
                                      placeholder="finger_lt"
                                      value={this.state.finger_lt}
                                      onChange={(e) => this.setState({ finger_lt: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td>ANKLE</td>
                                    <td><input
                                      type="text"


                                      name="ankle_rt"
                                      placeholder="ankle_rt"
                                      value={this.state.ankle_rt}
                                      onChange={(e) => this.setState({ ankle_rt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="ankle_lt"
                                      placeholder="ankle_lt"
                                      value={this.state.ankle_lt}
                                      onChange={(e) => this.setState({ ankle_lt: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                  </tr>


                                  <tr>
                                    <td>PALM</td>
                                    <td><input
                                      type="text"


                                      name="palm_rt"
                                      placeholder="palm_rt"
                                      value={this.state.palm_rt}
                                      onChange={(e) => this.setState({ palm_rt: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="palm_lt"
                                      placeholder="palm_lt"
                                      value={this.state.palm_lt}
                                      onChange={(e) => this.setState({ palm_lt: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td>OTHERS</td>
                                    <td><input
                                      type="text"


                                      name="other_rt"
                                      placeholder="other_rt"
                                      value={this.state.other_rt}
                                      onChange={(e) => this.setState({ other_rt: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="other_lt"
                                      placeholder="other_lt"
                                      value={this.state.other_lt}
                                      onChange={(e) => this.setState({ other_lt: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                  </tr>

                                  <tr>
                                    <td>ELBOW</td>
                                    <td><input
                                      type="text"


                                      name="elbow_rt"
                                      placeholder="elbow_rt"
                                      value={this.state.elbow_rt}
                                      onChange={(e) => this.setState({ elbow_rt: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                    <td><input
                                      type="text"


                                      name="elbow_lt"
                                      placeholder="elbow_lt"
                                      value={this.state.elbow_lt}
                                      onChange={(e) => this.setState({ elbow_lt: e.target.value })}
                                      maxlength="50" size="4" /></td>

                                    <td>SPINE</td>
                                    <td><input
                                      type="text"


                                      name="spine_rt"
                                      placeholder="spine_rt"
                                      value={this.state.spine_rt}
                                      onChange={(e) => this.setState({ spine_rt: e.target.value })}

                                      maxlength="50" size="4" /></td>


                                    <td><input
                                      type="text"


                                      name="spine_lt"
                                      placeholder="spine_lt"
                                      value={this.state.spine_lt}
                                      onChange={(e) => this.setState({ spine_lt: e.target.value })}

                                      maxlength="50" size="4" /></td>

                                  </tr>


                                </tbody>
                              </Table>



                            </Row>


                          </Container>






                          <Button
                            className="btn-fill pull-right"
                            type="submit"
                            name=""
                            variant="info"
                          >
                            Update Examination
                </Button>





                        </Form>
                      </Card.Body>
                    </Card>
                  </Col>
                </Row>

                <Button
                  style={{ "alignItems": "left" }}
                  className="btn-fill pull-right"
                  type="submit"
                  name=""
                  variant="danger"
                  onClick={(e) => this.delete(this.state.pid, e)}                  >
                  Delete
                  </Button>
              </Container>
  }
          </>
    
      </>
    );
  }
}

export default Examination_Main

// export default User;
// import React from "react";
// import axios from 'axios'
// // react-bootstrap components
// import {
//   Badge,
//   Button,
//   Card,
//   Form,
//   Navbar,
//   Nav,
//   Container,
//   Row,
//   Col,
// } from "react-bootstrap";


// class User extends React.Component {
//   constructor(props){
//     super(props);
//     this.state ={
//       users:[],
//       id:0,
//       pid:''
//       ,date:''
//       ,code:''
//       ,place:''
//       ,pname:''
//       ,fhname:''
//       ,age:''
//       ,sex:''
//       ,education:''
//       ,address:''
//       ,phone:''
//       ,district:''
//       ,state:''
//       ,area:''
//       ,referred_by:''
//       ,occupation:''
//       ,ethnic:''
//       ,marital_status:''
//       ,marital_status_years:''
//       ,menses_frequency:''
//       ,menses_loss:''
//       ,menarche_years:''
//       ,hysterectomy:''
//       ,hysterectomy_years:''
//       ,menopause:''
//       ,menopause_years:''
//       ,children:''
//       ,children_male:''
//       ,children_female:''
//       ,abortions:''
//       ,abortions_number:''
//       ,abortions_cause:''
//       ,current_lactation:''
//       ,contraception_methods:''
//       ,contraception_methods_type:''
//       ,hormone_treatment:''
//       ,addiction:''
//       ,tobacco:''
//       ,tobacco_years:''
//       ,smoking:''
//       ,smoking_years:''
//       ,alcohol:''
//       ,alcohol_years:''
//       ,family_history:''
//       ,health_condition:''
//       ,remarks:''
//       ,comorbidities:''
//       ,onset:''
//       ,onset_duration:''
//       ,onset_duration_type:''
//       ,first_symptom:''
//       ,initial_site_joint:''
//       ,soft_tissue:''
//       ,others:''
//       ,course1:''
//       ,course2:''
//       ,pattern1:''
//       ,pattern2:''
//       ,current_relapse:''
//       ,current_relapse_type:''
//       ,detailed_history:''
//       ,pasthistory:''
//       ,surgical_history:''
//       ,drug_history:''
//       ,drug_allergy:''
//       ,drug_allergy_type:''
//       ,bowelhabit:''
//       ,sleep:''
//       ,user_id:''

//     }

//   }


//   componentDidMount(){
//     axios.get(`https://abcapi.vidaria.in/bpatientdetails/${this.props.match.params.id}`)
//     .then((res)=>{
//       console.log(res)
//       this.setState({
//         id:res.data.bpatient.id,

//         pid:res.data.bpatient.pid,
//         date:res.data.bpatient.date,
//         code:res.data.bpatient.code,
//         place:res.data.bpatient.place,
//         pname:res.data.bpatient.pname,
//         fhname:res.data.bpatient.fhname,
//         age:res.data.bpatient.age,
//         sex:res.data.bpatient.sex,
//         education:res.data.bpatient.education,
//         address:res.data.bpatient.address,
//         phone:res.data.bpatient.phone,
//         district:res.data.bpatient.district,
//         state:res.data.bpatient.state,
//         area:res.data.bpatient.area,
//         referred_by:res.data.bpatient.referred_by,
//         occupation:res.data.bpatient.occupation,
//         ethnic:res.data.bpatient.ethnic,
//         marital_status:res.data.bpatient.marital_status,
//         marital_status_years :res.data.bpatient.marital_status_years,
//         menses_frequency:res.data.bpatient.menses_frequency,
//         menses_loss:res.data.bpatient.menses_loss,
//         menarche_years :res.data.bpatient.menarche_years,
//         hystrectomy :res.data.bpatient.hystrectomy,
//         hystrectomy_years:res.data.bpatient.hystrectomy_years,
//         menopause:res.data.bpatient.menopause,
//         menopause_years:res.data.bpatient.menarche_years,
//         children:res.data.bpatient.children,
//         children_male:res.data.bpatient.children_male,
//         children_female:res.data.bpatient.children_female,
//         abortions:res.data.bpatient.abortions,
//         abortions_number:res.data.bpatient.abortions_number,
//         abortions_cause:res.data.bpatient.abortions_cause,
//         breastfed:res.data.bpatient.breastfed,
//         breastfed_years:res.data.bpatient.breastfed_years,
//         current_lactation:res.data.bpatient.current_lactation,
//         contraception_methods:res.data.bpatient.contraception_methods,
//         contraception_methods_type:res.data.bpatient.contraception_methods_type,
//         hormone_treatment:res.data.bpatient.hormone_treatment,
//         addiction:res.data.bpatient.addiction,
//         tobacco:res.data.bpatient.tobacco,
//         tobacco_years:res.data.bpatient.tobacco_years,
//         smoking:res.data.bpatient.smoking,
//         smoking_years:res.data.bpatient.smoking_years,
//         alcohol:res.data.bpatient.alcohol,
//         alcohol_years:res.data.bpatient.alcohol_years,
//         family_history:res.data.bpatient.family_history,
//         comorbidities:res.data.bpatient.comorbidities,
//         breastlump:res.data.bpatient.breastlump,
//         breastlump_location:res.data.bpatient.breastlump_location,
//         breastlump_size:res.data.bpatient.breastlump_size,
//         overlying_skin:res.data.bpatient.overlying_skin,
//         axillarylump:res.data.bpatient.axillarylump,
//         axillarylump_side:res.data.bpatient.axillarylump_side,
//         matted:res.data.bpatient.matted,
//         axillarylump_size:res.data.bpatient.axillarylump_size,
//         nipple_discharge:res.data.bpatient.nipple_discharge,
//         nipple_discharge_frequency:res.data.bpatient.nipple_discharge_frequency,
//         nipple_discharge_colour:res.data.bpatient.nipple_discharge_colour,
//         mastalgia:res.data.bpatient.mastalgia,
//         mastitis:res.data.bpatient.mastitis,
//         ulcer:res.data.bpatient.ulcer,
//         nipple_inversion:res.data.bpatient.nipple_inversion,
//         others:res.data.bpatient.others,
//         duration:res.data.bpatient.duration,
//         pasthistory:res.data.bpatient.pasthistory,
//         surgical_history:res.data.bpatient.surgical_history,
//         drug_history:res.data.bpatient.drug_history,
//         drug_allergy:res.data.bpatient.drug_allergy,
//         drug_allergy_type:res.data.bpatient.drug_allergy_type,
//         bowelhabit:res.data.bpatient.bowelhabit,
//         bladderhabit:res.data.bpatient.bladderhabit,
//         sleep:res.data.bpatient.sleep,
//         appetite:res.data.bpatient.appetite,
//         weight:res.data.bpatient.weight,
//         height:res.data.bpatient.height,
//         bmi:res.data.bpatient.bmi,
//         bp:res.data.bpatient.bp,
//         pulse:res.data.bpatient.pulse,
//         temp:res.data.bpatient.temp,
//         respiratory_rate:res.data.bpatient.respiratory_rate,
//         health_condition:res.data.bpatient.health_condition,
//         examination_remarks:res.data.bpatient.examination_remarks,
//         bra_size:res.data.bpatient.bra_size,
//         usg:res.data.bpatient.usg,
//         mmg:res.data.bpatient.mmg,
//         mri:res.data.bpatient.mri,
//         fnac:res.data.bpatient.fnac,
//         core_biopsy:res.data.bpatient.core_biopsy,
//         incision_biopsy:res.data.bpatient.incision_biopsy,
//         investigation_remarks:res.data.bpatient.investigation_remarks,
//         blood_investigation:res.data.bpatient.blood_investigation,
//         diagnosis:res.data.bpatient.diagnosis,
//         treatment_plan:res.data.bpatient.treatment_plan,
//         user_id:res.data.bpatient.user.id


//       })
//     })
//   }
//   submit(event,id){
//     event.preventDefault();
//     if(id === 0){
//       axios.post("https://abcapi.vidaria.in/addbpatient",{

//       pid:this.state.pid
//       ,date:this.state.date
//       ,code:this.state.code
//       ,place:this.state.place
//       ,pname:this.state.pname
//       ,fhname:this.state.fhname
//       ,age:this.state.age
//       ,sex:this.state.sex
//       ,education:this.state.education
//       ,address:this.state.address
//       ,phone:this.state.phone
//       ,district:this.state.district
//       ,state:this.state.state
//       ,area:this.state.area
//       ,referred_by:this.state.referred_by
//       ,occupation:this.state.occupation
//       ,ethnic:this.state.ethnic
//       ,marital_status:this.state.marital_status
//       ,marital_status_years:this.state.marital_status_years
//       ,menses_frequency:this.state.menses_frequency
//       ,menses_loss:this.state.menses_loss
//       ,menarche_years:this.state.menarche_years
//       ,hysterectomy:this.state.hysterectomy
//       ,hysterectomy_years:this.state.hysterectomy_years
//       ,menopause:this.state.menopause
//       ,menopause_years:this.state.menopause_years
//       ,children:this.state.children
//       ,children_male:this.state.children_male
//       ,children_female:this.state.children_female
//       ,abortions:this.state.abortions
//       ,abortions_number:this.state.abortions_number
//       ,abortions_cause:this.state.abortions_cause
//       ,current_lactation:this.state.current_lactation
//       ,contraception_methods:this.state.contraception_methods
//       ,contraception_methods_type:this.state.contraception_methods_type
//       ,hormone_treatment:this.state.hormone_treatment
//       ,addiction:this.state.addiction
//       ,tobacco:this.state.tobacco
//       ,tobacco_years:this.state.tobacco_years
//       ,smoking:this.state.smoking
//       ,smoking_years:this.state.smoking_years
//       ,alcohol:this.state.alcohol
//       ,alcohol_years:this.state.alcohol_years
//       ,family_history:this.state.family_history
//       ,health_condition:this.state.health_condition
//       ,remarks:this.state.remarks
//       ,comorbidities:this.state.comorbidities
//       ,onset:this.state.onset
//       ,onset_duration:this.state.onset_duration
//       ,onset_duration_type:this.state.onset_duration_type
//       ,first_symptom:this.state.first_symptom
//       ,initial_site_joint:this.state.initial_site_joint
//       ,soft_tissue:this.state.soft_tissue
//       ,others:this.state.others
//       ,course1:this.state.course1
//       ,course2:this.state.course2
//       ,pattern1:this.state.pattern1
//       ,pattern2:this.state.pattern2
//       ,current_relapse:this.state.current_relapse
//       ,current_relapse_type:this.state.current_relapse_type
//       ,detailed_history:this.state.detailed_history
//       ,pasthistory:this.state.pasthistory
//       ,surgical_history:this.state.surgical_history
//       ,drug_history:this.state.drug_history
//       ,drug_allergy:this.state.drug_allergy
//       ,drug_allergy_type:this.state.drug_allergy_type
//       ,bowelhabit:this.state.bowelhabit
//       ,sleep:this.state.sleep
//       ,user_id:this.state.user_id
//           })
//       .then((res)=>{
//         this.componentDidMount();
//       })
//     }else{
//       axios.put(`https://abcapi.vidaria.in/updatebpatient/${this.props.match.params.id}`,{
//         id:this.state.id,


//         pid:this.state.pid
//         ,date:this.state.date
//         ,code:this.state.code
//         ,place:this.state.place
//         ,pname:this.state.pname
//         ,fhname:this.state.fhname
//         ,age:this.state.age
//         ,sex:this.state.sex
//         ,education:this.state.education
//         ,address:this.state.address
//         ,phone:this.state.phone
//         ,district:this.state.district
//         ,state:this.state.state
//         ,area:this.state.area
//         ,referred_by:this.state.referred_by
//         ,occupation:this.state.occupation
//         ,ethnic:this.state.ethnic
//         ,marital_status:this.state.marital_status
//         ,marital_status_years:this.state.marital_status_years
//         ,menses_frequency:this.state.menses_frequency
//         ,menses_loss:this.state.menses_loss
//         ,menarche_years:this.state.menarche_years
//         ,hysterectomy:this.state.hysterectomy
//         ,hysterectomy_years:this.state.hysterectomy_years
//         ,menopause:this.state.menopause
//         ,menopause_years:this.state.menopause_years
//         ,children:this.state.children
//         ,children_male:this.state.children_male
//         ,children_female:this.state.children_female
//         ,abortions:this.state.abortions
//         ,abortions_number:this.state.abortions_number
//         ,abortions_cause:this.state.abortions_cause
//         ,current_lactation:this.state.current_lactation
//         ,contraception_methods:this.state.contraception_methods
//         ,contraception_methods_type:this.state.contraception_methods_type
//         ,hormone_treatment:this.state.hormone_treatment
//         ,addiction:this.state.addiction
//         ,tobacco:this.state.tobacco
//         ,tobacco_years:this.state.tobacco_years
//         ,smoking:this.state.smoking
//         ,smoking_years:this.state.smoking_years
//         ,alcohol:this.state.alcohol
//         ,alcohol_years:this.state.alcohol_years
//         ,family_history:this.state.family_history
//         ,health_condition:this.state.health_condition
//         ,remarks:this.state.remarks
//         ,comorbidities:this.state.comorbidities
//         ,onset:this.state.onset
//         ,onset_duration:this.state.onset_duration
//         ,onset_duration_type:this.state.onset_duration_type
//         ,first_symptom:this.state.first_symptom
//         ,initial_site_joint:this.state.initial_site_joint
//         ,soft_tissue:this.state.soft_tissue
//         ,others:this.state.others
//         ,course1:this.state.course1
//         ,course2:this.state.course2
//         ,pattern1:this.state.pattern1
//         ,pattern2:this.state.pattern2
//         ,current_relapse:this.state.current_relapse
//         ,current_relapse_type:this.state.current_relapse_type
//         ,detailed_history:this.state.detailed_history
//         ,pasthistory:this.state.pasthistory
//         ,surgical_history:this.state.surgical_history
//         ,drug_history:this.state.drug_history
//         ,drug_allergy:this.state.drug_allergy
//         ,drug_allergy_type:this.state.drug_allergy_type
//         ,bowelhabit:this.state.bowelhabit
//         ,sleep:this.state.sleep
//         ,user_id:this.state.user_id
//       }).then(()=>{
//         this.props.history.push(`/admin/dashboard`)
//       })

//     }

//   }
//   delete(id){
//     axios.delete(`https://abcapi.vidaria.in/userdelete/${id}`)
//     .then(()=>{
//       this.componentDidMount();
//     })
//   }
//   // edit(id){
//   //   axios.get(`https://abcapi.vidaria.in/userdetails/1`)
//   //   .then((res)=>{
//   //     console.log(res.data.user);
//   //     this.setState({
//   //       id:res.data.user.id,
//   //       username:res.data.user.username,
//   //       email:res.data.user.email,
//   //       phone:res.data.user.phone,
//   //       password:res.data.user.password,
//   //       role:res.data.user.role,
//   //       speciality:res.data.user.speciality,
//   //     })
//   //   })
//   // }

//   render()


// {
//   return (
//     <>
//       <Container fluid>
//         <Row>
//           <Col md="8">
//             <Card>
//               <Card.Header>
//                 <Card.Title as="h4">Edit Profile</Card.Title>
//               </Card.Header>
//               <Card.Body>
//                 <Form onSubmit={(e)=>this.submit(e,this.state.id)}>


//                 <Row>
//           <Col md="6">


//           <Form.Group>
//         <Form.Label>Patient Id</Form.Label><br></br>
//           <Form.Control
//            type="number"

//            name="pid"                          
//            placeholder="patient id"
//            value={this.state.pid}
//            onChange={(e)=>this.setState({pid:e.target.value})}></Form.Control>
//           </Form.Group>



//           <Form.Group>
//           <Form.Label>Date</Form.Label><br></br>
//           <Form.Control 
//           type="text"  
//           name="date"
//           placeholder="Date"
//           value={this.state.date}
//           onChange={(e)=>this.setState({date:e.target.value})}  />
//           </Form.Group>
//         </Col>


//         <Col md="6">
//         <Form.Group>
//           <Form.Label>code</Form.Label><br></br>
//           <Form.Control 
//           type="text"  
//           name="code"
//           placeholder="Code"
//           value={this.state.code}
//           onChange={(e)=>this.setState({code:e.target.value})}/>
//           </Form.Group>


//           <Form.Group>
//           <Form.Label>place</Form.Label><br></br>
//           <Form.Control 
//           type="text"  
//           name="place"
//           placeholder="place"
//           value={this.state.place}
//           onChange={(e)=>this.setState({place:e.target.value})}/>
//           </Form.Group>
//           </Col>
//           </Row>


//           <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>Patient Name</Form.Label><br></br>
//           <Form.Control 
//           type="text"  
//           name="pname"
//           placeholder="pname"
//           value={this.state.pname}
//           onChange={(e)=>this.setState({pname:e.target.value})}/>
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>Father or Husband name</Form.Label><br></br>
//           <Form.Control 
//           type="text"  
//           name="fhname"
//           placeholder="fhname"
//           value={this.state.fhname}
//           onChange={(e)=>this.setState({fhname:e.target.value})}/>
//           </Form.Group>
// </Col>
// <Col md="6">

// <Form.Group>
//           <Form.Label>Age</Form.Label><br></br>
//           <Form.Control 
//           type="number"
//           name="age"  
//           placeholder="Age"
//           value={this.state.age}
//           onChange={(e)=>this.setState({age:e.target.value})}/>
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>Gender</Form.Label><br></br>
//           <Form.Control 
//           type="text"  
//           name="sex"
//           placeholder="Gender"
//           value={this.state.sex}
//           onChange={(e)=>this.setState({sex:e.target.value})}/>
//           </Form.Group>

// </Col>
// </Row>



// <Row>
//   <Col md="6">
//   <Form.Group>
//             <Form.Label>Education</Form.Label><br></br>
//           <Form.Control  
//           type="text"  
//           name="education"
//           placeholder="Education"
//           value={this.state.education}
//           onChange={(e)=>this.setState({education:e.target.value})}
//           />
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>Address</Form.Label><br></br>
//           <Form.Control  
//           type="text"  
//           name="address"
//           placeholder="Address"
//           value={this.state.address}
//           onChange={(e)=>this.setState({address:e.target.value})}
//           />
//           </Form.Group>

// </Col>
// <Col md="6">
// <Form.Group>
//           <Form.Label>Phone</Form.Label><br></br>
//           <Form.Control  
//           type="number"
//           name="phone"  
//           placeholder="Phone"
//           value={this.state.phone}
//           onChange={(e)=>this.setState({phone:e.target.value})}/>
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>District</Form.Label><br></br>
//           <Form.Control  
//           type="text"  
//           name="district"
//           placeholder="District"
//           value={this.state.district}
//           onChange={(e)=>this.setState({district:e.target.value})}
//           />
//           </Form.Group>

// </Col>
// </Row> 

// <Row>
//   <Col md="6">
//   <Form.Group>
//           <Form.Label>State</Form.Label><br></br>
//           <Form.Control  
//           type="text"  
//           name="state"
//           placeholder="State"
//           value={this.state.state}
//           onChange={(e)=>this.setState({state:e.target.value})}
//           />
//           </Form.Group>


//           <Form.Group>
//           <Form.Label>Area</Form.Label><br></br>
//           <Form.Control  
//           type="text"  
//           name="area"
//           placeholder="Area"
//           value={this.state.area}
//           onChange={(e)=>this.setState({area:e.target.value})}
//           />
//           </Form.Group>

//           </Col>
//           <Col md="6">
//           <Form.Group>

//           <Form.Label>Referred_by</Form.Label><br></br>
//           <Form.Control  
//           type="text"  
//           name="referred_by"
//           placeholder="Referred by"
//           value={this.state.referred_by}
//           onChange={(e)=>this.setState({referred_by:e.target.value})}
//           />
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>Occupation</Form.Label><br></br>
//           <Form.Control  
//           type="text"  
//           name="occupation"
//           placeholder="Occupation"
//           value={this.state.occupation}
//           onChange={(e)=>this.setState({occupation:e.target.value})}
//           />
//           </Form.Group>


// </Col>
// </Row>


//             <Row>
//   <Col md="6">
//   <Form.Group>
//           <Form.Label>Ethnic</Form.Label><br></br>
//            <Form.Control  
//            type="text"  
//            name="ethnic"
//             placeholder="ethnic"
//           value={this.state.ethnic}
//           onChange={(e)=>this.setState({ethnic:e.target.value})}

//            />
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>Marital_status</Form.Label><br></br>
//            <Form.Control  
//            type="text"  
//            name="marital_status"
//             placeholder="Marital status"
//           value={this.state.marital_status}
//           onChange={(e)=>this.setState({marital_status:e.target.value})}

//            />
//           </Form.Group>

//          </Col> 

//          <Col md="6">
//          <Form.Group>
//           <Form.Label>marital_status_years</Form.Label><br></br>
//            <Form.Control  
//            type="number"
//            name="marital_status_years"
//             placeholder="marital status years"
//           value={this.state.marital_status_years}
//           onChange={(e)=>this.setState({marital_status_years:e.target.value})}  
//          />
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>menses_frequency</Form.Label><br></br>
//            <Form.Control  
//            type="text"  
//            name="menses_frequency"
//             placeholder="menses frequency"
//           value={this.state.menses_frequency}
//           onChange={(e)=>this.setState({menses_frequency:e.target.value})}
//            />
//           </Form.Group>

//           </Col>
//           </Row>



//           <Row>


//   <Col md="6">   
//   <Form.Group>
//           <Form.Label>menses_loss</Form.Label><br></br>
//            <Form.Control  
//            type="text"  
//            name="menses_loss"
//             placeholder="menses loss"
//           value={this.state.menses_loss}
//           onChange={(e)=>this.setState({menses_loss:e.target.value})}
//     />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>menarche_years</Form.Label><br></br>
//            <Form.Control  
//            type="number"
//            name="menarche_years"
//             placeholder="menarche year"
//           value={this.state.menarche_years}
//           onChange={(e)=>this.setState({menarche_years:e.target.value})}  
//           />
//           </Form.Group>
//           </Col>
//           <Col md="6"> 
//           <Form.Group>  

//           <Form.Label>hystrectomy</Form.Label><br></br>
//            <Form.Control  
//            type="text"  
//            name="hystrectomy"
//             placeholder="hystrectomy"
//           value={this.state.hystrectomy}
//           onChange={(e)=>this.setState({hystrectomy:e.target.value})}
//         />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>hystrectomy_years</Form.Label><br></br>
//            <Form.Control  
//            type="number"
//            name="hystrectomy_years"
//             placeholder="hystrectomy year"
//           value={this.state.hystrectomy_years}
//           onChange={(e)=>this.setState({hystrectomy_years:e.target.value})}  
//            />
//           </Form.Group>

// </Col>
// </Row>




// <Row>
//           <Col md="6">

//           <Form.Group>
//           <Form.Label>menopause</Form.Label><br></br>
//           <Form.Control 
//           type="text" 	
//           name="menopause"
//              placeholder="menopause"
//           value={this.state.menopause}
//           onChange={(e)=>this.setState({menopause:e.target.value})}  					

// />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>menopause_years</Form.Label><br></br>
//            <Form.Control 
//            type="number"
//            name="menopause_years"
//               placeholder="menopause year"
//           value={this.state.menopause_years}
//           onChange={(e)=>this.setState({menopause_years:e.target.value})}   						
// />
//            </Form.Group>
//           </Col>


//           <Col md="6">
//           <Form.Group>

//           <Form.Label>no of children</Form.Label><br></br>
//            <Form.Control 
//            type="number"
//            name="children"
//               placeholder="no of children"
//           value={this.state.children}
//           onChange={(e)=>this.setState({children:e.target.value})}   						
// />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>children_male</Form.Label><br></br>
//            <Form.Control 
//            type="number"
//            name="children_male"
//               placeholder="children_male"
//           value={this.state.children_male}
//           onChange={(e)=>this.setState({children_male:e.target.value})}   						
//      />
//            </Form.Group>

// </Col>
// </Row>

// <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>children_female</Form.Label><br></br>
//            <Form.Control 
//            type="number"
//            name="children_female"
//               placeholder="children_female "
//           value={this.state.children_female}
//           onChange={(e)=>this.setState({children_female:e.target.value})}   						
//  />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>abortions</Form.Label><br></br>
//            <Form.Control 
//            type="text" 	
//            name="abortions"
//               placeholder="abortions"
//           value={this.state.abortions}
//           onChange={(e)=>this.setState({abortions:e.target.value})}  					
// />
//            </Form.Group>
// </Col>
// <Col md="6">
// <Form.Group>
//           <Form.Label>abortions_number</Form.Label><br></br>
//            <Form.Control 
//            type="number"
//            name="abortions_number"
//               placeholder="abortions_number"
//           value={this.state.abortions_number}
//           onChange={(e)=>this.setState({abortions_number:e.target.value})}   						
//     />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>abortions_cause</Form.Label><br></br>
//            <Form.Control 
//            type="text" 	
//            name="abortions_cause"
//               placeholder="abortions_cause"
//           value={this.state.abortions_cause}
//           onChange={(e)=>this.setState({abortions_cause:e.target.value})}  					
// />
//            </Form.Group>
//          </Col>
//          </Row>




//          <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>breastfed</Form.Label><br></br>
//            <Form.Control  
//            type="text" 	
//            name="breastfed"	
//                placeholder="breastfed"
//           value={this.state.breastfed}
//           onChange={(e)=>this.setState({breastfed:e.target.value})}  					
//  />
//           </Form.Group>

//           <Form.Group>
//            <Form.Label>breastfed_years</Form.Label><br></br>
//            <Form.Control  
//            type="number"
//            name="breastfed_years" 
//                placeholder="breastfed_years"
//           value={this.state.breastfed_years}
//           onChange={(e)=>this.setState({breastfed_years:e.target.value})}  							
//        />
//           </Form.Group>
// </Col>

// <Col md="6">

// <Form.Group>
//            <Form.Label>current_lactation</Form.Label><br></br>
//            <Form.Control  
//            type="text" 	
//            name="current_lactation"	
//                placeholder="current_lactation"
//           value={this.state.current_lactation}
//           onChange={(e)=>this.setState({current_lactation:e.target.value})}  					
//   />
//           </Form.Group>

//           <Form.Group>
//            <Form.Label>contraception_methods</Form.Label><br></br>
//            <Form.Control  
//            type="text" 	
//            name="contraception_methods"	
//                placeholder="contraception_methods"
//           value={this.state.contraception_methods}
//           onChange={(e)=>this.setState({contraception_methods:e.target.value})}  					
// />
//           </Form.Group>
//          </Col>
//          </Row>



//        <Row>
//           <Col md="6">
//           <Form.Group>
//          <Form.Label>contraception_methods_type</Form.Label><br></br>
//            <Form.Control  
//            type="text" 	
//            name="contraception_methods_type"	
//                placeholder="contraception_methods_type"
//           value={this.state.contraception_methods_type}
//           onChange={(e)=>this.setState({contraception_methods_type:e.target.value})}  					
// />
//           </Form.Group>
//           <Form.Group>
//            <Form.Label>hormone_treatment</Form.Label><br></br>
//            <Form.Control  
//            type="text" 	
//            name="hormone_treatment"	
//                placeholder="hormone_treatment"
//           value={this.state.hormone_treatment}
//           onChange={(e)=>this.setState({hormone_treatment:e.target.value})}  					
// />
//           </Form.Group>
//           </Col>

//           <Col md="6">
//           <Form.Group>
//            <Form.Label>addiction</Form.Label><br></br>
//            <Form.Control  
//            type="text" 	
//            name="addiction"	
//                placeholder="addiction"
//           value={this.state.addiction}
//           onChange={(e)=>this.setState({addiction:e.target.value})}  					
// />
//           </Form.Group>
//           <Form.Group>
//            <Form.Label>tobacco</Form.Label><br></br>
//            <Form.Control  
//            type="text" 	
//            name="tobacco"	
//                placeholder="tobacco"
//           value={this.state.tobacco}
//           onChange={(e)=>this.setState({tobacco:e.target.value})}  					
// />
//           </Form.Group>

//           </Col>
//           </Row>


//           <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>tobacco_years</Form.Label><br></br>
//           <Form.Control 
//           type="number"
//           name="tobacco_years" 
//              placeholder="tobacco_years"
//           value={this.state.tobacco_years}
//           onChange={(e)=>this.setState({tobacco_years:e.target.value})}  						
// />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>smoking</Form.Label><br></br>
//           <Form.Control 
//           type="text" 	
//           name="smoking"	
//              placeholder="smoking"
//           value={this.state.smoking}
//           onChange={(e)=>this.setState({smoking:e.target.value})}  				
// />
//           </Form.Group>
//           </Col>

//           <Col md="6">
//           <Form.Group>
//           <Form.Label>smoking_years</Form.Label><br></br>
//           <Form.Control 
//           type="number"
//           name="smoking_years" 
//              placeholder="smoking_years"
//           value={this.state.smoking_years}
//           onChange={(e)=>this.setState({smoking_years:e.target.value})}  						
//  />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>alcohol</Form.Label><br></br>
//           <Form.Control 
//           type="text" 	
//           name="alcohol"	
//              placeholder="alcohol"
//           value={this.state.alcohol}
//           onChange={(e)=>this.setState({alcohol:e.target.value})}  				
//  />
//           </Form.Group>
//           </Col>

//           </Row>

//           <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>alcohol_years</Form.Label><br></br>
//           <Form.Control 
//           type="number"
//           name="alcohol_years" 
//              placeholder="alcohol_years"
//           value={this.state.alcohol_years}
//           onChange={(e)=>this.setState({alcohol_years:e.target.value})}  						
//    />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>family_history</Form.Label><br></br>
//           <Form.Control 
//           type="text" 	
//           name="family_history"	
//              placeholder="family_history"
//           value={this.state.family_history}
//           onChange={(e)=>this.setState({family_history:e.target.value})}  				
// />
//           </Form.Group>
//           </Col>


//           <Col md="6">
//           <Form.Group>
//           <Form.Label>comorbidities</Form.Label><br></br>
//           <Form.Control 
//           type="text" 	
//           name="comorbidities"	
//              placeholder="comorbidities"
//           value={this.state.comorbidities}
//           onChange={(e)=>this.setState({comorbidities:e.target.value})}  				
// />
//           </Form.Group>


//           <Form.Group>
//           <Form.Label>breastlump</Form.Label><br></br>
//           <Form.Control 
//           type="text" 	
//           name="breastlump"	
//              placeholder="breastlump"
//           value={this.state.breastlump}
//           onChange={(e)=>this.setState({breastlump:e.target.value})}  				
// />




// </Form.Group>
//           </Col>
//           </Row>


// <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>breastlump_location</Form.Label><br></br>
//          <Form.Control 
//          type="text"

//  name="breastlump_location"        placeholder="breastlump_location"
//           value={this.state.breastlump_location}
//           onChange={(e)=>this.setState({breastlump_location:e.target.value})}   						
//      />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>breastlump_size</Form.Label><br></br>
//          <Form.Control 
//          type="text"

//  name="breastlump_size"        placeholder="breastlump_size"
//           value={this.state.breastlump_size}
//           onChange={(e)=>this.setState({breastlump_size:e.target.value})}   						
// />
//            </Form.Group>
// </Col>





// <Col md="6">
// <Form.Group>
//           <Form.Label>overlying_skin</Form.Label><br></br>
//          <Form.Control 
//          type="text"

//  name="overlying_skin"        placeholder="overlying_skin"
//           value={this.state.overlying_skin}
//           onChange={(e)=>this.setState({overlying_skin:e.target.value})}   						
//   />
//            </Form.Group>

//           <Form.Group>
//           <Form.Label>axillarylump</Form.Label><br></br>
//          <Form.Control 
//          type="text"

//  name="axillarylump"        placeholder="axillarylump"
//           value={this.state.axillarylump}
//           onChange={(e)=>this.setState({axillarylump:e.target.value})}   						
// />
//            </Form.Group>
// </Col>
// </Row>


// <Row>
//           <Col md="6">
//           <Form.Group>

//           <Form.Label>axillarylump_side</Form.Label><br></br>
//          <Form.Control 
//          type="text"

//  name="axillarylump_side"        placeholder="axillarylump_side"
//           value={this.state.axillarylump_side}
//           onChange={(e)=>this.setState({axillarylump_side:e.target.value})}   						
// />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>matted</Form.Label><br></br>
//          <Form.Control 
//          type="text"

//  name="matted"        placeholder="matted"
//           value={this.state.matted}
//           onChange={(e)=>this.setState({matted:e.target.value})}   						
// />
//            </Form.Group>
// </Col>

// <Col md="6">
// <Form.Group>
//           <Form.Label>axillarylump_size</Form.Label><br></br>
//          <Form.Control 
//          type="text"

//  name="axillarylump_size"        placeholder="axillarylump_size"
//           value={this.state.axillarylump_size}
//           onChange={(e)=>this.setState({axillarylump_size:e.target.value})}   						
// />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>nipple_discharge</Form.Label><br></br>
//          <Form.Control 
//          type="text"

//  name="nipple_discharge"        placeholder="nipple_discharge"
//           value={this.state.nipple_discharge}
//           onChange={(e)=>this.setState({nipple_discharge:e.target.value})}   						
//  />
//            </Form.Group>

//           </Col>
//           </Row>

//           <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>nipple_discharge_frequency</Form.Label><br></br>
//          <Form.Control 
//          type="text"

//  name="nipple_discharge_frequency"        placeholder="nipple_discharge_frequency"
//           value={this.state.nipple_discharge_frequency}
//           onChange={(e)=>this.setState({nipple_discharge_frequency:e.target.value})}   						
// />
//            </Form.Group>
//           <Form.Group>
//           <Form.Label>nipple_discharge_colour</Form.Label><br></br>
//          <Form.Control 
//          type="text"

//  name="nipple_discharge_colour"        placeholder="nipple_discharge_colour"
//           value={this.state.nipple_discharge_colour}
//           onChange={(e)=>this.setState({nipple_discharge_colour:e.target.value})}   						
//     />
//            </Form.Group>
// </Col>

// <Col md="6">
// <Form.Group>
//           <Form.Label>mastalgia</Form.Label><br></br>
//          <Form.Control 
//          type="text"

//  name="mastalgia"        placeholder="mastalgia"
//           value={this.state.mastalgia}
//           onChange={(e)=>this.setState({mastalgia:e.target.value})}   						
// />
//            </Form.Group>

//           <Form.Group>
//           <Form.Label>mastitis</Form.Label><br></br>
//          <Form.Control 
//          type="text"

//  name="mastitis"        placeholder="mastitis"
//           value={this.state.mastitis}
//           onChange={(e)=>this.setState({mastitis:e.target.value})}   						
// />
//            </Form.Group>
//  </Col>
// </Row>


// <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>ulcer</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="ulcer"	
//            placeholder="ulcer"
//           value={this.state.ulcer}
//           onChange={(e)=>this.setState({ulcer:e.target.value})}  				
// />
//           </Form.Group>


//           <Form.Group>
//           <Form.Label>nipple_inversion</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="nipple_inversion"	
//            placeholder="nipple_inversion"
//           value={this.state.nipple_inversion}
//           onChange={(e)=>this.setState({nipple_inversion:e.target.value})}  				
// />
//           </Form.Group>


// </Col>
// <Col md="6">
// <Form.Group>
//           <Form.Label>others</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="others"	
//            placeholder="others"
//           value={this.state.others}
//           onChange={(e)=>this.setState({others:e.target.value})}  				
// />
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>duration</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="duration"	
//            placeholder="duration"
//           value={this.state.duration}
//           onChange={(e)=>this.setState({duration:e.target.value})}  				
//    />
//           </Form.Group>
// </Col>
// </Row>


// <Row>

//           <Col md="6">
//           <Form.Group>
//           <Form.Label>pasthistory</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="pasthistory"	
//            placeholder="pasthistory"
//           value={this.state.pasthistory}
//           onChange={(e)=>this.setState({pasthistory:e.target.value})}  				
// />
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>surgical_history</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="surgical_history"	
//            placeholder="surgical_history"
//           value={this.state.surgical_history}
//           onChange={(e)=>this.setState({surgical_history:e.target.value})}  				
// />
//           </Form.Group>
//           </Col>


// <Col md="6">
// <Form.Group>
//         <Form.Label>drug_history</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="drug_history"	
//            placeholder="drug_history"
//           value={this.state.drug_history}
//           onChange={(e)=>this.setState({drug_history:e.target.value})}  				
//  />
//           </Form.Group>


//           <Form.Group>
//           <Form.Label>drug_allergy</Form.Label>
//            <Form.Control 
//            type="text" 	
//            name="drug_allergy"	
//            placeholder="drug_allergy"
//           value={this.state.drug_allergy}
//           onChange={(e)=>this.setState({drug_allergy:e.target.value})}  				
// />
//           </Form.Group>
// </Col>

// </Row>



// <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>drug_allergy_type</Form.Label>
//           <Form.Control 
//           type="text" 	
//           name="drug_allergy_type"	
//           placeholder="drug_allergy_type"
//           value={this.state.drug_allergy_type}
//           onChange={(e)=>this.setState({drug_allergy_type:e.target.value})}  				
//     />
//           </Form.Group>

//           <Form.Group>
//           <Form.Label>bowelhabit</Form.Label>
//           <Form.Control 
//           type="text" 	
//           name="bowelhabit"	
//           placeholder="bowelhabit"
//           value={this.state.bowelhabit}
//           onChange={(e)=>this.setState({bowelhabit:e.target.value})}  				
// />
//           </Form.Group>

// </Col>

// <Col md="6">
// <Form.Group>
//           <Form.Label>bladderhabit</Form.Label>
//           <Form.Control 
//           type="text" 	
//           name="bladderhabit"	
//           placeholder="bladderhabit"
//           value={this.state.bladderhabit}
//           onChange={(e)=>this.setState({bladderhabit:e.target.value})}  				
//      />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>sleep</Form.Label>
//           <Form.Control 
//           type="text" 	
//           name="sleep"	
//           placeholder="sleep"
//           value={this.state.sleep}
//           onChange={(e)=>this.setState({sleep:e.target.value})}  				
// />
//           </Form.Group>

//    </Col>
//    </Row>

// <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>appetite</Form.Label>
//           <Form.Control 
//           type="text" 	
//           name="appetite"	
//           placeholder="appetite"
//           value={this.state.appetite}
//           onChange={(e)=>this.setState({appetite:e.target.value})}  				
// />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>weight</Form.Label>
//           <Form.Control 
//           type="number"
//           name="weight" 
//           placeholder="weight"
//           value={this.state.weight}
//           onChange={(e)=>this.setState({weight:e.target.value})}  						
//   />
//           </Form.Group>
// </Col>
// <Col md="6">
// <Form.Group>
//           <Form.Label>height</Form.Label>
//           <Form.Control 
//           type="number"
//           name="height" 
//           placeholder="height"
//           value={this.state.height}
//           onChange={(e)=>this.setState({height:e.target.value})}  						
// />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>bmi</Form.Label>
//           <Form.Control 
//           type="text" 	
//           name="bmi"	
//           placeholder="bmi"
//           value={this.state.bmi}
//           onChange={(e)=>this.setState({bmi:e.target.value})}  				
// />
//           </Form.Group>
// </Col>
// </Row>


// <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>bp</Form.Label>
//          <Form.Control 
//          type="number"
//          name="bp" 
//          placeholder="bp"
//           value={this.state.bp}
//           onChange={(e)=>this.setState({bp:e.target.value})}  						
// />
//          </Form.Group>


//           <Form.Group>
//           <Form.Label>pulse</Form.Label>
//          <Form.Control 
//          type="number"
//          name="pulse" 
//          placeholder="pulse"
//           value={this.state.pulse}
//           onChange={(e)=>this.setState({pulse:e.target.value})}  						
// />
//          </Form.Group>
//        </Col>
//        <Col md="6">
//        <Form.Group>
//           <Form.Label>temp</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="temp"	
//          placeholder="temp"
//           value={this.state.temp}
//           onChange={(e)=>this.setState({temp:e.target.value})}  				
//   />
//          </Form.Group>
//           <Form.Group>
//           <Form.Label>respiratory_rate</Form.Label>
//          <Form.Control 
//          type="number"
//          name="respiratory_rate" 
//          placeholder="respiratory_rate"
//           value={this.state.respiratory_rate}
//           onChange={(e)=>this.setState({respiratory_rate:e.target.value})}  						
// />
//          </Form.Group>

//           </Col>

//           </Row>

//           <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>health_condition</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="health_condition"	
//          placeholder="health_condition"
//           value={this.state.health_condition}
//           onChange={(e)=>this.setState({health_condition:e.target.value})}  				
// />
//          </Form.Group>
//           <Form.Group>
//           <Form.Label>examination_remarks</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="examination_remarks"	
//          placeholder="examination_remarks"
//           value={this.state.examination_remarks}
//           onChange={(e)=>this.setState({examination_remarks:e.target.value})}  				
// />
//          </Form.Group>
//           </Col>

//           <Col md="6">
//           <Form.Group>
//           <Form.Label>bra_size</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="bra_size"	
//          placeholder="bra_size"
//           value={this.state.bra_size}
//           onChange={(e)=>this.setState({bra_size:e.target.value})}  				
//  />
//          </Form.Group>
//           <Form.Group>
//           <Form.Label>usg</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="usg"	
//          placeholder="usg"
//           value={this.state.usg}
//           onChange={(e)=>this.setState({usg:e.target.value})}  				
// />
//          </Form.Group>
// </Col>
// </Row>




// <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>mmg</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="mmg"	
//          placeholder="mmg"
//           value={this.state.mmg}
//           onChange={(e)=>this.setState({mmg:e.target.value})}  				
//  />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>mri</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="mri"	
//          placeholder="mri"
//           value={this.state.mri}
//           onChange={(e)=>this.setState({mri:e.target.value})}  				
//  />
//           </Form.Group>
//           </Col>
//           <Col md="6">

//           <Form.Group>
//           <Form.Label>fnac</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="fnac"	
//          placeholder="fnac"
//           value={this.state.fnac}
//           onChange={(e)=>this.setState({fnac:e.target.value})}  				
//   />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>core_biopsy</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="core_biopsy"	
//          placeholder="core_biopsy"
//           value={this.state.core_biopsy}
//           onChange={(e)=>this.setState({core_biopsy:e.target.value})}  				
// />

//           </Form.Group>
//         </Col>
//         </Row>

//         <Row>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>incision_biopsy</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="incision_biopsy"	
//          placeholder="incision_biopsy"
//           value={this.state.incision_biopsy}
//           onChange={(e)=>this.setState({incision_biopsy:e.target.value})}  				
//  />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>investigation_remarks</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="investigation_remarks"	
//          placeholder="investigation_remarks"
//           value={this.state.investigation_remarks}
//           onChange={(e)=>this.setState({investigation_remarks:e.target.value})}  				
//  />
//           </Form.Group>
//           </Col>

//           <Col md="6">
//           <Form.Group>
//           <Form.Label>blood_investigation</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="blood_investigation"	
//          placeholder="blood_investigation"
//           value={this.state.blood_investigation}
//           onChange={(e)=>this.setState({blood_investigation:e.target.value})}  				
// />
//           </Form.Group>
//           <Form.Group>
//           <Form.Label>diagnosis</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="diagnosis"	
//          placeholder="diagnosis"
//           value={this.state.diagnosis}
//           onChange={(e)=>this.setState({diagnosis:e.target.value})}  				
// />
//           </Form.Group>
//           </Col>
//           </Row>




//         <Row>

//           <Col md="6">
//           <Form.Group>
//           <Form.Label>treatment_plan</Form.Label>
//          <Form.Control 
//          type="text" 	
//          name="treatment_plan"	
//          placeholder="treatment_plan"
//           value={this.state.treatment_plan}
//           onChange={(e)=>this.setState({treatment_plan:e.target.value})}  				
//   />
//           </Form.Group>

//           </Col>
//           <Col md="6">
//           <Form.Group>
//           <Form.Label>User Id</Form.Label>
//          <Form.Control 
//          type="number"
//          name="user_id" 
//          placeholder="User Id"
//           value={this.state.user_id}
//           onChange={(e)=>this.setState({user_id:e.target.value})}  	
//           disabled					
// />
//           </Form.Group>

// </Col>
// </Row>



// <Button
//                     className="btn-fill pull-right"
//                     type="submit"
//                     name=""
//                     variant="info"
//                   >
//                     Update User
//                   </Button>





//                        </Form>
//                        </Card.Body>
//                        </Card>
//                        </Col>
//                        </Row>

//       </Container>
//     </>
//   );
// }
// }

// export default User;
