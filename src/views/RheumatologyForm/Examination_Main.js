

import React, { Component } from 'react';

import Examination from './Examination';
import Mus_examination from './Mus_examination'
import Rom_examination from './Rom_examination'
import Soft_tis_examination from './Soft_tis_examination'
import Deformities_examination from './Deformities_examination';
import Hypermobility_examination from './Hypermobility_examination';
// import Health_ass_ques from './Health_ass_ques';
// import Inves_Hematological from './Inves_Hematological';

// import Inves_Biochemical from './Inves_Biochemical';
// import Inves_Immonological from './Inves_Immonological';
// import Inves_Radiological from './Inves_Radiological';
// import Inves_Radiological from './Inves_Radiological';
import Summary from './Examination_Summary';


import axios from 'axios';
// import { NULL } from 'node-sass';

class Examination_Main extends Component {
    state = {
        step:1, 
        sid:0,

general_condition:'',weight:'',height:'',bmi:'',bp:'',pulse:'',temp:'',respiratory_rate:'',ex_health_condition:'',ex_remarks:'',eyes:'',skin_morphology:'',skin_pattern:'',skin_color:'',distribution:'',other_affects:'',mucosa:'',hair_loss:'',dandruff:'',nail:'',lymphnodes:'',lymphnodes_number:'',tender:'',tender_type:'',lymphnodes_tender_others:'',vascular:'',cns_examination:'',cvs_examination:'',chest_examination:'',abdomen_examination:'',tm_t1:'',tm_s1:'',tm_t2:'',tm_s2:'',scl_t1:'',scl_s1:'',scl_t2:'',scl_s2:'',acl_t1:'',acl_s1:'',acl_t2:'',acl_s2:'',sh_t1:'',sh_s1:'',sh_t2:'',sh_s2:'',elbow_t1:'',elbow_s1:'',elbow_t2:'',elbow_s2:'',infru_t1:'',infru_s1:'',infru_t2:'',infru_s2:'',cmc1_t1:'',cmc1_s1:'',cmc1_t2:'',cmc1_s2:'',wrist_t1:'',wrist_s1:'',wrist_t2:'',wrist_s2:'',dip2_t1:'',dip2_s1:'',dip2_t2:'',dip2_s2:'',dip3_t1:'',dip3_s1:'',dip3_t2:'',dip3_s2:'',dip4_t1:'',dip4_s1:'',dip4_t2:'',dip4_s2:'',dip5_t1:'',dip5_s1:'',dip5_t2:'',dip5_s2:'',ip1_t1:'',ip1_s1:'',ip1_t2:'',ip1_s2:'',ip2_t1:'',ip2_s1:'',ip2_t2:'',ip2_s2:'',pip3_t1:'',pip3_s1:'',pip3_t2:'',pip3_s2:'',pip4_t1:'',pip4_s1:'',pip4_t2:'',pip4_s2:'',pip5_t1:'',pip5_s1:'',pip5_t2:'',pip5_s2:'',mcp1_t1:'',mcp1_s1:'',mcp1_t2:'',mcp1_s2:'',mcp2_t1:'',mcp2_s1:'',mcp2_t2:'',mcp2_s2:'',mcp3_t1:'',mcp3_s1:'',mcp3_t2:'',mcp3_s2:'',mcp4_t1:'',mcp4_s1:'',mcp4_t2:'',mcp4_s2:'',mcp5_t1:'',mcp5_s1:'',mcp5_t2:'',mcp5_s2:'',hip_t1:'',hip_s1:'',hip_t2:'',hip_s2:'',knee_t1:'',knee_s1:'',knee_t2:'',knee_s2:'',a_tt_t1:'',a_tt_s1:'',a_tt_t2:'',a_tt_s2:'',a_tc_t1:'',a_tc_s1:'',a_tc_t2:'',a_tc_s2:'',mtl_t1:'',mtl_s1:'',mtl_t2:'',mtl_s2:'',mtp1_t1:'',mtp1_s1:'',mtp1_t2:'',mtp1_s2:'',mtp2_t1:'',mtp2_s1:'',mtp2_t2:'',mtp2_s2:'',mtp3_t1:'',mtp3_s1:'',mtp3_t2:'',mtp3_s2:'',mtp4_t1:'',mtp4_s1:'',mtp4_t2:'',mtp4_s2:'',mtp5_t1:'',mtp5_s1:'',mtp5_t2:'',mtp5_s2:'',ip1b_t1:'',ip1b_s1:'',ip1b_t2:'',ip1b_s2:'',ip2b_t1:'',ip2b_s1:'',ip2b_t2:'',ip2b_s2:'',ip3b_t1:'',ip3b_s1:'',ip3b_t2:'',ip3b_s2:'',ip4b_t1:'',ip4b_s1:'',ip4b_t2:'',ip4b_s2:'',ip5b_t1:'',ip5b_s1:'',ip5b_t2:'',ip5b_s2:'',s1_t1:'',s1_s1:'',s1_t2:'',s1_s2:'',cervical_anky:'',cervical_flex:'',cervical_ext:'',cervical_rtrot:'',cervical_ltrot:'',cervical_rtfl:'',cervical_ltfl:'',cervical_pt:'',thorasic_anky:'',thorasic_flex:'',thorasic_ext:'',thorasic_rtrot:'',thorasic_ltrot:'',thorasic_rtfl:'',thorasic_ltfl:'',thorasic_pt:'',lumbar_anky:'',lumbar_flex:'',lumbar_ext:'',lumbar_rtrot:'',lumbar_ltrot:'',lumbar_rtfl:'',lumbar_ltfl:'',lumbar_pt:'',opt_rt:'',opt_lt:'',lcer_rt:'',lcer_lt:'',trpz_rt:'',trpz_lt:'',scap_rt:'',scap_lt:'',zcst_rt:'',zcst_lt:'',epdl_rt:'',epdl_lt:'',glut_rt:'',glut_lt:'',trcr_rt:'',trcr_lt:'',knee_rt:'',knee_lt:'',ta_rt:'',ta_lt:'',calf_rt:'',calf_lt:'',sole_rt:'',sole_lt:'',rhand_fl:'',rhand_ex:'',rhand_ab:'',rhand_add:'',lhand_fl:'',lhand_ex:'',lhand_ab:'',lhand_add:'',rwrist_fl:'',rwrist_ex:'',rwrist_ab:'',rwrist_add:'',lwrist_fl:'',lwrist_ex:'',lwrist_ab:'',lwrist_add:'',relb_fl:'',relb_ex:'',relb_ab:'',relb_add:'',shrt_fl:'',shrt_ex:'',shrt_ab:'',shrt_add:'',shlt_fl:'',shlt_ex:'',shlt_ab:'',shlt_add:'',kneert_fl:'',kneert_ex:'',kneert_ab:'',kneert_add:'',kneelt_fl:'',kneelt_ex:'',kneelt_ab:'',kneelt_add:'',footrt_fl:'',footrt_ex:'',footrt_ab:'',footrt_add:'',footlt_fl:'',footlt_ex:'',footlt_ab:'',footlt_add:'',thumb_rt:'',thumb_lt:'',finger_rt:'',finger_lt:'',palm_rt:'',palm_lt:'',elbow_rt:'',elbow_lt:'',hy_knee_rt:'',hy_knee_lt:'',ankle_rt:'',ankle_lt:'',other_rt:'',other_lt:'',spine_rt:'',spine_lt:'',pid:'',user_id:''
      }

      nextStep = () => {
        const { step } = this.state
        this.setState({
          step: step + 1
        })
      }

  
      prevStep = () => {
        const { step } = this.state
        this.setState({
          step: step - 1
        })
      }
    
      dataSend = (pid) =>
      {
        pid=1
        const { sid } = this.state
        this.setState({
          sid: sid + 1
        })
      }
  
      handleChange = input => event => {
        this.setState({ [input]: event.target.value })
      }




      saveAll = () => {

        let Obj={
       
            general_condition:this.state.general_condition,
            weight:this.state.weight,
            height:this.state.height,
            bmi:this.state.bmi,
            bp:this.state.bp,
            pulse:this.state.pulse,
            temp:this.state.temp,
            respiratory_rate:this.state.respiratory_rate,
            ex_health_condition:this.state.ex_health_condition,
            ex_remarks:this.state.ex_remarks,
            eyes:this.state.eyes,
            skin_morphology:this.state.skin_morphology,
    
    
    
            skin_pattern:this.state.skin_pattern,
            skin_color:this.state.skin_color,
            distribution:this.state.distribution,
            other_affects:this.state.other_affects,
            mucosa:this.state.mucosa,
    
    
    
            hair_loss:this.state.hair_loss,
            dandruff:this.state.dandruff,
            nail:this.state.nail,
            lymphnodes:this.state.lymphnodes,
            lymphnodes_number:this.state.lymphnodes_number,
    
    
    
    
            tender:this.state.tender,
            tender_type:this.state.tender_type,
            lymphnodes_tender_others:this.state.lymphnodes_tender_others,
            vascular:this.state.vascular,
            cns_examination:this.state.cns_examination,
            cvs_examination:this.state.cvs_examination,
            chest_examination:this.state.chest_examination,
            abdomen_examination:this.state.abdomen_examination,
    
    
            tm_t1:this.state.tm_t1,
            tm_s1:this.state.tm_s1,
            tm_t2:this.state.tm_t2,
            tm_s2:this.state.tm_s2,
    
            scl_t1:this.state.scl_t1,
            scl_s1:this.state.scl_s1,
            scl_t2:this.state.scl_t2,
            scl_s2:this.state.scl_s2,
    
    
    
            acl_t1:this.state.acl_t1,
            acl_s1:this.state.acl_s1,
            acl_t2:this.state.acl_t2,
            acl_s2:this.state.acl_s2,
    
    
            sh_t1:this.state.sh_t1,
            sh_s1:this.state.sh_s1,
            sh_t2:this.state.sh_t2,
            sh_s2:this.state.sh_s2,
    
            elbow_t1:this.state.elbow_t1,
            elbow_s1:this.state.elbow_s1,
            elbow_t2:this.state.elbow_t2,
            elbow_s2:this.state.elbow_s2,
    
    
            infru_t1:this.state.infru_t1,
            infru_s1:this.state.infru_s1,
            infru_t2:this.state.infru_t2,
            infru_s2:this.state.infru_s2,
    
    
            cmc1_t1:this.state.cmc1_t1,
            cmc1_s1:this.state.cmc1_s1,
            cmc1_t2:this.state.cmc1_t2,
            cmc1_s2:this.state.cmc1_s2,
    
    
    
            wrist_t1:this.state.wrist_t1,
            wrist_s1:this.state.wrist_s1,
            wrist_t2:this.state.wrist_t2,
            wrist_s2:this.state.wrist_s2,
    
    
            dip2_t1:this.state.dip2_t1,
            dip2_s1:this.state.dip2_s1,
            dip2_t2:this.state.dip2_t2,
            dip2_s2:this.state.dip2_s2,
    
    
            dip3_t1:this.state.dip3_t1,
            dip3_s1:this.state.dip3_s1,
            dip3_t2:this.state.dip3_t2,
            dip3_s2:this.state.dip3_s2,
    
    
            dip4_t1:this.state.dip4_t1,
            dip4_s1:this.state.dip4_s1,
            dip4_t2:this.state.dip4_t2,
            dip4_s2:this.state.dip4_s2,
    
    
    
            dip5_t1:this.state.dip5_t1,
            dip5_s1:this.state.dip5_s1,
            dip5_t2:this.state.dip5_t2,
            dip5_s2:this.state.dip5_s2,
    
    
            ip1_t1:this.state.ip1_t1,
            ip1_s1:this.state.ip1_s1,
            ip1_t2:this.state.ip1_t2,
            ip1_s1:this.state.ip1_s2,
    
    
            ip2_t1:this.state.ip2_t1,
            ip2_s1:this.state.ip2_s1,
            ip2_t2:this.state.ip2_t2,
            ip2_s1:this.state.ip2_s2,
    
    
            pip3_t1:this.state.pip3_t1,
            pip3_s1:this.state.pip3_s1,
            pip3_t2:this.state.pip3_t2,
            pip3_s1:this.state.pip3_s2,
    
    
            pip4_t1:this.state.pip4_t1,
            pip4_s1:this.state.pip4_s1,
            pip4_t2:this.state.pip4_t2,
            pip4_s1:this.state.pip4_s2,
    
            pip5_t1:this.state.pip5_t1,
            pip5_s1:this.state.pip5_s1,
            pip5_t2:this.state.pip5_t2,
            pip5_s1:this.state.pip5_s2,
    
            mcp1_t1:this.state.mcp1_t1,
            mcp1_s1:this.state.mcp1_s1,
            mcp1_t2:this.state.mcp1_t2,
            mcp1_s1:this.state.mcp1_s2,
    
    
            mcp2_t1:this.state.mcp2_t1,
            mcp2_s1:this.state.mcp2_s1,
            mcp2_t2:this.state.mcp2_t2,
            mcp2_s1:this.state.mcp2_s2,
    
            mcp3_t1:this.state.mcp3_t1,
            mcp3_s1:this.state.mcp3_s1,
            mcp3_t2:this.state.mcp3_t2,
            mcp3_s1:this.state.mcp3_s2,
    
    
            mcp4_t1:this.state.mcp4_t1,
            mcp4_s1:this.state.mcp4_s1,
            mcp4_t2:this.state.mcp4_t2,
            mcp4_s1:this.state.mcp4_s2,
    
            mcp5_t1:this.state.mcp5_t1,
            mcp5_s1:this.state.mcp5_s1,
            mcp5_t2:this.state.mcp5_t2,
            mcp5_s1:this.state.mcp5_s2,
    
            hip_t1:this.state.hip_t1,
            hip_s1:this.state.hip_s1,
            hip_t2:this.state.hip_t2,
            hip_s1:this.state.hip_s2,
    
    
            knee_t1:this.state.knee_t1,
            knee_s1:this.state.knee_s1,
            knee_t2:this.state.knee_t2,
            knee_s1:this.state.knee_s2,
    
    
            a_tt_t1:this.state.a_tt_t1,
            a_tt_s1:this.state.a_tt_s1,
            a_tt_t2:this.state.a_tt_t2,
            a_tt_s1:this.state.a_tt_s2,
    
            a_tc_t1:this.state.a_tc_t1,
            a_tc_s1:this.state.a_tc_s1,
            a_tc_t2:this.state.a_tc_t2,
            a_tc_s1:this.state.a_tc_s2,
    
            mtl_t1:this.state.mtl_t1,
            mtl_s1:this.state.mtl_s1,
            mtl_t2:this.state.mtl_t2,
            mtl_s1:this.state.mtl_s2,
    
    
            mtp1_t1:this.state.mtp1_t1,
            mtp1_s1:this.state.mtp1_s1,
            mtp1_t2:this.state.mtp1_t2,
            mtp1_s1:this.state.mtp1_s2,
    
    
            mtp2_t1:this.state.mtp2_t1,
            mtp2_s1:this.state.mtp2_s1,
            mtp2_t2:this.state.mtp2_t2,
            mtp2_s1:this.state.mtp2_s2,
    
    
            mtp3_t1:this.state.mtp3_t1,
            mtp3_s1:this.state.mtp3_s1,
            mtp3_t2:this.state.mtp3_t2,
            mtp3_s1:this.state.mtp3_s2,
    
    
            mtp4_t1:this.state.mtp4_t1,
            mtp4_s1:this.state.mtp4_s1,
            mtp4_t2:this.state.mtp4_t2,
            mtp4_s1:this.state.mtp4_s2,
    
    
            mtp5_t1:this.state.mtp5_t1,
            mtp5_s1:this.state.mtp5_s1,
            mtp5_t2:this.state.mtp5_t2,
            mtp5_s1:this.state.mtp5_s2,
    
    
            ip1b_t1:this.state.ip1b_t1,
            ip1b_s1:this.state.ip1b_s1,
            ip1b_t2:this.state.ip1b_t2,
            ip1b_s1:this.state.ip1b_s2,
    
    
            ip2b_t1:this.state.ip2b_t1,
            ip2b_s1:this.state.ip2b_s1,
            ip2b_t2:this.state.ip2b_t2,
            ip2b_s1:this.state.ip2b_s2,
    
    
            ip3b_t1:this.state.ip3b_t1,
            ip3b_s1:this.state.ip3b_s1,
            ip3b_t2:this.state.ip3b_t2,
            ip3b_s1:this.state.ip3b_s2,
    
            ip4b_t1:this.state.ip4b_t1,
            ip4b_s1:this.state.ip4b_s1,
            ip4b_t2:this.state.ip4b_t2,
            ip4b_s1:this.state.ip4b_s2,
    
    
            ip5b_t1:this.state.ip5b_t1,
            ip5b_s1:this.state.ip5b_s1,
            ip5b_t2:this.state.ip5b_t2,
            ip5b_s1:this.state.ip5b_s2,
    
    
            s1_t1:this.state.s1_t1,
            s1_s1:this.state.s1_s1,
            s1_t2:this.state.s1_t2,
            s1_s1:this.state.s1_s2,
    
    
            cervical_anky:this.state.cervical_anky,
            cervical_flex:this.state.cervical_flex,
            cervical_ext:this.state.cervical_ext,
            cervical_rtrot:this.state.cervical_rtrot,
    
            cervical_ltrot:this.state.cervical_ltrot,
            cervical_rtfl:this.state.cervical_rtfl,
            cervical_ltfl:this.state.cervical_ltfl,
            cervical_pt:this.state.cervical_pt,
    
    
            thorasic_anky:this.state.thorasic_anky,
            thorasic_flex:this.state.thorasic_flex,
            thorasic_ext:this.state.thorasic_ext,
            thorasic_rtrot:this.state.thorasic_rtrot,
    
            thorasic_ltrot:this.state.thorasic_ltrot,
            thorasic_rtfl:this.state.thorasic_rtfl,
            thorasic_ltfl:this.state.thorasic_ltfl,
            thorasic_pt:this.state.thorasic_pt,
    
    
    
    
    
            lumbar_anky:this.state.lumbar_anky,
            lumbar_flex:this.state.lumbar_flex,
            lumbar_ext:this.state.lumbar_ext,
            lumbar_rtrot:this.state.lumbar_rtrot,
            lumbar_ltrot:this.state.lumbar_ltrot,
    
    
    
            lumbar_rtfl:this.state.lumbar_rtfl,
            lumbar_ltfl:this.state.lumbar_ltfl,
            lumbar_pt:this.state.lumbar_pt,
    
    
            opt_rt:this.state.opt_rt,
            opt_lt:this.state.opt_lt,
    
            lcer_rt:this.state.lcer_rt,
            lcer_lt:this.state.lcer_lt,
            trpz_rt:this.state.trpz_rt,
    
    
            trpz_lt:this.state.trpz_lt,
            scap_rt:this.state.scap_rt,
            scap_lt:this.state.scap_lt,
     
            zcst_rt:this.state.zcst_rt,
            zcst_lt:this.state.zcst_lt,
            epdl_rt:this.state.epdl_rt,
    
    
            epdl_lt:this.state.epdl_lt,
            glut_rt:this.state.glut_rt,
            glut_lt:this.state.glut_lt,
    
    
            trcr_rt:this.state.trcr_rt,
            trcr_lt:this.state.trcr_lt,
            knee_rt:this.state.knee_rt,
    
    
    
    
            knee_lt:this.state.knee_lt,
            ta_rt:this.state.ta_rt,
            ta_lt:this.state.ta_lt,
    
    
            calf_rt:this.state.calf_rt,
            calf_lt:this.state.calf_lt,
            sole_rt:this.state.sole_rt,
    
    
            sole_lt:this.state.sole_lt,
            rhand_fl:this.state.rhand_fl,
            rhand_ex:this.state.rhand_ex,
    
    
    
    
            rhand_ab:this.state.rhand_ab,
            rhand_add:this.state.rhand_add,
            lhand_fl:this.state.lhand_fl,
    
    
            lhand_ex:this.state.lhand_ex,
            lhand_ab:this.state.lhand_ab,
            lhand_add:this.state.lhand_add,
    
    
    
            rwrist_fl:this.state.rwrist_fl,
            rwrist_ex:this.state.rwrist_ex,
            rwrist_ab:this.state.rwrist_ab,
    
    
            rwrist_add:this.state.rwrist_add,
            lwrist_fl:this.state.lwrist_fl,
            lwrist_ex:this.state.lwrist_ex,
    
    
    
            lwrist_ab:this.state.lwrist_ab,
            lwrist_add:this.state.lwrist_add,
            relb_fl:this.state.relb_fl,
    
    
            relb_ex:this.state.relb_ex,
            relb_ab:this.state.relb_ab,
            relb_add:this.state.relb_add,
    
    
            shrt_fl:this.state.shrt_fl,
            shrt_ex:this.state.shrt_ex,
            shrt_ab:this.state.shrt_ab,
    
    
            shrt_add:this.state.shrt_add,
            shlt_fl:this.state.shlt_fl,
            shlt_ex:this.state.shlt_ex,
    
    
    
            shlt_ab:this.state.shlt_ab,
            shlt_add:this.state.shlt_add,
            kneert_fl:this.state.kneert_fl,
    
    
            kneert_ex:this.state.kneert_ex,
            kneert_ab:this.state.kneert_ab,
            kneert_add:this.state.kneert_add,
    
    
    
    
            kneelt_fl:this.state.kneelt_fl,
            kneelt_ex:this.state.kneelt_ex,
            kneelt_ab:this.state.kneelt_ab,
    
    
    
            kneelt_add:this.state.kneelt_add,
            footrt_fl:this.state.footrt_fl,
            footrt_ex:this.state.footrt_ex,
    
    
            footrt_ab:this.state.footrt_ab,
            footrt_add:this.state.footrt_add,
            footlt_fl:this.state.footlt_fl,
    
    
            footlt_ex:this.state.footlt_ex,
            footlt_ab:this.state.footlt_ab,
            footlt_add:this.state.footlt_add,
    
    
    
            thumb_rt:this.state.thumb_rt,
            thumb_lt:this.state.thumb_lt,
            finger_rt:this.state.finger_rt,
    
    
            finger_lt:this.state.finger_lt,
            palm_rt:this.state.palm_rt,
            palm_lt:this.state.palm_lt,
    
    
    
    
            elbow_rt:this.state.elbow_rt,
            elbow_lt:this.state.elbow_lt,
            hy_knee_rt:this.state.hy_knee_rt,
    
    
    
            hy_knee_lt:this.state.hy_knee_lt,
            ankle_rt:this.state.ankle_rt,
            ankle_lt:this.state.ankle_lt,
    
    
            other_rt:this.state.other_rt,
            other_lt:this.state.other_lt,
            spine_rt:this.state.spine_rt,
    
    
    
    
    
    
    
            spine_lt:this.state.spine_lt,
            pid:this.state.pid,
      
       

            user_id:this.state.user_id,



          
        }
            axios.post(`https://abcapi.vidaria.in/addbpatient`,Obj)
            .then(res => {
            console.log(res);
            console.log(res.data);
      })
      
      }

  
	render() {
		const { step } = this.state;
    console.log(step)
    const { sid } = this.state;
    console.log(sid)
		const {
general_condition,weight,height,bmi,bp,pulse,temp,respiratory_rate,ex_health_condition,ex_remarks,eyes,skin_morphology,skin_pattern,skin_color,distribution,other_affects,mucosa,hair_loss,dandruff,nail,lymphnodes,lymphnodes_number,tender,tender_type,lymphnodes_tender_others,vascular,cns_examination,cvs_examination,chest_examination,abdomen_examination,tm_t1,tm_s1,tm_t2,tm_s2,scl_t1,scl_s1,scl_t2,scl_s2,acl_t1,acl_s1,acl_t2,acl_s2,sh_t1,sh_s1,sh_t2,sh_s2,elbow_t1,elbow_s1,elbow_t2,elbow_s2,infru_t1,infru_s1,infru_t2,infru_s2,cmc1_t1,cmc1_s1,cmc1_t2,cmc1_s2,wrist_t1,wrist_s1,wrist_t2,wrist_s2,dip2_t1,dip2_s1,dip2_t2,dip2_s2,dip3_t1,dip3_s1,dip3_t2,dip3_s2,dip4_t1,dip4_s1,dip4_t2,dip4_s2,dip5_t1,dip5_s1,dip5_t2,dip5_s2,ip1_t1,ip1_s1,ip1_t2,ip1_s2,ip2_t1,ip2_s1,ip2_t2,ip2_s2,pip3_t1,pip3_s1,pip3_t2,pip3_s2,pip4_t1,pip4_s1,pip4_t2,pip4_s2,pip5_t1,pip5_s1,pip5_t2,pip5_s2,mcp1_t1,mcp1_s1,mcp1_t2,mcp1_s2,mcp2_t1,mcp2_s1,mcp2_t2,mcp2_s2,mcp3_t1,mcp3_s1,mcp3_t2,mcp3_s2,mcp4_t1,mcp4_s1,mcp4_t2,mcp4_s2,mcp5_t1,mcp5_s1,mcp5_t2,mcp5_s2,hip_t1,hip_s1,hip_t2,hip_s2,knee_t1,knee_s1,knee_t2,knee_s2,a_tt_t1,a_tt_s1,a_tt_t2,a_tt_s2,a_tc_t1,a_tc_s1,a_tc_t2,a_tc_s2,mtl_t1,mtl_s1,mtl_t2,mtl_s2,mtp1_t1,mtp1_s1,mtp1_t2,mtp1_s2,mtp2_t1,mtp2_s1,mtp2_t2,mtp2_s2,mtp3_t1,mtp3_s1,mtp3_t2,mtp3_s2,mtp4_t1,mtp4_s1,mtp4_t2,mtp4_s2,mtp5_t1,mtp5_s1,mtp5_t2,mtp5_s2,ip1b_t1,ip1b_s1,ip1b_t2,ip1b_s2,ip2b_t1,ip2b_s1,ip2b_t2,ip2b_s2,ip3b_t1,ip3b_s1,ip3b_t2,ip3b_s2,ip4b_t1,ip4b_s1,ip4b_t2,ip4b_s2,ip5b_t1,ip5b_s1,ip5b_t2,ip5b_s2,s1_t1,s1_s1,s1_t2,s1_s2,cervical_anky,cervical_flex,cervical_ext,cervical_rtrot,cervical_ltrot,cervical_rtfl,cervical_ltfl,cervical_pt,thorasic_anky,thorasic_flex,thorasic_ext,thorasic_rtrot,thorasic_ltrot,thorasic_rtfl,thorasic_ltfl,thorasic_pt,lumbar_anky,lumbar_flex,lumbar_ext,lumbar_rtrot,lumbar_ltrot,lumbar_rtfl,lumbar_ltfl,lumbar_pt,opt_rt,opt_lt,lcer_rt,lcer_lt,trpz_rt,trpz_lt,scap_rt,scap_lt,zcst_rt,zcst_lt,epdl_rt,epdl_lt,glut_rt,glut_lt,trcr_rt,trcr_lt,knee_rt,knee_lt,ta_rt,ta_lt,calf_rt,calf_lt,sole_rt,sole_lt,rhand_fl,rhand_ex,rhand_ab,rhand_add,lhand_fl,lhand_ex,lhand_ab,lhand_add,rwrist_fl,rwrist_ex,rwrist_ab,rwrist_add,lwrist_fl,lwrist_ex,lwrist_ab,lwrist_add,relb_fl,relb_ex,relb_ab,relb_add,shrt_fl,shrt_ex,shrt_ab,shrt_add,shlt_fl,shlt_ex,shlt_ab,shlt_add,kneert_fl,kneert_ex,kneert_ab,kneert_add,kneelt_fl,kneelt_ex,kneelt_ab,kneelt_add,footrt_fl,footrt_ex,footrt_ab,footrt_add,footlt_fl,footlt_ex,footlt_ab,footlt_add,thumb_rt,thumb_lt,finger_rt,finger_lt,palm_rt,palm_lt,elbow_rt,elbow_lt,hy_knee_rt,hy_knee_lt,ankle_rt,ankle_lt,other_rt,other_lt,spine_rt,spine_lt,pid,user_id
      
      } = this.state;
    
		const values = { 
general_condition,weight,height,bmi,bp,pulse,temp,respiratory_rate,ex_health_condition,ex_remarks,eyes,skin_morphology,skin_pattern,skin_color,distribution,other_affects,mucosa,hair_loss,dandruff,nail,lymphnodes,lymphnodes_number,tender,tender_type,lymphnodes_tender_others,vascular,cns_examination,cvs_examination,chest_examination,abdomen_examination,tm_t1,tm_s1,tm_t2,tm_s2,scl_t1,scl_s1,scl_t2,scl_s2,acl_t1,acl_s1,acl_t2,acl_s2,sh_t1,sh_s1,sh_t2,sh_s2,elbow_t1,elbow_s1,elbow_t2,elbow_s2,infru_t1,infru_s1,infru_t2,infru_s2,cmc1_t1,cmc1_s1,cmc1_t2,cmc1_s2,wrist_t1,wrist_s1,wrist_t2,wrist_s2,dip2_t1,dip2_s1,dip2_t2,dip2_s2,dip3_t1,dip3_s1,dip3_t2,dip3_s2,dip4_t1,dip4_s1,dip4_t2,dip4_s2,dip5_t1,dip5_s1,dip5_t2,dip5_s2,ip1_t1,ip1_s1,ip1_t2,ip1_s2,ip2_t1,ip2_s1,ip2_t2,ip2_s2,pip3_t1,pip3_s1,pip3_t2,pip3_s2,pip4_t1,pip4_s1,pip4_t2,pip4_s2,pip5_t1,pip5_s1,pip5_t2,pip5_s2,mcp1_t1,mcp1_s1,mcp1_t2,mcp1_s2,mcp2_t1,mcp2_s1,mcp2_t2,mcp2_s2,mcp3_t1,mcp3_s1,mcp3_t2,mcp3_s2,mcp4_t1,mcp4_s1,mcp4_t2,mcp4_s2,mcp5_t1,mcp5_s1,mcp5_t2,mcp5_s2,hip_t1,hip_s1,hip_t2,hip_s2,knee_t1,knee_s1,knee_t2,knee_s2,a_tt_t1,a_tt_s1,a_tt_t2,a_tt_s2,a_tc_t1,a_tc_s1,a_tc_t2,a_tc_s2,mtl_t1,mtl_s1,mtl_t2,mtl_s2,mtp1_t1,mtp1_s1,mtp1_t2,mtp1_s2,mtp2_t1,mtp2_s1,mtp2_t2,mtp2_s2,mtp3_t1,mtp3_s1,mtp3_t2,mtp3_s2,mtp4_t1,mtp4_s1,mtp4_t2,mtp4_s2,mtp5_t1,mtp5_s1,mtp5_t2,mtp5_s2,ip1b_t1,ip1b_s1,ip1b_t2,ip1b_s2,ip2b_t1,ip2b_s1,ip2b_t2,ip2b_s2,ip3b_t1,ip3b_s1,ip3b_t2,ip3b_s2,ip4b_t1,ip4b_s1,ip4b_t2,ip4b_s2,ip5b_t1,ip5b_s1,ip5b_t2,ip5b_s2,s1_t1,s1_s1,s1_t2,s1_s2,cervical_anky,cervical_flex,cervical_ext,cervical_rtrot,cervical_ltrot,cervical_rtfl,cervical_ltfl,cervical_pt,thorasic_anky,thorasic_flex,thorasic_ext,thorasic_rtrot,thorasic_ltrot,thorasic_rtfl,thorasic_ltfl,thorasic_pt,lumbar_anky,lumbar_flex,lumbar_ext,lumbar_rtrot,lumbar_ltrot,lumbar_rtfl,lumbar_ltfl,lumbar_pt,opt_rt,opt_lt,lcer_rt,lcer_lt,trpz_rt,trpz_lt,scap_rt,scap_lt,zcst_rt,zcst_lt,epdl_rt,epdl_lt,glut_rt,glut_lt,trcr_rt,trcr_lt,knee_rt,knee_lt,ta_rt,ta_lt,calf_rt,calf_lt,sole_rt,sole_lt,rhand_fl,rhand_ex,rhand_ab,rhand_add,lhand_fl,lhand_ex,lhand_ab,lhand_add,rwrist_fl,rwrist_ex,rwrist_ab,rwrist_add,lwrist_fl,lwrist_ex,lwrist_ab,lwrist_add,relb_fl,relb_ex,relb_ab,relb_add,shrt_fl,shrt_ex,shrt_ab,shrt_add,shlt_fl,shlt_ex,shlt_ab,shlt_add,kneert_fl,kneert_ex,kneert_ab,kneert_add,kneelt_fl,kneelt_ex,kneelt_ab,kneelt_add,footrt_fl,footrt_ex,footrt_ab,footrt_add,footlt_fl,footlt_ex,footlt_ab,footlt_add,thumb_rt,thumb_lt,finger_rt,finger_lt,palm_rt,palm_lt,elbow_rt,elbow_lt,hy_knee_rt,hy_knee_lt,ankle_rt,ankle_lt,other_rt,other_lt,spine_rt,spine_lt,pid,user_id
    
    };
		switch (step) {
	// 		case 1:
	// 			return <BasicInfo
	// 				nextStep={this.nextStep}
	// 				handleChange={this.handleChange}
	// 				values={values}
	// 			/>


    //   case 2:
    //     return <Complaints
    //         nextStep={this.nextStep}
    //         prevStep={this.prevStep}
    //         // saveAll={this.saveAll.bind(this)} 
    //         handleChange={this.handleChange}
    //         values={values}
    //       />

    //   case 3:
    //     return <Summary 
    //              nextStep={this.nextStep}
    //              prevStep={this.prevStep}
    //              values={values}
    //              saveAll={this.saveAll.bind(this)} 
    //              />
      case 1:
        return <Examination
                nextStep={this.nextStep}
                prevStep={this.prevStep}

                handleChange={this.handleChange}
                values={values}
              />


	  case 2:
		return <Mus_examination
					nextStep={this.nextStep}
					prevStep={this.prevStep}
					handleChange={this.handleChange}
					values={values}
				/>


      case 3:
        return <Rom_examination
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleChange={this.handleChange}
            values={values}
          />
          
          

      case 4:
        return <Soft_tis_examination
                nextStep={this.nextStep}
                prevStep={this.prevStep}
                handleChange={this.handleChange}
                values={values}
              /> 


      case 5:
        return <Deformities_examination
                    nextStep={this.nextStep}
                    prevStep={this.prevStep}
                    handleChange={this.handleChange}
                    values={values}
                  />         
	  case 6:
		return <Hypermobility_examination
               nextStep={this.nextStep}
               prevStep={this.prevStep}
               handleChange={this.handleChange}
               values={values}
               dataSend={this.dataSend}

        />


      case 7:
        return <Summary 
                 nextStep={this.nextStep}
                 prevStep={this.prevStep}
                 values={values}
                 saveAll={this.saveAll.bind(this)} 
                 />

      // case 9:
      //   return <Health_ass_ques
      //            nextStep={this.nextStep}
      //            prevStep={this.prevStep}
      //            handleChange={this.handleChange}
      //            values={values}
      //     />



      // case 10:
			// 	return <Inves_Hematological
      //          nextStep={this.nextStep}
      //          prevStep={this.prevStep}
      //          handleChange={this.handleChange}
      //          values={values}
      //   />



      // case 11:
			// 	return <Inves_Biochemical
      //          nextStep={this.nextStep}
      //          prevStep={this.prevStep}
      //          handleChange={this.handleChange}
      //          values={values}
      //   />


      // case 12:
			// 	return <Inves_Immonological
      //          nextStep={this.nextStep}
      //          prevStep={this.prevStep}
      //          handleChange={this.handleChange}
      //          values={values}
      //   />

      // case 13:
      //   return <Inves_Radiological
      //            nextStep={this.nextStep}
      //            prevStep={this.prevStep}
      //            handleChange={this.handleChange}
      //            values={values}
      //     />



     
	
		}
	}
}

export default Examination_Main;