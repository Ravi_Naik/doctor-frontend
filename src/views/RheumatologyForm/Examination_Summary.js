


import React
,     { Component } from 'react';
import { Col
    ,     Row } from 'react-bootstrap';
import { Button } from 'react-bootstrap';


class Summary extends Component {

  saveAndContinue = (e) => {
		e.preventDefault();
		this.props.nextStep();
	}

	back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}

  render(){
const { values: {
general_condition
,    weight
,    height
,    bmi
,    bp
,    pulse
,    temp
,    respiratory_rate
,    ex_health_condition
,    ex_remarks
,    eyes
,    skin_morphology
,    skin_pattern
,    skin_color
,    distribution
,    other_affects
,    mucosa
,    hair_loss
,    dandruff
,    nail
,    lymphnodes
,    lymphnodes_number
,    tender
,    tender_type
,    lymphnodes_tender_others
,    vascular
,    cns_examination
,    cvs_examination
,    chest_examination
,    abdomen_examination
,    tm_t1
,    tm_s1
,    tm_t2
,    tm_s2
,    scl_t1
,    scl_s1
,    scl_t2
,    scl_s2
,    acl_t1
,    acl_s1
,    acl_t2
,    acl_s2
,    sh_t1
,    sh_s1
,    sh_t2
,    sh_s2
,    elbow_t1
,    elbow_s1
,    elbow_t2
,    elbow_s2
,    infru_t1
,    infru_s1
,    infru_t2
,    infru_s2
,    cmc1_t1
,    cmc1_s1
,    cmc1_t2
,    cmc1_s2
,    wrist_t1
,    wrist_s1
,    wrist_t2
,    wrist_s2
,    dip2_t1
,    dip2_s1
,    dip2_t2
,    dip2_s2
,    dip3_t1
,    dip3_s1
,    dip3_t2
,    dip3_s2
,    dip4_t1
,    dip4_s1
,    dip4_t2
,    dip4_s2
,    dip5_t1
,    dip5_s1
,    dip5_t2
,    dip5_s2
,    ip1_t1
,    ip1_s1
,    ip1_t2
,    ip1_s2
,    ip2_t1
,    ip2_s1
,    ip2_t2
,    ip2_s2
,    pip3_t1
,    pip3_s1
,    pip3_t2
,    pip3_s2
,    pip4_t1
,    pip4_s1
,    pip4_t2
,    pip4_s2
,    pip5_t1
,    pip5_s1
,    pip5_t2
,    pip5_s2
,    mcp1_t1
,    mcp1_s1
,    mcp1_t2
,    mcp1_s2
,    mcp2_t1
,    mcp2_s1
,    mcp2_t2
,    mcp2_s2
,    mcp3_t1
,    mcp3_s1
,    mcp3_t2
,    mcp3_s2
,    mcp4_t1
,    mcp4_s1
,    mcp4_t2
,    mcp4_s2
,    mcp5_t1
,    mcp5_s1
,    mcp5_t2
,    mcp5_s2
,    hip_t1
,    hip_s1
,    hip_t2
,    hip_s2
,    knee_t1
,    knee_s1
,    knee_t2
,    knee_s2
,    a_tt_t1
,    a_tt_s1
,    a_tt_t2
,    a_tt_s2
,    a_tc_t1
,    a_tc_s1
,    a_tc_t2
,    a_tc_s2
,    mtl_t1
,    mtl_s1
,    mtl_t2
,    mtl_s2
,    mtp1_t1
,    mtp1_s1
,    mtp1_t2
,    mtp1_s2
,    mtp2_t1
,    mtp2_s1
,    mtp2_t2
,    mtp2_s2
,    mtp3_t1
,    mtp3_s1
,    mtp3_t2
,    mtp3_s2
,    mtp4_t1
,    mtp4_s1
,    mtp4_t2
,    mtp4_s2
,    mtp5_t1
,    mtp5_s1
,    mtp5_t2
,    mtp5_s2
,    ip1b_t1
,    ip1b_s1
,    ip1b_t2
,    ip1b_s2
,    ip2b_t1
,    ip2b_s1
,    ip2b_t2
,    ip2b_s2
,    ip3b_t1
,    ip3b_s1
,    ip3b_t2
,    ip3b_s2
,    ip4b_t1
,    ip4b_s1
,    ip4b_t2
,    ip4b_s2
,    ip5b_t1
,    ip5b_s1
,    ip5b_t2
,    ip5b_s2
,    s1_t1
,    s1_s1
,    s1_t2
,    s1_s2
,    cervical_anky
,    cervical_flex
,    cervical_ext
,    cervical_rtrot
,    cervical_ltrot
,    cervical_rtfl
,    cervical_ltfl
,    cervical_pt
,    thorasic_anky
,    thorasic_flex
,    thorasic_ext
,    thorasic_rtrot
,    thorasic_ltrot
,    thorasic_rtfl
,    thorasic_ltfl
,    thorasic_pt
,    lumbar_anky
,    lumbar_flex
,    lumbar_ext
,    lumbar_rtrot
,    lumbar_ltrot
,    lumbar_rtfl
,    lumbar_ltfl
,    lumbar_pt
,    opt_rt
,    opt_lt
,    lcer_rt
,    lcer_lt
,    trpz_rt
,    trpz_lt
,    scap_rt
,    scap_lt
,    zcst_rt
,    zcst_lt
,    epdl_rt
,    epdl_lt
,    glut_rt
,    glut_lt
,    trcr_rt
,    trcr_lt
,    knee_rt
,    knee_lt
,    ta_rt
,    ta_lt
,    calf_rt
,    calf_lt
,    sole_rt
,    sole_lt
,    rhand_fl
,    rhand_ex
,    rhand_ab
,    rhand_add
,    lhand_fl
,    lhand_ex
,    lhand_ab
,    lhand_add
,    rwrist_fl
,    rwrist_ex
,    rwrist_ab
,    rwrist_add
,    lwrist_fl
,    lwrist_ex
,    lwrist_ab
,    lwrist_add
,    relb_fl
,    relb_ex
,    relb_ab
,    relb_add
,    shrt_fl
,    shrt_ex
,    shrt_ab
,    shrt_add
,    shlt_fl
,    shlt_ex
,    shlt_ab
,    shlt_add
,    kneert_fl
,    kneert_ex
,    kneert_ab
,    kneert_add
,    kneelt_fl
,    kneelt_ex
,    kneelt_ab
,    kneelt_add
,    footrt_fl
,    footrt_ex
,    footrt_ab
,    footrt_add
,    footlt_fl
,    footlt_ex
,    footlt_ab
,    footlt_add
,    thumb_rt
,    thumb_lt
,    finger_rt
,    finger_lt
,    palm_rt
,    palm_lt
,    elbow_rt
,    elbow_lt
,    hy_knee_rt
,    hy_knee_lt
,    ankle_rt
,    ankle_lt
,    other_rt
,    other_lt
,    spine_rt
,    spine_lt
,    pid
,    user_id

    
    
    } } = this.props;

    return (

    <>
    <h2>Summary</h2>
    <Row>
   
   
  
         <Col md="6">

            <li>
              general_condition: {general_condition}
            </li>
            <li>
              weight: {weight}
            </li>
            <li>
              height: {height}
            </li>
            <li>
              bmi: {bmi}
            </li>
            <li>
              bp: {bp}
            </li>

            <li>
              pulse: {pulse}
            </li>



            <li>
              temp: {temp}
            </li>
             
  <li>
              respiratory_rate: {respiratory_rate}
            </li>
            <li>
              ex_health_condition: {ex_health_condition}
            </li>
            <li>
              ex_remarks: {ex_remarks}
            </li>
            <li>
              eyes: {eyes}
            </li>



            <li>
              skin_morphology: {skin_morphology}
            </li>


            
           
           

            <li>
              skin_pattern: {skin_pattern}
            </li>
            <li>
              skin_color: {skin_color}
            </li>
            <li>
              distribution: {distribution}
            </li>
            <li>
              other_affects: {other_affects}
            </li>
            <li>
            mucosa: {mucosa}
            </li>




            <li>
              hair_loss: {hair_loss}
            </li>


            <li>
              dandruff: {dandruff}
            </li>

            <li>
              nail: {nail}
            </li>
            <li>
              lymphnodes: {lymphnodes}
            </li>
        
            <li>
            lymphnodes_number: {lymphnodes_number}
            </li>


            <li>
            tender: {tender}
            </li>
            
          

         
            <li>
              tender_type: {tender_type}
            </li>
            <li>
              lymphnodes_tender_others: {lymphnodes_tender_others}
            </li>
            <li>
              vascular: {vascular}
            </li>
            <li>
              cns_examination: {cns_examination}
            </li>
            <li>
              cvs_examination: {cvs_examination}
            </li>
            <li>
            chest_examination: {chest_examination}
            </li>
            
           

            <li>
              abdomen_examination: {abdomen_examination}
            </li>
</Col>

    



         <Col md="6">

            <li>
              tm_t1: {tm_t1}
            </li>

            <li>
              tm_s1: {tm_s1}
            </li>
            <li>
              tm_t2: {tm_t2}
            </li>
            <li>
              tm_s2: {tm_s2}
            </li>




            <li>
               scl_t1: { scl_t1}
            </li>

            <li>
               scl_s1: { scl_s1}
            </li>
            <li>
               scl_t2: { scl_t2}
            </li>
            <li>
               scl_s2: { scl_s2}
            </li>
          


 


<li>
               acl_t1: { acl_t1}
            </li>

            <li>
               acl_s1: { acl_s1}
            </li>
            <li>
               acl_t2: { acl_t2}
            </li>
            <li>
               acl_s2: { acl_s2}
            </li>



<li>
               sh_t1: { sh_t1}
            </li>

            <li>
               sh_s1: { sh_s1}
            </li>
            <li>
               sh_t2: { sh_t2}
            </li>
            <li>
               sh_s2: { sh_s2}
            </li>



<li>
               elbow_t1: { elbow_t1}
            </li>

            <li>
               elbow_s1: { elbow_s1}
            </li>
            <li>
               elbow_t2: { elbow_t2}
            </li>
            <li>
               elbow_s2: { elbow_s2}
            </li>

            <li>
               infru_t1: { infru_t1}
            </li>

            <li>
               infru_s1: { infru_s1}
            </li>
            <li>
               infru_t2: { infru_t2}
            </li>
            <li>
               infru_s2: { infru_s2}
            </li>




            <li>
               cmc1_t1: { cmc1_t1}
            </li>

            <li>
               cmc1_s1: { cmc1_s1}
            </li>
            <li>
               cmc1_t2: { cmc1_t2}
            </li>
            <li>
               cmc1_s2: { cmc1_s2}
            </li>

     





<li>
               wrist_t1: { wrist_t1}
            </li>

            <li>
               wrist_s1: { wrist_s1}
            </li>


            </Col>
      


          
         <Col md="6">
            <li>
               wrist_t2: { wrist_t2}
            </li>
            <li>
               wrist_s2: { wrist_s2}
            </li>


            <li>
               dip2_t1: { dip2_t1}
            </li>

            <li>
               dip2_s1: { dip2_s1}
            </li>
            <li>
               dip2_t2: { dip2_t2}
            </li>
            <li>
               dip2_s2: { dip2_s2}
            </li>




            <li>
               dip3_t1: { dip3_t1}
            </li>

            <li>
               dip3_s1: { dip3_s1}
            </li>
            <li>
               dip3_t2: { dip3_t2}
            </li>
            <li>
               dip3_s2: { dip3_s2}
            </li>




            <li>
               dip4_t1: { dip4_t1}
            </li>

            <li>
               dip4_s1: { dip4_s1}
            </li>
            <li>
               dip4_t2: { dip4_t2}
            </li>
            <li>
               dip4_s2: { dip4_s2}
            </li>


            <li>
               dip5_t1: { dip5_t1}
            </li>

            <li>
               dip5_s1: { dip5_s1}
            </li>
            <li>
               dip5_t2: { dip5_t2}
            </li>
            <li>
               dip5_s2: { dip5_s2}
            </li>





<li>
               ip1_t1: { ip1_t1}
            </li>

            <li>
               ip1_s1: { ip1_s1}
            </li>
            <li>
               ip1_t2: { ip1_t2}
            </li>
            <li>
               ip1_s2: { ip1_s2}
            </li>

            <li>
               ip2_t1: { ip2_t1}
            </li>

            <li>
               ip2_s1: { ip2_s1}
            </li>
            <li>
               ip2_t2: { ip2_t2}
            </li>
            <li>
               ip2_s2: { ip2_s2}
            </li>

            <li>
               pip3_t1: { pip3_t1}
            </li>

            <li>
               pip3_s1: { pip3_s1}
            </li>
            <li>
               pip3_t2: { pip3_t2}
            </li>
            <li>
               pip3_s2: { pip3_s2}
            </li>

</Col>




<Col md="6">
    <li>
               pip4_t1: { pip4_t1}
            </li>

            <li>
               pip4_s1: { pip4_s1}
            </li>
            <li>
               pip4_t2: { pip4_t2}
            </li>
            <li>
               pip4_s2: { pip4_s2}
            </li>



            <li>
               pip5_t1: { pip5_t1}
            </li>

            <li>
               pip5_s1: { pip5_s1}
            </li>
            <li>
               pip5_t2: { pip5_t2}
            </li>
            <li>
               pip5_s2: { pip5_s2}
            </li>



            <li>
               mcp1_t1: { mcp1_t1}
            </li>

            <li>
               mcp1_s1: { mcp1_s1}
            </li>
            <li>
               mcp1_t2: { mcp1_t2}
            </li>
            <li>
               mcp1_s2: { mcp1_s2}
            </li>



            <li>
              mcp2_t1: {mcp2_t1}
            </li>

            <li>
              mcp2_s1: {mcp2_s1}
            </li>
            <li>
              mcp2_t2: {mcp2_t2}
            </li>
            <li>
              mcp2_s2: {mcp2_s2}
            </li>


            <li>
              mcp3_t1: {mcp3_t1}
            </li>

            <li>
              mcp3_s1: {mcp3_s1}
            </li>
            <li>
              mcp3_t2: {mcp3_t2}
            </li>
            <li>
              mcp3_s2: {mcp3_s2}
            </li>


            <li>
              mcp4_t1: {mcp4_t1}
            </li>

            <li>
              mcp4_s1: {mcp4_s1}
            </li>
            <li>
              mcp4_t2: {mcp4_t2}
            </li>
            <li>
              mcp4_s2: {mcp4_s2}
            </li>


            <li>
              mcp5_t1: {mcp5_t1}
            </li>

            <li>
              mcp5_s1: {mcp5_s1}
            </li>
            <li>
              mcp5_t2: {mcp5_t2}
            </li>
            <li>
              mcp5_s2: {mcp5_s2}
            </li>






<li>
              hip_t1: {hip_t1}
            </li>

            <li>
              hip_s1: {hip_s1}
            </li>
            <li>
              hip_t2: {hip_t2}
            </li>
            <li>
              hip_s2: {hip_s2}
            </li>

            <li>
              knee_t1: {knee_t1}
            </li>

            <li>
              knee_s1: {knee_s1}
            </li>
            <li>
              knee_t2: {knee_t2}
            </li>
            <li>
              knee_s2: {knee_s2}
            </li>

            <li>
              a_tt_t1: {a_tt_t1}
            </li>

            <li>
              a_tt_s1: {a_tt_s1}
            </li>
            <li>
              a_tt_t2: {a_tt_t2}
            </li>
            <li>
              a_tt_s2: {a_tt_s2}
            </li>

</Col>

<Col md="6">


            <li>
              mtl_t1: {mtl_t1}
            </li>

            <li>
              mtl_s1: {mtl_s1}
            </li>
            <li>
              mtl_t2: {mtl_t2}
            </li>
            <li>
              mtl_s2: {mtl_s2}
            </li>





            <li>
              mtp1_t1: {mtp1_t1}
            </li>

            <li>
              mtp1_s1: {mtp1_s1}
            </li>
            <li>
              mtp1_t2: {mtp1_t2}
            </li>
            <li>
              mtp1_s2: {mtp1_s2}
            </li>



            <li>
              mtp2_t1: {mtp2_t1}
            </li>

            <li>
              mtp2_s1: {mtp2_s1}
            </li>
            <li>
              mtp2_t2: {mtp2_t2}
            </li>
            <li>
              mtp2_s2: {mtp2_s2}
            </li>



            <li>
              mtp3_t1: {mtp3_t1}
            </li>

            <li>
              mtp3_s1: {mtp3_s1}
            </li>
            <li>
              mtp3_t2: {mtp3_t2}
            </li>
            <li>
              mtp3_s2: {mtp3_s2}
            </li>

            <li>
              mtp4_t1: {mtp4_t1}
            </li>

            <li>
              mtp4_s1: {mtp4_s1}
            </li>
            <li>
              mtp4_t2: {mtp4_t2}
            </li>
            <li>
              mtp4_s2: {mtp4_s2}
            </li>

            <li>
              mtp5_t1: {mtp5_t1}
            </li>

            <li>
              mtp5_s1: {mtp5_s1}
            </li>
            <li>
              mtp5_t2: {mtp5_t2}
            </li>
            <li>
              mtp5_s2: {mtp5_s2}
            </li>



<li>
              ip1b_t1: {ip1b_t1}
            </li>

            <li>
              ip1b_s1: {ip1b_s1}
            </li>
            <li>
              ip1b_t2: {ip1b_t2}
            </li>
            <li>
              ip1b_s2: {ip1b_s2}
            </li>


            <li>
              ip2b_t1: {ip2b_t1}
            </li>

            <li>
              ip2b_s1: {ip2b_s1}
            </li>
            <li>
              ip2b_t2: {ip2b_t2}
            </li>
            <li>
              ip2b_s2: {ip2b_s2}
            </li>




            <li>
              ip3b_t1: {ip3b_t1}
            </li>

            <li>
              ip3b_s1: {ip3b_s1}
            </li>
            <li>
              ip3b_t2: {ip3b_t2}
            </li>
            <li>
              ip3b_s2: {ip3b_s2}
            </li>

</Col>

<Col  md="6">
            <li>
              ip4b_t1: {ip4b_t1}
            </li>

            <li>
              ip4b_s1: {ip4b_s1}
            </li>
            <li>
              ip4b_t2: {ip4b_t2}
            </li>
            <li>
              ip4b_s2: {ip4b_s2}
            </li>

            <li>
              ip5b_t1: {ip5b_t1}
            </li>

            <li>
              ip5b_s1: {ip5b_s1}
            </li>
            <li>
              ip5b_t2: {ip5b_t2}
            </li>
            <li>
              ip5b_s2: {ip5b_s2}
            </li>






            <li>
              s1_t1: {s1_t1}
            </li>

            <li>
              s1_s1: {s1_s1}
            </li>
            <li>
              s1_t2: {s1_t2}
            </li>
            <li>
              s1_s2: {s1_s2}
            </li>
           

<li>
              cervical_anky: {cervical_anky}
            </li>

            <li>
              cervical_flex: {cervical_flex}
            </li>
            <li>
              cervical_ext: {cervical_ext}
            </li>
            <li>
              cervical_rtrot: {cervical_rtrot}
            </li>


            <li>
              cervical_ltrot: {cervical_ltrot}
            </li>

            <li>
              cervical_rtfl: {cervical_rtfl}
            </li>
            <li>
              cervical_ltfl: {cervical_ltfl}
            </li>
            <li>
              cervical_pt: {cervical_pt}
            </li>


            <li>
              thorasic_anky: {thorasic_anky}
            </li>

            <li>
              thorasic_flex: {thorasic_flex}
            </li>
            <li>
              thorasic_ext: {thorasic_ext}
            </li>
            <li>
              thorasic_rtrot: {thorasic_rtrot}
            </li>


            <li>
              thorasic_ltrot: {thorasic_ltrot}
            </li>

            <li>
              thorasic_rtfl: {thorasic_rtfl}
            </li>
            <li>
              thorasic_ltfl: {thorasic_ltfl}
            </li>
            <li>
              thorasic_pt: {thorasic_pt}
            </li>





            <li>
              lumbar_anky: {lumbar_anky}
            </li>

            <li>
              lumbar_flex: {lumbar_flex}
            </li>
            <li>
              lumbar_ext: {lumbar_ext}
            </li>
            <li>
              lumbar_rtrot: {lumbar_rtrot}
            </li>


            <li>
              lumbar_ltrot: {lumbar_ltrot}
            </li>

            <li>
              lumbar_rtfl: {lumbar_rtfl}
            </li>
            <li>
              lumbar_ltfl: {lumbar_ltfl}
            </li>
            <li>
              lumbar_pt: {lumbar_pt}
            </li>





        


<li>
              opt_rt: {opt_rt}
            </li>
            <li>
              opt_lt: {opt_lt}
            </li>


            <li>
              lcer_rt: {lcer_rt}
            </li>
            <li>
              lcer_lt: {lcer_lt}
            </li>

</Col>

<Col md="6">
            <li>
               trpz_rt: { trpz_rt}
            </li>
            <li>
               trpz_lt: { trpz_lt}
            </li>


            <li>
              scap_rt: {scap_rt}
            </li>
            <li>
              scap_lt: {scap_lt}
            </li>


            <li>
               zcst_rt: { zcst_rt}
            </li>
            <li>
               zcst_lt: { zcst_lt}
            </li>


            <li>
               epdl_rt: { epdl_rt}
            </li>
            <li>
               epdl_lt: { epdl_lt}
            </li>





            <li>
                glut_rt: {  glut_rt}
            </li>
            <li>
                glut_lt: {  glut_lt}
            </li>


            <li>
               trcr_rt: { trcr_rt}
            </li>
            <li>
               trcr_lt: { trcr_lt}
            </li>




            <li>
                knee_rt: {  knee_rt}
            </li>
            <li>
                knee_lt: {  knee_lt}
            </li>


            <li>
               ta_rt: { ta_rt}
            </li>
            <li>
               ta_lt: { ta_lt}
            </li>



            <li>
                calf_rt: {  calf_rt}
            </li>
            <li>
                calf_lt: {  calf_lt}
            </li>


            <li>
               sole_rt: { sole_rt}
            </li>
            <li>
               sole_lt: { sole_lt}
            </li>





<li>
                rhand_fl: {  rhand_fl}
            </li>
            <li>
                rhand_ex: {  rhand_ex}
            </li>


            <li>
               rhand_ab: { rhand_ab}
            </li>
            <li>
               rhand_add: { rhand_add}
            </li>


            <li>
                lhand_fl: {  lhand_fl}
            </li>
            <li>
                lhand_ex: {  lhand_ex}
            </li>


            <li>
               lhand_ab: { lhand_ab}
            </li>
            <li>
               lhand_add: { lhand_add}
            </li>






<li>
                rwrist_fl: {  rwrist_fl}
            </li>
            <li>
                rwrist_ex: {  rwrist_ex}
            </li>


            <li>
               rwrist_ab: { rwrist_ab}
            </li>
            <li>
               rwrist_add: { rwrist_add}
            </li>





            <li>
                lwrist_fl: {  lwrist_fl}
            </li>
            <li>
                lwrist_ex: {  lwrist_ex}
            </li>


            <li>
               lwrist_ab: { lwrist_ab}
            </li>
            <li>
               lwrist_add: { lwrist_add}
            </li>



</Col>

<Col md="6">


<li>
                relb_fl: {  relb_fl}
            </li>
            <li>
                relb_ex: {  relb_ex}
            </li>


            <li>
               relb_ab: { relb_ab}
            </li>
            <li>
               relb_add: { relb_add}
            </li>






            <li>
               shrt_fl: { shrt_fl}
            </li>
            <li>
               shrt_ex: { shrt_ex}
            </li>


            <li>
              shrt_ab: {shrt_ab}
            </li>
            <li>
              shrt_add: {shrt_add}
            </li>






<li>
                shlt_fl: {  shlt_fl}
            </li>
            <li>
                shlt_ex: {  shlt_ex}
            </li>


            <li>
               shlt_ab: { shlt_ab}
            </li>
            <li>
               shlt_add: { shlt_add}
            </li>






            <li>
               kneert_fl: { kneert_fl}
            </li>
            <li>
               kneert_ex: { kneert_ex}
            </li>


            <li>
              kneert_ab: {kneert_ab}
            </li>
            <li>
              kneert_add: {kneert_add}
            </li>






<li>
                kneelt_fl: {  kneelt_fl}
            </li>
            <li>
                kneelt_ex: {  kneelt_ex}
            </li>


            <li>
               kneelt_ab: { kneelt_ab}
            </li>
            <li>
               kneelt_add: { kneelt_add}
            </li>





            <li>
               footrt_fl: { footrt_fl}
            </li>
            <li>
               footrt_ex: { footrt_ex}
            </li>


            <li>
              footrt_ab: {footrt_ab}
            </li>
            <li>
              footrt_add: {footrt_add}
            </li>






<li>
                footlt_fl: {  footlt_fl}
            </li>
            <li>
                footlt_ex: {  footlt_ex}
            </li>


            <li>
               footlt_ab: { footlt_ab}
            </li>
            <li>
               footlt_add: { footlt_add}
            </li>








<li>
               thumb_rt: { thumb_rt}
            </li>
            <li>
               thumb_lt: { thumb_lt}
            </li>


            <li>
              finger_rt: {finger_rt}
            </li>
            <li>
              finger_lt: {finger_lt}
            </li>






<li>
                palm_rt: {  palm_rt}
            </li>
            <li>
                palm_lt: {  palm_lt}
            </li>


            <li>
               elbow_rt: { elbow_rt}
            </li>
            <li>
               elbow_lt: { elbow_lt}
            </li>





            <li>
                hy_knee_rt: {  hy_knee_rt}
            </li>
            <li>
                hy_knee_lt: {  hy_knee_lt}
            </li>


            <li>
               ankle_rt: { ankle_rt}
            </li>
            <li>
               ankle_lt: { ankle_lt}
            </li>



</Col>

<Col md="6">




            <li>
                other_rt: {  other_rt}
            </li>
            <li>
                other_lt: {  other_lt}
            </li>


            <li>
               spine_rt: { spine_rt}
            </li>
            <li>
               spine_lt: { spine_lt}
            </li>

    
            <li>
              pid: {pid}
            </li>
            <li>
            user_id: {user_id}
            </li>


         


           


<Button className="btn-fill"  onClick={this.back}>Back</Button>


          
         
            
              
         
         <Button className="btn-fill" variant="success" onClick={this.props.saveAll}>Submit</Button>

            
          </Col>
        </Row>

      </>
 
    )
  }
}
export default Summary;
