import React from "react";
import axios from 'axios'
// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Form,
  Navbar,
  Nav,
  Container,
  Row,
  Col,
  Table,
  Image
} from "react-bootstrap";

import Swal from 'sweetalert2' 
import {Redirect} from "react-router-dom"


class followup extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      users:[],
      products: [],

      id:'',
       pid: this.props.match.params.id,
       user_id:'',
      code:'',
   
      pname:'',

      age:'',
      sex:'',
  
      address:'',
      phone:'',
      firstvisit:'',
      lastvisit:'',
      timelapsed:'',
bp:'',
wt:'',
followup:'',
course_result:'',
improved_percent :'',                              
deteriorate_percent :'',                              
history :'',                             
diagnosis :'',                              
addnewdiagnosis :'',                             
anorexia :'',                              
anorexia_duration :'',                            
anorexia_severity :'',                              
anorexia_cause :'',                            
nausea :'',                              
nausea_duration :'',                             
nausea_severity :'',                          
nausea_cause :'',                            
vomiting :'',                           
vomiting_duration :'',                             
vomiting_severity :'',                              
vomiting_cause :'',                            
acidity :'',                           
acidity_duration :'',                            
acidity_severity :'',                             
acidity_cause :'',                            
pain_abdomen :'',                             
pain_duration :'',                             
pain_severity :'',                            
pain_cause :'',                           
diarrhoea :'',                              
diarrhoea_duration :'',                              
diarrhoea_severity :'',                              
diarrhoea_cause :'',                              
oral_ulcers :'',                             
ulcers_duration :'',                              
ulcers_severity :'',                            
ulcers_cause :'',                          
constipation :'',                              
constipation_duration :'',                             
constipation_severity :'',                             
constipation_cause :'',                              
skinrash :'',                            
skinrash_duration :'',                            
skinrash_severity :'',                              
skinrash_cause :'',                              
hairfall :'',                          
hairfall_duration :'',                             
hairfall_severity :'',                              
hairfall_cause :'',                              
othertoxics :'',                             
othertoxics_duration :'',                              
othertoxics_severity :'',                              
othertoxics_cause :'',                              

hcq :'',                              
mtx :'',                              
ssz :'',                              
lef :'',                              
azt :'',                              
mmf :'',                              
pred :'',                             
nsaid :'',                              
esr :'',                              
hb :'',                              
tlc :'',                              
dlc :'',                              
platelet :'',                              
urine :'',                              
bun :'',                              
cr :'',                              
sgot :'',                              
sgpt :'',                              
alb :'',                              
glob :'',                              
ua :'',                              
bsl :'',                              
rf :'',                              
crp :'',                              
bili :'',                              
other :'',                              
xray :'',    
treatment_plan:'',
tm_t1:'',
tm_s1:'',
tm_t2:'',
tm_s2:'',

scl_t1:'',
scl_s1:'',
scl_t2:'',
scl_s2:'',

acl_t1:'',
acl_s1:'',
acl_t2:'',
acl_s2:'',

sh_t1:'',
sh_s1:'',
sh_t2:'',
sh_s2:'',

elbow_t1:'',
elbow_s1:'',
elbow_t2:'',
elbow_s2:'',


infru_t1:'',
infru_s1:'',
infru_t2:'',
infru_s2:'',


cmc1_t1:'',
cmc1_s1:'',
cmc1_t2:'',
cmc1_s2:'',


wrist_t1:'',
wrist_s1:'',
wrist_t2:'',
wrist_s2:'',


dip2_t1:'',
dip2_s1:'',
dip2_t2:'',
dip2_s2:'',

dip3_t1:'',
dip3_s1:'',
dip3_t2:'',
dip3_s2:'',

dip4_t1:'',
dip4_s1:'',
dip4_t2:'',
dip4_s2:'',

dip5_t1:'',
dip5_s1:'',
dip5_t2:'',
dip5_s2:'',
// missing
r1ip1_t1 :'',
r1ip1_s1 :'', 
r1ip1_t2 :'', 
r1ip1_s2 :'',
r1ip2_t1 :'', 
r1ip2_s1 :'',
r1ip2_t2 :'', 
r1ip2_s2 :'',

pip3_t1:'',
pip3_s1:'',
pip3_t2:'',
pip3_s2:'',


pip4_t1:'',
pip4_s1:'',
pip4_t2:'',
pip4_s2:'',

pip5_t1:'',
pip5_s1:'',
pip5_t2:'',
pip5_s2:'',

mcp1_t1:'',
mcp1_s1:'',
mcp1_t2:'',
mcp1_s2:'',

mcp2_t1:'',
mcp2_s1:'',
mcp2_t2:'',
mcp2_s2:'',

mcp3_t1:'',
mcp3_s1:'',
mcp3_t2:'',
mcp3_s2:'',

mcp4_t1:'',
mcp4_s1:'',
mcp4_t2:'',
mcp4_s2:'',

mcp5_t1:'',
mcp5_s1:'',
mcp5_t2:'',
mcp5_s2:'',


hip_t1:'',
hip_s1:'',
hip_t2:'',
hip_s2:'',

knee_t1:'',
knee_s1:'',
knee_t2:'',
knee_s2:'',

a_tt_t1:'',
a_tt_s1:'',
a_tt_t2:'',
a_tt_s2:'',

a_tc_t1:'',
a_tc_s1:'',
a_tc_t2:'',
a_tc_s2:'',

mtl_t1:'',
mtl_s1:'',
mtl_t2:'',
mtl_s2:'',

mtp1_t1:'',
mtp1_s1:'',
mtp1_t2:'',
mtp1_s2:'',

mtp2_t1:'',
mtp2_s1:'',
mtp2_t2:'',
mtp2_s2:'',

mtp3_t1:'',
mtp3_s1:'',
mtp3_t2:'',
mtp3_s2:'',

mtp4_t1:'',
mtp4_s1:'',
mtp4_t2:'',
mtp4_s2:'',

mtp5_t1:'',
mtp5_s1:'',
mtp5_t2:'',
mtp5_s2:'',


// change
r2ip1_t1 :'',
r2ip1_s1 :'',
r2ip1_t2 :'',
r2ip1_s2 :'',
r2ip2_t1 :'',
r2ip2_s1 :'',
r2ip2_t2 :'',
r2ip2_s2 :'',



ip3_t1:'',
ip3_s1:'',
ip3_t2:'',
ip3_s2:'',
ip4_t1:'',
ip4_s1:'',
ip4_t2:'',
ip4_s2:'',
ip5_t1:'',
ip5_s1:'',
ip5_t2:'',
ip5_s2:'',
// ip4b_t1:'',
// ip4b_s1:'',
// ip4b_t2:'',
// ip4b_s2:'',
// ip5b_t1:'',
// ip5b_s1:'',
// ip5b_t2:'',
// ip5b_s2:'',
s1_t1:'',
s1_s1:'',
s1_t2:'',
s1_s2:'',

cervical_anky:'',
cervical_flex:'',
cervical_ext:'',
cervical_rtrot:'',
cervical_ltrot:'',
cervical_rtfl:'',
cervical_ltfl:'',
cervical_pt:'',

thorasic_anky:'',
thorasic_flex:'',
thorasic_ext:'',
thorasic_rtrot:'',
thorasic_ltrot:'',
thorasic_rtfl:'',
thorasic_ltfl:'',
thorasic_pt:'',

lumbar_anky:'',
lumbar_flex:'',
lumbar_ext:'',
lumbar_rtrot:'',
lumbar_ltrot:'',
lumbar_rtfl:'',
lumbar_ltfl:'',
lumbar_pt:'',

opt_rt:'',
opt_lt:'',
lcer_rt:'',
lcer_lt:'',
trpz_rt:'',
trpz_lt:'',
scap_rt:'',
scap_lt:'',
zcst_rt:'',
zcst_lt:'',
epdl_rt:'',
epdl_lt:'',
glut_rt:'',
glut_lt:'',
trcr_rt:'',
trcr_lt:'',
knee_rt:'',
knee_lt:'',
ta_rt:'',
ta_lt:'',
calf_rt:'',
calf_lt:'',
sole_rt:'',
sole_lt:'',
rhand_fl:'',
rhand_ex:'',
rhand_ab:'',
rhand_add:'',
rhand_slx:'',

lhand_fl:'',
lhand_ex:'',
lhand_ab:'',
lhand_add:'',
lhand_slx:'',


rwrist_fl:'',
rwrist_ex:'',
rwrist_ab:'',
rwrist_add:'',
rwrist_slx:'',


lwrist_fl:'',
lwrist_ex:'',
lwrist_ab:'',
lwrist_add:'',
lwrist_slx:'',

relb_fl:'',
relb_ex:'',
relb_ab:'',
relb_add:'',
relb_slx:'',

lelb_fl :'',
lelb_ex :'',
lelb_ab :'',
lelb_add:'',
lelb_slx:'',


shrt_fl:'',
shrt_ex:'',
shrt_ab:'',
shrt_add:'',
shrt_slx:'',

shlt_fl:'',
shlt_ex:'',
shlt_ab:'',
shlt_add:'',
shlt_slx:'',

kneert_fl:'',
kneert_ex:'',
kneert_ab:'',
kneert_add:'',
kneert_slx:'',

kneelt_fl:'',
kneelt_ex:'',
kneelt_ab:'',
kneelt_add:'',
kneelt_slx:'',

footrt_fl:'',
footrt_ex:'',
footrt_ab:'',
footrt_add:'',
footrt_slx:'',

footlt_fl:'',
footlt_ex:'',
footlt_ab:'',
footlt_add:'',
footlt_slx:'',

thumb_rt:'',
thumb_lt:'',
finger_rt:'',
finger_lt:'',
palm_rt:'',
palm_lt:'',
elbow_rt:'',
elbow_lt:'',
hy_knee_rt:'',
hy_knee_lt:'',
ankle_rt:'',
ankle_lt:'',
other_rt:'',
other_lt:'',
spine_rt:'',
spine_lt:'',

can_u_dress_urself:'',
can_u_wash_ur_hair:'',
can_u_comb_ur_hair:'',
dressing_score :'',
can_u_stand_from_chair:'',

can_u_get_inout_from_bed:'',
can_u_sit_grossteg_onfloor:'',
arising_score:'',

can_u_cut_vegetables:'',
can_u_lift_glass:'',
can_u_break_roti_from_1hand:'',
eating_score:'',

can_u_walk:'',
can_u_climb_5steps:'',
walking_score:'',


can_u_take_bath:'',
can_u_wash_dry_urbody:'',
can_u_get_onoff_toilet:'',
hygiene_score:'',

can_u_weigh_2kg:'',
can_u_bend_and_pickcloths:'',
reaching_score:'',

can_u_open_bottle:'',
can_u_turntaps_onoff:'',
can_u_open_latches:'',
grip_score:'',

can_u_work_office_house:'',
can_u_run_errands:'',
can_u_get_inout_of_bus:'',
activities_score:'',

patient_assessment_pain:'',
grip_strength_rt:'',
grip_strength_hg:'',
grip_strength_lt:'',
early_mrng_stiffness:'',
sleep:'',
general_health_assessment:'',                         



    }

  }



  opensweetalert()
  {
    Swal.fire({
      title: 'Patient record updated',
      position: 'top-end',
   icon: 'success', 
  showConfirmButton: false,
  timer: 1200
      
    }).then(function() {
      window.location = "/admin/dashboard";
  });
  }

  norecordsweetalert()
  {
    Swal.fire({
      title: 'No record Found',
      position: 'top-end',
   icon: 'success', 
  showConfirmButton: false,
  timer: 1200
      
    }).then(function() {
      window.location = "/admin/dashboard";
  });
  }


  componentDidMount(){
    axios.get(`https://abcapi.vidaria.in/allfollowuprheumadetails?X-AUTH=abc123&pid=${this.state.pid}`)
    .then((res)=>{
      var array= res.data.RheumatologyFollowup
      console.log(array)
     


      console.log('value',array.pid)
      if (array != '' )
      {
             
      this.setState({
        id:res.data.RheumatologyFollowup.id,
user_id:res.data.RheumatologyFollowup[0].user_id,
pid:res.data.RheumatologyFollowup[0].pid,

code:res.data.RheumatologyFollowup[0].code,
pname:res.data.RheumatologyFollowup[0].pname,
age:res.data.RheumatologyFollowup[0].age,
sex:res.data.RheumatologyFollowup[0].sex,


address:res.data.RheumatologyFollowup[0].address,
phone:res.data.RheumatologyFollowup[0].phone,
firstvisit:res.data.RheumatologyFollowup[0].firstvisit,
lastvisit:res.data.RheumatologyFollowup[0].lastvisit,


timelapsed:res.data.RheumatologyFollowup[0].timelapsed,
bp:res.data.RheumatologyFollowup[0].bp,
wt:res.data.RheumatologyFollowup[0].wt,
followup:res.data.RheumatologyFollowup[0].followup,



course_result:res.data.RheumatologyFollowup[0].course_result,

improved_percent:res.data.RheumatologyFollowup[0].improved_percent,
deteriorate_percent:res.data.RheumatologyFollowup[0].deteriorate_percent,


history:res.data.RheumatologyFollowup[0].history,
diagnosis:res.data.RheumatologyFollowup[0].diagnosis,



addnewdiagnosis:res.data.RheumatologyFollowup[0].addnewdiagnosis,
anorexia:res.data.RheumatologyFollowup[0].anorexia,
anorexia_duration:res.data.RheumatologyFollowup[0].anorexia_duration,
anorexia_severity:res.data.RheumatologyFollowup[0].anorexia_severity,
anorexia_cause:res.data.RheumatologyFollowup[0].anorexia_cause,




nausea:res.data.RheumatologyFollowup[0].nausea,
nausea_duration:res.data.RheumatologyFollowup[0].nausea_duration,
nausea_severity:res.data.RheumatologyFollowup[0].nausea_severity,
nausea_cause:res.data.RheumatologyFollowup[0].nausea_cause,
vomiting:res.data.RheumatologyFollowup[0].vomiting,
vomiting_duration:res.data.RheumatologyFollowup[0].vomiting_duration,
vomiting_severity:res.data.RheumatologyFollowup[0].vomiting_severity,
vomiting_cause:res.data.RheumatologyFollowup[0].vomiting_cause,



acidity:res.data.RheumatologyFollowup[0].acidity,
acidity_duration:res.data.RheumatologyFollowup[0].acidity_duration,
acidity_severity:res.data.RheumatologyFollowup[0].acidity_severity,
acidity_cause:res.data.RheumatologyFollowup[0].acidity_cause,
pain_abdomen:res.data.RheumatologyFollowup[0].pain_abdomen,
pain_duration:res.data.RheumatologyFollowup[0].pain_duration,
pain_severity:res.data.RheumatologyFollowup[0].pain_severity,
pain_cause:res.data.RheumatologyFollowup[0].pain_cause,





diarrhoea:res.data.RheumatologyFollowup[0].diarrhoea,
diarrhoea_duration:res.data.RheumatologyFollowup[0].diarrhoea_duration,
diarrhoea_severity:res.data.RheumatologyFollowup[0].diarrhoea_severity,
diarrhoea_cause:res.data.RheumatologyFollowup[0].diarrhoea_cause,
oral_ulcers:res.data.RheumatologyFollowup[0].oral_ulcers,

ulcers_duration:res.data.RheumatologyFollowup[0].ulcers_duration,
ulcers_severity:res.data.RheumatologyFollowup[0].ulcers_severity,
ulcers_cause:res.data.RheumatologyFollowup[0].ulcers_cause,



constipation:res.data.RheumatologyFollowup[0].constipation,
constipation_duration:res.data.RheumatologyFollowup[0].constipation_duration,
constipation_severity:res.data.RheumatologyFollowup[0].constipation_severity,
constipation_cause:res.data.RheumatologyFollowup[0].constipation_cause,
skinrash:res.data.RheumatologyFollowup[0].skinrash,

skinrash_duration:res.data.RheumatologyFollowup[0].skinrash_duration,
skinrash_severity:res.data.RheumatologyFollowup[0].skinrash_severity,
skinrash_cause:res.data.RheumatologyFollowup[0].skinrash_cause,


hairfall:res.data.RheumatologyFollowup[0].hairfall,
hairfall_duration:res.data.RheumatologyFollowup[0].hairfall_duration,
hairfall_severity:res.data.RheumatologyFollowup[0].hairfall_severity,
hairfall_cause:res.data.RheumatologyFollowup[0].hairfall_cause,
othertoxics:res.data.RheumatologyFollowup[0].othertoxics,

othertoxics_duration:res.data.RheumatologyFollowup[0].othertoxics_duration,
othertoxics_severity:res.data.RheumatologyFollowup[0].othertoxics_severity,
othertoxics_cause:res.data.RheumatologyFollowup[0].othertoxics_cause,





hairfall:res.data.RheumatologyFollowup[0].hairfall,
hairfall_duration:res.data.RheumatologyFollowup[0].hairfall_duration,
hairfall_severity:res.data.RheumatologyFollowup[0].hairfall_severity,
hairfall_cause:res.data.RheumatologyFollowup[0].hairfall_cause,
othertoxics:res.data.RheumatologyFollowup[0].othertoxics,

othertoxics_duration:res.data.RheumatologyFollowup[0].othertoxics_duration,
othertoxics_severity:res.data.RheumatologyFollowup[0].othertoxics_severity,
othertoxics_cause:res.data.RheumatologyFollowup[0].othertoxics_cause,


hcq : res.data.RheumatologyFollowup[0].hcq,
mtx : res.data.RheumatologyFollowup[0].mtx,
ssz : res.data.RheumatologyFollowup[0].ssz,
lef : res.data.RheumatologyFollowup[0].lef,
azt : res.data.RheumatologyFollowup[0].azt,
mmf : res.data.RheumatologyFollowup[0].mmf,
pred : res.data.RheumatologyFollowup[0].pred,
nsaid : res.data.RheumatologyFollowup[0].nsaid,
esr : res.data.RheumatologyFollowup[0].esr,
hb : res.data.RheumatologyFollowup[0].hb,
tlc : res.data.RheumatologyFollowup[0].tlc,
dlc : res.data.RheumatologyFollowup[0].dlc,
platelet : res.data.RheumatologyFollowup[0].platelet,
urine : res.data.RheumatologyFollowup[0].urine,
bun : res.data.RheumatologyFollowup[0].bun,
cr : res.data.RheumatologyFollowup[0].cr,
sgot : res.data.RheumatologyFollowup[0].sgot,
sgpt : res.data.RheumatologyFollowup[0].sgpt,
alb : res.data.RheumatologyFollowup[0].alb,
glob : res.data.RheumatologyFollowup[0].glob,
ua : res.data.RheumatologyFollowup[0].ua,
bsl : res.data.RheumatologyFollowup[0].bsl,
rf : res.data.RheumatologyFollowup[0].rf,
crp : res.data.RheumatologyFollowup[0].crp,
bili : res.data.RheumatologyFollowup[0].bili,
other : res.data.RheumatologyFollowup[0].other,
xray : res.data.RheumatologyFollowup[0].xray,
treatment_plan : res.data.RheumatologyFollowup[0].treatment_plan,







tm_t1:res.data.RheumatologyFollowup[0].tm_t1,
tm_s1:res.data.RheumatologyFollowup[0].tm_s1,
tm_t2:res.data.RheumatologyFollowup[0].tm_t2,
tm_s2:res.data.RheumatologyFollowup[0].tm_s2,

scl_t1:res.data.RheumatologyFollowup[0].scl_t1,
scl_s1:res.data.RheumatologyFollowup[0].scl_s1,
scl_t2:res.data.RheumatologyFollowup[0].scl_t2,
scl_s2:res.data.RheumatologyFollowup[0].scl_s2,



acl_t1:res.data.RheumatologyFollowup[0].acl_t1,
acl_s1:res.data.RheumatologyFollowup[0].acl_s1,
acl_t2:res.data.RheumatologyFollowup[0].acl_t2,
acl_s2:res.data.RheumatologyFollowup[0].acl_s2,


sh_t1:res.data.RheumatologyFollowup[0].sh_t1,
sh_s1:res.data.RheumatologyFollowup[0].sh_s1,
sh_t2:res.data.RheumatologyFollowup[0].sh_t2,
sh_s2:res.data.RheumatologyFollowup[0].sh_s2,

elbow_t1:res.data.RheumatologyFollowup[0].elbow_t1,
elbow_s1:res.data.RheumatologyFollowup[0].elbow_s1,
elbow_t2:res.data.RheumatologyFollowup[0].elbow_t2,
elbow_s2:res.data.RheumatologyFollowup[0].elbow_s2,


infru_t1:res.data.RheumatologyFollowup[0].infru_t1,
infru_s1:res.data.RheumatologyFollowup[0].infru_s1,
infru_t2:res.data.RheumatologyFollowup[0].infru_t2,
infru_s2:res.data.RheumatologyFollowup[0].infru_s2,


cmc1_t1:res.data.RheumatologyFollowup[0].cmc1_t1,
cmc1_s1:res.data.RheumatologyFollowup[0].cmc1_s1,
cmc1_t2:res.data.RheumatologyFollowup[0].cmc1_t2,
cmc1_s2:res.data.RheumatologyFollowup[0].cmc1_s2,



wrist_t1:res.data.RheumatologyFollowup[0].wrist_t1,
wrist_s1:res.data.RheumatologyFollowup[0].wrist_s1,
wrist_t2:res.data.RheumatologyFollowup[0].wrist_t2,
wrist_s2:res.data.RheumatologyFollowup[0].wrist_s2,


dip2_t1:res.data.RheumatologyFollowup[0].dip2_t1,
dip2_s1:res.data.RheumatologyFollowup[0].dip2_s1,
dip2_t2:res.data.RheumatologyFollowup[0].dip2_t2,
dip2_s2:res.data.RheumatologyFollowup[0].dip2_s2,


dip3_t1:res.data.RheumatologyFollowup[0].dip3_t1,
dip3_s1:res.data.RheumatologyFollowup[0].dip3_s1,
dip3_t2:res.data.RheumatologyFollowup[0].dip3_t2,
dip3_s2:res.data.RheumatologyFollowup[0].dip3_s2,


dip4_t1:res.data.RheumatologyFollowup[0].dip4_t1,
dip4_s1:res.data.RheumatologyFollowup[0].dip4_s1,
dip4_t2:res.data.RheumatologyFollowup[0].dip4_t2,
dip4_s2:res.data.RheumatologyFollowup[0].dip4_s2,



dip5_t1:res.data.RheumatologyFollowup[0].dip5_t1,
dip5_s1:res.data.RheumatologyFollowup[0].dip5_s1,
dip5_t2:res.data.RheumatologyFollowup[0].dip5_t2,
dip5_s2:res.data.RheumatologyFollowup[0].dip5_s2,


r1ip1_t1:res.data.RheumatologyFollowup[0].r1ip1_t1,
r1ip1_s1:res.data.RheumatologyFollowup[0].r1ip1_s1,
r1ip1_t2:res.data.RheumatologyFollowup[0].r1ip1_t2,
r1ip1_s2:res.data.RheumatologyFollowup[0].r1ip1_s2,


r1ip2_t1:res.data.RheumatologyFollowup[0].r1ip2_t1,
r1ip2_s1:res.data.RheumatologyFollowup[0].r1ip2_s1,
r1ip2_t2:res.data.RheumatologyFollowup[0].r1ip2_t2,
r1ip2_s2:res.data.RheumatologyFollowup[0].r1ip2_s2,


pip3_t1:res.data.RheumatologyFollowup[0].pip3_t1,
pip3_s1:res.data.RheumatologyFollowup[0].pip3_s1,
pip3_t2:res.data.RheumatologyFollowup[0].pip3_t2,
pip3_s2:res.data.RheumatologyFollowup[0].pip3_s2,


pip4_t1:res.data.RheumatologyFollowup[0].pip4_t1,
pip4_s1:res.data.RheumatologyFollowup[0].pip4_s1,
pip4_t2:res.data.RheumatologyFollowup[0].pip4_t2,
pip4_s2:res.data.RheumatologyFollowup[0].pip4_s2,

pip5_t1:res.data.RheumatologyFollowup[0].pip5_t1,
pip5_s1:res.data.RheumatologyFollowup[0].pip5_s1,
pip5_t2:res.data.RheumatologyFollowup[0].pip5_t2,
pip5_s2:res.data.RheumatologyFollowup[0].pip5_s2,

mcp1_t1:res.data.RheumatologyFollowup[0].mcp1_t1,
mcp1_s1:res.data.RheumatologyFollowup[0].mcp1_s1,
mcp1_t2:res.data.RheumatologyFollowup[0].mcp1_t2,
mcp1_s2:res.data.RheumatologyFollowup[0].mcp1_s2,


mcp2_t1:res.data.RheumatologyFollowup[0].mcp2_t1,
mcp2_s1:res.data.RheumatologyFollowup[0].mcp2_s1,
mcp2_t2:res.data.RheumatologyFollowup[0].mcp2_t2,
mcp2_s2:res.data.RheumatologyFollowup[0].mcp2_s2,

mcp3_t1:res.data.RheumatologyFollowup[0].mcp3_t1,
mcp3_s1:res.data.RheumatologyFollowup[0].mcp3_s1,
mcp3_t2:res.data.RheumatologyFollowup[0].mcp3_t2,
mcp3_s2:res.data.RheumatologyFollowup[0].mcp3_s2,


mcp4_t1:res.data.RheumatologyFollowup[0].mcp4_t1,
mcp4_s1:res.data.RheumatologyFollowup[0].mcp4_s1,
mcp4_t2:res.data.RheumatologyFollowup[0].mcp4_t2,
mcp4_s2:res.data.RheumatologyFollowup[0].mcp4_s2,

mcp5_t1:res.data.RheumatologyFollowup[0].mcp5_t1,
mcp5_s1:res.data.RheumatologyFollowup[0].mcp5_s1,
mcp5_t2:res.data.RheumatologyFollowup[0].mcp5_t2,
mcp5_s2:res.data.RheumatologyFollowup[0].mcp5_s2,

hip_t1:res.data.RheumatologyFollowup[0].hip_t1,
hip_s1:res.data.RheumatologyFollowup[0].hip_s1,
hip_t2:res.data.RheumatologyFollowup[0].hip_t2,
hip_s2:res.data.RheumatologyFollowup[0].hip_s2,


knee_t1:res.data.RheumatologyFollowup[0].knee_t1,
knee_s1:res.data.RheumatologyFollowup[0].knee_s1,
knee_t2:res.data.RheumatologyFollowup[0].knee_t2,
knee_s2:res.data.RheumatologyFollowup[0].knee_s2,


a_tt_t1:res.data.RheumatologyFollowup[0].a_tt_t1,
a_tt_s1:res.data.RheumatologyFollowup[0].a_tt_s1,
a_tt_t2:res.data.RheumatologyFollowup[0].a_tt_t2,
a_tt_s2:res.data.RheumatologyFollowup[0].a_tt_s2,

a_tc_t1:res.data.RheumatologyFollowup[0].a_tc_t1,
a_tc_s1:res.data.RheumatologyFollowup[0].a_tc_s1,
a_tc_t2:res.data.RheumatologyFollowup[0].a_tc_t2,
a_tc_s2:res.data.RheumatologyFollowup[0].a_tc_s2,

mtl_t1:res.data.RheumatologyFollowup[0].mtl_t1,
mtl_s1:res.data.RheumatologyFollowup[0].mtl_s1,
mtl_t2:res.data.RheumatologyFollowup[0].mtl_t2,
mtl_s2:res.data.RheumatologyFollowup[0].mtl_s2,


mtp1_t1:res.data.RheumatologyFollowup[0].mtp1_t1,
mtp1_s1:res.data.RheumatologyFollowup[0].mtp1_s1,
mtp1_t2:res.data.RheumatologyFollowup[0].mtp1_t2,
mtp1_s2:res.data.RheumatologyFollowup[0].mtp1_s2,


mtp2_t1:res.data.RheumatologyFollowup[0].mtp2_t1,
mtp2_s1:res.data.RheumatologyFollowup[0].mtp2_s1,
mtp2_t2:res.data.RheumatologyFollowup[0].mtp2_t2,
mtp2_s2:res.data.RheumatologyFollowup[0].mtp2_s2,


mtp3_t1:res.data.RheumatologyFollowup[0].mtp3_t1,
mtp3_s1:res.data.RheumatologyFollowup[0].mtp3_s1,
mtp3_t2:res.data.RheumatologyFollowup[0].mtp3_t2,
mtp3_s2:res.data.RheumatologyFollowup[0].mtp3_s2,


mtp4_t1:res.data.RheumatologyFollowup[0].mtp4_t1,
mtp4_s1:res.data.RheumatologyFollowup[0].mtp4_s1,
mtp4_t2:res.data.RheumatologyFollowup[0].mtp4_t2,
mtp4_s2:res.data.RheumatologyFollowup[0].mtp4_s2,


mtp5_t1:res.data.RheumatologyFollowup[0].mtp5_t1,
mtp5_s1:res.data.RheumatologyFollowup[0].mtp5_s1,
mtp5_t2:res.data.RheumatologyFollowup[0].mtp5_t2,
mtp5_s2:res.data.RheumatologyFollowup[0].mtp5_s2,





r2ip1_t1:res.data.RheumatologyFollowup[0].r2ip1_t1,
r2ip1_s1:res.data.RheumatologyFollowup[0].r2ip1_s1,
r2ip1_t2:res.data.RheumatologyFollowup[0].r2ip1_t2,
r2ip1_s2:res.data.RheumatologyFollowup[0].r2ip1_s2,


r2ip2_t1:res.data.RheumatologyFollowup[0].r2ip2_t1,
r2ip2_s1:res.data.RheumatologyFollowup[0].r2ip2_s1,
r2ip2_t2:res.data.RheumatologyFollowup[0].r2ip2_t2,
r2ip2_s2:res.data.RheumatologyFollowup[0].r2ip2_s2,





ip3_t1:res.data.RheumatologyFollowup[0].ip3_t1,
ip3_s1:res.data.RheumatologyFollowup[0].ip3_s1,
ip3_t2:res.data.RheumatologyFollowup[0].ip3_t2,
ip3_s2:res.data.RheumatologyFollowup[0].ip3_s2,

ip4_t1:res.data.RheumatologyFollowup[0].ip4_t1,
ip4_s1:res.data.RheumatologyFollowup[0].ip4_s1,
ip4_t2:res.data.RheumatologyFollowup[0].ip4_t2,
ip4_s2:res.data.RheumatologyFollowup[0].ip4_s2,


ip5_t1:res.data.RheumatologyFollowup[0].ip5_t1,
ip5_s1:res.data.RheumatologyFollowup[0].ip5_s1,
ip5_t2:res.data.RheumatologyFollowup[0].ip5_t2,
ip5_s2:res.data.RheumatologyFollowup[0].ip5_s2,


s1_t1:res.data.RheumatologyFollowup[0].s1_t1,
s1_s1:res.data.RheumatologyFollowup[0].s1_s1,
s1_t2:res.data.RheumatologyFollowup[0].s1_t2,
s1_s2:res.data.RheumatologyFollowup[0].s1_s2,


cervical_anky:res.data.RheumatologyFollowup[0].cervical_anky,
cervical_flex:res.data.RheumatologyFollowup[0].cervical_flex,
cervical_ext:res.data.RheumatologyFollowup[0].cervical_ext,
cervical_rtrot:res.data.RheumatologyFollowup[0].cervical_rtrot,

cervical_ltrot:res.data.RheumatologyFollowup[0].cervical_ltrot,
cervical_rtfl:res.data.RheumatologyFollowup[0].cervical_rtfl,
cervical_ltfl:res.data.RheumatologyFollowup[0].cervical_ltfl,
cervical_pt:res.data.RheumatologyFollowup[0].cervical_pt,


thorasic_anky:res.data.RheumatologyFollowup[0].thorasic_anky,
thorasic_flex:res.data.RheumatologyFollowup[0].thorasic_flex,
thorasic_ext:res.data.RheumatologyFollowup[0].thorasic_ext,
thorasic_rtrot:res.data.RheumatologyFollowup[0].thorasic_rtrot,

thorasic_ltrot:res.data.RheumatologyFollowup[0].thorasic_ltrot,
thorasic_rtfl:res.data.RheumatologyFollowup[0].thorasic_rtfl,
thorasic_ltfl:res.data.RheumatologyFollowup[0].thorasic_ltfl,
thorasic_pt:res.data.RheumatologyFollowup[0].thorasic_pt,





lumbar_anky:res.data.RheumatologyFollowup[0].lumbar_anky,
lumbar_flex:res.data.RheumatologyFollowup[0].lumbar_flex,
lumbar_ext:res.data.RheumatologyFollowup[0].lumbar_ext,
lumbar_rtrot:res.data.RheumatologyFollowup[0].lumbar_rtrot,
lumbar_ltrot:res.data.RheumatologyFollowup[0].lumbar_ltrot,



lumbar_rtfl:res.data.RheumatologyFollowup[0].lumbar_rtfl,
lumbar_ltfl:res.data.RheumatologyFollowup[0].lumbar_ltfl,
lumbar_pt:res.data.RheumatologyFollowup[0].lumbar_pt,


opt_rt:res.data.RheumatologyFollowup[0].opt_rt,
opt_lt:res.data.RheumatologyFollowup[0].opt_lt,

lcer_rt:res.data.RheumatologyFollowup[0].lcer_rt,
lcer_lt:res.data.RheumatologyFollowup[0].lcer_lt,
trpz_rt:res.data.RheumatologyFollowup[0].trpz_rt,


trpz_lt:res.data.RheumatologyFollowup[0].trpz_lt,
scap_rt:res.data.RheumatologyFollowup[0].scap_rt,
scap_lt:res.data.RheumatologyFollowup[0].scap_lt,

zcst_rt:res.data.RheumatologyFollowup[0].zcst_rt,
zcst_lt:res.data.RheumatologyFollowup[0].zcst_lt,
epdl_rt:res.data.RheumatologyFollowup[0].epdl_rt,


epdl_lt:res.data.RheumatologyFollowup[0].epdl_lt,
glut_rt:res.data.RheumatologyFollowup[0].glut_rt,
glut_lt:res.data.RheumatologyFollowup[0].glut_lt,


trcr_rt:res.data.RheumatologyFollowup[0].trcr_rt,
trcr_lt:res.data.RheumatologyFollowup[0].trcr_lt,
knee_rt:res.data.RheumatologyFollowup[0].knee_rt,




knee_lt:res.data.RheumatologyFollowup[0].knee_lt,
ta_rt:res.data.RheumatologyFollowup[0].ta_rt,
ta_lt:res.data.RheumatologyFollowup[0].ta_lt,


calf_rt:res.data.RheumatologyFollowup[0].calf_rt,
calf_lt:res.data.RheumatologyFollowup[0].calf_lt,
sole_rt:res.data.RheumatologyFollowup[0].sole_rt,


sole_lt:res.data.RheumatologyFollowup[0].sole_lt,


rhand_fl:res.data.RheumatologyFollowup[0].rhand_fl,
rhand_ex:res.data.RheumatologyFollowup[0].rhand_ex,
rhand_ab:res.data.RheumatologyFollowup[0].rhand_ab,
rhand_add:res.data.RheumatologyFollowup[0].rhand_add,
rhand_slx:res.data.RheumatologyFollowup[0].rhand_slx,



lhand_fl:res.data.RheumatologyFollowup[0].lhand_fl,
lhand_ex:res.data.RheumatologyFollowup[0].lhand_ex,
lhand_ab:res.data.RheumatologyFollowup[0].lhand_ab,
lhand_add:res.data.RheumatologyFollowup[0].lhand_add,
lhand_slx:res.data.RheumatologyFollowup[0].lhand_slx,




rwrist_fl:res.data.RheumatologyFollowup[0].rwrist_fl,
rwrist_ex:res.data.RheumatologyFollowup[0].rwrist_ex,
rwrist_ab:res.data.RheumatologyFollowup[0].rwrist_ab,
rwrist_add:res.data.RheumatologyFollowup[0].rwrist_add,
rwrist_slx:res.data.RheumatologyFollowup[0].rwrist_slx,




lwrist_fl:res.data.RheumatologyFollowup[0].lwrist_fl,
lwrist_ex:res.data.RheumatologyFollowup[0].lwrist_ex,
lwrist_ab:res.data.RheumatologyFollowup[0].lwrist_ab,
lwrist_add:res.data.RheumatologyFollowup[0].lwrist_add,
lwrist_slx:res.data.RheumatologyFollowup[0].lwrist_slx,



relb_fl:res.data.RheumatologyFollowup[0].relb_fl,
relb_ex:res.data.RheumatologyFollowup[0].relb_ex,
relb_ab:res.data.RheumatologyFollowup[0].relb_ab,
relb_add:res.data.RheumatologyFollowup[0].relb_add,
relb_slx:res.data.RheumatologyFollowup[0].relb_slx,



lelb_fl:res.data.RheumatologyFollowup[0].lelb_fl,
lelb_ex:res.data.RheumatologyFollowup[0].lelb_ex,
lelb_ab:res.data.RheumatologyFollowup[0].lelb_ab,
lelb_add:res.data.RheumatologyFollowup[0].lelb_add,
lelb_slx:res.data.RheumatologyFollowup[0].lelb_slx,


shrt_fl:res.data.RheumatologyFollowup[0].shrt_fl,
shrt_ex:res.data.RheumatologyFollowup[0].shrt_ex,
shrt_ab:res.data.RheumatologyFollowup[0].shrt_ab,
shrt_add:res.data.RheumatologyFollowup[0].shrt_add,
shrt_slx:res.data.RheumatologyFollowup[0].shrt_slx,


shlt_fl:res.data.RheumatologyFollowup[0].shlt_fl,
shlt_ex:res.data.RheumatologyFollowup[0].shlt_ex,
shlt_ab:res.data.RheumatologyFollowup[0].shlt_ab,
shlt_add:res.data.RheumatologyFollowup[0].shlt_add,
shlt_slx:res.data.RheumatologyFollowup[0].shlt_slx,



kneert_fl:res.data.RheumatologyFollowup[0].kneert_fl,
kneert_ex:res.data.RheumatologyFollowup[0].kneert_ex,
kneert_ab:res.data.RheumatologyFollowup[0].kneert_ab,
kneert_add:res.data.RheumatologyFollowup[0].kneert_add,
kneert_slx:res.data.RheumatologyFollowup[0].kneert_slx,





kneelt_fl:res.data.RheumatologyFollowup[0].kneelt_fl,
kneelt_ex:res.data.RheumatologyFollowup[0].kneelt_ex,
kneelt_ab:res.data.RheumatologyFollowup[0].kneelt_ab,
kneelt_add:res.data.RheumatologyFollowup[0].kneelt_add,
kneelt_slx:res.data.RheumatologyFollowup[0].kneelt_slx,


footrt_fl:res.data.RheumatologyFollowup[0].footrt_fl,
footrt_ex:res.data.RheumatologyFollowup[0].footrt_ex,
footrt_ab:res.data.RheumatologyFollowup[0].footrt_ab,
footrt_add:res.data.RheumatologyFollowup[0].footrt_add,
footrt_slx:res.data.RheumatologyFollowup[0].footrt_slx,



footlt_fl:res.data.RheumatologyFollowup[0].footlt_fl,
footlt_ex:res.data.RheumatologyFollowup[0].footlt_ex,
footlt_ab:res.data.RheumatologyFollowup[0].footlt_ab,
footlt_add:res.data.RheumatologyFollowup[0].footlt_add,
footlt_slx:res.data.RheumatologyFollowup[0].footlt_slx,



thumb_rt:res.data.RheumatologyFollowup[0].thumb_rt,
thumb_lt:res.data.RheumatologyFollowup[0].thumb_lt,
finger_rt:res.data.RheumatologyFollowup[0].finger_rt,


finger_lt:res.data.RheumatologyFollowup[0].finger_lt,
palm_rt:res.data.RheumatologyFollowup[0].palm_rt,
palm_lt:res.data.RheumatologyFollowup[0].palm_lt,




elbow_rt:res.data.RheumatologyFollowup[0].elbow_rt,
elbow_lt:res.data.RheumatologyFollowup[0].elbow_lt,
hy_knee_rt:res.data.RheumatologyFollowup[0].hy_knee_rt,



hy_knee_lt:res.data.RheumatologyFollowup[0].hy_knee_lt,
ankle_rt:res.data.RheumatologyFollowup[0].ankle_rt,
ankle_lt:res.data.RheumatologyFollowup[0].ankle_lt,


other_rt:res.data.RheumatologyFollowup[0].other_rt,
other_lt:res.data.RheumatologyFollowup[0].other_lt,
spine_rt:res.data.RheumatologyFollowup[0].spine_rt,
spine_lt:res.data.RheumatologyFollowup[0].spine_lt,

can_u_dress_urself:res.data.RheumatologyFollowup[0].can_u_dress_urself,
can_u_wash_ur_hair:res.data.RheumatologyFollowup[0].can_u_wash_ur_hair,
can_u_comb_ur_hair:res.data.RheumatologyFollowup[0].can_u_comb_ur_hair,
dressing_score:res.data.RheumatologyFollowup[0].dressing_score,


can_u_stand_from_chair:res.data.RheumatologyFollowup[0].can_u_stand_from_chair,
can_u_get_inout_from_bed:res.data.RheumatologyFollowup[0].can_u_get_inout_from_bed,
can_u_sit_grossteg_onfloor:res.data.RheumatologyFollowup[0].can_u_sit_grossteg_onfloor,
arising_score:res.data.RheumatologyFollowup[0].arising_score,


can_u_cut_vegetables:res.data.RheumatologyFollowup[0].can_u_cut_vegetables,
can_u_lift_glass:res.data.RheumatologyFollowup[0].can_u_lift_glass,
can_u_break_roti_from_1hand:res.data.RheumatologyFollowup[0].can_u_break_roti_from_1hand,
eating_score:res.data.RheumatologyFollowup[0].eating_score,


can_u_walk:res.data.RheumatologyFollowup[0].can_u_walk,
can_u_climb_5steps:res.data.RheumatologyFollowup[0].can_u_climb_5steps,
walking_score:res.data.RheumatologyFollowup[0].walking_score,


can_u_take_bath:res.data.RheumatologyFollowup[0].can_u_take_bath,
can_u_wash_dry_urbody:res.data.RheumatologyFollowup[0].can_u_wash_dry_urbody,
can_u_get_onoff_toilet:res.data.RheumatologyFollowup[0].can_u_get_onoff_toilet,
hygiene_score:res.data.RheumatologyFollowup[0].hygiene_score,


can_u_weigh_2kg:res.data.RheumatologyFollowup[0].can_u_weigh_2kg,
can_u_bend_and_pickcloths:res.data.RheumatologyFollowup[0].can_u_bend_and_pickcloths,
reaching_score:res.data.RheumatologyFollowup[0].reaching_score,



can_u_open_bottle:res.data.RheumatologyFollowup[0].can_u_open_bottle,
can_u_turntaps_onoff:res.data.RheumatologyFollowup[0].can_u_turntaps_onoff,  
can_u_open_latches:res.data.RheumatologyFollowup[0].can_u_open_latches,
grip_score:res.data.RheumatologyFollowup[0].grip_score,


can_u_work_office_house:res.data.RheumatologyFollowup[0].can_u_work_office_house,
can_u_run_errands:res.data.RheumatologyFollowup[0].can_u_run_errands,
can_u_get_inout_of_bus:res.data.RheumatologyFollowup[0].can_u_get_inout_of_bus,
activities_score:res.data.RheumatologyFollowup[0].activities_score,


patient_assessment_pain:res.data.RheumatologyFollowup[0].patient_assessment_pain,
grip_strength_rt:res.data.RheumatologyFollowup[0].grip_strength_rt,
grip_strength_hg:res.data.RheumatologyFollowup[0].grip_strength_hg,
grip_strength_lt:res.data.RheumatologyFollowup[0].grip_strength_lt,


early_mrng_stiffness:res.data.RheumatologyFollowup[0].early_mrng_stiffness,
sleep:res.data.RheumatologyFollowup[0].sleep,
general_health_assessment:res.data.RheumatologyFollowup[0].general_health_assessment,



   
      
        
      })
      
     } else {
       this.setState({ pid: 0 })
     }
           
// const no=res.data.RheumatologyFollowup[0].pid
// console.log(no)
 
    })
    axios.get(`https://abcapi.vidaria.in/rfattachmentslist?X-AUTH=abc123&pid=${this.props.match.params.id}`)
    .then(res => {
      const products = res.data.RFattachments;
      console.log(products)
      for (let i = 0; i < products.length; i++) {
        this.setState({ products });
     
      }
    });
  }






  submit = e => {
    e.preventDefault()
   
    
    axios.post(`https://abcapi.vidaria.in/addactivity?X-AUTH=abc123`,{

      activity :"updated Followup details of Rheumatology  patient",
      user_id:this.state.user_id,

    })
      axios.put(`https://abcapi.vidaria.in/updatefollowuprheuma?X-AUTH=abc123`,{


    pid:this.state.pid,



    user_id:this.state.user_id,

    code:this.state.code,
    pname:this.state.pname,
    age:this.state.age,
    sex:this.state.sex,


    address:this.state.address,
    phone:this.state.phone,
    firstvisit:this.state.firstvisit,
    lastvisit:this.state.lastvisit,


    timelapsed:this.state.timelapsed,
    bp:this.state.bp,
    wt:this.state.wt,
    followup:this.state.followup,



    course_result:this.state.course_result,

    improved_percent:this.state.improved_percent,
    deteriorate_percent:this.state.deteriorate_percent,


    history:this.state.history,
    diagnosis:this.state.diagnosis,



    addnewdiagnosis:this.state.addnewdiagnosis,
    anorexia:this.state.anorexia,
    anorexia_duration:this.state.anorexia_duration,
    anorexia_severity:this.state.anorexia_severity,
    anorexia_cause:this.state.anorexia_cause,




    nausea:this.state.nausea,
    nausea_duration:this.state.nausea_duration,
    nausea_severity:this.state.nausea_severity,
    nausea_cause:this.state.nausea_cause,
    vomiting:this.state.vomiting,
    vomiting_duration:this.state.vomiting_duration,
    vomiting_severity:this.state.vomiting_severity,
    vomiting_cause:this.state.vomiting_cause,



    acidity:this.state.acidity,
    acidity_duration:this.state.acidity_duration,
    acidity_severity:this.state.acidity_severity,
    acidity_cause:this.state.acidity_cause,
    pain_abdomen:this.state.pain_abdomen,
    pain_duration:this.state.pain_duration,
    pain_severity:this.state.pain_severity,
    pain_cause:this.state.pain_cause,





    diarrhoea:this.state.diarrhoea,
    diarrhoea_duration:this.state.diarrhoea_duration,
    diarrhoea_severity:this.state.diarrhoea_severity,
    diarrhoea_cause:this.state.diarrhoea_cause,
    oral_ulcers:this.state.oral_ulcers,

    ulcers_duration:this.state.ulcers_duration,
    ulcers_severity:this.state.ulcers_severity,
    ulcers_cause:this.state.ulcers_cause,



    constipation:this.state.constipation,
    constipation_duration:this.state.constipation_duration,
    constipation_severity:this.state.constipation_severity,
    constipation_cause:this.state.constipation_cause,
    skinrash:this.state.skinrash,
    
    skinrash_duration:this.state.skinrash_duration,
    skinrash_severity:this.state.skinrash_severity,
    skinrash_cause:this.state.skinrash_cause,


    hairfall:this.state.hairfall,
    hairfall_duration:this.state.hairfall_duration,
    hairfall_severity:this.state.hairfall_severity,
    hairfall_cause:this.state.hairfall_cause,
    othertoxics:this.state.othertoxics,
    
    othertoxics_duration:this.state.othertoxics_duration,
    othertoxics_severity:this.state.othertoxics_severity,
    othertoxics_cause:this.state.othertoxics_cause,





    hairfall:this.state.hairfall,
    hairfall_duration:this.state.hairfall_duration,
    hairfall_severity:this.state.hairfall_severity,
    hairfall_cause:this.state.hairfall_cause,
    othertoxics:this.state.othertoxics,
    
    othertoxics_duration:this.state.othertoxics_duration,
    othertoxics_severity:this.state.othertoxics_severity,
    othertoxics_cause:this.state.othertoxics_cause,


    hcq : this.state.hcq,
    mtx : this.state.mtx,
    ssz : this.state.ssz,
    lef : this.state.lef,
    azt : this.state.azt,
    mmf : this.state.mmf,
    pred : this.state.pred,
    nsaid : this.state.nsaid,
    esr : this.state.esr,
    hb : this.state.hb,
    tlc : this.state.tlc,
    dlc : this.state.dlc,
    platelet : this.state.platelet,
    urine : this.state.urine,
    bun : this.state.bun,
    cr : this.state.cr,
    sgot : this.state.sgot,
    sgpt : this.state.sgpt,
    alb : this.state.alb,
    glob : this.state.glob,
    ua : this.state.ua,
    bsl : this.state.bsl,
    rf : this.state.rf,
    crp : this.state.crp,
    bili : this.state.bili,
    other : this.state.other,
    xray : this.state.xray,
    treatment_plan : this.state.treatment_plan,







    tm_t1:this.state.tm_t1,
    tm_s1:this.state.tm_s1,
    tm_t2:this.state.tm_t2,
    tm_s2:this.state.tm_s2,

    scl_t1:this.state.scl_t1,
    scl_s1:this.state.scl_s1,
    scl_t2:this.state.scl_t2,
    scl_s2:this.state.scl_s2,



    acl_t1:this.state.acl_t1,
    acl_s1:this.state.acl_s1,
    acl_t2:this.state.acl_t2,
    acl_s2:this.state.acl_s2,


    sh_t1:this.state.sh_t1,
    sh_s1:this.state.sh_s1,
    sh_t2:this.state.sh_t2,
    sh_s2:this.state.sh_s2,

    elbow_t1:this.state.elbow_t1,
    elbow_s1:this.state.elbow_s1,
    elbow_t2:this.state.elbow_t2,
    elbow_s2:this.state.elbow_s2,


    infru_t1:this.state.infru_t1,
    infru_s1:this.state.infru_s1,
    infru_t2:this.state.infru_t2,
    infru_s2:this.state.infru_s2,


    cmc1_t1:this.state.cmc1_t1,
    cmc1_s1:this.state.cmc1_s1,
    cmc1_t2:this.state.cmc1_t2,
    cmc1_s2:this.state.cmc1_s2,



    wrist_t1:this.state.wrist_t1,
    wrist_s1:this.state.wrist_s1,
    wrist_t2:this.state.wrist_t2,
    wrist_s2:this.state.wrist_s2,


    dip2_t1:this.state.dip2_t1,
    dip2_s1:this.state.dip2_s1,
    dip2_t2:this.state.dip2_t2,
    dip2_s2:this.state.dip2_s2,


    dip3_t1:this.state.dip3_t1,
    dip3_s1:this.state.dip3_s1,
    dip3_t2:this.state.dip3_t2,
    dip3_s2:this.state.dip3_s2,


    dip4_t1:this.state.dip4_t1,
    dip4_s1:this.state.dip4_s1,
    dip4_t2:this.state.dip4_t2,
    dip4_s2:this.state.dip4_s2,



    dip5_t1:this.state.dip5_t1,
    dip5_s1:this.state.dip5_s1,
    dip5_t2:this.state.dip5_t2,
    dip5_s2:this.state.dip5_s2,


    r1ip1_t1:this.state.r1ip1_t1,
    r1ip1_s1:this.state.r1ip1_s1,
    r1ip1_t2:this.state.r1ip1_t2,
    r1ip1_s2:this.state.r1ip1_s2,


    r1ip2_t1:this.state.r1ip2_t1,
    r1ip2_s1:this.state.r1ip2_s1,
    r1ip2_t2:this.state.r1ip2_t2,
    r1ip2_s2:this.state.r1ip2_s2,


    pip3_t1:this.state.pip3_t1,
    pip3_s1:this.state.pip3_s1,
    pip3_t2:this.state.pip3_t2,
    pip3_s2:this.state.pip3_s2,


    pip4_t1:this.state.pip4_t1,
    pip4_s1:this.state.pip4_s1,
    pip4_t2:this.state.pip4_t2,
    pip4_s2:this.state.pip4_s2,

    pip5_t1:this.state.pip5_t1,
    pip5_s1:this.state.pip5_s1,
    pip5_t2:this.state.pip5_t2,
    pip5_s2:this.state.pip5_s2,

    mcp1_t1:this.state.mcp1_t1,
    mcp1_s1:this.state.mcp1_s1,
    mcp1_t2:this.state.mcp1_t2,
    mcp1_s2:this.state.mcp1_s2,


    mcp2_t1:this.state.mcp2_t1,
    mcp2_s1:this.state.mcp2_s1,
    mcp2_t2:this.state.mcp2_t2,
    mcp2_s2:this.state.mcp2_s2,

    mcp3_t1:this.state.mcp3_t1,
    mcp3_s1:this.state.mcp3_s1,
    mcp3_t2:this.state.mcp3_t2,
    mcp3_s2:this.state.mcp3_s2,


    mcp4_t1:this.state.mcp4_t1,
    mcp4_s1:this.state.mcp4_s1,
    mcp4_t2:this.state.mcp4_t2,
    mcp4_s2:this.state.mcp4_s2,

    mcp5_t1:this.state.mcp5_t1,
    mcp5_s1:this.state.mcp5_s1,
    mcp5_t2:this.state.mcp5_t2,
    mcp5_s2:this.state.mcp5_s2,

    hip_t1:this.state.hip_t1,
    hip_s1:this.state.hip_s1,
    hip_t2:this.state.hip_t2,
    hip_s2:this.state.hip_s2,


    knee_t1:this.state.knee_t1,
    knee_s1:this.state.knee_s1,
    knee_t2:this.state.knee_t2,
    knee_s2:this.state.knee_s2,


    a_tt_t1:this.state.a_tt_t1,
    a_tt_s1:this.state.a_tt_s1,
    a_tt_t2:this.state.a_tt_t2,
    a_tt_s2:this.state.a_tt_s2,

    a_tc_t1:this.state.a_tc_t1,
    a_tc_s1:this.state.a_tc_s1,
    a_tc_t2:this.state.a_tc_t2,
    a_tc_s2:this.state.a_tc_s2,

    mtl_t1:this.state.mtl_t1,
    mtl_s1:this.state.mtl_s1,
    mtl_t2:this.state.mtl_t2,
    mtl_s2:this.state.mtl_s2,


    mtp1_t1:this.state.mtp1_t1,
    mtp1_s1:this.state.mtp1_s1,
    mtp1_t2:this.state.mtp1_t2,
    mtp1_s2:this.state.mtp1_s2,


    mtp2_t1:this.state.mtp2_t1,
    mtp2_s1:this.state.mtp2_s1,
    mtp2_t2:this.state.mtp2_t2,
    mtp2_s2:this.state.mtp2_s2,


    mtp3_t1:this.state.mtp3_t1,
    mtp3_s1:this.state.mtp3_s1,
    mtp3_t2:this.state.mtp3_t2,
    mtp3_s2:this.state.mtp3_s2,


    mtp4_t1:this.state.mtp4_t1,
    mtp4_s1:this.state.mtp4_s1,
    mtp4_t2:this.state.mtp4_t2,
    mtp4_s2:this.state.mtp4_s2,


    mtp5_t1:this.state.mtp5_t1,
    mtp5_s1:this.state.mtp5_s1,
    mtp5_t2:this.state.mtp5_t2,
    mtp5_s2:this.state.mtp5_s2,


    r2ip1_t1:this.state.r2ip1_t1,
    r2ip1_s1:this.state.r2ip1_s1,
    r2ip1_t2:this.state.r2ip1_t2,
    r2ip1_s2:this.state.r2ip1_s2,


    r2ip2_t1:this.state.r2ip2_t1,
    r2ip2_s1:this.state.r2ip2_s1,
    r2ip2_t2:this.state.r2ip2_t2,
    r2ip2_s2:this.state.r2ip2_s2,


    ip3_t1:this.state.ip3_t1,
    ip3_s1:this.state.ip3_s1,
    ip3_t2:this.state.ip3_t2,
    ip3_s2:this.state.ip3_s2,

    ip4_t1:this.state.ip4_t1,
    ip4_s1:this.state.ip4_s1,
    ip4_t2:this.state.ip4_t2,
    ip4_s2:this.state.ip4_s2,


    ip5_t1:this.state.ip5_t1,
    ip5_s1:this.state.ip5_s1,
    ip5_t2:this.state.ip5_t2,
    ip5_s2:this.state.ip5_s2,


    s1_t1:this.state.s1_t1,
    s1_s1:this.state.s1_s1,
    s1_t2:this.state.s1_t2,
    s1_s2:this.state.s1_s2,


    cervical_anky:this.state.cervical_anky,
    cervical_flex:this.state.cervical_flex,
    cervical_ext:this.state.cervical_ext,
    cervical_rtrot:this.state.cervical_rtrot,

    cervical_ltrot:this.state.cervical_ltrot,
    cervical_rtfl:this.state.cervical_rtfl,
    cervical_ltfl:this.state.cervical_ltfl,
    cervical_pt:this.state.cervical_pt,


    thorasic_anky:this.state.thorasic_anky,
    thorasic_flex:this.state.thorasic_flex,
    thorasic_ext:this.state.thorasic_ext,
    thorasic_rtrot:this.state.thorasic_rtrot,

    thorasic_ltrot:this.state.thorasic_ltrot,
    thorasic_rtfl:this.state.thorasic_rtfl,
    thorasic_ltfl:this.state.thorasic_ltfl,
    thorasic_pt:this.state.thorasic_pt,





    lumbar_anky:this.state.lumbar_anky,
    lumbar_flex:this.state.lumbar_flex,
    lumbar_ext:this.state.lumbar_ext,
    lumbar_rtrot:this.state.lumbar_rtrot,
    lumbar_ltrot:this.state.lumbar_ltrot,



    lumbar_rtfl:this.state.lumbar_rtfl,
    lumbar_ltfl:this.state.lumbar_ltfl,
    lumbar_pt:this.state.lumbar_pt,


    opt_rt:this.state.opt_rt,
    opt_lt:this.state.opt_lt,

    lcer_rt:this.state.lcer_rt,
    lcer_lt:this.state.lcer_lt,
    trpz_rt:this.state.trpz_rt,


    trpz_lt:this.state.trpz_lt,
    scap_rt:this.state.scap_rt,
    scap_lt:this.state.scap_lt,

    zcst_rt:this.state.zcst_rt,
    zcst_lt:this.state.zcst_lt,
    epdl_rt:this.state.epdl_rt,


    epdl_lt:this.state.epdl_lt,
    glut_rt:this.state.glut_rt,
    glut_lt:this.state.glut_lt,


    trcr_rt:this.state.trcr_rt,
    trcr_lt:this.state.trcr_lt,
    knee_rt:this.state.knee_rt,




    knee_lt:this.state.knee_lt,
    ta_rt:this.state.ta_rt,
    ta_lt:this.state.ta_lt,


    calf_rt:this.state.calf_rt,
    calf_lt:this.state.calf_lt,
    sole_rt:this.state.sole_rt,


    sole_lt:this.state.sole_lt,


    rhand_fl:this.state.rhand_fl,
    rhand_ex:this.state.rhand_ex,
    rhand_ab:this.state.rhand_ab,
    rhand_add:this.state.rhand_add,
    rhand_slx:this.state.rhand_slx,



    lhand_fl:this.state.lhand_fl,
    lhand_ex:this.state.lhand_ex,
    lhand_ab:this.state.lhand_ab,
    lhand_add:this.state.lhand_add,
    lhand_slx:this.state.lhand_slx,




    rwrist_fl:this.state.rwrist_fl,
    rwrist_ex:this.state.rwrist_ex,
    rwrist_ab:this.state.rwrist_ab,
    rwrist_add:this.state.rwrist_add,
    rwrist_slx:this.state.rwrist_slx,




    lwrist_fl:this.state.lwrist_fl,
    lwrist_ex:this.state.lwrist_ex,
    lwrist_ab:this.state.lwrist_ab,
    lwrist_add:this.state.lwrist_add,
    lwrist_slx:this.state.lwrist_slx,



    relb_fl:this.state.relb_fl,
    relb_ex:this.state.relb_ex,
    relb_ab:this.state.relb_ab,
    relb_add:this.state.relb_add,
    relb_slx:this.state.relb_slx,



    lelb_fl:this.state.lelb_fl,
    lelb_ex:this.state.lelb_ex,
    lelb_ab:this.state.lelb_ab,
    lelb_add:this.state.lelb_add,
    lelb_slx:this.state.lelb_slx,



    shrt_fl:this.state.shrt_fl,
    shrt_ex:this.state.shrt_ex,
    shrt_ab:this.state.shrt_ab,
    shrt_add:this.state.shrt_add,
    shrt_slx:this.state.shrt_slx,


    shlt_fl:this.state.shlt_fl,
    shlt_ex:this.state.shlt_ex,
    shlt_ab:this.state.shlt_ab,
    shlt_add:this.state.shlt_add,
    shlt_slx:this.state.shlt_slx,



    kneert_fl:this.state.kneert_fl,
    kneert_ex:this.state.kneert_ex,
    kneert_ab:this.state.kneert_ab,
    kneert_add:this.state.kneert_add,
    kneert_slx:this.state.kneert_slx,





    kneelt_fl:this.state.kneelt_fl,
    kneelt_ex:this.state.kneelt_ex,
    kneelt_ab:this.state.kneelt_ab,
    kneelt_add:this.state.kneelt_add,
    kneelt_slx:this.state.kneelt_slx,


    footrt_fl:this.state.footrt_fl,
    footrt_ex:this.state.footrt_ex,
    footrt_ab:this.state.footrt_ab,
    footrt_add:this.state.footrt_add,
    footrt_slx:this.state.footrt_slx,



    footlt_fl:this.state.footlt_fl,
    footlt_ex:this.state.footlt_ex,
    footlt_ab:this.state.footlt_ab,
    footlt_add:this.state.footlt_add,
    footlt_slx:this.state.footlt_slx,



    thumb_rt:this.state.thumb_rt,
    thumb_lt:this.state.thumb_lt,
    finger_rt:this.state.finger_rt,


    finger_lt:this.state.finger_lt,
    palm_rt:this.state.palm_rt,
    palm_lt:this.state.palm_lt,




    elbow_rt:this.state.elbow_rt,
    elbow_lt:this.state.elbow_lt,
    hy_knee_rt:this.state.hy_knee_rt,



    hy_knee_lt:this.state.hy_knee_lt,
    ankle_rt:this.state.ankle_rt,
    ankle_lt:this.state.ankle_lt,


    other_rt:this.state.other_rt,
    other_lt:this.state.other_lt,
    spine_rt:this.state.spine_rt,
    spine_lt:this.state.spine_lt,
   
    can_u_dress_urself:this.state.can_u_dress_urself,
    can_u_wash_ur_hair:this.state.can_u_wash_ur_hair,
    can_u_comb_ur_hair:this.state.can_u_comb_ur_hair,
    dressing_score:this.state.dressing_score,



    can_u_stand_from_chair:this.state.can_u_stand_from_chair,
    can_u_get_inout_from_bed:this.state.can_u_get_inout_from_bed,
    can_u_sit_grossteg_onfloor:this.state.can_u_sit_grossteg_onfloor,
    arising_score:this.state.arising_score,


    can_u_cut_vegetables:this.state.can_u_cut_vegetables,
    can_u_lift_glass:this.state.can_u_lift_glass,
    can_u_break_roti_from_1hand:this.state.can_u_break_roti_from_1hand,
    eating_score:this.state.eating_score,


    can_u_walk:this.state.can_u_walk,
    can_u_climb_5steps:this.state.can_u_climb_5steps,
    walking_score:this.state.walking_score,




    can_u_take_bath:this.state.can_u_take_bath,
    can_u_wash_dry_urbody:this.state.can_u_wash_dry_urbody,
    can_u_get_onoff_toilet:this.state.can_u_get_onoff_toilet,
    hygiene_score:this.state.hygiene_score,


    can_u_weigh_2kg:this.state.can_u_weigh_2kg,
    can_u_bend_and_pickcloths:this.state.can_u_bend_and_pickcloths,
    reaching_score:this.state.reaching_score,


    can_u_open_bottle:this.state.can_u_open_bottle,
    can_u_turntaps_onoff:this.state.can_u_turntaps_onoff,  
    can_u_open_latches:this.state.can_u_open_latches,
    grip_score:this.state.grip_score,



    can_u_work_office_house:this.state.can_u_work_office_house,
    can_u_run_errands:this.state.can_u_run_errands,
    can_u_get_inout_of_bus:this.state.can_u_get_inout_of_bus,
    activities_score:this.state.activities_score,


    patient_assessment_pain:this.state.patient_assessment_pain,
    grip_strength_rt:this.state.grip_strength_rt,
    grip_strength_hg:this.state.grip_strength_hg,
    grip_strength_lt:this.state.grip_strength_lt,


    early_mrng_stiffness:this.state.early_mrng_stiffness,
    sleep:this.state.sleep,
    general_health_assessment:this.state.general_health_assessment,




      })
      
      .then((res)=>{
        console.log(res.data.success)
        if(res.data.success == "false"){
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: res.data.message,
            
          })
        }else{
       this.opensweetalert()
        }
        // this.opensweetalert()
        // this.componentDidMount();

      })

    }

  


  render()


{
 
  const p = this.state.pid;
  console.log("render ",p)


  return (
    <>
    
    <>
            {this.state.pid == 0 ?
            <h1 className="text-center">{this.norecordsweetalert()}</h1>
              
              : 


      <Container fluid>
        <Row>
          <Col md="8">
            <Card>
              <Card.Header>
                <Card.Title as="h4">Rheumatology  Follow-Up </Card.Title>
              </Card.Header>
              <Card.Body>
                <Form onSubmit={this.submit}>


                <Row>
          <Col md="6">
         
{/* 
          <Form.Group>
        <Form.Label>Patient Id</Form.Label><br></br>
          <Form.Control
           type="number"
           readOnly
           name="pid"                          
           placeholder="patient id"
           value={this.state.pid}
           onChange={(e)=>this.setState({pid:e.target.value})}></Form.Control>
          </Form.Group> */}

<Form.Group>
          <Form.Label>code</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="code"
          placeholder="Code"
          value={this.state.code}
          onChange={(e)=>this.setState({code:e.target.value})}/>
          </Form.Group>

          {/* <Form.Group>
          <Form.Label>User Id</Form.Label><br></br>
          <Form.Control 
          type="number"  
          readOnly
          name="user_id"
          placeholder="number"
          value={this.state.user_id}
          onChange={(e)=>this.setState({user_id:e.target.value})}  />
          </Form.Group> */}
        </Col>

        
        <Col md="6">
        {/* <Form.Group>
          <Form.Label>code</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="code"
          placeholder="Code"
          value={this.state.code}
          onChange={(e)=>this.setState({code:e.target.value})}/>
          </Form.Group> */}

          <Form.Group>
          <Form.Label>Patient Name</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="pname"
          placeholder="pname"
          value={this.state.pname}
          onChange={(e)=>this.setState({pname:e.target.value})}/>
          </Form.Group>
          </Col>
          </Row>


          <Row>
          <Col md="6">
       

          <Form.Group>
          <Form.Label>Age</Form.Label><br></br>
          <Form.Control 
          type="number"
          name="age"  
          placeholder="Age"
          value={this.state.age}
          onChange={(e)=>this.setState({age:e.target.value})}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>Gender</Form.Label><br></br>
          <Form.Control 
          type="text"  
          name="sex"
          placeholder="Gender"
          value={this.state.sex}
          onChange={(e)=>this.setState({sex:e.target.value})}/>
          </Form.Group>


</Col>
<Col md="6">



        

          <Form.Group>
          <Form.Label>Address</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="address"
          placeholder="Address"
          value={this.state.address}
          onChange={(e)=>this.setState({address:e.target.value})}
          />
          </Form.Group>


          <Form.Group>
          <Form.Label>Phone</Form.Label><br></br>
          <Form.Control  
          type="number"
          name="phone"  
          placeholder="Phone"
          value={this.state.phone}
          onChange={(e)=>this.setState({phone:e.target.value})}/>
          </Form.Group>

</Col>
</Row>



<Row>

<Col md="6">


          <Form.Group>
          <Form.Label>firstvisit</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="firstvisit"
          placeholder="firstvisit"
          value={this.state.firstvisit}
          onChange={(e)=>this.setState({firstvisit:e.target.value})}
          />
          </Form.Group>


          <Form.Group>
          <Form.Label>lastvisit</Form.Label><br></br>
          <Form.Control  
          type="text"
          name="lastvisit"  
          placeholder="lastvisit"
          value={this.state.lastvisit}
          onChange={(e)=>this.setState({lastvisit:e.target.value})}/>
          </Form.Group>

</Col>
<Col md="6">
  <Form.Group>
          <Form.Label>timelapsed</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="timelapsed"
          placeholder="timelapsed"
          value={this.state.timelapsed}
          onChange={(e)=>this.setState({timelapsed:e.target.value})}
          />
          </Form.Group>


          <Form.Group>
          <Form.Label>bp</Form.Label><br></br>
          <Form.Control  
          type="number"  
          name="bp"
          placeholder="bp"
          value={this.state.bp}
          onChange={(e)=>this.setState({bp:e.target.value})}
          />
          </Form.Group>

          </Col>


</Row> 

<Row>
 
          <Col md="6">
          <Form.Group>

          <Form.Label>Weight</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="wt"
          placeholder="weight"
          value={this.state.wt}
          onChange={(e)=>this.setState({wt:e.target.value})}
          />
          </Form.Group>

          <Form.Group>
          <Form.Label>followup</Form.Label><br></br>
          <Form.Control  
          type="text"  
          name="followup"
          placeholder="followup"
          value={this.state.followup}
          onChange={(e)=>this.setState({followup:e.target.value})}
          />
          </Form.Group>


</Col>

<Col md="6">
  <Form.Group>
          <Form.Label>course_result</Form.Label><br></br>
           <Form.Control  
           type="text"  
           name="course_result"
            placeholder="course_result"
          value={this.state.course_result}
          onChange={(e)=>this.setState({course_result:e.target.value})}
          
           />
          </Form.Group>

          <Form.Group>
          <Form.Label>improved_percent</Form.Label><br></br>
           <Form.Control  
           type="number"  
           name="improved_percent"
            placeholder="improved_percent"
          value={this.state.improved_percent}
          onChange={(e)=>this.setState({improved_percent:e.target.value})}
          
           />
          </Form.Group>

         </Col> 


</Row>
          

            <Row>
  
      
         <Col md="6">
         <Form.Group>
          <Form.Label>deteriorate_percent</Form.Label><br></br>
           <Form.Control  
           type="number"
           name="deteriorate_percent"
            placeholder="deteriorate_percent"
          value={this.state.deteriorate_percent}
          onChange={(e)=>this.setState({deteriorate_percent:e.target.value})}  
         />
          </Form.Group>

          <Form.Group>
          <Form.Label>history</Form.Label><br></br>
           <Form.Control  
           type="text"  
           name="history"
            placeholder="history"
          value={this.state.history}
          onChange={(e)=>this.setState({history:e.target.value})}
           />
          </Form.Group>

          </Col>


          <Col md="6">   
  <Form.Group>
          <Form.Label>diagnosis</Form.Label><br></br>
           <Form.Control  
           type="text"  
           name="diagnosis"
            placeholder="diagnosis"
          value={this.state.diagnosis}
          onChange={(e)=>this.setState({diagnosis:e.target.value})}
    />
          </Form.Group>
          <Form.Group>
          <Form.Label>add new diagnosis</Form.Label><br></br>
           <Form.Control  
           type="text"
           name="addnewdiagnosis"
            placeholder="addnewdiagnosis"
          value={this.state.addnewdiagnosis}
          onChange={(e)=>this.setState({addnewdiagnosis:e.target.value})}  
          />
          </Form.Group>
          </Col>


          </Row>



<h3> Toxicity table</h3>
<Table striped bordered hover>
  <thead>
    <tr>
    <th>Adverse Effects</th>
      <th>YES/NO</th>
      <th>Duration</th>
      <th>Severity (mild/moderate/severe)</th>
      <th>Cause</th>
    
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td>ANOREXIA</td>

      <td><input type="text" 	onChange={(e)=>this.setState({anorexia:e.target.value})} 
Value={this.state.anorexia} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={(e)=>this.setState({anorexia_duration:e.target.value})} 
Value={this.state.anorexia_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={(e)=>this.setState({anorexia_severity:e.target.value})} 
Value={this.state.anorexia_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={(e)=>this.setState({anorexia_cause:e.target.value})} 
Value={this.state.anorexia_cause} maxlength="50" size="6" /></td>


    </tr>


    <tr>
      <td>NAUSEA</td>

      <td><input type="text" 	onChange={(e)=>this.setState({nausea:e.target.value})} 
Value={this.state.nausea} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={(e)=>this.setState({nausea_duration:e.target.value})} 
Value={this.state.nausea_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={(e)=>this.setState({nausea_severity:e.target.value})} 
Value={this.state.nausea_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={(e)=>this.setState({nausea_cause:e.target.value})} 
Value={this.state.nausea_cause} maxlength="50" size="6" /></td>


    </tr>





    <tr>
      <td>VOMITING</td>

      <td><input type="text" 	onChange={(e)=>this.setState({vomiting:e.target.value})} 
Value={this.state.vomiting} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={(e)=>this.setState({vomiting_duration:e.target.value})} 
Value={this.state.vomiting_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={(e)=>this.setState({vomiting_severity:e.target.value})} 
Value={this.state.vomiting_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={(e)=>this.setState({vomiting_cause:e.target.value})} 
Value={this.state.vomiting_cause} maxlength="50" size="6" /></td>


    </tr>




    <tr>
      <td>ACIDITY</td>

      <td><input type="text" 	onChange={(e)=>this.setState({acidity:e.target.value})} 
Value={this.state.acidity} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={(e)=>this.setState({acidity_duration:e.target.value})} 
Value={this.state.acidity_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={(e)=>this.setState({acidity_severity:e.target.value})} 
Value={this.state.acidity_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={(e)=>this.setState({acidity_cause:e.target.value})} 
Value={this.state.acidity_cause} maxlength="50" size="6" /></td>


    </tr>
  


    <tr>
      <td>PAIN ABDOMEN</td>

      <td><input type="text" 	onChange={(e)=>this.setState({pain_abdomen:e.target.value})} 
Value={this.state.pain_abdomen} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={(e)=>this.setState({pain_duration:e.target.value})} 
Value={this.state.pain_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={(e)=>this.setState({pain_severity:e.target.value})} 
Value={this.state.pain_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={(e)=>this.setState({pain_cause:e.target.value})} 
Value={this.state.pain_cause} maxlength="50" size="6" /></td>

    </tr>









    <tr>
      <td>DIARRHOEA</td>

      <td><input type="text" 	onChange={(e)=>this.setState({diarrhoea:e.target.value})} 
Value={this.state.diarrhoea} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={(e)=>this.setState({diarrhoea_duration:e.target.value})} 
Value={this.state.diarrhoea_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={(e)=>this.setState({diarrhoea_severity:e.target.value})} 
Value={this.state.diarrhoea_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={(e)=>this.setState({diarrhoea_cause:e.target.value})} 
Value={this.state.diarrhoea_cause} maxlength="50" size="6" /></td>


    </tr>


    <tr>
      <td>ORAL ULCERS</td>

      <td><input type="text" 	onChange={(e)=>this.setState({oral_ulcers:e.target.value})} 
Value={this.state.oral_ulcers} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={(e)=>this.setState({ulcers_duration:e.target.value})} 
Value={this.state.ulcers_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={(e)=>this.setState({ulcers_severity:e.target.value})} 
Value={this.state.ulcers_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={(e)=>this.setState({ulcers_cause:e.target.value})} 
Value={this.state.ulcers_cause} maxlength="50" size="6" /></td>


    </tr>





    <tr>
      <td>CONSTIPATION</td>

      <td><input type="text" 	onChange={(e)=>this.setState({constipation:e.target.value})} 
Value={this.state.constipation} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={(e)=>this.setState({constipation_duration:e.target.value})} 
Value={this.state.constipation_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={(e)=>this.setState({constipation_severity:e.target.value})} 
Value={this.state.constipation_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={(e)=>this.setState({constipation_cause:e.target.value})} 
Value={this.state.constipation_cause} maxlength="50" size="6" /></td>


    </tr>




    <tr>
      <td>SKIN RASH</td>

      <td><input type="text" 	onChange={(e)=>this.setState({skinrash:e.target.value})} 
Value={this.state.skinrash} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={(e)=>this.setState({skinrash_duration:e.target.value})} 
Value={this.state.skinrash_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={(e)=>this.setState({skinrash_severity:e.target.value})} 
Value={this.state.skinrash_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={(e)=>this.setState({skinrash_cause:e.target.value})} 
Value={this.state.skinrash_cause} maxlength="50" size="6" /></td>


    </tr>
  


    <tr>
      <td>HAIR FALL</td>

      <td><input type="text" 	onChange={(e)=>this.setState({hairfall:e.target.value})} 
Value={this.state.hairfall} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={(e)=>this.setState({hairfall_duration:e.target.value})} 
Value={this.state.hairfall_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={(e)=>this.setState({hairfall_severity:e.target.value})} 
Value={this.state.hairfall_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={(e)=>this.setState({hairfall_cause:e.target.value})} 
Value={this.state.hairfall_cause} maxlength="50" size="6" /></td>

    </tr>


    <tr>
      <td>Others</td>

      <td><input type="text" 	onChange={(e)=>this.setState({othertoxics:e.target.value})} 
Value={this.state.othertoxics} maxlength="50" size="6" /></td>
     
     <td><input type="text" 						onChange={(e)=>this.setState({othertoxics_duration:e.target.value})} 
Value={this.state.othertoxics_duration} maxlength="50" size="6" /></td>


<td><input type="text" 	onChange={(e)=>this.setState({othertoxics_severity:e.target.value})} 
Value={this.state.othertoxics_severity} maxlength="50" size="10" /></td>

      <td><input type="text" 	onChange={(e)=>this.setState({othertoxics_cause:e.target.value})} 
Value={this.state.othertoxics_cause} maxlength="50" size="6" /></td>

    </tr>





  </tbody>
</Table>




<h3> Drugs begin on</h3>
<Row>
          <Col md="6">
          <Form.Group>
        <Form.Label>HCQ</Form.Label><br></br>
          <Form.Control type="text" onChange={(e)=>this.setState({hcq:e.target.value})} 
						Value={this.state.hcq}/>
          </Form.Group>


          <Form.Group>
          <Form.Label>MTX</Form.Label><br></br>
          <Form.Control type="text"  onChange={(e)=>this.setState({mtx:e.target.value})}   Value={this.state.mtx}/>
          </Form.Group>


         
        </Col>

        
        <Col md="6">
        <Form.Group>
          <Form.Label>SSZ</Form.Label><br></br>
          <Form.Control type="text"  onChange={(e)=>this.setState({ssz:e.target.value})}  Value={this.state.ssz}  />
          </Form.Group>



          <Form.Group>
          <Form.Label>LEP</Form.Label><br></br>
          <Form.Control type="text"  onChange={(e)=>this.setState({lef:e.target.value})}  Value={this.state.lef}/>
          </Form.Group>


          
          </Col>
          </Row>


          <Row>

          <Col md="6">

<Form.Group>
          <Form.Label>AZT</Form.Label><br></br>
          <Form.Control type="text"  onChange={(e)=>this.setState({azt:e.target.value})}  Value={this.state.azt}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>MMF</Form.Label><br></br>
          <Form.Control type="text"  onChange={(e)=>this.setState({mmf:e.target.value})}  Value={this.state.mmf}/>
          </Form.Group>

</Col>

<Col md="6">

<Form.Group>

          <Form.Label>PRED</Form.Label><br></br>
          <Form.Control type="text"  onChange={(e)=>this.setState({pred:e.target.value})}  Value={this.state.pred}/>
                    </Form.Group>

                    <Form.Group>

<Form.Label>NSAID</Form.Label><br></br>
<Form.Control type="text"  onChange={(e)=>this.setState({nsaid:e.target.value})}  Value={this.state.nsaid}/>
          </Form.Group>

</Col>
</Row>



<Table striped bordered hover>
  <thead>
    <tr>
    <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td>ESR</td>

      <td><input type="text" 	onChange={(e)=>this.setState({esr:e.target.value})} 
Value={this.state.esr} maxlength="50" size="6" /></td>
     


<td>BUN</td>

      <td><input type="text" 	onChange={(e)=>this.setState({bun:e.target.value})} 
Value={this.state.bun} maxlength="50" size="6" /></td>



<td>U.A</td>

      <td><input type="text" 	onChange={(e)=>this.setState({ua:e.target.value})} 
Value={this.state.ua} maxlength="50" size="6" /></td>

    </tr>



    <tr>
      <td>HB</td>

      <td><input type="text" 	onChange={(e)=>this.setState({hb:e.target.value})} 
Value={this.state.hb} maxlength="50" size="6" /></td>
     


<td>CR</td>

      <td><input type="text" 	onChange={(e)=>this.setState({cr:e.target.value})} 
Value={this.state.cr} maxlength="50" size="6" /></td>


<td>BSL</td>

      <td><input type="text" 	onChange={(e)=>this.setState({bsl:e.target.value})} 
Value={this.state.bsl} maxlength="50" size="6" /></td>


    </tr>

    <tr>
      <td>TLC</td>

      <td><input type="text" 	onChange={(e)=>this.setState({tlc:e.target.value})} 
Value={this.state.tlc} maxlength="50" size="6" /></td>
     


<td>SGOT</td>

      <td><input type="text" 	onChange={(e)=>this.setState({sgot:e.target.value})} 
Value={this.state.sgot} maxlength="50" size="6" /></td>



<td>RF</td>

      <td><input type="text" 	onChange={(e)=>this.setState({rf:e.target.value})} 
Value={this.state.rf} maxlength="50" size="6" /></td>

    </tr>

    <tr>
      <td>DLC-P/L/E/M</td>

      <td><input type="text" 	onChange={(e)=>this.setState({dlc:e.target.value})} 
Value={this.state.dlc} maxlength="50" size="6" /></td>
     


<td>SGPT</td>

      <td><input type="text" 	onChange={(e)=>this.setState({sgpt:e.target.value})} 
Value={this.state.sgpt} maxlength="50" size="6" /></td>


<td>CRP</td>

      <td><input type="text" 	onChange={(e)=>this.setState({crp:e.target.value})} 
Value={this.state.crp} maxlength="50" size="6" /></td>


    </tr>



    <tr>
      <td>PLATELET</td>

      <td><input type="text" 	onChange={(e)=>this.setState({platelet:e.target.value})} 
Value={this.state.platelet} maxlength="50" size="6" /></td>
     


<td>ALB</td>

      <td><input type="text" 	onChange={(e)=>this.setState({alb:e.target.value})} 
Value={this.state.alb} maxlength="50" size="6" /></td>



<td>BILI(T)</td>

      <td><input type="text" 	onChange={(e)=>this.setState({bili:e.target.value})} 
Value={this.state.bili} maxlength="50" size="6" /></td>

    </tr>



    <tr>
      <td>URINE</td>

      <td><input type="text" 	onChange={(e)=>this.setState({urine:e.target.value})} 
Value={this.state.urine} maxlength="50" size="6" /></td>
     


<td>GLOB</td>

      <td><input type="text" 	onChange={(e)=>this.setState({glob:e.target.value})} 
Value={this.state.glob} maxlength="50" size="6" /></td>




<td>OTHERS</td>

      <td><input type="text" 	onChange={(e)=>this.setState({other:e.target.value})} 
Value={this.state.other} maxlength="50" size="6" /></td>

    </tr>
  </tbody>
</Table>





<Row>

<Col md="6">

<Form.Group>
<Form.Label>X-ray</Form.Label><br></br>
<Form.Control type="text"  onChange={(e)=>this.setState({xray:e.target.value})}  Value={this.state.xray}/>
</Form.Group>


</Col>

<Col md="6">

<Form.Group>


<Form.Label>Treatment Plan</Form.Label><br></br>
<Form.Control type="text"  onChange={(e)=>this.setState({treatment_plan:e.target.value})}  Value={this.state.treatment_plan}/>
</Form.Group>

</Col>
</Row>






<h3>Patient MUSCULOSKELETAL_EXAMINATIONS</h3>
         <h5>joint_evaluation: Information</h5> 

        <Row>

  
        <Table striped bordered hover>
  <thead>
    <tr>
      <th>T</th>
      <th>S</th>
      <th>RT LT</th>
      <th>T</th>
      <th>S</th>
      <th>T</th>
      <th>S</th>
      <th>RT  LT</th>
      <th>T</th>
      <th>S</th>

    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
          <input 
        type="text"
        name="tm_t1" 
        placeholder="tm_t1"
        value={this.state.tm_t1}
        onChange={(e)=>this.setState({tm_t1:e.target.value})}
        maxlength="50" size="4" 
        />
        
    </td>


      <td><input type="text"
      
      
      

      name="tm_s1" 
placeholder="tm_s1"
 value={this.state.tm_s1}
 onChange={(e)=>this.setState({tm_s1:e.target.value})}
 maxlength="50" size="4" /></td>
      <td>TM</td>
      <td><input type="text"
      
      
      

      name="tm_t2" 
placeholder="tm_t2"
 value={this.state.tm_t2}
 onChange={(e)=>this.setState({tm_t2:e.target.value})}
 maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="tm_s2" 
placeholder="tm_s2"
 value={this.state.tm_s2}
 onChange={(e)=>this.setState({tm_s2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            



            name="mcp3_t1" 
placeholder="mcp3_t1"
 value={this.state.mcp3_t1}
 onChange={(e)=>this.setState({mcp3_t1:e.target.value})}
 maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="mcp3_s1" 
placeholder="mcp3_s1"
 value={this.state.mcp3_s1}
 onChange={(e)=>this.setState({mcp3_s1:e.target.value})}
       maxlength="50" size="4" /></td>

      <td>MCP3</td>
            <td><input type="text"
            
            
            

            name="mcp3_t2" 
placeholder="mcp3_t2"
 value={this.state.mcp3_t2}
 onChange={(e)=>this.setState({mcp3_t2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mcp3_s2" 
placeholder="mcp3_s2"
 value={this.state.mcp3_s2}
 onChange={(e)=>this.setState({mcp3_s2:e.target.value})}
 maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text"
    
    
    

    name="scl_t1" 
placeholder="scl_t1"
 value={this.state.scl_t1}
 onChange={(e)=>this.setState({scl_t1:e.target.value})}
maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="scl_s1" 
placeholder="scl_s1"
 value={this.state.scl_s1}
 onChange={(e)=>this.setState({scl_s1:e.target.value})}
   maxlength="50" size="4" /></td>
      <td>SCL</td>
      <td><input type="text"
      
      
      

      name="scl_t2" 
placeholder="scl_t2"
 value={this.state.scl_t2}
 onChange={(e)=>this.setState({scl_t2:e.target.value})}
  maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="scl_s2" 
placeholder="scl_s2"
 value={this.state.scl_s2}
 onChange={(e)=>this.setState({scl_s2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mcp4_t1" 
placeholder="mcp4_t1"
 value={this.state.mcp4_t1}
 onChange={(e)=>this.setState({mcp4_t1:e.target.value})}
 maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="mcp4_s1" 
placeholder="mcp4_s1"
 value={this.state.mcp4_s1}
 onChange={(e)=>this.setState({mcp4_s1:e.target.value})}
 maxlength="50" size="4" /></td>

      <td>MCP 4</td>
            <td><input type="text"
            
            
            

            name="mcp4_t2" 
placeholder="mcp4_t2"
 value={this.state.mcp4_t2}
 onChange={(e)=>this.setState({mcp4_t2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mcp4_s2" 
placeholder="mcp4_s2"
 value={this.state.mcp4_s2}
 onChange={(e)=>this.setState({mcp4_s2:e.target.value})}
 maxlength="50" size="4" /></td>


    </tr>
    <tr>
    <td><input type="text"
    
    
    

    name="acl_t1" 
placeholder="acl_t1"
 value={this.state.acl_t1}
 onChange={(e)=>this.setState({acl_t1:e.target.value})}
  maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="acl_s1" 
placeholder="acl_s1"
 value={this.state.acl_s1}
 onChange={(e)=>this.setState({acl_s1:e.target.value})}
 maxlength="50" size="4" /></td>
      <td>ACL</td>
      <td><input type="text"
      
      
      

      name="acl_t2" 
placeholder="acl_t2"
 value={this.state.acl_t2}
 onChange={(e)=>this.setState({acl_t2:e.target.value})}
maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="acl_s2" 
placeholder="acl_s2"
 value={this.state.acl_s2}
 onChange={(e)=>this.setState({acl_s2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mcp5_t1" 
placeholder="mcp5_t1"
 value={this.state.mcp5_t1}
 onChange={(e)=>this.setState({mcp5_t1:e.target.value})}
  maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="mcp5_s1" 
placeholder="mcp5_s1"
 value={this.state.mcp5_s1}
 onChange={(e)=>this.setState({mcp5_s1:e.target.value})}
maxlength="50" size="4" /></td>

      <td>MCP 5</td>
      <td><input type="text"
      
      
      

      name="mcp5_t2" 
placeholder="mcp5_t2"
 value={this.state.mcp5_t2}
 onChange={(e)=>this.setState({mcp5_t2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mcp5_s2" 
placeholder="mcp5_s2"
 value={this.state.mcp5_s2}
 onChange={(e)=>this.setState({mcp5_s2:e.target.value})}
 maxlength="50" size="4" /></td>

    </tr>

    <tr>
    <td><input type="text"
    
    
    

    name="sh_t1" 
placeholder="sh_t1"
 value={this.state.sh_t1}
 onChange={(e)=>this.setState({sh_t1:e.target.value})}
  maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="sh_s1" 
placeholder="sh_s1"
 value={this.state.sh_s1}
 onChange={(e)=>this.setState({sh_s1:e.target.value})}
maxlength="50" size="4" /></td>
      <td>SH</td>
      <td><input type="text"
      
      
      

      name="sh_t2" 
placeholder="sh_t2"
 value={this.state.sh_t2}
 onChange={(e)=>this.setState({sh_t2:e.target.value})}
 maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="sh_s2" 
placeholder="sh_s2"
 value={this.state.sh_s2}
 onChange={(e)=>this.setState({sh_s2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="hip_t1" 
placeholder="hip_t1"
 value={this.state.hip_t1}
 onChange={(e)=>this.setState({hip_t1:e.target.value})}
  maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="hip_s1" 
placeholder="hip_s1"
 value={this.state.hip_s1}
 onChange={(e)=>this.setState({hip_s1:e.target.value})}
  maxlength="50" size="4" /></td>

      <td>HIP</td>
            <td><input type="text"
            
            
            

            name="hip_t2" 
placeholder="hip_t2"
 value={this.state.hip_t2}
 onChange={(e)=>this.setState({hip_t2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="hip_s2" 
placeholder="hip_s2"
 value={this.state.hip_s2}
 onChange={(e)=>this.setState({hip_s2:e.target.value})}
 maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text"
    
    
    

    name="elbow_t1" 
placeholder="elbow_t1"
 value={this.state.elbow_t1}
 onChange={(e)=>this.setState({elbow_t1:e.target.value})}
maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="elbow_s1" 
placeholder="elbow_s1"
 value={this.state.elbow_s1}
 onChange={(e)=>this.setState({elbow_s1:e.target.value})}
 maxlength="50" size="4" /></td>
      <td>ELBOW</td>
      <td><input type="text"
      
      
      

      name="elbow_t2" 
placeholder="elbow_t2"
 value={this.state.elbow_t2}
 onChange={(e)=>this.setState({elbow_t2:e.target.value})}
 maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="elbow_s2" 
placeholder="elbow_s2"
 value={this.state.elbow_s2}
 onChange={(e)=>this.setState({elbow_s2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="knee_t1" 
placeholder="knee_t1"
 value={this.state.knee_t1}
 onChange={(e)=>this.setState({knee_t1:e.target.value})}
 maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="knee_s1" 
placeholder="knee_s1"
 value={this.state.knee_s1}
 onChange={(e)=>this.setState({knee_s1:e.target.value})}
  maxlength="50" size="4" /></td>

      <td>KNEE</td>
            <td><input type="text"
            
            
            

            name="knee_t2" 
placeholder="knee_t2"
 value={this.state.knee_t2}
 onChange={(e)=>this.setState({knee_t2:e.target.value})}
   maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="knee_s2" 
placeholder="knee_s2"
 value={this.state.knee_s2}
 onChange={(e)=>this.setState({knee_s2:e.target.value})}
 maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text"
    
    
    

    name="infru_t1" 
placeholder="infru_t1"
 value={this.state.infru_t1}
 onChange={(e)=>this.setState({infru_t1:e.target.value})}
 maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="infru_s1" 
placeholder="infru_s1"
 value={this.state.infru_s1}
 onChange={(e)=>this.setState({infru_s1:e.target.value})}
maxlength="50" size="4" /></td>
      <td>INF RU</td>
      <td><input type="text"
      
      
      

      name="infru_t2" 
placeholder="infru_t2"
 value={this.state.infru_t2}
 onChange={(e)=>this.setState({infru_t2:e.target.value})}
 maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="infru_s2" 
placeholder="infru_s2"
 value={this.state.infru_s2}
 onChange={(e)=>this.setState({infru_s2:e.target.value})}
  maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="a_tt_t1" 
placeholder="a_tt_t1"
 value={this.state.a_tt_t1}
 onChange={(e)=>this.setState({a_tt_t1:e.target.value})}
 maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="a_tt_s1" 
placeholder="a_tt_s1"
 value={this.state.a_tt_s1}
 onChange={(e)=>this.setState({a_tt_s1:e.target.value})}
 maxlength="50" size="4" /></td>

      <td>A(TT)</td>
            <td><input type="text"
            
            
            

            name="a_tt_t2" 
placeholder="a_tt_t2"
 value={this.state.a_tt_t2}
 onChange={(e)=>this.setState({a_tt_t2:e.target.value})}
  maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="a_tt_s2" 
placeholder="a_tt_s2"
 value={this.state.a_tt_s2}
 onChange={(e)=>this.setState({a_tt_s2:e.target.value})}
  maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text"
    
    
    

    name="cmc1_t1" 
placeholder="cmc1_t1"
 value={this.state.cmc1_t1}
 onChange={(e)=>this.setState({cmc1_t1:e.target.value})}
 maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="cmc1_s1" 
placeholder="cmc1_s1"
 value={this.state.cmc1_s1}
 onChange={(e)=>this.setState({cmc1_s1:e.target.value})}
 maxlength="50" size="4" /></td>
      <td>CMC 1</td>
      <td><input type="text"
      
      
      

      name="cmc1_t2" 
placeholder="cmc1_t2"
 value={this.state.cmc1_t2}
 onChange={(e)=>this.setState({cmc1_t2:e.target.value})}
  maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="cmc1_s2" 
placeholder="cmc1_s2"
 value={this.state.cmc1_s2}
 onChange={(e)=>this.setState({cmc1_s2:e.target.value})}
          maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="a_tc_t1" 
placeholder="a_tc_t1"
 value={this.state.a_tc_t1}
 onChange={(e)=>this.setState({a_tc_t1:e.target.value})}
 maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="a_tc_s1" 
placeholder="a_tc_s1"
 value={this.state.a_tc_s1}
 onChange={(e)=>this.setState({a_tc_s1:e.target.value})}
 maxlength="50" size="4" /></td>

      <td>A(TC)</td>
            <td><input type="text"
            
            
            

            name="a_tc_t2" 
placeholder="a_tc_t2"
 value={this.state.a_tc_t2}
 onChange={(e)=>this.setState({a_tc_t2:e.target.value})}
      maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="a_tc_s2" 
placeholder="a_tc_s2"
 value={this.state.a_tc_s2}
 onChange={(e)=>this.setState({a_tc_s2:e.target.value})}
 maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text"
    
    
    

    name="wrist_t1" 
placeholder="wrist_t1"
 value={this.state.wrist_t1}
 onChange={(e)=>this.setState({wrist_t1:e.target.value})}
  maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="wrist_s1" 
placeholder="wrist_s1"
 value={this.state.wrist_s1}
 onChange={(e)=>this.setState({wrist_s1:e.target.value})}
    maxlength="50" size="4" /></td>
      <td>WRIST</td>
      <td><input type="text"
      
      
      

      name="wrist_t2" 
placeholder="wrist_t2"
 value={this.state.wrist_t2}
 onChange={(e)=>this.setState({wrist_t2:e.target.value})}
 maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="wrist_s2" 
placeholder="wrist_s2"
 value={this.state.wrist_s2}
 onChange={(e)=>this.setState({wrist_s2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mtl_t1" 
placeholder="mtl_t1"
 value={this.state.mtl_t1}
 onChange={(e)=>this.setState({mtl_t1:e.target.value})}
  maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="mtl_s1" 
placeholder="mtl_s1"
 value={this.state.mtl_s1}
 onChange={(e)=>this.setState({mtl_s1:e.target.value})}
  maxlength="50" size="4" /></td>

      <td>MTL</td>
            <td><input type="text"
            
            
            

            name="mtl_t2" 
placeholder="mtl_t2"
 value={this.state.mtl_t2}
 onChange={(e)=>this.setState({mtl_t2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mtl_s2" 
placeholder="mtl_s2"
 value={this.state.mtl_s2}
 onChange={(e)=>this.setState({mtl_s2:e.target.value})}
 maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text"
    
    
    

    name="dip2_t1" 
placeholder="dip2_t1"
 value={this.state.dip2_t1}
 onChange={(e)=>this.setState({dip2_t1:e.target.value})}
 maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="dip2_s1" 
placeholder="dip2_s1"
 value={this.state.dip2_s1}
 onChange={(e)=>this.setState({dip2_s1:e.target.value})}
 maxlength="50" size="4" /></td>
      <td>DIP2</td>
      <td><input type="text"
      
      
      

      name="dip2_t2" 
placeholder="dip2_t2"
 value={this.state.dip2_t2}
 onChange={(e)=>this.setState({dip2_t2:e.target.value})}
 maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="dip2_s2" 
placeholder="dip2_s2"
 value={this.state.dip2_s2}
 onChange={(e)=>this.setState({dip2_s2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mtp1_t1" 
placeholder="mtp1_t1"
 value={this.state.mtp1_t1}
 onChange={(e)=>this.setState({mtp1_t1:e.target.value})}
 maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="mtp1_s1" 
placeholder="mtp1_s1"
 value={this.state.mtp1_s1}
 onChange={(e)=>this.setState({mtp1_s1:e.target.value})}
    maxlength="50" size="4" /></td>

      <td>MTP 1</td>
            <td><input type="text"
            
            
            

            name="mtp1_t2" 
placeholder="mtp1_t2"
 value={this.state.mtp1_t2}
 onChange={(e)=>this.setState({mtp1_t2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mtp1_s2" 
placeholder="mtp1_s2"
 value={this.state.mtp1_s2}
 onChange={(e)=>this.setState({mtp1_s2:e.target.value})}
 maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text"
    
    
    

    name="dip3_t1" 
placeholder="dip3_t1"
 value={this.state.dip3_t1}
 onChange={(e)=>this.setState({dip3_t1:e.target.value})}
 maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="dip3_s1" 
placeholder="dip3_s1"
 value={this.state.dip3_s1}
 onChange={(e)=>this.setState({dip3_s1:e.target.value})}
 maxlength="50" size="4" /></td>
      <td>DIP3</td>
      <td><input type="text"
      
      
      

      name="dip3_t2" 
placeholder="dip3_t2"
 value={this.state.dip3_t2}
 onChange={(e)=>this.setState({dip3_t2:e.target.value})}
    maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="dip3_s2" 
placeholder="dip3_s2"
 value={this.state.dip3_s2}
 onChange={(e)=>this.setState({dip3_s2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mtp2_t1" 
placeholder="mtp2_t1"
 value={this.state.mtp2_t1}
 onChange={(e)=>this.setState({mtp2_t1:e.target.value})}
  maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="mtp2_s1" 
placeholder="mtp2_s1"
 value={this.state.mtp2_s1}
 onChange={(e)=>this.setState({mtp2_s1:e.target.value})}
     maxlength="50" size="4" /></td>

      <td>MTP 2</td>
            <td><input type="text"
            
            
            

            name="mtp2_t2" 
placeholder="mtp2_t2"
 value={this.state.mtp2_t2}
 onChange={(e)=>this.setState({mtp2_t2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mtp2_s2" 
placeholder="mtp2_s2"
 value={this.state.mtp2_s2}
 onChange={(e)=>this.setState({mtp2_s2:e.target.value})}
   maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text"
    
    
    

    name="dip4_t1" 
placeholder="dip4_t1"
 value={this.state.dip4_t1}
 onChange={(e)=>this.setState({dip4_t1:e.target.value})}
maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="dip4_s1" 
placeholder="dip4_s1"
 value={this.state.dip4_s1}
 onChange={(e)=>this.setState({dip4_s1:e.target.value})}
   maxlength="50" size="4" /></td>
      <td>DIP4</td>
      <td><input type="text"
      
      
      

      name="dip4_t2" 
placeholder="dip4_t2"
 value={this.state.dip4_t2}
 onChange={(e)=>this.setState({dip4_t2:e.target.value})}
  maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="dip4_s2" 
placeholder="dip4_s2"
 value={this.state.dip4_s2}
 onChange={(e)=>this.setState({dip4_s2:e.target.value})}
maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mtp3_t1" 
placeholder="mtp3_t1"
 value={this.state.mtp3_t1}
 onChange={(e)=>this.setState({mtp3_t1:e.target.value})}
      maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="mtp3_s1" 
placeholder="mtp3_s1"
 value={this.state.mtp3_s1}
 onChange={(e)=>this.setState({mtp3_s1:e.target.value})}
 maxlength="50" size="4" /></td>

      <td>MTP 3</td>
            <td><input type="text"
            
            
            

            name="mtp3_t2" 
placeholder="mtp3_t2"
 value={this.state.mtp3_t2}
 onChange={(e)=>this.setState({mtp3_t2:e.target.value})}
   maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mtp3_s2" 
placeholder="mtp3_s2"
 value={this.state.mtp3_s2}
 onChange={(e)=>this.setState({mtp3_s2:e.target.value})}
  maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text"
    
    
    

    name="dip5_t1" 
placeholder="dip5_t1"
 value={this.state.dip5_t1}
 onChange={(e)=>this.setState({dip5_t1:e.target.value})}
 maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="dip5_s1" 
placeholder="dip5_s1"
 value={this.state.dip5_s1}
 onChange={(e)=>this.setState({dip5_s1:e.target.value})}
  maxlength="50" size="4" /></td>
      <td>DIP5</td>
      <td><input type="text"
      
      
      

      name="dip5_t2" 
placeholder="dip5_t2"
 value={this.state.dip5_t2}
 onChange={(e)=>this.setState({dip5_t2:e.target.value})}
   maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="dip5_s2" 
placeholder="dip5_s2"
 value={this.state.dip5_s2}
 onChange={(e)=>this.setState({dip5_s2:e.target.value})}
   maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mtp4_t1" 
placeholder="mtp4_t1"
 value={this.state.mtp4_t1}
 onChange={(e)=>this.setState({mtp4_t1:e.target.value})}
    maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="mtp4_s1" 
placeholder="mtp4_s1"
 value={this.state.mtp4_s1}
 onChange={(e)=>this.setState({mtp4_s1:e.target.value})}
 maxlength="50" size="4" /></td>

      <td>MTP 4</td>
            <td><input type="text"
            
            
            

            name="mtp4_t2" 
placeholder="mtp4_t2"
 value={this.state.mtp4_t2}
 onChange={(e)=>this.setState({mtp4_t2:e.target.value})}
      maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mtp4_s2" 
placeholder="mtp4_s2"
 value={this.state.mtp4_s2}
 onChange={(e)=>this.setState({mtp4_s2:e.target.value})}
  maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text"
    
    
    

    name="r1ip1_t1" 
placeholder="r1ip1_t1"
 value={this.state.r1ip1_t1}
 onChange={(e)=>this.setState({r1ip1_t1:e.target.value})}
maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="r1ip1_s1" 
placeholder="r1ip1_s1"
 value={this.state.r1ip1_s1}
 onChange={(e)=>this.setState({r1ip1_s1:e.target.value})}
 maxlength="50" size="4" /></td>
      <td>IP 1</td>
      <td><input type="text"
      
      
      

      name="r1ip1_t2" 
placeholder="r1ip1_t2"
 value={this.state.r1ip1_t2}
 onChange={(e)=>this.setState({r1ip1_t2:e.target.value})}
  maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="r1ip1_s2" 
placeholder="r1ip1_s2"
 value={this.state.r1ip1_s2}
 onChange={(e)=>this.setState({r1ip1_s2:e.target.value})}
     maxlength="50" size="4" /></td>



            <td><input type="text"
            
            
            

            name="mtp5_t1" 
placeholder="mtp5_t1"
 value={this.state.mtp5_t1}
 onChange={(e)=>this.setState({mtp5_t1:e.target.value})}
 maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="mtp5_s1" 
placeholder="mtp5_s1"
 value={this.state.mtp5_s1}
 onChange={(e)=>this.setState({mtp5_s1:e.target.value})}
      maxlength="50" size="4" /></td>

      <td>MTP 5</td>
            <td><input type="text"
            
            
            

            name="mtp5_t2" 
placeholder="mtp5_t2"
 value={this.state.mtp5_t2}
 onChange={(e)=>this.setState({mtp5_t2:e.target.value})}
     maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="mtp5_s2" 
placeholder="mtp5_s2"
 value={this.state.mtp5_s2}
 onChange={(e)=>this.setState({mtp5_s2:e.target.value})}
   maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text"
    
    
    

    name="r1ip2_t1" 
placeholder="r1ip2_t1"
 value={this.state.r1ip2_t1}
 onChange={(e)=>this.setState({r1ip2_t1:e.target.value})}
   maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="r1ip2_s1" 
placeholder="r1ip2_s1"
 value={this.state.r1ip2_s1}
 onChange={(e)=>this.setState({r1ip2_s1:e.target.value})}
   maxlength="50" size="4" /></td>
      <td>IP 2</td>
      <td><input type="text"
      
      
      

      name="r1ip2_t2" 
placeholder="r1ip2_t2"
 value={this.state.r1ip2_t2}
 onChange={(e)=>this.setState({r1ip2_t2:e.target.value})}
 maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="r1ip2_s2" 
placeholder="r1ip2_s2"
 value={this.state.r1ip2_s2}
 onChange={(e)=>this.setState({r1ip2_s2:e.target.value})}
  maxlength="50" size="4" /></td>



            <td><input type="text"
            
            
            

            name="r2ip1_t1" 
placeholder="r2ip1_t1"
 value={this.state.r2ip1_t1}
 onChange={(e)=>this.setState({r2ip1_t1:e.target.value})}
 maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="r2ip1_s1" 
placeholder="r2ip1_s1"
 value={this.state.r2ip1_s1}
 onChange={(e)=>this.setState({r2ip1_s1:e.target.value})}
      maxlength="50" size="4" /></td>

      <td>IP 1</td>
            <td><input type="text"
            
            
            

            name="r2ip1_t2" 
placeholder="r2ip1_t2"
 value={this.state.r2ip1_t2}
 onChange={(e)=>this.setState({r2ip1_t2:e.target.value})}
          maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="r2ip1_s2" 
placeholder="r2ip1_s2"
 value={this.state.r2ip1_s2}
 onChange={(e)=>this.setState({r2ip1_s2:e.target.value})}
 maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text"
    
    
    

    name="pip3_t1" 
placeholder="pip3_t1"
 value={this.state.pip3_t1}
 onChange={(e)=>this.setState({pip3_t1:e.target.value})}
 maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="pip3_s1" 
placeholder="pip3_s1"
 value={this.state.pip3_s1}
 onChange={(e)=>this.setState({pip3_s1:e.target.value})}
    maxlength="50" size="4" /></td>
      <td>PIP 3</td>
      <td><input type="text"
      
      
      

      name="pip3_t2" 
placeholder="pip3_t2"
 value={this.state.pip3_t2}
 onChange={(e)=>this.setState({pip3_t2:e.target.value})}
   maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="pip3_s2" 
placeholder="pip3_s2"
 value={this.state.pip3_s2}
 onChange={(e)=>this.setState({pip3_s2:e.target.value})}
   maxlength="50" size="4" /></td>



            <td><input type="text"
            
            
            












            name="r2ip2_t1" 
placeholder="r2ip2_t1"
 value={this.state.r2ip2_t1}
 onChange={(e)=>this.setState({r2ip2_t1:e.target.value})}
   maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="r2ip2_s1" 
placeholder="r2ip2_s1"
 value={this.state.r2ip2_s1}
 onChange={(e)=>this.setState({r2ip2_s1:e.target.value})}
  maxlength="50" size="4" /></td>

      <td>IP 2</td>
            <td><input type="text"
            
            
            

            name="r2ip2_t2" 
placeholder="r2ip2_t2"
 value={this.state.r2ip2_t2}
 onChange={(e)=>this.setState({r2ip2_t2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="r2ip2_s2" 
placeholder="r2ip2_s2"
 value={this.state.r2ip2_s2}
 onChange={(e)=>this.setState({r2ip2_s2:e.target.value})}
 maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text"
    
    







    

    name="pip4_t1" 
placeholder="pip4_t1"
 value={this.state.pip4_t1}
 onChange={(e)=>this.setState({pip4_t1:e.target.value})}
   maxlength="50" size="4" /></td>


      <td><input type="text"
      
      
      

      name="pip4_s1" 
placeholder="pip4_s1"
 value={this.state.pip4_s1}
 onChange={(e)=>this.setState({pip4_s1:e.target.value})}
 maxlength="50" size="4" /></td>
      <td>PIP 4</td>
      <td><input type="text"
      
      
      

      name="pip4_t2" 
placeholder="pip4_t2"
 value={this.state.pip4_t2}
 onChange={(e)=>this.setState({pip4_t2:e.target.value})}
 maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="pip4_s2" 
placeholder="pip4_s2"
 value={this.state.pip4_s2}
 onChange={(e)=>this.setState({pip4_s2:e.target.value})}
  maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            







            name="ip3_t1" 
placeholder="ip3_t1"
 value={this.state.ip3_t1}
 onChange={(e)=>this.setState({ip3_t1:e.target.value})}
    maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="ip3_s1" 
placeholder="ip3_s1"
 value={this.state.ip3_s1}
 onChange={(e)=>this.setState({ip3_s1:e.target.value})}
  maxlength="50" size="4" /></td>

      <td>IP 3</td>
            <td><input type="text"
            
            
            

            name="ip3_t2" 
placeholder="ip3_t2"
 value={this.state.ip3_t2}
 onChange={(e)=>this.setState({ip3_t2:e.target.value})}
  maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="ip3_s2" 
placeholder="ip3_s2"
 value={this.state.ip3_s2}
 onChange={(e)=>this.setState({ip3_s2:e.target.value})}
     maxlength="50" size="4" /></td>

    </tr>







    <tr>
    <td><input type="text"
    
  

    name="pip5_t1" 
placeholder="pip5_t1"
 value={this.state.pip5_t1}
 onChange={(e)=>this.setState({pip5_t1:e.target.value})}
  maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="pip5_s1" 
placeholder="pip5_s1"
 value={this.state.pip5_s1}
 onChange={(e)=>this.setState({pip5_s1:e.target.value})}
   maxlength="50" size="4" /></td>
      <td>PIP 5</td>
      <td><input type="text"
      
      
      

      name="pip5_t2" 
placeholder="pip5_t2"
 value={this.state.pip5_t2}
 onChange={(e)=>this.setState({pip5_t2:e.target.value})}
 maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="pip5_s2" 
placeholder="pip5_s2"
 value={this.state.pip5_s2}
 onChange={(e)=>this.setState({pip5_s2:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            





            name="ip4_t1" 
placeholder="ip4_t1"
 value={this.state.ip4_t1}
 onChange={(e)=>this.setState({ip4_t1:e.target.value})}
      maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="ip4_s1" 
placeholder="ip4_s1"
 value={this.state.ip4_s1}
 onChange={(e)=>this.setState({ip4_s1:e.target.value})}
     maxlength="50" size="4" /></td>

      <td>IP 4</td>
            <td><input type="text"
            
            
            

            name="ip4_t2" 
placeholder="ip4_t2"
 value={this.state.ip4_t2}
 onChange={(e)=>this.setState({ip4_t2:e.target.value})}
    maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="ip4_s2" 
placeholder="ip4_s2"
 value={this.state.ip4_s2}
 onChange={(e)=>this.setState({ip4_s2:e.target.value})}
          maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text"
    
    
    

    name="mcp1_t1" 
placeholder="mcp1_t1"
 value={this.state.mcp1_t1}
 onChange={(e)=>this.setState({mcp1_t1:e.target.value})}
 maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="mcp1_s1" 
placeholder="mcp1_s1"
 value={this.state.mcp1_s1}
 onChange={(e)=>this.setState({mcp1_s1:e.target.value})}
    maxlength="50" size="4" /></td>
      <td>MCP 1</td>
      <td><input type="text"
      
      
      

      name="mcp1_t2" 
placeholder="mcp1_t2"
 value={this.state.mcp1_t2}
 onChange={(e)=>this.setState({mcp1_t2:e.target.value})}
    maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="mcp1_s2" 
placeholder="mcp1_s2"
 value={this.state.mcp1_s2}
 onChange={(e)=>this.setState({mcp1_s2:e.target.value})}
       maxlength="50" size="4" /></td>





            <td><input type="text"
            name="ip5_t1" 
placeholder="ip5_t1"
 value={this.state.ip5_t1}
 onChange={(e)=>this.setState({ip5_t1:e.target.value})}
     maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="ip5_s1" 
placeholder="ip5_s1"
 value={this.state.ip5_s1}
 onChange={(e)=>this.setState({ip5_s1:e.target.value})}
      maxlength="50" size="4" /></td>

      <td>IP 5</td>
            <td><input type="text"
            
            
            

            name="ip5_t2" 
placeholder="ip5_t2"
 value={this.state.ip5_t2}
 onChange={(e)=>this.setState({ip5_t2:e.target.value})}
          maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="ip5_s2" 
placeholder="ip5_s2"
 value={this.state.ip5_s2}
 onChange={(e)=>this.setState({ip5_s2:e.target.value})}
            maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td><input type="text"
    
    
    

    name="mcp2_t1" 
placeholder="mcp2_t1"
 value={this.state.mcp2_t1}
 onChange={(e)=>this.setState({mcp2_t1:e.target.value})}
    maxlength="50" size="4" /></td>
      <td><input type="text"
      
      
      

      name="mcp2_s1" 
placeholder="mcp2_s1"
 value={this.state.mcp2_s1}
 onChange={(e)=>this.setState({mcp2_s1:e.target.value})}
      maxlength="50" size="4" /></td>
      <td>MCP 2</td>
      <td><input type="text"
      
      
      

      name="mcp2_t2" 
placeholder="mcp2_t2"
 value={this.state.mcp2_t2}
 onChange={(e)=>this.setState({mcp2_t2:e.target.value})}
      maxlength="50" size="4" /></td>
            <td><input type="text"
            
            
            

            name="mcp2_s2" 
placeholder="mcp2_s2"
 value={this.state.mcp2_s2}
 onChange={(e)=>this.setState({mcp2_s2:e.target.value})}
   maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            








            name="s1_t1" 
placeholder="s1_t1"
 value={this.state.s1_t1}
 onChange={(e)=>this.setState({s1_t1:e.target.value})}
        maxlength="50" size="4" /></td>

           <td><input type="text"
           
           
           

           name="s1_s1" 
placeholder="s1_s1"
 value={this.state.s1_s1}
 onChange={(e)=>this.setState({s1_s1:e.target.value})}
       maxlength="50" size="4" /></td>

      <td>S1</td>
            <td><input type="text"
            
            
            

            name="s1_t2" 
placeholder="s1_t2"
 value={this.state.s1_t2}
 onChange={(e)=>this.setState({s1_t2:e.target.value})}
       maxlength="50" size="4" /></td>

            <td><input type="text"
            
            
            

            name="s1_s2" 
placeholder="s1_s2"
 value={this.state.s1_s2}
 onChange={(e)=>this.setState({s1_s2:e.target.value})}
        maxlength="50" size="4" /></td>

    </tr>
   
  </tbody>
</Table>
</Row>



<Container fluid>
      <h3>Patient MUSCULOSKELETAL_EXAMINATIONS</h3>
         <h5>Rom: Information</h5> 

        <Row>

        <Table striped bordered hover>
  <thead>
    <tr>
      <th></th>
      <th>ANKY</th>
      <th>FLEX</th>
      <th>EXT</th>
      <th>RT-ROT</th>
      <th>LT-ROT</th>
      <th>RT-FL</th>
      <th>LT-FL</th>
      <th>P/T</th>
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td>CERVICAL</td>
      <td><input 
      type="text"
      


            name="cervical_anky" 
placeholder="cervical_anky"
 value={this.state.cervical_anky}
 onChange={(e)=>this.setState({cervical_anky:e.target.value})}
     maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     


            name="cervical_flex" 
placeholder="cervical_flex"
 value={this.state.cervical_flex}
 onChange={(e)=>this.setState({cervical_flex:e.target.value})}
 maxlength="50" size="4" /></td>

      <td><input 
      type="text"
      


            name="cervical_ext" 
placeholder="cervical_ext"
 value={this.state.cervical_ext}
 onChange={(e)=>this.setState({cervical_ext:e.target.value})}
 maxlength="50" size="4" /></td>
            <td><input 
            type="text"
            


            name="cervical_rtrot" 
placeholder="cervical_rtrot"
 value={this.state.cervical_rtrot}
 onChange={(e)=>this.setState({cervical_rtrot:e.target.value})}
            maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            


            name="cervical_ltrot" 
placeholder="cervical_ltrot"
 value={this.state.cervical_ltrot}
 onChange={(e)=>this.setState({cervical_ltrot:e.target.value})}
             maxlength="50" size="4" /></td>

           <td><input 
           type="text"
           


            name="cervical_rtfl" 
placeholder="cervical_rtfl"
 value={this.state.cervical_rtfl}
 onChange={(e)=>this.setState({cervical_rtfl:e.target.value})}
  maxlength="50" size="4" /></td>

      
<td><input 
type="text"



            name="cervical_ltfl" 
placeholder="cervical_ltfl"
 value={this.state.cervical_ltfl}
 onChange={(e)=>this.setState({cervical_ltfl:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            


            name="cervical_pt" 
placeholder="cervical_pt"
 value={this.state.cervical_pt}
 onChange={(e)=>this.setState({cervical_pt:e.target.value})}
        maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td>THORASIC</td>
      <td><input 
      type="text"
      


            name="thorasic_anky" 
placeholder="thorasic_anky"
 value={this.state.thorasic_anky}
 onChange={(e)=>this.setState({thorasic_anky:e.target.value})}
    maxlength="50" size="4" /></td>
   
   <td><input 
   type="text"
   


            name="thorasic_flex" 
placeholder="thorasic_flex"
 value={this.state.thorasic_flex}
 onChange={(e)=>this.setState({thorasic_flex:e.target.value})}
    maxlength="50" size="4" /></td>

      <td><input 
      type="text"
      


            name="thorasic_ext" 
placeholder="thorasic_ext"
 value={this.state.thorasic_ext}
 onChange={(e)=>this.setState({thorasic_ext:e.target.value})}
   maxlength="50" size="4" /></td>
            <td><input 
            type="text"
            


            name="thorasic_rtrot" 
placeholder="thorasic_rtrot"
 value={this.state.thorasic_rtrot}
 onChange={(e)=>this.setState({thorasic_rtrot:e.target.value})}
           maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            


            name="thorasic_ltrot" 
placeholder="thorasic_ltrot"
 value={this.state.thorasic_ltrot}
 onChange={(e)=>this.setState({thorasic_ltrot:e.target.value})}
           maxlength="50" size="4" /></td>

           <td><input 
           type="text"
           


            name="thorasic_rtfl" 
placeholder="thorasic_rtfl"
 value={this.state.thorasic_rtfl}
 onChange={(e)=>this.setState({thorasic_rtfl:e.target.value})}
       maxlength="50" size="4" /></td>

      
<td><input 
type="text"



            name="thorasic_ltfl" 
placeholder="thorasic_ltfl"
 value={this.state.thorasic_ltfl}
 onChange={(e)=>this.setState({thorasic_ltfl:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            


            name="thorasic_pt" 
placeholder="thorasic_pt"
 value={this.state.thorasic_pt}
 onChange={(e)=>this.setState({thorasic_pt:e.target.value})}
      maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td>LUMBAR</td>
      <td><input 
      type="text"
      


            name="lumbar_anky" 
placeholder="lumbar_anky"
 value={this.state.lumbar_anky}
 onChange={(e)=>this.setState({lumbar_anky:e.target.value})}
   maxlength="50" size="4" /></td>

<td><input 
type="text"



            name="lumbar_flex" 
placeholder="lumbar_flex"
 value={this.state.lumbar_flex}
 onChange={(e)=>this.setState({lumbar_flex:e.target.value})}
 maxlength="50" size="4" /></td>
      <td><input 
      type="text"
      


            name="lumbar_ext" 
placeholder="lumbar_ext"
 value={this.state.lumbar_ext}
 onChange={(e)=>this.setState({lumbar_ext:e.target.value})}
       maxlength="50" size="4" /></td>
            <td><input 
            type="text"
            


            name="lumbar_rtrot" 
placeholder="lumbar_rtrot"
 value={this.state.lumbar_rtrot}
 onChange={(e)=>this.setState({lumbar_rtrot:e.target.value})}
             maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            


            name="lumbar_ltrot" 
placeholder="lumbar_ltrot"
 value={this.state.lumbar_ltrot}
 onChange={(e)=>this.setState({lumbar_ltrot:e.target.value})}
         maxlength="50" size="4" /></td>

           <td><input 
           type="text"
           


            name="lumbar_rtfl" 
placeholder="lumbar_rtfl"
 value={this.state.lumbar_rtfl}
 onChange={(e)=>this.setState({lumbar_rtfl:e.target.value})}
      maxlength="50" size="4" /></td>

      
<td><input 
type="text"



            name="lumbar_ltfl" 
placeholder="lumbar_ltfl"
 value={this.state.lumbar_ltfl}
 onChange={(e)=>this.setState({lumbar_ltfl:e.target.value})}
 maxlength="50" size="4" /></td>

      <td><input 
      type="text"
      


            name="lumbar_pt" 
placeholder="lumbar_pt"
 value={this.state.lumbar_pt}
 onChange={(e)=>this.setState({lumbar_pt:e.target.value})}
     maxlength="50" size="4" /></td>

 
    </tr>

   
  </tbody>
</Table>



   
      </Row>

      
      </Container>



      <Container fluid>
      <h3>Patient MUSCULOSKELETAL_EXAMINATIONS</h3>
         <h5>Soft Tissue RHEUMATISM: Information</h5> 

        <Row>

  
  
        <Table striped bordered hover>
  <thead>
    <tr>
      <th></th>
      <th>RT</th>
      <th>LT</th>
      <th></th>
      <th>RT</th>
      <th>LT</th>
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td>OPT</td>
      <td><input 
      type="text"
      

                  name="opt_rt" 
placeholder="opt_rt"
 value={this.state.opt_rt}
 onChange={(e)=>this.setState({opt_rt:e.target.value})}
  maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     

                 name="opt_lt" 
placeholder="opt_lt"
 value={this.state.opt_lt}
 onChange={(e)=>this.setState({opt_lt:e.target.value})}
   maxlength="50" size="4" /></td>

      <td>GLUT</td>
            <td><input 
            type="text"
            

                        name="glut_rt" 
placeholder="glut_rt"
 value={this.state.glut_rt}
 onChange={(e)=>this.setState({glut_rt:e.target.value})}
          maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            

                        name="glut_lt" 
placeholder="glut_lt"
 value={this.state.glut_lt}
 onChange={(e)=>this.setState({glut_lt:e.target.value})}
          maxlength="50" size="4" /></td>

    </tr>
    <tr>
      <td>LCER</td>
      <td><input 
      type="text"
      

                  name="lcer_rt" 
placeholder="lcer_rt"
 value={this.state.lcer_rt}
 onChange={(e)=>this.setState({lcer_rt:e.target.value})}
   maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     

                 name="lcer_lt" 
placeholder="lcer_lt"
 value={this.state.lcer_lt}
 onChange={(e)=>this.setState({lcer_lt:e.target.value})}
maxlength="50" size="4" /></td>

      <td>TRCR</td>
            <td><input 
            type="text"
            

                        name="trcr_rt" 
placeholder="trcr_rt"
 value={this.state.trcr_rt}
 onChange={(e)=>this.setState({trcr_rt:e.target.value})}
            maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            

                        name="trcr_lt" 
placeholder="trcr_lt"
 value={this.state.trcr_lt}
 onChange={(e)=>this.setState({trcr_lt:e.target.value})}
        maxlength="50" size="4" /></td>

    </tr>


    <tr>
      <td>TRPZ</td>
      <td><input 
      type="text"
      

                  name="trpz_rt" 
placeholder="trpz_rt"
 value={this.state.trpz_rt}
 onChange={(e)=>this.setState({trpz_rt:e.target.value})}
  maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     

                 name="trpz_lt" 
placeholder="trpz_lt"
 value={this.state.trpz_lt}
 onChange={(e)=>this.setState({trpz_lt:e.target.value})}
   maxlength="50" size="4" /></td>

      <td>KNEE</td>
            <td><input 
            type="text"
            

                        name="knee_rt" 
placeholder="knee_rt"
 value={this.state.knee_rt}
 onChange={(e)=>this.setState({knee_rt:e.target.value})}
           maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            

                        name="knee_lt" 
placeholder="knee_lt"
 value={this.state.knee_lt}
 onChange={(e)=>this.setState({knee_lt:e.target.value})}
        maxlength="50" size="4" /></td>

    </tr>

    <tr>
      <td>SCAP</td>
      <td><input 
      type="text"
      

                  name="scap_rt" 
placeholder="scap_rt"
 value={this.state.scap_rt}
 onChange={(e)=>this.setState({scap_rt:e.target.value})}
     maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     

                 name="scap_lt" 
placeholder="scap_lt"
 value={this.state.scap_lt}
 onChange={(e)=>this.setState({scap_lt:e.target.value})}
 maxlength="50" size="4" /></td>

      <td>TA</td>
            <td><input 
            type="text"
            

                        name="ta_rt" 
placeholder="ta_rt"
 value={this.state.ta_rt}
 onChange={(e)=>this.setState({ta_rt:e.target.value})}
       maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            

                        name="ta_lt" 
placeholder="ta_lt"
 value={this.state.ta_lt}
 onChange={(e)=>this.setState({ta_lt:e.target.value})}
   maxlength="50" size="4" /></td>

    </tr>
    <tr>
      <td>Z CST</td>
      <td><input 
      type="text"
      

                  name="zcst_rt" 
placeholder="zcst_rt"
 value={this.state.zcst_rt}
 onChange={(e)=>this.setState({zcst_rt:e.target.value})}
    maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     

                 name="zcst_lt" 
placeholder="zcst_lt"
 value={this.state.zcst_lt}
 onChange={(e)=>this.setState({zcst_lt:e.target.value})}
   maxlength="50" size="4" /></td>

      <td>CALF</td>
            <td><input 
            type="text"
            

                        name="calf_rt" 
placeholder="calf_rt"
 value={this.state.calf_rt}
 onChange={(e)=>this.setState({calf_rt:e.target.value})}
      maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            

                        name="calf_lt" 
placeholder="calf_lt"
 value={this.state.calf_lt}
 onChange={(e)=>this.setState({calf_lt:e.target.value})}
           maxlength="50" size="4" /></td>

    </tr>
    <tr>
      <td>EPDL</td>
      <td><input 
      type="text"
      

                  name="epdl_rt" 
placeholder="epdl_rt"
 value={this.state.epdl_rt}
 onChange={(e)=>this.setState({epdl_rt:e.target.value})}
 maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     

                 name="epdl_lt" 
placeholder="epdl_lt"
 value={this.state.epdl_lt}
 onChange={(e)=>this.setState({epdl_lt:e.target.value})}
     maxlength="50" size="4" /></td>

      <td>SOLE</td>
            <td><input 
            type="text"
            

                        name="sole_rt" 
placeholder="sole_rt"
 value={this.state.sole_rt}
 onChange={(e)=>this.setState({sole_rt:e.target.value})}
           maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            

                        name="sole_lt" 
placeholder="sole_lt"
 value={this.state.sole_lt}
 onChange={(e)=>this.setState({sole_lt:e.target.value})}
          maxlength="50" size="4" /></td>

    </tr>



   
  </tbody>
</Table>

     
   
      </Row>

      
      </Container>


      <Container fluid>
      <h3>Patient MUSCULOSKELETAL_EXAMINATIONS</h3>
         <h5>DEFORMITIES: Information</h5> 

        <Row>

  
        <Table striped bordered hover>
  <thead>
    <tr>
      <th>JOINT</th>
      <th>FL</th>
      <th>EX</th>
      <th>AB</th>
      <th>ADD</th>
      <th>SLX</th>
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td>HAND(R)</td>
      <td><input 
      type="text"
      
                       name="rhand_fl" 
placeholder="rhand_fl"
 value={this.state.rhand_fl}
 onChange={(e)=>this.setState({rhand_fl:e.target.value})}

     maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     
                      name="rhand_ex" 
placeholder="rhand_ex"
 value={this.state.rhand_ex}
 onChange={(e)=>this.setState({rhand_ex:e.target.value})}
 maxlength="50" size="4" /></td>

      
            <td><input 
            type="text"
            
                             name="rhand_ab" 
placeholder="rhand_ab"
 value={this.state.rhand_ab}
 onChange={(e)=>this.setState({rhand_ab:e.target.value})}

            maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            
                             name="rhand_add" 
placeholder="rhand_add"
 value={this.state.rhand_add}
 onChange={(e)=>this.setState({rhand_add:e.target.value})}

   maxlength="50" size="4" /></td>

<td><input 
type="text"

                 name="rhand_slx" 
placeholder="rhand_slx"
 value={this.state.rhand_slx}
 onChange={(e)=>this.setState({rhand_slx:e.target.value})}

 maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td>HAND(L)</td>
      <td><input 
      type="text"
      
                       name="lhand_fl" 
placeholder="lhand_fl"
 value={this.state.lhand_fl}
 onChange={(e)=>this.setState({lhand_fl:e.target.value})}
 maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     
                      name="lhand_ex" 
placeholder="lhand_ex"
 value={this.state.lhand_ex}
 onChange={(e)=>this.setState({lhand_ex:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            
                             name="lhand_ab" 
placeholder="lhand_ab"
 value={this.state.lhand_ab}
 onChange={(e)=>this.setState({lhand_ab:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            
                             name="lhand_add" 
placeholder="lhand_add"
 value={this.state.lhand_add}
 onChange={(e)=>this.setState({lhand_add:e.target.value})}
 maxlength="50" size="4" /></td>

<td><input 
type="text"

                 name="lhand_slx" 
placeholder="lhand_slx"
 value={this.state.lhand_slx}
 onChange={(e)=>this.setState({lhand_slx:e.target.value})}
maxlength="50" size="4" /></td>

    </tr>


    <tr>
    <td>WRIST(R)</td>
      <td><input 
      type="text"
      
                       name="rwrist_fl" 
placeholder="rwrist_fl"
 value={this.state.rwrist_fl}
 onChange={(e)=>this.setState({rwrist_fl:e.target.value})}

    maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     
                      name="rwrist_ex" 
placeholder="rwrist_ex"
 value={this.state.rwrist_ex}
 onChange={(e)=>this.setState({rwrist_ex:e.target.value})}
 maxlength="50" size="4" /></td>

     
            <td><input 
            type="text"
            
                             name="rwrist_ab" 
placeholder="rwrist_ab"
 value={this.state.rwrist_ab}
 onChange={(e)=>this.setState({rwrist_ab:e.target.value})}

      maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            
                             name="rwrist_add" 
placeholder="rwrist_add"
 value={this.state.rwrist_add}
 onChange={(e)=>this.setState({rwrist_add:e.target.value})}

  maxlength="50" size="4" /></td>


<td><input 
type="text"

                 name="rwrist_slx" 
placeholder="rwrist_slx"
 value={this.state.rwrist_slx}
 onChange={(e)=>this.setState({rwrist_slx:e.target.value})}

 maxlength="50" size="4" /></td>

    </tr>

    <tr>
    <td>WRIST(L)</td>
      <td><input 
      type="text"
      
                       name="lwrist_fl" 
placeholder="lwrist_fl"
 value={this.state.lwrist_fl}
 onChange={(e)=>this.setState({lwrist_fl:e.target.value})}

     maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     
                      name="lwrist_ex" 
placeholder="lwrist_ex"
 value={this.state.lwrist_ex}
 onChange={(e)=>this.setState({lwrist_ex:e.target.value})}

    maxlength="50" size="4" /></td>

      
            <td><input 
            type="text"
            
                             name="lwrist_ab" 
placeholder="lwrist_ab"
 value={this.state.lwrist_ab}
 onChange={(e)=>this.setState({lwrist_ab:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            
                             name="lwrist_add" 
placeholder="lwrist_add"
 value={this.state.lwrist_add}
 onChange={(e)=>this.setState({lwrist_add:e.target.value})}
 maxlength="50" size="4" /></td>

<td><input 
type="text"

                 name="lwrist_slx" 
placeholder="lwrist_slx"
 value={this.state.lwrist_slx}
 onChange={(e)=>this.setState({lwrist_slx:e.target.value})}

 maxlength="50" size="4" /></td>

    </tr>


    <tr>
    <td>ELB(R)</td>
      <td><input 
      type="text"
      
                       name="relb_fl" 
placeholder="relb_fl"
 value={this.state.relb_fl}
 onChange={(e)=>this.setState({relb_fl:e.target.value})}

   maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     
                      name="relb_ex" 
placeholder="relb_ex"
 value={this.state.relb_ex}
 onChange={(e)=>this.setState({relb_ex:e.target.value})}

   maxlength="50" size="4" /></td>

      
            <td><input 
            type="text"
            
                             name="relb_ab" 
placeholder="relb_ab"
 value={this.state.relb_ab}
 onChange={(e)=>this.setState({relb_ab:e.target.value})}

      maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            
                             name="relb_add" 
placeholder="relb_add"
 value={this.state.relb_add}
 onChange={(e)=>this.setState({relb_add:e.target.value})}
 maxlength="50" size="4" /></td>

<td><input 
type="text"

                 name="relb_slx" 
placeholder="relb_slx"
 value={this.state.relb_slx}
 onChange={(e)=>this.setState({relb_slx:e.target.value})}

 maxlength="50" size="4" /></td>

    </tr>


    <tr>
    <td>ELB(L)</td>
      <td><input 
      type="text"
      
                       name="lelb_fl" 
placeholder="lelb_fl"
 value={this.state.lelb_fl}
 onChange={(e)=>this.setState({lelb_fl:e.target.value})}

    maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     
                      name="lelb_ex" 
placeholder="lelb_ex"
 value={this.state.lelb_ex}
 onChange={(e)=>this.setState({lelb_ex:e.target.value})}

  maxlength="50" size="4" /></td>

     
            <td><input 
            type="text"
            
                             name="lelb_ab" 
placeholder="lelb_ab"
 value={this.state.lelb_ab}
 onChange={(e)=>this.setState({lelb_ab:e.target.value})}

   maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            
                             name="lelb_add" 
placeholder="lelb_add"
 value={this.state.lelb_add}
 onChange={(e)=>this.setState({lelb_add:e.target.value})}

   maxlength="50" size="4" /></td>

<td><input 
type="text"

                 name="lelb_slx" 
placeholder="lelb_slx"
 value={this.state.lelb_slx}
 onChange={(e)=>this.setState({lelb_slx:e.target.value})}

 maxlength="50" size="4" /></td>

    </tr>


    <tr>
    <td>SH (RT)</td>
      <td><input 
      type="text"
      
                       name="shrt_fl" 
placeholder="shrt_fl"
 value={this.state.shrt_fl}
 onChange={(e)=>this.setState({shrt_fl:e.target.value})}

    maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     
                      name="shrt_ex" 
placeholder="shrt_ex"
 value={this.state.shrt_ex}
 onChange={(e)=>this.setState({shrt_ex:e.target.value})}

     maxlength="50" size="4" /></td>

     
            <td><input 
            type="text"
            
                             name="shrt_ab" 
placeholder="shrt_ab"
 value={this.state.shrt_ab}
 onChange={(e)=>this.setState({shrt_ab:e.target.value})}

  maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            
                             name="shrt_add" 
placeholder="shrt_add"
 value={this.state.shrt_add}
 onChange={(e)=>this.setState({shrt_add:e.target.value})}
 maxlength="50" size="4" /></td>

<td><input 
type="text"

                 name="shrt_slx" 
placeholder="shrt_slx"
 value={this.state.shrt_slx}
 onChange={(e)=>this.setState({shrt_slx:e.target.value})}

maxlength="50" size="4" /></td>

    </tr>


<tr>
    <td>SH (LT)</td>
      <td><input 
      type="text"
      
                       name="shlt_fl" 
placeholder="shlt_fl"
 value={this.state.shlt_fl}
 onChange={(e)=>this.setState({shlt_fl:e.target.value})}
 maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     
                      name="shlt_ex" 
placeholder="shlt_ex"
 value={this.state.shlt_ex}
 onChange={(e)=>this.setState({shlt_ex:e.target.value})}
maxlength="50" size="4" /></td>

     
            <td><input 
            type="text"
            
                             name="shlt_ab" 
placeholder="shlt_ab"
 value={this.state.shlt_ab}
 onChange={(e)=>this.setState({shlt_ab:e.target.value})}

  maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            
                             name="shlt_add" 
placeholder="shlt_add"
 value={this.state.shlt_add}
 onChange={(e)=>this.setState({shlt_add:e.target.value})}

      maxlength="50" size="4" /></td>
            <td><input 
            type="text"
            
                             name="shlt_slx" 
placeholder="shlt_slx"
 value={this.state.shlt_slx}
 onChange={(e)=>this.setState({shlt_slx:e.target.value})}

 maxlength="50" size="4" /></td>

    </tr>

    <tr>
    <td>KNEE (RL)</td>
      <td><input 
      type="text"
      
                       name="kneert_fl" 
placeholder="kneert_fl"
 value={this.state.kneert_fl}
 onChange={(e)=>this.setState({kneert_fl:e.target.value})}

  maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     
                      name="kneert_ex" 
placeholder="kneert_ex"
 value={this.state.kneert_ex}
 onChange={(e)=>this.setState({kneert_ex:e.target.value})}
 maxlength="50" size="4" /></td>

     
            <td><input 
            type="text"
            
                             name="kneert_ab" 
placeholder="kneert_ab"
 value={this.state.kneert_ab}
 onChange={(e)=>this.setState({kneert_ab:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            
                             name="kneert_add" 
placeholder="kneert_add"
 value={this.state.kneert_add}
 onChange={(e)=>this.setState({kneert_add:e.target.value})}

       maxlength="50" size="4" /></td>
            <td><input 
            type="text"
            
                             name="kneert_slx" 
placeholder="kneert_slx"
 value={this.state.kneert_slx}
 onChange={(e)=>this.setState({kneert_slx:e.target.value})}
 maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td>KNEE (LT)</td>
      <td><input 
      type="text"
      
                       name="kneelt_fl" 
placeholder="kneelt_fl"
 value={this.state.kneelt_fl}
 onChange={(e)=>this.setState({kneelt_fl:e.target.value})}
 maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     
                      name="kneelt_ex" 
placeholder="kneelt_ex"
 value={this.state.kneelt_ex}
 onChange={(e)=>this.setState({kneelt_ex:e.target.value})}

   maxlength="50" size="4" /></td>

     
            <td><input 
            type="text"
            
                             name="kneelt_ab" 
placeholder="kneelt_ab"
 value={this.state.kneelt_ab}
 onChange={(e)=>this.setState({kneelt_ab:e.target.value})}

  maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            
                             name="kneelt_add" 
placeholder="kneelt_add"
 value={this.state.kneelt_add}
 onChange={(e)=>this.setState({kneelt_add:e.target.value})}

       maxlength="50" size="4" /></td>


<td><input 
type="text"

                 name="kneelt_slx" 
placeholder="kneelt_slx"
 value={this.state.kneelt_slx}
 onChange={(e)=>this.setState({kneelt_slx:e.target.value})}
 maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td>FOOT (RL)</td>
      <td><input 
      type="text"
      
                       name="footrt_fl" 
placeholder="footrt_fl"
 value={this.state.footrt_fl}
 onChange={(e)=>this.setState({footrt_fl:e.target.value})}

 maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     
                      name="footrt_ex" 
placeholder="footrt_ex"
 value={this.state.footrt_ex}
 onChange={(e)=>this.setState({footrt_ex:e.target.value})}

 maxlength="50" size="4" /></td>

     
            <td><input 
            type="text"
            
                             name="footrt_ab" 
placeholder="footrt_ab"
 value={this.state.footrt_ab}
 onChange={(e)=>this.setState({footrt_ab:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            
                             name="footrt_add" 
placeholder="footrt_add"
 value={this.state.footrt_add}
 onChange={(e)=>this.setState({footrt_add:e.target.value})}
 maxlength="50" size="4" /></td>


<td><input 
type="text"

                 name="footrt_slx" 
placeholder="footrt_slx"
 value={this.state.footrt_slx}
 onChange={(e)=>this.setState({footrt_slx:e.target.value})}

 maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td>FOOT (LT)</td>
      <td><input 
      type="text"
      
                       name="footlt_fl" 
placeholder="footlt_fl"
 value={this.state.footlt_fl}
 onChange={(e)=>this.setState({footlt_fl:e.target.value})}
 maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     
                      name="footlt_ex" 
placeholder="footlt_ex"
 value={this.state.footlt_ex}
 onChange={(e)=>this.setState({footlt_ex:e.target.value})}
maxlength="50" size="4" /></td>

     
            <td><input 
            type="text"
            
                             name="footlt_ab" 
placeholder="footlt_ab"
 value={this.state.footlt_ab}
 onChange={(e)=>this.setState({footlt_ab:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            
                             name="footlt_add" 
placeholder="footlt_add"
 value={this.state.footlt_add}
 onChange={(e)=>this.setState({footlt_add:e.target.value})}

   maxlength="50" size="4" /></td>

<td><input 
type="text"

                 name="footlt_slx" 
placeholder="footlt_slx"
 value={this.state.footlt_slx}
 onChange={(e)=>this.setState({footlt_slx:e.target.value})}
 maxlength="50" size="4" /></td>

    </tr>

 

    
   
   
  </tbody>
</Table>


   
      </Row>

      
      </Container>



      <Container fluid>
      <h3>Patient MUSCULOSKELETAL_EXAMINATIONS</h3>
         <h5>Hypermobility: Information</h5> 

        <Row>

        <Table striped bordered hover>
  <thead>
    <tr>
      <th></th>
      <th>RT</th>
      <th>LT</th>
      <th></th>
      <th>RT</th>
      <th>LT</th>
     
     

    </tr>
  </thead>
  <tbody>
  <tr>
      <td>THUMB</td>
      <td><input 
      type="text"
      

                 name="thumb_rt" 
placeholder="thumb_rt"
 value={this.state.thumb_rt}
 onChange={(e)=>this.setState({thumb_rt:e.target.value})}
 maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     

                 name="thumb_lt" 
placeholder="thumb_lt"
 value={this.state.thumb_lt}
 onChange={(e)=>this.setState({thumb_lt:e.target.value})}

      maxlength="50" size="4" /></td>

      <td>KNEE</td>
            <td><input 
            type="text"
            

                 name="hy_knee_rt" 
placeholder="hy_knee_rt"
 value={this.state.hy_knee_rt}
 onChange={(e)=>this.setState({hy_knee_rt:e.target.value})}

       maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            

                 name="hy_knee_lt" 
placeholder="hy_knee_lt"
 value={this.state.hy_knee_lt}
 onChange={(e)=>this.setState({hy_knee_lt:e.target.value})}

        maxlength="50" size="4" /></td>

    </tr>
    <tr>
      <td>FINGER</td>
      <td><input 
      type="text"
      

                 name="finger_rt" 
placeholder="finger_rt"
 value={this.state.finger_rt}
 onChange={(e)=>this.setState({finger_rt:e.target.value})}
 maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     

                 name="finger_lt" 
placeholder="finger_lt"
 value={this.state.finger_lt}
 onChange={(e)=>this.setState({finger_lt:e.target.value})}

 maxlength="50" size="4" /></td>

      <td>ANKLE</td>
            <td><input 
            type="text"
            

                 name="ankle_rt" 
placeholder="ankle_rt"
 value={this.state.ankle_rt}
 onChange={(e)=>this.setState({ankle_rt:e.target.value})}
 maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            

                 name="ankle_lt" 
placeholder="ankle_lt"
 value={this.state.ankle_lt}
 onChange={(e)=>this.setState({ankle_lt:e.target.value})}

      maxlength="50" size="4" /></td>

    </tr>


    <tr>
      <td>PALM</td>
      <td><input 
      type="text"
      

                 name="palm_rt" 
placeholder="palm_rt"
 value={this.state.palm_rt}
 onChange={(e)=>this.setState({palm_rt:e.target.value})}

   maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     

                 name="palm_lt" 
placeholder="palm_lt"
 value={this.state.palm_lt}
 onChange={(e)=>this.setState({palm_lt:e.target.value})}

maxlength="50" size="4" /></td>

      <td>OTHERS</td>
            <td><input 
            type="text"
            

                 name="other_rt" 
placeholder="other_rt"
 value={this.state.other_rt}
 onChange={(e)=>this.setState({other_rt:e.target.value})}

  maxlength="50" size="4" /></td>

            <td><input 
            type="text"
            

                 name="other_lt" 
placeholder="other_lt"
 value={this.state.other_lt}
 onChange={(e)=>this.setState({other_lt:e.target.value})}

 maxlength="50" size="4" /></td>

    </tr>

    <tr>
      <td>ELBOW</td>
      <td><input 
      type="text"
      

                 name="elbow_rt" 
placeholder="elbow_rt"
 value={this.state.elbow_rt}
 onChange={(e)=>this.setState({elbow_rt:e.target.value})}

   maxlength="50" size="4" /></td>
     
     <td><input 
     type="text"
     

                 name="elbow_lt" 
placeholder="elbow_lt"
 value={this.state.elbow_lt}
 onChange={(e)=>this.setState({elbow_lt:e.target.value})}
 maxlength="50" size="4" /></td>

      <td>SPINE</td>
            <td><input 
            type="text"
            

                 name="spine_rt" 
placeholder="spine_rt"
 value={this.state.spine_rt}
 onChange={(e)=>this.setState({spine_rt:e.target.value})}

   maxlength="50" size="4" /></td>


<td><input 
type="text"


                 name="spine_lt" 
placeholder="spine_lt"
 value={this.state.spine_lt}
 onChange={(e)=>this.setState({spine_lt:e.target.value})}

 maxlength="50" size="4" /></td>

    </tr>
   
   
  </tbody>
</Table>

<h3>Patient Investigation</h3>
         <h5>Health_assessment_questionaire:(Use 0,1,2 and 3)</h5> 

        <Row>

  
        <Table striped bordered hover>
  <thead>
    <tr>
      <th>are you able to </th>
      <th></th>
      <th></th>
      <th></th>
      
      <th>Score</th>
     
     

    </tr>
  </thead>
  <tbody>
  <tr>
      <td>Dressing</td>
      <td>
          <label>Dress yourself</label><br></br>
          <input type="text" placeholder="yes/no"	onChange={(e)=>this.setState({can_u_dress_urself:e.target.value})}
   name="can_u_dress_urself"      Value={this.state.can_u_dress_urself} maxlength="50" size="5" /></td>
     
     <td>          <label>Wash Your Hair</label><br></br>

         <input type="text" placeholder="yes/no"						onChange={(e)=>this.setState({can_u_wash_ur_hair:e.target.value})}
   name="can_u_wash_ur_hair"      Value={this.state.can_u_wash_ur_hair} maxlength="50" size="5" /></td>

   
            <td>          <label>Comb your Hair</label><br></br>

                
                <input type="text" 		placeholder="yes/no"				onChange={(e)=>this.setState({can_u_comb_ur_hair:e.target.value})}
   name="can_u_comb_ur_hair"      Value={this.state.can_u_comb_ur_hair} maxlength="50" size="5" /></td>

            <td><input type="number" 						onChange={(e)=>this.setState({dressing_score:e.target.value})}
   name="dressing_score"      Value={this.state.dressing_score} maxlength="50" size="5" /></td>

    </tr>


    <tr>
      <td>Arising</td>
      <td>      <label>Stand From Chair</label><br></br>
          <input type="text" placeholder="yes/no" 	onChange={(e)=>this.setState({can_u_stand_from_chair:e.target.value})}
   name="can_u_stand_from_chair"      Value={this.state.can_u_stand_from_chair} maxlength="50" size="5" /></td>
     
     <td><label>Get in out From Bed</label><br></br>
         <input type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_get_inout_from_bed:e.target.value})}
   name="can_u_get_inout_from_bed"      Value={this.state.can_u_get_inout_from_bed} maxlength="50" size="5" /></td>

     
            <td><label>Sit Gross teg on Floor</label><br></br>
                <input type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_sit_grossteg_onfloor:e.target.value})}
   name="can_u_sit_grossteg_onfloor"      Value={this.state.can_u_sit_grossteg_onfloor} maxlength="50" size="5" /></td>

            <td><input type="number" 						onChange={(e)=>this.setState({arising_score:e.target.value})}
   name="arising_score"      Value={this.state.arising_score} maxlength="50" size="5" /></td>

    </tr>


    <tr>
      <td>Eating</td>
      <td><label>Cut Vegetables</label><br></br>
          <input type="text" placeholder="yes/no"	onChange={(e)=>this.setState({can_u_cut_vegetables:e.target.value})}
   name="can_u_cut_vegetables"      Value={this.state.can_u_cut_vegetables} maxlength="50" size="5" /></td>
     
     <td><label>Lift a glass to your mouth</label><br></br>
         <input type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_lift_glass:e.target.value})}
   name="can_u_lift_glass"      Value={this.state.can_u_lift_glass} maxlength="50" size="5" /></td>

    
            <td><label>Break Roti from 1 hand</label><br></br>
                <input type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_break_roti_from_1hand:e.target.value})}
   name="can_u_break_roti_from_1hand"      Value={this.state.can_u_break_roti_from_1hand} maxlength="50" size="5" /></td>

            <td>
                <input type="number" 						onChange={(e)=>this.setState({eating_score:e.target.value})}
   name="eating_score"      Value={this.state.eating_score} maxlength="50" size="5" /></td>

    </tr>

    <tr>
      <td>Walking</td>
      <td><label>Walking Outdoors</label><br></br>
          <input type="text" placeholder="yes/no"	onChange={(e)=>this.setState({can_u_walk:e.target.value})}
   name="can_u_walk"      Value={this.state.can_u_walk} maxlength="50" size="5" /></td>
     
     <td><label>Climb up 5 Steps</label><br></br>
         <input type="text" placeholder="yes/no"						onChange={(e)=>this.setState({can_u_climb_5steps:e.target.value})}
   name="can_u_climb_5steps"      Value={this.state.can_u_climb_5steps} maxlength="50" size="5" /></td>

   
            <td></td>


<td>
    <input type="number" 				onChange={(e)=>this.setState({walking_score:e.target.value})}
   name="walking_score"      Value={this.state.walking_score} maxlength="50" size="5" /></td>

    </tr>
   


    <tr>
      <td>Hygiene</td>
      <td><label>Take a Bath</label><br></br>
          <input type="text" placeholder="yes/no"	onChange={(e)=>this.setState({can_u_take_bath:e.target.value})}
   name="can_u_take_bath"      Value={this.state.can_u_take_bath} maxlength="50" size="5" /></td>
     
     <td><label>Was and dry your body </label><br></br>
         <input type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_wash_dry_urbody:e.target.value})}
   name="can_u_wash_dry_urbody"      Value={this.state.can_u_wash_dry_urbody} maxlength="50" size="5" /></td>

    
            <td><label>Get on and off the toilet</label><br></br>
                <input type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_get_onoff_toilet:e.target.value})}
   name="can_u_get_onoff_toilet"      Value={this.state.can_u_get_onoff_toilet} maxlength="50" size="5" /></td>

            <td>
                <input type="number" 						onChange={(e)=>this.setState({hygiene_score:e.target.value})}
   name="hygiene_score"      Value={this.state.hygiene_score} maxlength="50" size="5" /></td>

    </tr>
   


    <tr>
      <td>Reaching</td>
      <td><label>Reach and get down a 2 kg objects</label><br></br>
          <input type="text" placeholder="yes/no"	onChange={(e)=>this.setState({can_u_weigh_2kg:e.target.value})}
   name="can_u_weigh_2kg"      Value={this.state.can_u_weigh_2kg} maxlength="50" size="5" /></td>
     
     <td><label>Bend down to pick up clothing from floor </label><br></br>
         <input type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_bend_and_pickcloths:e.target.value})}
   name="can_u_bend_and_pickcloths"      Value={this.state.can_u_bend_and_pickcloths} maxlength="50" size="5" /></td>

    
            <td></td>

            <td>
                <input type="number" 						onChange={(e)=>this.setState({reaching_score:e.target.value})}
   name="reaching_score"      Value={this.state.reaching_score} maxlength="50" size="5" /></td>

    </tr>



    <tr>
      <td>Grip</td>
      <td><label>Open a bottle previously opened</label><br></br>
          <input type="text" placeholder="yes/no"	onChange={(e)=>this.setState({can_u_open_bottle:e.target.value})}
   name="can_u_open_bottle"      Value={this.state.can_u_open_bottle} maxlength="50" size="5" /></td>
     
     <td><label>Turn taps on and off </label><br></br>
         <input type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_turntaps_onoff:e.target.value})}
   name="can_u_turntaps_onoff"      Value={this.state.can_u_turntaps_onoff} maxlength="50" size="5" /></td>

    
            <td><label>Open done Latches</label><br></br>
                <input type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_open_latches:e.target.value})}
   name="can_u_open_latches"      Value={this.state.can_u_open_latches} maxlength="50" size="5" /></td>

            <td>
                <input type="number" 						onChange={(e)=>this.setState({grip_score:e.target.value})}
   name="grip_score"      Value={this.state.grip_score} maxlength="50" size="5" /></td>

    </tr>



    <tr>
      <td>Activities</td>
      <td><label>Work in office/house</label><br></br>
          <input type="text" placeholder="yes/no"	onChange={(e)=>this.setState({can_u_work_office_house:e.target.value})}
   name="can_u_work_office_house"      Value={this.state.can_u_work_office_house} maxlength="50" size="5" /></td>
     
     <td><label>Run errands and shop</label><br></br>
         <input type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_run_errands:e.target.value})}
   name="can_u_run_errands"      Value={this.state.can_u_run_errands} maxlength="50" size="5" /></td>

    
            <td><label>Get in and out of bus</label><br></br>
                <input type="text" 	placeholder="yes/no"					onChange={(e)=>this.setState({can_u_get_inout_of_bus:e.target.value})}
   name="can_u_get_inout_of_bus"      Value={this.state.can_u_get_inout_of_bus} maxlength="50" size="5" /></td>

            <td>
                <input type="number" 						onChange={(e)=>this.setState({activities_score:e.target.value})}
   name="activities_score"      Value={this.state.activities_score} maxlength="50" size="5" /></td>

    </tr>
  </tbody>
</Table><br></br>



<h4>Modified CRD Pune Version HAQ Score</h4>

<Table striped bordered hover>
  <thead>
    <tr>
      <th>Modified CRD Pune Version HAQ Score</th>
      <th>/24</th>
      
     
     

    </tr>
  </thead>
  <tbody>
  <tr>
      <td>Patient assessment pain(VSA)</td>
      <td><input type="number" 	onChange={(e)=>this.setState({patient_assessment_pain:e.target.value})}
   name="patient_assessment_pain"      Value={this.state.patient_assessment_pain} maxlength="50" size="10" />cm(on a scale of 10cm)</td>
     

    </tr>
    <tr>
      <td>Grip strength</td>
      <td><input type="number" 	onChange={(e)=>this.setState({grip_strength_rt:e.target.value})}
   name="grip_strength_rt"      Value={this.state.grip_strength_rt} maxlength="50" size="10" />MM HG of RT  <br></br><br></br>


 <input type="number" 	onChange={(e)=>this.setState({grip_strength_hg:e.target.value})}
   name="grip_strength_hg"      Value={this.state.grip_strength_hg} maxlength="50" size="10" />MM HG of HG <br></br><br></br>


   <input type="number" 	onChange={(e)=>this.setState({grip_strength_lt:e.target.value})}
   name="grip_strength_lt"      Value={this.state.grip_strength_lt} maxlength="50" size="10" />MM HG of LT
</td>


 
    </tr>


    <tr>
      <td>Early morning stiffness</td>
      <td><input type="number" 	onChange={(e)=>this.setState({early_mrng_stiffness:e.target.value})}
   name="early_mrng_stiffness"      Value={this.state.early_mrng_stiffness} maxlength="50" size="10" />Mins</td>

 
    </tr>

    <tr>
      <td>Sleep</td>
      <td><input type="text" 	onChange={(e)=>this.setState({sleep:e.target.value})}
   name="sleep"      Value={this.state.sleep} maxlength="50" size="20"/> (normal/distributed/excess)</td>


    </tr>
   
    <tr>
      <td>General health assessment</td>
      <td><input type="number" 	onChange={(e)=>this.setState({general_health_assessment:e.target.value})}
   name="general_health_assessment"      Value={this.state.general_health_assessment} maxlength="50" size="10" />mm (on a 100 cm scale)</td>


    </tr>
   
  </tbody>
</Table>


</Row>
</Row>
</Container>










<Button
                    className="btn-fill pull-right"
                    type="submit"
                    name=""
                    variant="info"
                  >
                    Add Rheumatology Patient Follow-up
                  </Button>

        



                       </Form>
                       </Card.Body>
                       </Card>
                       </Col>
                       </Row>


                       <div>
                        <h4>Attachments</h4>



        {
          this.state.products.map((i, index) => {
            console.log(i.attachment)

          var image= i.attachment.toString()

            var ext = image.split(".").pop()
            console.log(ext)
            if (ext == "jpeg" || ext == "png" || ext == "jpg"){
              console.log(i.attachment)

              return(
                <div>
                   <Row>

    <Col xs={4} md={4}>
      <Image src={`https://abcapi.vidaria.in/files/${i.attachment}?X-AUTH=abc123`}   fluid /><br></br><br></br>

      </Col>
      </Row>
                  </div>
                  
              )

            }else{
            console.log("im also coming")
              return(
                <video width="750" height="500" controls >
                <source src={`https://abcapi.vidaria.in/files/${i.attachment}?X-AUTH=abc123`} type="video/mp4"/>
          </video>
              )
              
   

            }
  
    })
  }
  </div>



                       
      </Container>
      }
     </>  
    </>
  );
        }
}


export default followup;
