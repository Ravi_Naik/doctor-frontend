
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Table,
  Col,
} from "react-bootstrap";

class Inves_Hematological extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}


    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Investigation</h2>
         <h5>hematological date: Information</h5> 

        <Row>

        <Form>
  
        <Table striped bordered hover>
  <thead>
    <tr>
    <th>DATE</th>
      <th>ESR</th>
      <th>HB</th>
      <th>TLC</th>
      <th>PLEB</th>
      <th>PLAT</th>
      <th>URINE</th>
      <th>OTHERS</th>
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td><input type="date" 	onChange={this.props.handleChange('hematological_date')}
defaultValue={values.hematological_date} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('hem_esr')}
defaultValue={values.hem_esr} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('hem_hb')}
defaultValue={values.hem_hb} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('hem_tlc')}
defaultValue={values.hem_tlc} maxlength="50" size="4" /></td>


            <td><input type="text" 						onChange={this.props.handleChange('hem_pleb')}
defaultValue={values.hem_pleb} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('hem_plat')}
defaultValue={values.hem_plat} maxlength="50" size="4" /></td>


<td><input type="text" 						onChange={this.props.handleChange('hem_urine')}
defaultValue={values.hem_urine} maxlength="50" size="4" /></td>


<td><input type="text" 						onChange={this.props.handleChange('hem_others')}
defaultValue={values.hem_others} maxlength="50" size="4" /></td>

    </tr>
    <tr>
      <td><input type="date" 	onChange={this.props.handleChange('hematological_date2')}
defaultValue={values.hematological_date2} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('hem_esr2')}
defaultValue={values.hem_esr2} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('hem_hb2')}
defaultValue={values.hem_hb2} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('hem_tlc2')}
defaultValue={values.hem_tlc2} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('hem_pleb2')}
defaultValue={values.hem_pleb2} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('hem_plat2')}
defaultValue={values.hem_plat2} maxlength="50" size="4" /></td>

<td><input type="text" 						onChange={this.props.handleChange('hem_urine2')}
defaultValue={values.hem_urine2} maxlength="50" size="4" /></td>

<td><input type="text" 						onChange={this.props.handleChange('hem_others2')}
defaultValue={values.hem_others2} maxlength="50" size="4" /></td>

    </tr>


    <tr>
      <td><input type="date" 	onChange={this.props.handleChange('hematological_date3')}
defaultValue={values.hematological_date3} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('hem_esr3')}
defaultValue={values.hem_esr3} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('hem_hb3')}
defaultValue={values.hem_hb3} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('hem_tlc3')}
defaultValue={values.hem_tlc3} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('hem_pleb3')}
defaultValue={values.hem_pleb3} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('hem_plat3')}
defaultValue={values.hem_plat3} maxlength="50" size="4" /></td>


<td><input type="text" 						onChange={this.props.handleChange('hem_urine3')}
defaultValue={values.hem_urine3} maxlength="50" size="4" /></td>


<td><input type="text" 						onChange={this.props.handleChange('hem_others3')}
defaultValue={values.hem_others3} maxlength="50" size="4" /></td>

    </tr>

    <tr>
      <td><input type="date" 	onChange={this.props.handleChange('hematological_date4')}
defaultValue={values.hematological_date4} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('hem_esr4')}
defaultValue={values.hem_esr4} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('hem_hb4')}
defaultValue={values.hem_hb4} maxlength="50" size="4" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('hem_tlc4')}
defaultValue={values.hem_tlc4} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('hem_pleb4')}
defaultValue={values.hem_pleb4} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('hem_plat4')}
defaultValue={values.hem_plat4} maxlength="50" size="4" /></td>


<td><input type="text" 						onChange={this.props.handleChange('hem_urine4')}
defaultValue={values.hem_urine4} maxlength="50" size="4" /></td>


<td><input type="text" 						onChange={this.props.handleChange('hem_others4')}
defaultValue={values.hem_others4} maxlength="50" size="4" /></td>

    </tr>


    <tr>
      <td><input type="date" 	onChange={this.props.handleChange('hematological_date5')}
defaultValue={values.hematological_date5} maxlength="50" size="5" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('hem_esr5')}
defaultValue={values.hem_esr5} maxlength="50" size="5" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('hem_hb5')}
defaultValue={values.hem_hb5} maxlength="50" size="5" /></td>

      <td><input type="text" 	onChange={this.props.handleChange('hem_tlc5')}
defaultValue={values.hem_tlc5} maxlength="50" size="5" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('hem_pleb5')}
defaultValue={values.hem_pleb5} maxlength="50" size="5" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('hem_plat5')}
defaultValue={values.hem_plat5} maxlength="50" size="5" /></td>


<td><input type="text" 						onChange={this.props.handleChange('hem_urine5')}
defaultValue={values.hem_urine5} maxlength="50" size="5" /></td>


<td><input type="text" 						onChange={this.props.handleChange('hem_others5')}
defaultValue={values.hem_others5} maxlength="50" size="5" /></td>

    </tr>
   
  </tbody>
</Table>

<Row>
<Col md="6">
{/* <Button className="btn-fill"  onClick={this.back}>Back</Button> */}

</Col>
<Col md="6">
<Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>
</Row>
        </Form>
   
      </Row>

      
      </Container>
    )
  }
}

export default Inves_Hematological;







