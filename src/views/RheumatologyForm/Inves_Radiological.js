
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
  Table
} from "react-bootstrap";

class Inves_Radiological extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}

    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Radiological  Information</h2>
<p>1) Radiological</p>
        <Row>

        <Form>
  
        <Row>
          <Col md="6">
          <Form.Group>
        <Form.Label>USG</Form.Label><br></br>
          <Form.Control type="text" onChange={this.props.handleChange('usg')}
						defaultValue={values.usg}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>X Ray Chest</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('xray_chest')} defaultValue={values.xray_chest}  />
          </Form.Group>
        </Col>

        
        <Col md="6">
        <Form.Group>
          <Form.Label>X Ray Joints</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('xray_joints')}  defaultValue={values.xray_joints}/>
          </Form.Group>


          <Form.Group>
          <Form.Label>MRI</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('mri')} defaultValue={values.mri}/>
          </Form.Group>
          </Col>
          </Row>


          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>Radiological Remark</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('radio_remarks')} defaultValue={values.radio_remarks}/>
          </Form.Group>

         
</Col>


</Row>





<h5>2) Disease activity Indices</h5>
<Table striped bordered hover>
  <thead>
    <tr>
      <th>Disease activity Indices</th>
      <th>yes/no</th>
     
     
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td>DAS 28</td>
      <td><input type="text" 	onChange={this.props.handleChange('das28')}
defaultValue={values.das28} maxlength="50" size="8" /></td>
     

    </tr>
    <tr>
      <td>SLEDAI</td>
      <td><input type="text" 	onChange={this.props.handleChange('sledai')}
defaultValue={values.sledai} maxlength="50" size="8" /></td>
     

    </tr>


    <tr>
      <td>BASDAI</td>
      <td><input type="text" 	onChange={this.props.handleChange('basdai')}
defaultValue={values.basdai} maxlength="50" size="8" /></td>
     

    </tr>

    <tr>
      <td>PASI</td>
      <td><input type="text" 	onChange={this.props.handleChange('pasi')}
defaultValue={values.pasi} maxlength="50" size="8" /></td>
     

    </tr>
    <tr>
      <td>BVAS</td>
      <td><input type="text" 	onChange={this.props.handleChange('bvas')}
defaultValue={values.bvas} maxlength="50" size="8" /></td>
     

    </tr>
    <tr>
      <td>MRSS</td>
      <td><input type="text" 	onChange={this.props.handleChange('mrss')}
defaultValue={values.mrss} maxlength="50" size="8" /></td>
     

    </tr>




    <tr>
      <td>SDAI  / CDAI</td>
      <td><input type="text" 	onChange={this.props.handleChange('sdai_cdai')}
defaultValue={values.sdai_cdai} maxlength="50" size="8" /></td>
     

    </tr>
  


    <tr>
      <td>ASDAS</td>
      <td><input type="text" 	onChange={this.props.handleChange('dapsa')}
defaultValue={values.dapsa} maxlength="50" size="8" /></td>
     

    </tr>

    <tr>
      <td>DAPSA</td>
      <td><input type="text" 	onChange={this.props.handleChange('vdi')}
defaultValue={values.vdi} maxlength="50" size="8" /></td>
     

    </tr>
    <tr>
      <td>VDI</td>
      <td><input type="text" 	onChange={this.props.handleChange('essdai')}
defaultValue={values.essdai} maxlength="50" size="8" /></td>
     

    </tr>
    <tr>
      <td>ESSDAI</td>
      <td><input type="text" 	onChange={this.props.handleChange('epdl_rt')}
defaultValue={values.epdl_rt} maxlength="50" size="8" /></td>
     

    </tr>


    <tr>
      <td>User ID</td>
      <td><input type="text" 	onChange={this.props.handleChange('user_id')}
defaultValue={values.user_id} maxlength="50" size="8" /></td>
     

    </tr>
   
  </tbody>
</Table>


<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">   

<Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>

</Row>





        </Form>
   
      </Row>
      </Container>
    )
  }
}

export default Inves_Radiological;







