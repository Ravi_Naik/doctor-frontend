

import React, { Component } from 'react';

// import Examination from './Examination';
// import Mus_examination from './Mus_examination'
// import Rom_examination from './Rom_examination'
// import Soft_tis_examination from './Soft_tis_examination'
// import Deformities_examination from './Deformities_examination';
// import Hypermobility_examination from './Hypermobility_examination';
import Health_ass_ques from './Health_ass_ques';
import Inves_Hematological from './Inves_Hematological';

import Inves_Biochemical from './Inves_Biochemical';
import Inves_Immonological from './Inves_Immonological';
import Inves_Radiological from './Inves_Radiological';
import Summary from './Investigation_Summary';


import axios from 'axios';

class Examination_Main extends Component {
    state = {
        step:1,
        id:'',
       can_u_dress_urself:'',
       can_u_wash_ur_hair:'',
       can_u_comb_ur_hair:'',
       can_u_stand_from_chair:'',
       can_u_get_inout_from_bed:'',
       can_u_sit_grossteg_onfloor:'',
       can_u_cut_vegetables:'',
       can_u_lift_glass:'',
       can_u_break_roti_from_1hand:'',
       can_u_walk:'',
       can_u_climb_5steps:'',
       can_u_take_bath:'',
       can_u_wash_dry_urbody:'',
       can_u_get_onoff_toilet:'',
       can_u_weigh_2kg:'',
       can_u_bend_and_pickcloths:'',
       can_u_open_bottle:'',
       can_u_turntaps_onoff:'',
       can_u_open_latches:'',
       can_u_work_office_house:'',
       can_u_run_errands:'',
       can_u_get_inout_of_bus:'',
       patient_assessment_pain:'',
       grip_strength_rt:'',
       grip_strength_hg:'',
       grip_strength_lt:'',
       early_mrng_stiffness:'',
       assess_sleep:'',
       general_health_assessment:'',
       classification_criteria_followig:'',
       diagnosis:'',
       treatment_plan:'',
       hematological_date:'',
       hem_esr:'',
       hem_hb:'',
       hem_tlc:'',
       hem_pleb:'',
       hem_plat:'',
       hem_urine:'',
       hem_others:'',
       hematological_date2:'',
       hem_esr2:'',
       hem_hb2:'',
       hem_tlc2:'',
       hem_pleb2:'',
       hem_plat2:'',
       hem_urine2:'',
       hem_others2:'',
       hematological_date3:'',
       hem_esr3:'',
       hem_hb3:'',
       hem_tlc3:'',
       hem_pleb3:'',
       hem_plat3:'',
       hem_urine3:'',
       hem_others3:'',
       hematological_date4:'',
       hem_esr4:'',
       hem_hb4:'',
       hem_tlc4:'',
       hem_pleb4:'',
       hem_plat4:'',
       hem_urine4:'',
       hem_others4:'',
       hematological_date5:'',
       hem_esr5:'',
       hem_hb5:'',
       hem_tlc5:'',
       hem_pleb5:'',
       hem_plat5:'',
       hem_urine5:'',
       hem_others5:'',
       biochemical_date:'',
       bio_lft:'',
       bio_rft:'',
       bio_crp:'',
       bio_bsl:'',
       bio_ua:'',
       bio_others:'',
       biochemical_date2:'',
       bio_lft2:'',
       bio_rft2:'',
       bio_crp2:'',
       bio_bsl2:'',
       bio_ua2:'',
       bio_others2:'',
       biochemical_date3:'',
       bio_lft3:'',
       bio_rft3:'',
       bio_crp3:'',
       bio_bsl3:'',
       bio_ua3:'',
       bio_others3:'',
       biochemical_date4:'',
       bio_lft4:'',
       bio_rft4:'',
       bio_crp4:'',
       bio_bsl4:'',
       bio_ua4:'',
       bio_others4:'',
       biochemical_date5:'',
       bio_lft5:'',
       bio_rft5:'',
       bio_crp5:'',
       bio_bsl5:'',
       bio_ua5:'',
       bio_others5:'',
       immunological_date:'',
       imm_rf:'',
       imm_accp:'',
       imm_ana:'',
       imm_anawb:'',
       imm_ace:'',
       imm_dsdna:'',
       imm_hlab27:'',
       imm_c3:'',
       imm_c4:'',
       imm_others:'',
       immunological_date2:'',
       imm_rf2:'',
       imm_accp2:'',
       imm_ana2:'',
       imm_anawb2:'',
       imm_ace2:'',
       imm_dsdna2:'',
       imm_hlab272:'',
       imm_c32:'',
       imm_c42:'',
       imm_others2:'',
       immunological_date3:'',
       imm_rf3:'',
       imm_accp3:'',
       imm_ana3:'',
       imm_anawb3:'',
       imm_ace3:'',
       imm_dsdna3:'',
       imm_hlab273:'',
       imm_c33:'',
       imm_c43:'',
       imm_others3:'',
       usg:'',
       xray_chest:'',
       xray_joints:'',
       mri:'',
       radio_remarks:'',
       das28:'',
       sledai:'',
       basdai:'',
       pasi:'',
       bvas:'',
       mrss:'',
       sdai_cdai:'',
       asdas:'',
       dapsa:'',
       vdi:'',
       essdai:'',
       pid:'',
       user_id:''
    }
      nextStep = () => {
        const { step } = this.state
        this.setState({
          step: step + 1
        })
      }

  
      prevStep = () => {
        const { step } = this.state
        this.setState({
          step: step - 1
        })
      }
    

      handleChange = input => event => {
        this.setState({ [input]: event.target.value })
      }


      saveAll = () => {

        let Obj={

            id:this.state.id,
       
            can_u_dress_urself:this.state.can_u_dress_urself,
            can_u_wash_ur_hair:this.state.can_u_wash_ur_hair,
    
    
    
            can_u_comb_ur_hair:this.state.can_u_comb_ur_hair,
            can_u_stand_from_chair:this.state.can_u_stand_from_chair,
            can_u_get_inout_from_bed:this.state.can_u_get_inout_from_bed,
    
    
            can_u_sit_grossteg_onfloor:this.state.can_u_sit_grossteg_onfloor,
            can_u_cut_vegetables:this.state.can_u_cut_vegetables,
            can_u_lift_glass:this.state.can_u_lift_glass,
    
    
            can_u_break_roti_from_1hand:this.state.can_u_break_roti_from_1hand,
            can_u_walk:this.state.can_u_walk,
            can_u_climb_5steps:this.state.can_u_climb_5steps,
    
    
    
            can_u_take_bath:this.state.can_u_take_bath,
            can_u_wash_dry_urbody:this.state.can_u_wash_dry_urbody,
            can_u_get_onoff_toilet:this.state.can_u_get_onoff_toilet,
    
    
            can_u_weigh_2kg:this.state.can_u_weigh_2kg,
            can_u_bend_and_pickcloths:this.state.can_u_bend_and_pickcloths,
            can_u_open_bottle:this.state.can_u_open_bottle,
    
    
    
    
            can_u_turntaps_onoff:this.state.can_u_turntaps_onoff,  
            can_u_open_latches:this.state.can_u_open_latches,
            can_u_work_office_house:this.state.can_u_work_office_house,
    
    
    
            can_u_run_errands:this.state.can_u_run_errands,
            can_u_get_inout_of_bus:this.state.can_u_get_inout_of_bus,
            patient_assessment_pain:this.state.patient_assessment_pain,
    
    
            grip_strength_rt:this.state.grip_strength_rt,
            grip_strength_hg:this.state.grip_strength_hg,
            grip_strength_lt:this.state.grip_strength_lt,
    
    
            early_mrng_stiffness:this.state.early_mrng_stiffness,
            assess_sleep:this.state.assess_sleep,
            general_health_assessment:this.state.general_health_assessment,
    
    
            classification_criteria_followig:this.state.classification_criteria_followig,
            diagnosis:this.state.diagnosis,
            treatment_plan:this.state.treatment_plan,
    
    
    
            hematological_date:this.state.hematological_date,
            hem_esr:this.state.hem_esr,
            hem_hb:this.state.hem_hb,
    
    
            hem_tlc:this.state.hem_tlc,
            hem_pleb:this.state.hem_pleb,
            hem_plat:this.state.hem_plat,
    
    
            hem_urine:this.state.hem_urine,
            hem_others:this.state.hem_others,
            hematological_date2:this.state.hematological_date2,
    
    
            hem_esr2:this.state.hem_esr2,
            hem_hb2:this.state.hem_hb2,
            hem_tlc2:this.state.hem_tlc2,
    
    
            hem_pleb2:this.state.hem_pleb2,
            hem_plat2:this.state.hem_plat2,
            hem_urine2:this.state.hem_urine2,
    
    
            hem_others2:this.state.hem_others2,
            hematological_date3:this.state.hematological_date3,
            hem_esr3:this.state.hem_esr3,
    
    
            hem_hb3:this.state.hem_hb3,
            hem_tlc3:this.state.hem_tlc3,
            hem_pleb3:this.state.hem_pleb3,
    
    
            hem_plat3:this.state.hem_plat3,
            hem_urine3:this.state.hem_urine3,
            hem_others3:this.state.hem_others3,
    
    
    
    
            hematological_date4:this.state.hematological_date4,
            hem_esr4:this.state.hem_esr4,
            hem_hb4:this.state.hem_hb4,
    
    
            hem_tlc4:this.state.hem_tlc4,
            hem_pleb4:this.state.hem_pleb4,
            hem_plat4:this.state.hem_plat4,
    
    
            hem_urine4:this.state.hem_urine4,
            hem_others4:this.state.hem_others4,
            hematological_date5:this.state.hematological_date5,
    
    
    
    
    
            hem_esr5:this.state.hem_esr5,
            hem_hb5:this.state.hem_hb5,
            hem_tlc5:this.state.hem_tlc5,
    
    
            hem_pleb5:this.state.hem_pleb5,
            hem_plat5:this.state.hem_plat5,
            hem_urine5:this.state.hem_urine5,
    
    
            hem_others5:this.state.hem_others5,
            biochemical_date:this.state.biochemical_date,
            bio_lft:this.state.bio_lft,
    
    
    
            bio_rft:this.state.bio_rft,
            bio_crp:this.state.bio_crp,
            bio_bsl:this.state.bio_bsl,
    
    
            bio_ua:this.state.bio_ua,
            bio_others:this.state.bio_others,
            biochemical_date2:this.state.biochemical_date2,
    
    
            bio_lft2:this.state.bio_lft2,
            bio_rft2:this.state.bio_rft2,
            bio_crp2:this.state.bio_crp2,
    
    
    
            bio_bsl2:this.state.bio_bsl2,
            bio_ua2:this.state.bio_ua2,
            bio_others2:this.state.bio_others2,
    
    
            biochemical_date3:this.state.biochemical_date3,
            bio_lft3:this.state.bio_lft3,
            bio_rft3:this.state.bio_rft3,
    
    
            bio_bsl3:this.state.bio_bsl3,
            bio_ua3:this.state.bio_ua3,
            bio_others3:this.state.bio_others3,
    
    
    
    
            biochemical_date4:this.state.biochemical_date4,
            bio_lft4:this.state.bio_lft4,
            bio_rft4:this.state.bio_rft4,
    
    
            bio_crp4:this.state.bio_crp4,
            bio_bsl4:this.state.bio_bsl4,
            bio_ua4:this.state.bio_ua4,
    
    
            bio_others4:this.state.bio_others4,
            biochemical_date5:this.state.biochemical_date5,
            bio_lft5:this.state.bio_lft5,
    
    
    
            bio_rft5:this.state.bio_rft5,
            bio_crp5:this.state.bio_crp5,
            bio_bsl5:this.state.bio_bsl5,
    
    
            bio_ua5:this.state.bio_ua5,
            bio_others5:this.state.bio_others5,
            immunological_date:this.state.immunological_date,
    
    
            imm_rf:this.state.imm_rf,
            imm_accp:this.state.imm_accp,
            imm_ana:this.state.imm_ana,
    
    
    
    
            imm_anawb:this.state.imm_anawb,
            imm_ace:this.state.imm_ace,
            imm_dsdna:this.state.imm_dsdna,
    
    
            imm_hlab27:this.state.imm_hlab27,
            imm_c3:this.state.imm_c3,
            imm_c4:this.state.imm_c4,
    
    
            imm_others:this.state.imm_others,
            immunological_date2:this.state.immunological_date2,
            imm_rf2:this.state.imm_rf2,
    
    
    
            imm_accp2:this.state.imm_accp2,
            imm_ana2:this.state.imm_ana2,
            imm_anawb2:this.state.imm_anawb2,
    
    
            imm_ace2:this.state.imm_ace2,
            imm_dsdna2:this.state.imm_dsdna2,
            imm_hlab272:this.state.imm_hlab272,
    
    
            imm_c32:this.state.imm_c32,
            imm_c42:this.state.imm_c42,
            imm_others2:this.state.imm_others2,
    
    
            immunological_date3:this.state.immunological_date3,
            imm_rf3:this.state.imm_rf3,
            imm_accp3:this.state.imm_accp3,
    
    
            imm_ana3:this.state.imm_ana3,
            imm_anawb3:this.state.imm_anawb3,
            imm_ace3:this.state.imm_ace3,
    
    
            imm_dsdna3:this.state.imm_dsdna3,
            imm_hlab273:this.state.imm_hlab273,
            imm_c33:this.state.imm_c33,
    
    
            imm_c43:this.state.imm_c43,
            imm_others3:this.state.imm_others3,
            usg:this.state.usg,
    
    
            xray_chest:this.state.xray_chest,
            xray_joints:this.state.xray_joints,
            mri:this.state.mri,
    
    
            radio_remarks:this.state.radio_remarks,
            das28:this.state.das28,
            sledai:this.state.sledai,
    
    
    
            basdai:this.state.basdai,
            pasi:this.state.pasi,
            bvas:this.state.bvas,
    
    
            mrss:this.state.mrss,
            sdai_cdai:this.state.sdai_cdai,
            asdas:this.state.asdas,
    
    
            dapsa:this.state.dapsa,
            vdi:this.state.vdi,
            essdai:this.state.essdai,
    
    
            pid:this.state.pid,
      
       

            user_id:this.state.user_id,



          
        }
            axios.post(`https://abcapi.vidaria.in/addbpatient`,Obj)
            .then(res => {
            console.log(res);
            console.log(res.data);
      })
      
      }

  
  
	render() {
		const { step } = this.state;
		const {
            id,
            can_u_dress_urself,can_u_wash_ur_hair,can_u_comb_ur_hair,can_u_stand_from_chair,can_u_get_inout_from_bed,can_u_sit_grossteg_onfloor,can_u_cut_vegetables,can_u_lift_glass,can_u_break_roti_from_1hand,can_u_walk,can_u_climb_5steps,can_u_take_bath,can_u_wash_dry_urbody,can_u_get_onoff_toilet,can_u_weigh_2kg,can_u_bend_and_pickcloths,can_u_open_bottle,can_u_turntaps_onoff,can_u_open_latches,can_u_work_office_house,can_u_run_errands,can_u_get_inout_of_bus,patient_assessment_pain,grip_strength_rt,grip_strength_hg,grip_strength_lt,early_mrng_stiffness,assess_sleep,general_health_assessment,classification_criteria_followig,diagnosis,treatment_plan,hematological_date,hem_esr,hem_hb,hem_tlc,hem_pleb,hem_plat,hem_urine,hem_others,hematological_date2,hem_esr2,hem_hb2,hem_tlc2,hem_pleb2,hem_plat2,hem_urine2,hem_others2,hematological_date3,hem_esr3,hem_hb3,hem_tlc3,hem_pleb3,hem_plat3,hem_urine3,hem_others3,hematological_date4,hem_esr4,hem_hb4,hem_tlc4,hem_pleb4,hem_plat4,hem_urine4,hem_others4,hematological_date5,hem_esr5,hem_hb5,hem_tlc5,hem_pleb5,hem_plat5,hem_urine5,hem_others5,biochemical_date,bio_lft,bio_rft,bio_crp,bio_bsl,bio_ua,bio_others,biochemical_date2,bio_lft2,bio_rft2,bio_crp2,bio_bsl2,bio_ua2,bio_others2,biochemical_date3,bio_lft3,bio_rft3,bio_crp3,bio_bsl3,bio_ua3,bio_others3,biochemical_date4,bio_lft4,bio_rft4,bio_crp4,bio_bsl4,bio_ua4,bio_others4,biochemical_date5,bio_lft5,bio_rft5,bio_crp5,bio_bsl5,bio_ua5,bio_others5,immunological_date,imm_rf,imm_accp,imm_ana,imm_anawb,imm_ace,imm_dsdna,imm_hlab27,imm_c3,imm_c4,imm_others,immunological_date2,imm_rf2,imm_accp2,imm_ana2,imm_anawb2,imm_ace2,imm_dsdna2,imm_hlab272,imm_c32,imm_c42,imm_others2,immunological_date3,imm_rf3,imm_accp3,imm_ana3,imm_anawb3,imm_ace3,imm_dsdna3,imm_hlab273,imm_c33,imm_c43,imm_others3,usg,xray_chest,xray_joints,mri,radio_remarks,das28,sledai,basdai,pasi,bvas,mrss,sdai_cdai,asdas,dapsa,vdi,essdai
            ,pid,user_id      
      } = this.state;

		const values = { 
            id,
            can_u_dress_urself,can_u_wash_ur_hair,can_u_comb_ur_hair,can_u_stand_from_chair,can_u_get_inout_from_bed,can_u_sit_grossteg_onfloor,can_u_cut_vegetables,can_u_lift_glass,can_u_break_roti_from_1hand,can_u_walk,can_u_climb_5steps,can_u_take_bath,can_u_wash_dry_urbody,can_u_get_onoff_toilet,can_u_weigh_2kg,can_u_bend_and_pickcloths,can_u_open_bottle,can_u_turntaps_onoff,can_u_open_latches,can_u_work_office_house,can_u_run_errands,can_u_get_inout_of_bus,patient_assessment_pain,grip_strength_rt,grip_strength_hg,grip_strength_lt,early_mrng_stiffness,assess_sleep,general_health_assessment,classification_criteria_followig,diagnosis,treatment_plan,hematological_date,hem_esr,hem_hb,hem_tlc,hem_pleb,hem_plat,hem_urine,hem_others,hematological_date2,hem_esr2,hem_hb2,hem_tlc2,hem_pleb2,hem_plat2,hem_urine2,hem_others2,hematological_date3,hem_esr3,hem_hb3,hem_tlc3,hem_pleb3,hem_plat3,hem_urine3,hem_others3,hematological_date4,hem_esr4,hem_hb4,hem_tlc4,hem_pleb4,hem_plat4,hem_urine4,hem_others4,hematological_date5,hem_esr5,hem_hb5,hem_tlc5,hem_pleb5,hem_plat5,hem_urine5,hem_others5,biochemical_date,bio_lft,bio_rft,bio_crp,bio_bsl,bio_ua,bio_others,biochemical_date2,bio_lft2,bio_rft2,bio_crp2,bio_bsl2,bio_ua2,bio_others2,biochemical_date3,bio_lft3,bio_rft3,bio_crp3,bio_bsl3,bio_ua3,bio_others3,biochemical_date4,bio_lft4,bio_rft4,bio_crp4,bio_bsl4,bio_ua4,bio_others4,biochemical_date5,bio_lft5,bio_rft5,bio_crp5,bio_bsl5,bio_ua5,bio_others5,immunological_date,imm_rf,imm_accp,imm_ana,imm_anawb,imm_ace,imm_dsdna,imm_hlab27,imm_c3,imm_c4,imm_others,immunological_date2,imm_rf2,imm_accp2,imm_ana2,imm_anawb2,imm_ace2,imm_dsdna2,imm_hlab272,imm_c32,imm_c42,imm_others2,immunological_date3,imm_rf3,imm_accp3,imm_ana3,imm_anawb3,imm_ace3,imm_dsdna3,imm_hlab273,imm_c33,imm_c43,imm_others3,usg,xray_chest,xray_joints,mri,radio_remarks,das28,sledai,basdai,pasi,bvas,mrss,sdai_cdai,asdas,dapsa,vdi,essdai
            ,pid,user_id    
    };
		switch (step) {
	// 		case 1:
	// 			return <BasicInfo
	// 				nextStep={this.nextStep}
	// 				handleChange={this.handleChange}
	// 				values={values}
	// 			/>


    //   case 2:
    //     return <Complaints
    //         nextStep={this.nextStep}
    //         prevStep={this.prevStep}
    //         // saveAll={this.saveAll.bind(this)} 
    //         handleChange={this.handleChange}
    //         values={values}
    //       />

    //   case 3:
    //     return <Summary 
    //              nextStep={this.nextStep}
    //              prevStep={this.prevStep}
    //              values={values}
    //              saveAll={this.saveAll.bind(this)} 
    //              />
    //   case 1:
    //     return <Examination
    //             nextStep={this.nextStep}
    //             prevStep={this.prevStep}

    //             handleChange={this.handleChange}
    //             values={values}
    //           />


	//   case 2:
	// 	return <Mus_examination
	// 				nextStep={this.nextStep}
	// 				prevStep={this.prevStep}
	// 				handleChange={this.handleChange}
	// 				values={values}
	// 			/>


    //   case 3:
    //     return <Rom_examination
    //         nextStep={this.nextStep}
    //         prevStep={this.prevStep}
    //         handleChange={this.handleChange}
    //         values={values}
    //       />
          
          

    //   case 4:
    //     return <Soft_tis_examination
    //             nextStep={this.nextStep}
    //             prevStep={this.prevStep}
    //             handleChange={this.handleChange}
    //             values={values}
    //           /> 


    //   case 5:
    //     return <Deformities_examination
    //                 nextStep={this.nextStep}
    //                 prevStep={this.prevStep}
    //                 handleChange={this.handleChange}
    //                 values={values}
    //               />         
	//   case 6:
	// 	return <Hypermobility_examination
    //            nextStep={this.nextStep}
    //            prevStep={this.prevStep}
    //            handleChange={this.handleChange}
    //            values={values}
    //     />


    //   case 7:
    //     return <Summary 
    //              nextStep={this.nextStep}
    //              prevStep={this.prevStep}
    //              values={values}
    //              saveAll={this.saveAll.bind(this)} 
    //              />

      



      case 1:
				return <Inves_Hematological
               nextStep={this.nextStep}
               prevStep={this.prevStep}
               handleChange={this.handleChange}
               values={values}
        />



      case 2:
				return <Inves_Biochemical
               nextStep={this.nextStep}
               prevStep={this.prevStep}
               handleChange={this.handleChange}
               values={values}
        />


      case 3:
				return <Inves_Immonological
               nextStep={this.nextStep}
               prevStep={this.prevStep}
               handleChange={this.handleChange}
               values={values}
        />

      case 4:
        return <Inves_Radiological
                 nextStep={this.nextStep}
                 prevStep={this.prevStep}
                 handleChange={this.handleChange}
                 values={values}
          />


      case 5:
        return <Health_ass_ques
                     nextStep={this.nextStep}
                     prevStep={this.prevStep}
                     handleChange={this.handleChange}
                     values={values}
              />

      case 6:
        return <Summary 
                 nextStep={this.nextStep}
                 prevStep={this.prevStep}
                 values={values}
                 saveAll={this.saveAll.bind(this)} 
                 />

     
	
		}
	}
}

export default Examination_Main;