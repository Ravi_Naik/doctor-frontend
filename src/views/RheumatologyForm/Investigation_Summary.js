


import React
,     { Component } from 'react';
import { Col
    ,     Row } from 'react-bootstrap';
import { Button } from 'react-bootstrap';


class Summary extends Component {

  saveAndContinue = (e) => {
		e.preventDefault();
		this.props.nextStep();
	}

	back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}

  render(){
const { values: {
    id,
    can_u_dress_urself
    ,can_u_wash_ur_hair
    ,can_u_comb_ur_hair
    ,can_u_stand_from_chair
    ,can_u_get_inout_from_bed
    ,can_u_sit_grossteg_onfloor
    ,can_u_cut_vegetables
    ,can_u_lift_glass
    ,can_u_break_roti_from_1hand
    ,can_u_walk
    ,can_u_climb_5steps
    ,can_u_take_bath
    ,can_u_wash_dry_urbody
    ,can_u_get_onoff_toilet
    ,can_u_weigh_2kg
    ,can_u_bend_and_pickcloths
    ,can_u_open_bottle
    ,can_u_turntaps_onoff
    ,can_u_open_latches
    ,can_u_work_office_house
    ,can_u_run_errands
    ,can_u_get_inout_of_bus
    ,patient_assessment_pain
    ,grip_strength_rt
    ,grip_strength_hg
    ,grip_strength_lt
    ,early_mrng_stiffness
    ,assess_sleep
    ,general_health_assessment
    ,classification_criteria_followig
    ,diagnosis
    ,treatment_plan
    ,hematological_date
    ,hem_esr
    ,hem_hb
    ,hem_tlc
    ,hem_pleb
    ,hem_plat
    ,hem_urine
    ,hem_others
    ,hematological_date2
    ,hem_esr2
    ,hem_hb2
    ,hem_tlc2
    ,hem_pleb2
    ,hem_plat2
    ,hem_urine2
    ,hem_others2
    ,hematological_date3
    ,hem_esr3
    ,hem_hb3
    ,hem_tlc3
    ,hem_pleb3
    ,hem_plat3
    ,hem_urine3
    ,hem_others3
    ,hematological_date4
    ,hem_esr4
    ,hem_hb4
    ,hem_tlc4
    ,hem_pleb4
    ,hem_plat4
    ,hem_urine4
    ,hem_others4
    ,hematological_date5
    ,hem_esr5
    ,hem_hb5
    ,hem_tlc5
    ,hem_pleb5
    ,hem_plat5
    ,hem_urine5
    ,hem_others5
    ,biochemical_date
    ,bio_lft
    ,bio_rft
    ,bio_crp
    ,bio_bsl
    ,bio_ua
    ,bio_others
    ,biochemical_date2
    ,bio_lft2
    ,bio_rft2
    ,bio_crp2
    ,bio_bsl2
    ,bio_ua2
    ,bio_others2
    ,biochemical_date3
    ,bio_lft3
    ,bio_rft3
    ,bio_crp3
    ,bio_bsl3
    ,bio_ua3
    ,bio_others3
    ,biochemical_date4
    ,bio_lft4
    ,bio_rft4
    ,bio_crp4
    ,bio_bsl4
    ,bio_ua4
    ,bio_others4
    ,biochemical_date5
    ,bio_lft5
    ,bio_rft5
    ,bio_crp5
    ,bio_bsl5
    ,bio_ua5
    ,bio_others5
    ,immunological_date
    ,imm_rf
    ,imm_accp
    ,imm_ana
    ,imm_anawb
    ,imm_ace
    ,imm_dsdna
    ,imm_hlab27
    ,imm_c3
    ,imm_c4
    ,imm_others
    ,immunological_date2
    ,imm_rf2
    ,imm_accp2
    ,imm_ana2
    ,imm_anawb2
    ,imm_ace2
    ,imm_dsdna2
    ,imm_hlab272
    ,imm_c32
    ,imm_c42
    ,imm_others2
    ,immunological_date3
    ,imm_rf3
    ,imm_accp3
    ,imm_ana3
    ,imm_anawb3
    ,imm_ace3
    ,imm_dsdna3
    ,imm_hlab273
    ,imm_c33
    ,imm_c43
    ,imm_others3
    ,usg
    ,xray_chest
    ,xray_joints
    ,mri
    ,radio_remarks
    ,das28
    ,sledai
    ,basdai
    ,pasi
    ,bvas
    ,mrss
    ,sdai_cdai
    ,asdas
    ,dapsa
    ,vdi
    ,essdai
    ,pid,user_id

    
    
    } } = this.props;

    return (

    <>
    <h2>Summary</h2>
    <Row>
   
  
  
         <Col md="6">

            <li>
              id: {id}
            </li>
            <li>
              can_u_dress_urself: {can_u_dress_urself}
            </li>
            <li>
              can_u_wash_ur_hair: {can_u_wash_ur_hair}
            </li>
            <li>
              can_u_comb_ur_hair: {can_u_comb_ur_hair}
            </li>
            <li>
              can_u_stand_from_chair: {can_u_stand_from_chair}
            </li>

            <li>
              can_u_get_inout_from_bed: {can_u_get_inout_from_bed}
            </li>



            <li>
              can_u_sit_grossteg_onfloor: {can_u_sit_grossteg_onfloor}
            </li>
             
  <li>
              can_u_cut_vegetables: {can_u_cut_vegetables}
            </li>
            <li>
              can_u_lift_glass: {can_u_lift_glass}
            </li>
            <li>
              can_u_break_roti_from_1hand: {can_u_break_roti_from_1hand}
            </li>
            <li>
              can_u_walk: {can_u_walk}
            </li>
    


            <li>
              can_u_climb_5steps: {can_u_climb_5steps}
            </li>


            
           
           

            <li>
              can_u_take_bath: {can_u_take_bath}
            </li>
            <li>
              can_u_wash_dry_urbody: {can_u_wash_dry_urbody}
            </li>
            <li>
              can_u_get_onoff_toilet: {can_u_get_onoff_toilet}
            </li>
            <li>
              can_u_weigh_2kg: {can_u_weigh_2kg}
            </li>
            <li>
            can_u_bend_and_pickcloths: {can_u_bend_and_pickcloths}
            </li>




            <li>
              can_u_open_bottle: {can_u_open_bottle}
            </li>


            <li>
              can_u_turntaps_onoff: {can_u_turntaps_onoff}
            </li>

            <li>
              can_u_open_latches: {can_u_open_latches}
            </li>

                 
    
            <li>
              can_u_work_office_house: {can_u_work_office_house}
            </li>
        
            <li>
            can_u_run_errands: {can_u_run_errands}
            </li>


            <li>
            can_u_get_inout_of_bus: {can_u_get_inout_of_bus}
            </li>
            
          

         
            <li>
              patient_assessment_pain: {patient_assessment_pain}
            </li>
            <li>
              grip_strength_rt: {grip_strength_rt}
            </li>

    
   

            <li>
              grip_strength_hg: {grip_strength_hg}
            </li>
            <li>
              grip_strength_lt: {grip_strength_lt}
            </li>
            <li>
              early_mrng_stiffness: {early_mrng_stiffness}
            </li>
            <li>
            assess_sleep: {assess_sleep}
            </li>
            
           

            <li>
              general_health_assessment: {general_health_assessment}
            </li>
</Col>

    



         <Col md="6">

            <li>
              classification_criteria_followig: {classification_criteria_followig}
            </li>

            <li>
              diagnosis: {diagnosis}
            </li>
            <li>
              treatment_plan: {treatment_plan}
            </li>
            <li>
              hematological_date: {hematological_date}
            </li>




            <li>
               hem_esr: { hem_esr}
            </li>

            <li>
               hem_hb: { hem_hb}
            </li>
            <li>
               hem_tlc: { hem_tlc}
            </li>
            <li>
               hem_pleb: { hem_pleb}
            </li>
          

    
 


<li>
               hem_plat: { hem_plat}
            </li>

            <li>
               hem_urine: { hem_urine}
            </li>
            <li>
               hem_others: { hem_others}
            </li>
            <li>
               hematological_date2: { hematological_date2}
            </li>


<li>
               hem_esr2: { hem_esr2}
            </li>

            <li>
               hem_hb2: { hem_hb2}
            </li>
            <li>
               hem_tlc2: { hem_tlc2}
            </li>
            <li>
               hem_pleb2: { hem_pleb2}
            </li>



<li>
               hem_plat2: { hem_plat2}
            </li>

            <li>
               hem_urine2: { hem_urine2}
            </li>
            <li>
               hem_others2: { hem_others2}
            </li>
            <li>
               hematological_date3: { hematological_date3}
            </li>



    
    
 
   
 
            <li>
               hem_esr3: { hem_esr3}
            </li>

            <li>
               hem_hb3: { hem_hb3}
            </li>
            <li>
               hem_tlc3: { hem_tlc3}
            </li>
            <li>
               hem_pleb3: { hem_pleb3}
            </li>




            <li>
               hem_plat3: { hem_plat3}
            </li>

            <li>
               hem_urine3: { hem_urine3}
            </li>
            <li>
               hem_others3: { hem_others3}
            </li>
            <li>
               hematological_date4: { hematological_date4}
            </li>

     





<li>
               hem_esr4: { hem_esr4}
            </li>

            <li>
               hem_hb4: { hem_hb4}
            </li>


            </Col>
      


          
         <Col md="6">
            <li>
               hem_tlc4: { hem_tlc4}
            </li>
            <li>
               hem_pleb4: { hem_pleb4}
            </li>


            <li>
               hem_plat4: { hem_plat4}
            </li>

            <li>
               hem_urine4: { hem_urine4}
            </li>


            <li>
               hem_others4: { hem_others4}
            </li>
            <li>
               hematological_date5: { hematological_date5}
            </li>




            <li>
               hem_esr5: { hem_esr5}
            </li>

            <li>
               hem_hb5: { hem_hb5}
            </li>
            <li>
               hem_tlc5: { hem_tlc5}
            </li>

            <li>
               hem_pleb5: { hem_pleb5}
            </li>




            <li>
               hem_plat5: { hem_plat5}
            </li>

            <li>
               hem_urine5: { hem_urine5}
            </li>
            <li>
               hem_others5: { hem_others5}
            </li>
            <li>
               biochemical_date: { biochemical_date}
            </li>


    
    
    
            <li>
               bio_lft: { bio_lft}
            </li>

            <li>
               bio_rft: { bio_rft}
            </li>
            <li>
               bio_crp: { bio_crp}
            </li>
            <li>
               bio_bsl: { bio_bsl}
            </li>





<li>
               bio_ua: { bio_ua}
            </li>

            <li>
               bio_others: { bio_others}
            </li>
            <li>
               biochemical_date2: { biochemical_date2}
            </li>
            <li>
               bio_lft2: { bio_lft2}
            </li>

  
   
    
            <li>
               bio_rft2: { bio_rft2}
            </li>

            <li>
               bio_crp2: { bio_crp2}
            </li>
            <li>
               bio_bsl2: { bio_bsl2}
            </li>
            <li>
               bio_ua2: { bio_ua2}
            </li>

            <li>
               bio_others2: { bio_others2}
            </li>

            <li>
               biochemical_date3: { biochemical_date3}
            </li>
            <li>
               bio_lft3: { bio_lft3}
            </li>
            <li>
               bio_rft3: { bio_rft3}
            </li>

</Col>




<Col md="6">
    <li>
               bio_crp3: { bio_crp3}
            </li>

            <li>
               bio_bsl3: { bio_bsl3}
            </li>
            <li>
               bio_ua3: { bio_ua3}
            </li>
            <li>
               bio_others3: { bio_others3}
            </li>



            <li>
               biochemical_date4: { biochemical_date4}
            </li>
  
   
            <li>
               bio_lft4: { bio_lft4}
            </li>
            <li>
               bio_rft4: { bio_rft4}
            </li>
            <li>
               bio_crp4: { bio_crp4}
            </li>



            <li>
               bio_bsl4: { bio_bsl4}
            </li>

            <li>
               bio_ua4: { bio_ua4}
            </li>
            <li>
               bio_others4: { bio_others4}
            </li>
            <li>
               biochemical_date5: { biochemical_date5}
            </li>
  





    <li>
              bio_lft5: {bio_lft5}
            </li>

            <li>
              bio_rft5: {bio_rft5}
            </li>
            <li>
              bio_crp5: {bio_crp5}
            </li>
            <li>
              bio_bsl5: {bio_bsl5}
            </li>

            <li>
              bio_ua5: {bio_ua5}
            </li>

            <li>
              bio_others5: {bio_others5}
            </li>
            <li>
              immunological_date: {immunological_date}
            </li>
         



















  
            <li>
              imm_rf: {imm_rf}
            </li>

            <li>
              imm_accp: {imm_accp}
            </li>
            <li>
              imm_ana: {imm_ana}
            </li>
            <li>
              imm_anawb: {imm_anawb}
            </li>


            <li>
              imm_ace: {imm_ace}
            </li>

            <li>
              imm_dsdna: {imm_dsdna}
            </li>
            <li>
              imm_hlab27: {imm_hlab27}
            </li>
            <li>
              imm_c3: {imm_c3}
            </li>


            <li>
              imm_c4: {imm_c4}
            </li>

            <li>
              imm_others: {imm_others}
            </li>



            <li>
              immunological_date2: {immunological_date2}
            </li>
            <li>
     
              imm_rf2: {imm_rf2}
            </li>


            <li>
              imm_accp2: {imm_accp2}
            </li>

            <li>
              imm_ana2: {imm_ana2}
            </li>
            <li>
              imm_anawb2: {imm_anawb2}
            </li>
            <li>
              imm_ace2: {imm_ace2}
            </li>






<li>
              imm_dsdna2: {imm_dsdna2}
            </li>

            <li>
              imm_hlab272: {imm_hlab272}
            </li>
            <li>
              imm_c32: {imm_c32}
            </li>
            <li>
              imm_c42: {imm_c42}
            </li>

            <li>
              imm_others2: {imm_others2}
            </li>

            <li>
              immunological_date3: {immunological_date3}
            </li>
            <li>
   
 
              imm_rf3: {imm_rf3}
            </li>
            <li>
              imm_accp3: {imm_accp3}
            </li>

            <li>
              imm_ana3: {imm_ana3}
            </li>

            <li>
              imm_anawb3: {imm_anawb3}
            </li>
            <li>
              imm_ace3: {imm_ace3}
            </li>
            <li>
              imm_dsdna3: {imm_dsdna3}
            </li>

</Col>

<Col md="6">


            <li>
              imm_hlab273: {imm_hlab273}
            </li>

            <li>
              imm_c33: {imm_c33}
            </li>
            <li>
              imm_c43: {imm_c43}
            </li>
            <li>
              imm_others3: {imm_others3}
            </li>





            <li>
              usg: {usg}
            </li>
  
 
            <li>
              xray_chest: {xray_chest}
            </li>
            <li>
              xray_joints: {xray_joints}
            </li>
            <li>
              mri: {mri}
            </li>



            <li>
              radio_remarks: {radio_remarks}
            </li>

            <li>
              das28: {das28}
            </li>
            <li>
              sledai: {sledai}
            </li>
            <li>
              basdai: {basdai}
            </li>



            <li>
              pasi: {pasi}
            </li>

            <li>
              bvas: {bvas}
            </li>
            <li>
              mrss: {mrss}
            </li>
            <li>
              sdai_cdai: {sdai_cdai}
            </li>
           



            <li>
              asdas: {asdas}
            </li>

            <li>
              dapsa: {dapsa}
            </li>

            <li>
              vdi: {vdi}
            </li>
            <li>
              essdai: {essdai}
            </li>
            <li>
              pid: {pid}
            </li>

            <li>
              user_id: {user_id}
            </li>

            

         


           


<Button className="btn-fill"  onClick={this.back}>Back</Button>


          
         
            
              
         
         <Button className="btn-fill" variant="success" onClick={this.props.saveAll}>Submit</Button>

            
          </Col>
        </Row>

      </>
 
    )
  }
}
export default Summary;
