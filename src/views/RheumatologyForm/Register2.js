
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class Register2 extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}

    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Information</h2>

        {/* <Row> */}

        <Form>
  
<Row>
  <Col md="6">
  <Form.Group>

            <Form.Label>Education</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('education')} defaultValue={values.education}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>Address</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('address')} defaultValue={values.address}/>
                    </Form.Group>


</Col>
<Col md="6">
<Form.Group>

          <Form.Label>Phone</Form.Label><br></br>
          <Form.Control  type="number" name="phone"
                        maxLength="10" onChange={this.props.handleChange('phone')} defaultValue={values.phone}/>
                    </Form.Group>



          <Form.Group>

          <Form.Label>District</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('district')} defaultValue={values.district}/>
                    </Form.Group>


</Col>
</Row> 

<Row>
  <Col md="6">
  <Form.Group>

          <Form.Label>State</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('state')} defaultValue={values.state}/>
                    </Form.Group>


          <Form.Group>


          <Form.Label>Area</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('area')}  defaultValue={values.area}/>
                    </Form.Group>


          </Col>
          <Col md="6">
          <Form.Group>

          <Form.Label>Referred_by</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('referred_by')}  defaultValue={values.referred_by}/>
                    </Form.Group>

          <Form.Group>

          <Form.Label>Occupation</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('occupation')}  defaultValue={values.occupation}/>
                    </Form.Group>


</Col>
</Row>



<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">   
           <Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>

</Row>









        </Form>
   
      {/* </Row> */}
      </Container>
    )
  }
}

export default Register2;







