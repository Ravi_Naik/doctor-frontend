
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class Register2 extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}

    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Information</h2>

        {/* <Row> */}

        <Form>
 
        <Row>
          <Col md="6">
       
          <Form.Group>
          <Form.Label>menopause</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('menopause')}
defaultValue={values.menopause}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>menopause_years</Form.Label><br></br>
           <Form.Control type="number" 						onChange={this.props.handleChange('menopause_years')}
 defaultValue={values.menopause_years}/>
           </Form.Group>
          </Col>


          <Col md="6">
          <Form.Group>

          <Form.Label>no of children</Form.Label><br></br>
           <Form.Control type="number" 						onChange={this.props.handleChange('children')}
defaultValue={values.children}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>children_male</Form.Label><br></br>
           <Form.Control type="number" 						onChange={this.props.handleChange('children_male')}
 defaultValue={values.children_male}/>
           </Form.Group>

</Col>
</Row>

<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>children_female</Form.Label><br></br>
           <Form.Control type="number" 						onChange={this.props.handleChange('children_female')}
defaultValue={values.children_female}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>abortions</Form.Label><br></br>
           <Form.Control type="text" 						onChange={this.props.handleChange('abortions')}
 defaultValue={values.abortions}/>
           </Form.Group>
</Col>
<Col md="6">
<Form.Group>
          <Form.Label>abortions_number</Form.Label><br></br>
           <Form.Control type="number" 						onChange={this.props.handleChange('abortions_number')}
 defaultValue={values.abortions_number}/>
           </Form.Group>
          <Form.Group>
          <Form.Label>abortions_cause</Form.Label><br></br>
           <Form.Control type="text" 						onChange={this.props.handleChange('abortions_cause')}
 defaultValue={values.abortions_cause}/>
           </Form.Group>
         </Col>
         </Row>


<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">   
           <Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>

</Row>









        </Form>
   
      {/* </Row> */}
      </Container>
    )
  }
}

export default Register2;







