
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class Register2 extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}

    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient  Information</h2>

        {/* <Row> */}

        <Form>
 
        <Row>
         <Col md="6">
         <Form.Group>
           <Form.Label>current_lactation</Form.Label><br></br>
           <Form.Control  type="text" 						onChange={this.props.handleChange('current_lactation')}
defaultValue={values.current_lactation}/>
          </Form.Group>

          <Form.Group>
           <Form.Label>contraception_methods</Form.Label><br></br>
           <Form.Control  type="text" 						onChange={this.props.handleChange('contraception_methods')}
defaultValue={values.contraception_methods}/>
          </Form.Group>
             </Col>

             <Col md="6">
             <Form.Group>
         <Form.Label>contraception_methods_type</Form.Label><br></br>
           <Form.Control  type="text" 						onChange={this.props.handleChange('contraception_methods_type')}
defaultValue={values.contraception_methods_type}/>
          </Form.Group>

          <Form.Group>
           <Form.Label>hormone_treatment</Form.Label><br></br>
           <Form.Control  type="text" 						onChange={this.props.handleChange('hormone_treatment')}
defaultValue={values.hormone_treatment}/>
          </Form.Group>
             </Col>
         </Row>

<Row>
<Col md="6">
<Form.Group>
           <Form.Label>addiction</Form.Label><br></br>
           <Form.Control  type="text" 						onChange={this.props.handleChange('addiction')}
defaultValue={values.addiction}/>
          </Form.Group>
          <Form.Group>
           <Form.Label>tobacco</Form.Label><br></br>
           <Form.Control  type="text" 						onChange={this.props.handleChange('tobacco')}
defaultValue={values.tobacco}/>
          </Form.Group>
    </Col>
    <Col md="6">

    <Form.Group>
          <Form.Label>tobacco_years</Form.Label><br></br>
          <Form.Control type="number" 						onChange={this.props.handleChange('tobacco_years')}
defaultValue={values.tobacco_years}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>smoking</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('smoking')}
 defaultValue={values.smoking}/>
          </Form.Group>
        </Col>

</Row>

<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">   
           <Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>

</Row>









        </Form>
   
      {/* </Row> */}
      </Container>
    )
  }
}

export default Register2;







