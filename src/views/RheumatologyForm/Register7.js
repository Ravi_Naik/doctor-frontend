
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class Register2 extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}

    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Complaint  Information</h2>

        {/* <Row> */}

        <Form>
 
       
<Row>
<Col md="6">
        <Form.Group>
          <Form.Label>onset_duration_type</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('onset_duration_type')}  defaultValue={values.onset_duration_type}/>
          </Form.Group>


          <Form.Group>
          <Form.Label>first_symptom</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('first_symptom')} defaultValue={values.first_symptom}/>
          </Form.Group>
          </Col>


<Col md="6">
<Form.Group>
          <Form.Label>initial_site_joint</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('initial_site_joint')} defaultValue={values.initial_site_joint}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>soft_tissue</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('soft_tissue')} defaultValue={values.soft_tissue}/>
          </Form.Group>
</Col>

</Row>



<Row>
<Col md="6">

<Form.Group>
          <Form.Label>others</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('others')} defaultValue={values.others}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>course1</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('course1')} defaultValue={values.course1}/>
          </Form.Group>

</Col>
<Col md="6">
  <Form.Group>

            <Form.Label>course2</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('course2')} defaultValue={values.course2}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>pattern1</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('pattern1')} defaultValue={values.pattern1}/>
                    </Form.Group>


</Col>
</Row>



<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">   
           <Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>

</Row>









        </Form>
   
      {/* </Row> */}
      </Container>
    )
  }
}

export default Register2;







