
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class Register2 extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}

    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Complaint  Information</h2>

        {/* <Row> */}

        <Form>
 
       
<Row>
<Col md="6">
<Form.Group>

          <Form.Label>pattern2</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('pattern2')} defaultValue={values.pattern2}/>
                    </Form.Group>



          <Form.Group>

          <Form.Label>current_relapse</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('current_relapse')} defaultValue={values.current_relapse}/>
                    </Form.Group>


</Col>


<Col md="6">
  <Form.Group>

          <Form.Label>current_relapse_type</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('current_relapse_type')} defaultValue={values.current_relapse_type}/>
                    </Form.Group>


          <Form.Group>


          <Form.Label>detailed_history</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('detailed_history')}  defaultValue={values.detailed_history}/>
                    </Form.Group>


          </Col>

</Row>



<Row>
<Col md="6">
          <Form.Group>

          <Form.Label>pasthistory</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('pasthistory')}  defaultValue={values.pasthistory}/>
                    </Form.Group>

          <Form.Group>

          <Form.Label>surgical_history</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('surgical_history')}  defaultValue={values.surgical_history}/>
                    </Form.Group>


</Col>
<Col md="6">
  <Form.Group>

          <Form.Label>drug_history</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('drug_history')}  defaultValue={values.drug_history}/>
                    </Form.Group>


          <Form.Group>

          <Form.Label>drug_allergy</Form.Label><br></br>
          <Form.Control type="text"  onChange={this.props.handleChange('drug_allergy')} defaultValue={values.drug_allergy}/>
                    </Form.Group>


         </Col> 
</Row>



<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">   
           <Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>

</Row>









        </Form>
   
      {/* </Row> */}
      </Container>
    )
  }
}

export default Register2;







