
import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Table,
  Col,
} from "react-bootstrap";

class Rom_examination extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}


    back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
  
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient MUSCULOSKELETAL_EXAMINATIONS</h2>
         <h5>Rom: Information</h5> 

        <Row>

        <Form>
  
        <Table striped bordered hover>
  <thead>
    <tr>
      <th></th>
      <th>ANKY</th>
      <th>FLEX</th>
      <th>EXT</th>
      <th>RT-ROT</th>
      <th>LT-ROT</th>
      <th>RT-FL</th>
      <th>LT-FL</th>
      <th>P/T</th>
     

    </tr>
  </thead>
  <tbody>
    <tr>
      <td>CERVICAL</td>
      <td><input type="text" 	onChange={this.props.handleChange('cervical_anky')}
defaultValue={values.cervical_anky} maxlength="50" size="4" /></td>
     
     <td><input type="text" 						onChange={this.props.handleChange('cervical_flex')}
defaultValue={values.cervical_flex} maxlength="50" size="4" /></td>

      <td><input type="text" 						onChange={this.props.handleChange('cervical_ext')}
defaultValue={values.cervical_ext} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('cervical_rtrot')}
defaultValue={values.cervical_rtrot} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('cervical_ltrot')}
defaultValue={values.cervical_ltrot} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('cervical_rtfl')}
defaultValue={values.cervical_rtfl} maxlength="50" size="4" /></td>

      
<td><input type="text" 						onChange={this.props.handleChange('cervical_ltfl')}
defaultValue={values.cervical_ltfl} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('cervical_pt')}
defaultValue={values.cervical_pt} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td>THORASIC</td>
      <td><input type="text" 						onChange={this.props.handleChange('thorasic_anky')}
defaultValue={values.thorasic_anky} maxlength="50" size="4" /></td>
   
   <td><input type="text" 						onChange={this.props.handleChange('thorasic_flex')}
defaultValue={values.thorasic_flex} maxlength="50" size="4" /></td>

      <td><input type="text" 						onChange={this.props.handleChange('thorasic_ext')}
defaultValue={values.thorasic_ext} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('thorasic_rtrot')}
defaultValue={values.thorasic_rtrot} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('thorasic_ltrot')}
defaultValue={values.thorasic_ltrot} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('thorasic_rtfl')}
defaultValue={values.thorasic_rtfl} maxlength="50" size="4" /></td>

      
<td><input type="text" 						onChange={this.props.handleChange('thorasic_ltfl')}
defaultValue={values.thorasic_ltfl} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('thorasic_pt')}
defaultValue={values.thorasic_pt} maxlength="50" size="4" /></td>

    </tr>
    <tr>
    <td>LUMBAR</td>
      <td><input type="text" 						onChange={this.props.handleChange('lumbar_anky')}
defaultValue={values.lumbar_anky} maxlength="50" size="4" /></td>

<td><input type="text" 						onChange={this.props.handleChange('lumbar_flex')}
defaultValue={values.lumbar_flex} maxlength="50" size="4" /></td>
      <td><input type="text" 						onChange={this.props.handleChange('lumbar_ext')}
defaultValue={values.lumbar_ext} maxlength="50" size="4" /></td>
            <td><input type="text" 						onChange={this.props.handleChange('lumbar_rtrot')}
defaultValue={values.lumbar_rtrot} maxlength="50" size="4" /></td>

            <td><input type="text" 						onChange={this.props.handleChange('lumbar_ltrot')}
defaultValue={values.lumbar_ltrot} maxlength="50" size="4" /></td>

           <td><input type="text" 						onChange={this.props.handleChange('lumbar_rtfl')}
defaultValue={values.lumbar_rtfl} maxlength="50" size="4" /></td>

      
<td><input type="text" 						onChange={this.props.handleChange('lumbar_ltfl')}
defaultValue={values.lumbar_ltfl} maxlength="50" size="4" /></td>

      <td><input type="text" 						onChange={this.props.handleChange('lumbar_pt')}
defaultValue={values.lumbar_pt} maxlength="50" size="4" /></td>

 
    </tr>

   
  </tbody>
</Table>
<Row>
<Col md="6">
<Button className="btn-fill"  onClick={this.back}>Back</Button>

</Col>
<Col md="6">
<Button variant="warning" className="btn-fill" onClick={this.saveAndContinue}>Save And Continue </Button>

</Col>
</Row>

        </Form>
   
      </Row>

      
      </Container>
    )
  }
}

export default Rom_examination;







