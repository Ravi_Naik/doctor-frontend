
 
// import React, { Component } from 'react';
// import { Form, Button } from 'semantic-ui-react';

// class Step2 extends Component {
// 	saveAndContinue = (e) => {
// 		e.preventDefault();
// 		this.props.nextStep();
// 	}

// 	back = (e) => {
// 		e.preventDefault();
// 		this.props.prevStep();
// 	}

// 	render() {
// 		const { values } = this.props
// 		return (
// 			<Form color='blue' >
// 				<h1 className="ui centered">Enter Personal Details</h1>
// 				<Form.Field>
// 					<label>password</label>
// 					<input placeholder='password'
// 						onChange={this.props.handleChange('password')}
// 						defaultValue={values.password}
// 					/>
// 				</Form.Field>
// 				<Form.Field>
// 					<label>role</label>
// 					<input placeholder='role'
// 						onChange={this.props.handleChange('role')}
// 						defaultValue={values.role}
// 					/>
// 				</Form.Field>
// 				<Form.Field>
// 					<label>speciality</label>
// 					<input placeholder='speciality'
// 						onChange={this.props.handleChange('speciality')}
// 						defaultValue={values.speciality}
// 					/>
// 				</Form.Field>
// 				<Button onClick={this.back}>Back</Button>
// 				<Button onClick={this.saveAndContinue}>Save And Continue </Button>
// 			</Form>
// 		)
// 	}
// }

// export default Step2;



import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class Step2 extends Component {
  saveAndContinue = (e) => {
		e.preventDefault()
		this.props.nextStep()
	}

	back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}
	
  render() {
  const { values } = this.props;
      return (
        <Container fluid>
      <h2>Patient Information</h2>

        <Row>

        <Form>





<Row>
  <Col md="6">
  <Form.Group>
          <Form.Label>Ethnic</Form.Label><br></br>
           <Form.Control  type="text"  onChange={this.props.handleChange('ethnic')}  defaultValue={values.ethnic}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>Marital_status</Form.Label><br></br>
           <Form.Control  type="text"  onChange={this.props.handleChange('marital_status')} defaultValue={values.marital_status}/>
          </Form.Group>

         </Col> 
      
         <Col md="6">
         <Form.Group>
          <Form.Label>marital_status_years</Form.Label><br></br>
           <Form.Control  type="number"  onChange={this.props.handleChange('marital_status_years')}  defaultValue={values.marital_status_years}/>
          </Form.Group>

          <Form.Group>
          <Form.Label>menses_frequency</Form.Label><br></br>
           <Form.Control  type="text"  onChange={this.props.handleChange('menses_frequency')} defaultValue={values.menses_frequency}/>
          </Form.Group>

          </Col>
          </Row>


          <Row>

			  
  <Col md="6">   
  <Form.Group>
          <Form.Label>menses_loss</Form.Label><br></br>
           <Form.Control  type="text"  onChange={this.props.handleChange('menses_loss')} defaultValue={values.menses_loss}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>menarche_years</Form.Label><br></br>
           <Form.Control  type="number"  onChange={this.props.handleChange('menarche_years')} defaultValue={values.menarche_years}/>
          </Form.Group>
          </Col>
          <Col md="6"> 
          <Form.Group>  

          <Form.Label>hystrectomy</Form.Label><br></br>
           <Form.Control  type="text"  onChange={this.props.handleChange('hystrectomy')} defaultValue={values.hystrectomy}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>hystrectomy_years</Form.Label><br></br>
           <Form.Control  type="number"  onChange={this.props.handleChange('hystrectomy_years')} defaultValue={values.hystrectomy_years}/>
          </Form.Group>


{/* 
          <Button onClick={this.saveAndContinue}>Save And Continue </Button>
		  <Button onClick={this.saveAndContinue}>Save And Continue </Button>
 */}


       </Col>
       </Row>

	   <Row>
	   <Col md="6">
	   <Button className="btn-fill"  onClick={this.back}>Back</Button>
	 

			   </Col>
			   <Col md="6">
			   <Button className="btn-fill" variant="warning"  onClick={this.saveAndContinue}>Save And Continue </Button>

		   </Col>
	   </Row>



		</Form>
   
      </Row>
      </Container>
	  )
	  }
	}

	export default Step2