
// import React, { Component } from 'react';
// import { Button, List } from 'semantic-ui-react';

// class Step3 extends Component {
// 	saveAndContinue = (e) => {
// 		e.preventDefault();
// 		this.props.nextStep();
// 	}

// 	back = (e) => {
// 		e.preventDefault();
// 		this.props.prevStep();
// 	}

// 	render() {
// 		const { values: { username,email,phone,password,role,speciality} } = this.props;

// 		return (
// 			<div>
// 				<h1 className="ui centered">Confirm your Details</h1>
// 				<p>Click Confirm if the following details have been correctly entered</p>
// 				<List>
// 					<List.Item>
// 						<List.Icon name='users' />
// 						<List.Content>Name: {username}</List.Content>
// 					</List.Item>
					
// 					<List.Item>
// 						<List.Icon name='mail' />
// 						<List.Content>
// 							<a href='mailto:jack@semantic-ui.com'>{email}</a>
// 						</List.Content>
// 					</List.Item>
// 					<List.Item>
// 						<List.Icon name='phone' />
// 						<List.Content>{phone}phone</List.Content>
// 					</List.Item>
// 					<List.Item>
// 						<List.Icon name='role' />
// 						<List.Content> {role}</List.Content>
// 					</List.Item>
//                     <List.Item>
// 						<List.Icon name='speciality' />
// 						<List.Content> {speciality}</List.Content>
// 					</List.Item>
// 				</List>

// 				<Button onClick={this.back}>Back</Button>
//                 <button onClick={this.props.saveAll}>Submit</button>
// 			</div>
// 		)
// 	}
// }

// export default Step3;



import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class Step4 extends Component {


  saveAndContinue = (e) => {
		e.preventDefault();
		this.props.nextStep();
	}

	back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}

  render() {
		const { values } = this.props
    return (
      <Container fluid>
      <h2>Patient Information</h2>

        <Row>

        <Form>

<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>tobacco_years</Form.Label><br></br>
          <Form.Control type="number" 						onChange={this.props.handleChange('tobacco_years')}
defaultValue={values.tobacco_years}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>smoking</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('smoking')}
 defaultValue={values.smoking}/>
          </Form.Group>
          </Col>

          <Col md="6">
          <Form.Group>
          <Form.Label>smoking_years</Form.Label><br></br>
          <Form.Control type="number" 						onChange={this.props.handleChange('smoking_years')}
defaultValue={values.smoking_years}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>alcohol</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('alcohol')}
defaultValue={values.alcohol}/>
          </Form.Group>
          </Col>

          </Row>

          <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>alcohol_years</Form.Label><br></br>
          <Form.Control type="number" 						onChange={this.props.handleChange('alcohol_years')}
 defaultValue={values.alcohol_years}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>family_history</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('family_history')}
 defaultValue={values.family_history}/>
          </Form.Group>
          </Col>


          <Col md="6">
          <Form.Group>
          <Form.Label>comorbidities</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('comorbidities')}
defaultValue={values.comorbidities}/>
          </Form.Group>

          
          <Form.Group>
          <Form.Label>breastlump</Form.Label><br></br>
          <Form.Control type="text" 						onChange={this.props.handleChange('breastlump')}
defaultValue={values.breastlump}/>
          </Form.Group>
          </Col>
          </Row>


{/*         


    <Row>
          <Col md="6">
          <label>breastlump_location</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('breastlump_location')}
 defaultValue={values.breastlump_location}/>
          <br/>
          <label>breastlump_size</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('breastlump_size')}
defaultValue={values.breastlump_size}/>
          <br/>
</Col>

<Col md="6">

          <label>overlying_skin</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('overlying_skin')}
defaultValue={values.overlying_skin}/>
          <br/>
          <label>axillarylump</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('axillarylump')}
defaultValue={values.axillarylump}/>
          <br/>
</Col>
</Row>


<Row>
          <Col md="6">

          <label>axillarylump_side</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('axillarylump_side')}
 defaultValue={values.axillarylump_side}/>
          <br/>
          <label>matted</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('matted')}
defaultValue={values.matted}/>
          <br/>
</Col>

<Col md="6">
          <label>axillarylump_size</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('axillarylump_size')}
defaultValue={values.axillarylump_size}/>
          <br/>
          <label>nipple_discharge</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('nipple_discharge')}
 defaultValue={values.nipple_discharge}/>
          <br/>

          </Col>
          </Row>
        
          <Row>
          <Col md="6">
          <label>nipple_discharge_frequency</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('nipple_discharge_frequency')}
 defaultValue={values.nipple_discharge_frequency}/>
          <br/>
          <label>nipple_discharge_colour</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('nipple_discharge_colour')}
defaultValue={values.nipple_discharge_colour}/>
          <br/><br></br>
          <Button onClick={this.back}>Back</Button>
</Col>

<Col md="6">
          <label>mastalgia</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('mastalgia')}
defaultValue={values.mastalgia}/>
          <br/>
          <label>mastitis</label><br></br>
          <input type="text" 						onChange={this.props.handleChange('mastitis')}
 defaultValue={values.mastitis}/>
          <br/><br></br>
 

          
			 <Button onClick={this.saveAndContinue}>Save And Continue </Button>
</Col>
</Row>  */}
<Row>
	   <Col md="6">
	   <Button className="btn-fill"  onClick={this.back}>Back</Button>
	 

			   </Col>
			   <Col md="6">
			   <Button className="btn-fill" variant="warning" onClick={this.saveAndContinue}>Save And Continue </Button>

		   </Col>
	   </Row>
			</Form>
			</Row>
			</Container>
	)
	}
}
export default Step4;