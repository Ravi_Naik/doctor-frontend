


import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import {
 

  Card,
  Form,
  Container,
  Row,
  Col,
} from "react-bootstrap";

class Step8 extends Component {
  saveAndContinue = (e) => {
		e.preventDefault();
		this.props.nextStep();
	}

	back = (e) => {
		e.preventDefault();
		this.props.prevStep();
	}

  render() 
  {
		const { values } = this.props
    return (
      <Container fluid>
      <h2>Patient Information</h2>

        <Row>

        <Form>







<Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>mmg</Form.Label>
         <Form.Control type="text" 						onChange={this.props.handleChange('mmg')}
defaultValue={values.mmg}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>mri</Form.Label>
         <Form.Control type="text" 						onChange={this.props.handleChange('mri')}
 defaultValue={values.mri}/>
          </Form.Group>
          </Col>
          <Col md="6">

          <Form.Group>
          <Form.Label>fnac</Form.Label>
         <Form.Control type="text" 						onChange={this.props.handleChange('fnac')}
 defaultValue={values.fnac}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>core_biopsy</Form.Label>
         <Form.Control type="text" 						onChange={this.props.handleChange('core_biopsy')}
defaultValue={values.core_biopsy}/>
        
          </Form.Group>
        </Col>
        </Row>

        <Row>
          <Col md="6">
          <Form.Group>
          <Form.Label>incision_biopsy</Form.Label>
         <Form.Control type="text" 						onChange={this.props.handleChange('incision_biopsy')}
defaultValue={values.incision_biopsy}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>investigation_remarks</Form.Label>
         <Form.Control type="text" 						onChange={this.props.handleChange('investigation_remarks')}
           defaultValue={values.investigation_remarks}/>
          </Form.Group>
          </Col>

          <Col md="6">
          <Form.Group>
          <Form.Label>blood_investigation</Form.Label>
         <Form.Control type="text" 						onChange={this.props.handleChange('blood_investigation')}
defaultValue={values.blood_investigation}/>
          </Form.Group>
          <Form.Group>
          <Form.Label>diagnosis</Form.Label>
         <Form.Control type="text" 						onChange={this.props.handleChange('diagnosis')}
defaultValue={values.diagnosis}/>
          </Form.Group>
          </Col>
          </Row>




        <Row>
        
          <Col md="6">
          <Form.Group>
          <Form.Label>treatment_plan</Form.Label>
         <Form.Control type="text" 						onChange={this.props.handleChange('treatment_plan')}
defaultValue={values.treatment_plan}/>
          </Form.Group>

          </Col>
          <Col md="6">
          <Form.Group>
          <Form.Label>User Id</Form.Label>
         <Form.Control type="number" 						onChange={this.props.handleChange('user_id')}
defaultValue={values.user_id}/>
          </Form.Group>

</Col>
</Row>
<Row>
	   <Col md="6">
	   <Button className="btn-fill"  onClick={this.back}>Back</Button>
	 

			   </Col>
			   <Col md="6">
			   <Button className="btn-fill" variant="warning" onClick={this.saveAndContinue}>Save And Continue </Button>

		   </Col>
	   </Row>
          
        </Form>
        
        
      

        </Row>
      </Container>
    )

  
  }

}

export default Step8;
