import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import Modal from "react-bootstrap/Modal";
import userdetails from '../views/admin'
import Swal from 'sweetalert2'

//Bootstrap and jQuery libraries
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.min.js';
//Datatable Modules
import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"
import $ from 'jquery';


// react-bootstrap components
import { Badge, Button, Card, Navbar, Nav, Table, Container, Row, Col, NavLink, Form } from "react-bootstrap";

class UserManagement extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      users: [],


      id: 0,
      username: "",
      email: "",
      phone: "",
      password: "",
      role: "",
      speciality: "",
      showHide: false,



    }

  }


  opensweetalert() {
    Swal.fire({
      title: 'User record added',
      text: "sucess",
      type: 'success',

    })
  }


  clearTable = () => {
    this.setState({ users: [] })
  }
  componentDidMount() {


    // $('#example').DataTable().destroy(true);


    axios.get(`https://abcapi.vidaria.in/alluserdetails?X-AUTH=abc123`)
      .then((res) => {
        console.log(res)
        $('#example').DataTable().destroy();
        this.setState({
          users: res.data.user,
          id: 0,
          username: '',
          email: '',
          phone: '',
          password: '',
          role: 'Admin',
          speciality: 'Rheumatology',
          activity: "User Added",
          user_id: userdetails()
        }, () => {
          $('#example').DataTable();
        })
      })



    $(document).ready(function () {
      $('#example').DataTable(

      );
    });




  }




  handleModalShowHide() {
    this.setState({ showHide: !this.state.showHide })
  }


  submit(event) {
    event.preventDefault();

    // const {username, role} = this.state
    console.log(this.state.role)

    axios.post(`https://abcapi.vidaria.in/addactivity?X-AUTH=abc123`, {

      activity: this.state.activity,
      user_id: this.state.user_id,

    })

    axios.post(`https://abcapi.vidaria.in/adduser?X-AUTH=abc123`, {
      username: this.state.username,
      email: this.state.email,
      phone: this.state.phone,
      password: this.state.password,
      role: this.state.role,
      speciality: this.state.speciality,

    })
      .then((res) => {
        if (res.data.success == "false") {

          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: res.data.message,

          })




          //  this.sameemailsweetalert()
          this.handleModalShowHide()

        }
        if (res.data.success == "true") {
          this.opensweetalert()
          this.componentDidMount();
          this.props.history.push(`/admin/umanagement`)
          console.log('yes aya mai')

        }



        console.log(res.data.message)
        console.log(res.data.success)
      })
  }


  componentWillUnmount() {
    $('#example').DataTable().destroy(true);

  }

  delete(id, e) {
    var del = "user deleted"

    e.preventDefault()
    Swal.fire({
      title: 'Do you want to delete the changes?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: `Delete`,
      denyButtonText: `Cancel`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('user deleted!', '', 'success')
        axios.delete(`https://abcapi.vidaria.in/userdelete?X-AUTH=abc123&uid=${id}`)
        axios.post(`https://abcapi.vidaria.in/addactivity?X-AUTH=abc123`, {

          activity: del,
          user_id: this.state.user_id,

        })
          .then(() => {
            this.componentDidMount();

          });
      } else if (result.isDenied) {
        Swal.fire('user record is safe !', '', 'info')
      }
    })



  }
  render() {

    return (

      <>
        <div>







          <Modal

            size="lg"
            aria-labelledby="example-custom-modal-styling-title"

            show={this.state.showHide}>
            <Modal.Header closeButton onClick={() => this.handleModalShowHide()}>
              <Modal.Title>Adding User</Modal.Title>
            </Modal.Header>
            <Modal.Body>


              <Container fluid>
                {/* <Row> */}
                <Col className="pr-1" md="12">
                  <Card>
                    <Card.Header>
                      {/* <Card.Title as="h4">Adding User</Card.Title> */}
                    </Card.Header>
                    <Card.Body>
                      <Form onSubmit={(e) => this.submit(e, this.state.id)} >

                        <Row>
                          <Col className="pr-1" md="6">
                            <Form.Group>
                              <label>Username</label>
                              <Form.Control
                                name="uname"
                                placeholder="Username"
                                type="text"
                                // value={this.state.username}
                                onChange={(e) => this.setState({ username: e.target.value })}

                              ></Form.Control>
                            </Form.Group>
                          </Col>


                          <Col className="pr-1" md="6">
                            <Form.Group>
                              <label>
                                Email address
                        </label>
                              <Form.Control
                                placeholder="Email"
                                type="email"
                                // value={this.state.email}
                                name="email"
                                pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                                onChange={(e) => this.setState({ email: e.target.value })}
                              ></Form.Control>
                            </Form.Group>
                          </Col>
                        </Row>


                        <Row>
                          <Col className="pr-1" md="6">
                            <Form.Group>
                              <label>Phone</label>
                              <Form.Control
                                name="phone"
                                onChange={(e) => this.setState({ phone: e.target.value })}
                                placeholder="phone"
                                // value={this.state.phone}
                                type="tel"
                                maxLength="10"
                              ></Form.Control>
                            </Form.Group>
                          </Col>

                          <Col className="pl-1" md="6">
                            <Form.Group>
                              <label>Password</label>
                              <Form.Control
                                name="password"
                                placeholder="password"
                                onChange={(e) => this.setState({ password: e.target.value })}
                                // value={this.state.password}
                                type="password"
                              ></Form.Control>
                            </Form.Group>
                          </Col>
                        </Row>

                        <Row>
                          <Col className="pr-1" md="6">
                            <Form.Group controlId="exampleForm.ControlSelect1">
                              <label>Role</label>
                              <Form.Control as="select" name="role" placeholder="role" type="text" value={this.state.role} onChange={(e) => this.setState({ role: e.target.value })} >
                                <option placeholder="select" value="nil"></option>

                                <option value="Admin">Admin</option>
                                <option >Doctor</option>
                                <option >Staff</option>
                              </Form.Control>
                            </Form.Group>


                          </Col>


                          <Col className="pr-1" md="6">
                            <Form.Group controlId="exampleForm.ControlSelect1">
                              <label>Speciality</label>
                              <Form.Control as="select" name="speciality" value={this.state.speciality} onChange={(e) => this.setState({ speciality: e.target.value })} placeholder="Speciality" type="text">
                                {/* <option></option> */}

                                <option value="Null"></option>
                                <option>Rheumatology</option>
                                <option>Breast Cancer</option>
                              </Form.Control>
                            </Form.Group>
                          </Col>

                        </Row>



                        <Button
                          className="btn-fill pull-right"
                          type="submit"
                          variant="info"
                          onClick={(e) => { this.submit(e, 0); this.handleModalShowHide() }}

                          closeButton
                        >
                          Add User
                  </Button>
                        <div className="clearfix"></div>
                      </Form>
                    </Card.Body>
                  </Card>
                </Col>

              </Container>



            </Modal.Body>
            {/* <Modal.Footer>
                   <p>Please Fill the Details</p>
                    </Modal.Footer> */}
          </Modal>

        </div>

        <Container fluid>
          <div className="container">
            <div className="row" >
              {/* <Link to={'UserForm'}  > */}
              <Button variant="info" onClick={() => this.handleModalShowHide()} className="btn-fill" type="submit" name="action" style={{ float: "right" }}>
                {/* <i class="material-icons">edit</i> */}
                  ADD USER
                </Button>
            </div>
          </div>
          <br></br>
          <Row>
            <Col md="12">
              <Card className="strpied-tabled-with-hover">
                <Card.Header>
                  <Card.Title as="h4">UserManagement</Card.Title>
                  <p className="card-category">
                    Here is a user data
                </p>
                </Card.Header>
                <Card.Body className="table-full-width table-responsive px-0">
                </Card.Body>
                {/* {this.state.users.length > 0 ? */}
                <table id="example" className="table table-striped table-bordered table-sm row-border hover mb-5">
                  <thead>
                    <tr>
                      <th>Id</th>

                      <th>Name</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Role</th>
                      <th>Speciality</th>
                      <th>Date of Subscribe</th>
                      <th>Edit</th>
                      <th>Delete</th>

                    </tr>
                  </thead>

                  <tbody>
                    {this.state.users.map((user) => (
                      <tr key={user.id}>
                        <td>{user.id}</td>
                        <td>{user.username}</td>
                        <td>{user.email}</td>
                        <td>{user.phone}</td>
                        <td>{user.role}</td>
                        <td>{user.speciality}</td>
                        <td>{user.dateofsub}</td>
                        <td>
                          <Link to={`user/edit/${user.id}`}>
                            <Button variant="primary" className="btn-fill" type="submit" name="action">Edit</Button>
                          </Link>
                        </td>
                        <td>
                          <Button onClick={(e) => this.delete(user.id, e)} variant="danger" className="btn-fill" type="submit" name="action">
                            Delete
                              </Button>
                        </td>
                      </tr>
                    ))}
                  </tbody>

                </table>
                {/* : ""} */}
              </Card>
            </Col>

          </Row>
        </Container>
      </>
    );
  }
}



export default UserManagement;


