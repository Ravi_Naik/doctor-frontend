
import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import Modal from "react-bootstrap/Modal";
import userdetails from '../views/admin'
import Swal from 'sweetalert2'
// react-bootstrap components
import {Badge,Button,Card,Navbar,Nav,Table,Container,Row,Col,NavLink,Form} from "react-bootstrap";






const emailRegex = RegExp(
  /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

const formValid = ({ formErrors, ...rest }) => {
  let valid = true;

  // validate form errors being empty
  Object.values(formErrors).forEach(val => {
    val.length > 0 && (valid = false);
  });

  // validate the form was filled out
  Object.values(rest).forEach(val => {
    val === null && (valid = false);
  });

  return valid;
};



class UserManagement extends React.Component {
  constructor(props){
    super(props);
    this.state ={
      users:[],
      id:0,
      username: "",
      email: "",  
      phone: "",
      password: "",
      role:"",
      speciality:"",
      showHide : false,
      formErrors: {
        username: "",
        email: "",
        phone: "",
        password: "",
        role:"",
        speciality:""
      }


    }

  }

  opensweetalert()
  {
    Swal.fire({
      title: 'User record added',
      text: "sucess",
      type: 'success',
      
    })
  }

  componentDidMount(){
    axios.get(`https://abcapi.vidaria.in/alluserdetails?X-AUTH=abc123`)
    .then((res)=>{
      console.log(res)
      this.setState({
        users:res.data.user,
        id:0,
        username:'',
        email:'',
        phone:'',
        password:'',
        role:'Admin',
        speciality:'Rheumatology',
        activity:"User Added",
        user_id:userdetails(),
        formErrors: {
          username: "",
          email: "",
          phone: "",
          password: "",
          role:"",
          speciality:""
        }

        
      })
    })
  }

handleModalShowHide() {
    this.setState({ showHide: !this.state.showHide })
}


submit(event){
  event.preventDefault();
  
  // const {username, role} = this.state
  console.log(this.state.role)
  

  if (formValid(this.state)) {


  axios.post(`https://abcapi.vidaria.in/addactivity?X-AUTH=abc123`,{

    activity :this.state.activity,
    user_id:this.state.user_id,

  })
    
    axios.post(`https://abcapi.vidaria.in/adduser?X-AUTH=abc123`,{
      username:this.state.username,
      email:this.state.email,
      phone:this.state.phone,
      password:this.state.password,
      role:this.state.role,
      speciality:this.state.speciality,

    })
    .then((res)=>{
      if (res.data.success == "false"){
  
  Swal.fire({
    icon: 'error',
    title: 'Oops...',
    text: res.data.message,
    
  })




      //  this.sameemailsweetalert()
        this.handleModalShowHide()
        
      }
      if(res.data.success == "true"){
        this.opensweetalert()
        this.componentDidMount();
        this.props.history.push(`/admin/umanagement`)
        console.log('yes aya mai')

      }
     
     
     
      console.log(res.data.message)
      console.log(res.data.success)
    })

  } else {
    console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
  }
}



handleChange = e => {
  e.preventDefault();
  const { name, value } = e.target;
  let formErrors = { ...this.state.formErrors };

  switch (name) {
    case "username":
      formErrors.username =
        value.length < 3 ? "minimum 3 characaters required" : "";
      break;
  
    case "email":
      formErrors.email = emailRegex.test(value)
        ? ""
        : "invalid email address";
      break;
      case "phone":
        formErrors.phone =
          value.length < 10 ? "minimum 10 characaters required" : "";
        break;

    case "password":
      formErrors.password =
        value.length < 6 ? "minimum 6 characaters required" : "";
      break;


      case "role":
        formErrors.role =
          value.length < 3 ? "minimum 3 characaters required" : "";
        break;

        case "speciality":
          formErrors.speciality =
            value.length < 3 ? "minimum 3 characaters required" : "";
          break;
    default:
      break;
  }

  this.setState({ formErrors, [name]: value }, () => console.log(this.state));
};






delete(id,e){
  var del="user deleted"

  e.preventDefault()
  Swal.fire({
    title: 'Do you want to delete the changes?',
    showDenyButton: true,
    showCancelButton: false,
    confirmButtonText: `Delete`,
    denyButtonText: `Cancel`,
  }).then((result) => {
    /* Read more about isConfirmed, isDenied below */
    if (result.isConfirmed) {
      Swal.fire('user deleted!', '', 'success')
        axios.delete(`https://abcapi.vidaria.in/userdelete?X-AUTH=abc123&uid=${id}`)
        axios.post(`https://abcapi.vidaria.in/addactivity?X-AUTH=abc123`,{

          activity :del,
          user_id:this.state.user_id,
    
        })
        .then(() => {
        this.componentDidMount();

        });
    } else if (result.isDenied) {
      Swal.fire('user record is safe !', '', 'info')
    }
  })

 

}
  render()
 {
  return (
    <>
            <div>
                






                <Modal 
                
                size="lg"
                 aria-labelledby="example-custom-modal-styling-title"
                
                show={this.state.showHide}>
                    <Modal.Header closeButton onClick={() => this.handleModalShowHide()}>
                    <Modal.Title>Adding User</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>


                    <Container fluid>
        {/* <Row> */}
          <Col className="pr-1" md="12">
            <Card>
              <Card.Header>
                {/* <Card.Title as="h4">Adding User</Card.Title> */}
              </Card.Header>
              <Card.Body>
                <Form onSubmit={(e)=>this.submit(e,this.state.id)} >
             
                  <Row>
                    <Col className="pr-1" md="6">
                      <Form.Group>
                        <label>Username</label>
                        <Form.Control
                      className={formErrors.username.length > 0 ? "error" : null}

                          name="uname"
                          placeholder="Username"
                          type="text"
                          noValidate
                          value={this.state.username}
                          onChange={this.handleChange}

                        ></Form.Control>
                          {formErrors.username.length > 0 && (
                <span className="errorMessage">{formErrors.username}</span>
              )}
                      </Form.Group>
                    </Col>
                   

                    <Col className="pr-1" md="6">
                      <Form.Group>
                        <label>
                          Email address
                        </label>
                        <Form.Control
                                        className={formErrors.email.length > 0 ? "error" : null}

                          placeholder="Email"
                          type="email"
                          value={this.state.email}
                          name="email"
                          pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" 
                          noValidate
                          onChange={this.handleChange}
                        ></Form.Control>
                                  {formErrors.email.length > 0 && (
                <span className="errorMessage">{formErrors.email}</span>
              )}
                      </Form.Group>
                    </Col>
                  </Row>


                  <Row>
                    <Col className="pr-1" md="6">
                      <Form.Group>
                        <label>Phone</label>
                        <Form.Control
                                              className={formErrors.phone.length > 0 ? "error" : null}

                        name="phone"
                        noValidate
                          onChange={this.handleChange}
                        placeholder="phone"
                        value={this.state.phone}
                        type="number"
                        maxLength="10"
                        ></Form.Control>
                                  {formErrors.phone.length > 0 && (
                <span className="errorMessage">{formErrors.phone}</span>
              )}
                      </Form.Group>
                    </Col>

                    <Col className="pl-1" md="6">
                      <Form.Group>
                        <label>Password</label>
                        <Form.Control
                                              className={formErrors.password.length > 0 ? "error" : null}

                          name="password"
                          placeholder="password"
                          noValidate
                          onChange={this.handleChange}                          value={this.state.password}
                          type="password"
                        ></Form.Control>
                                  {formErrors.password.length > 0 && (
                <span className="errorMessage">{formErrors.password}</span>
              )}
                      </Form.Group>
                    </Col>
                  </Row>

                  <Row>
                    <Col className="pr-1" md="6">
                      <Form.Group controlId="exampleForm.ControlSelect1">
                        <label>Role</label> 
                        <Form.Control as="select"                       className={formErrors.role.length > 0 ? "error" : null}
 name="role"  placeholder="role" type="text" value={this.state.role} noValidate
                          onChange={this.handleChange} >
                        <option placeholder="select" value="nil"></option>

                        <option value="Admin">Admin</option>
                        <option >Doctor</option>
                        <option >Staff</option>
                        </Form.Control>
                        {formErrors.role.length > 0 && (
                <span className="errorMessage">{formErrors.role}</span>
              )}
                      </Form.Group>


                    </Col>
                
                  
                    <Col className="pr-1" md="6">
                      <Form.Group   controlId="exampleForm.ControlSelect1">
                        <label>Speciality</label>
                        <Form.Control                       className={formErrors.speciality.length > 0 ? "error" : null}
 as="select" name="speciality" value={this.state.speciality} noValidate
                          onChange={this.handleChange} placeholder="Speciality" type="text">
                        {/* <option></option> */}

                        <option value="Null"></option>
                        <option>Rheumatology</option>
                        <option>Breast Cancer</option>
                        </Form.Control>
                        {formErrors.speciality.length > 0 && (
                <span className="errorMessage">{formErrors.speciality}</span>
              )}
                      </Form.Group>
                    </Col>

                  </Row> 



                  <Button
                    className="btn-fill pull-right"
                    type="submit"
                    variant="info"
                    onClick={(e) => {this.submit(e,0);this.handleModalShowHide()}}

                    closeButton
                  >
                    Add User
                  </Button>
                  <div className="clearfix"></div>
                </Form>
              </Card.Body>
            </Card>
          </Col>
       
      </Container>



                    </Modal.Body>
                    {/* <Modal.Footer>
                   <p>Please Fill the Details</p>
                    </Modal.Footer> */}
                </Modal>

            </div>

      <Container fluid>
        <div className="container">
          <div className="row" >
            {/* <Link to={'UserForm'}  > */}
      <Button variant="info" onClick={() => this.handleModalShowHide()} className="btn-fill" type="submit" name="action" style={{ float: "right" }}>
                  {/* <i class="material-icons">edit</i> */}
                  ADD USER
                </Button>
                </div>
                </div>
                <br></br>
        <Row>
          <Col md="12">
            <Card className="strpied-tabled-with-hover">
              <Card.Header>
                <Card.Title as="h4">UserManagement</Card.Title>
                <p className="card-category">
                  Here is a user data
                </p>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover table-striped">
                  <thead>
                    <tr>
                    <th className="border-0">ID</th>

                     <th className="border-0">Name</th>
                     <th className="border-0">Email</th>
                      <th className="border-0">Phone</th>
                      {/* <th className="border-0">Password</th> */}
                      <th className="border-0">Role</th>
                      <th className="border-0">Speciality</th>
                      <th className="border-0">Date of Subscribe</th>
                      <th className="border-0">Edit</th>
                      <th className="border-0">Delete</th>





            
                    </tr>
                  </thead>
                  <tbody>
                  {
            this.state.users.map(user=>
              <tr key={user.id}>
               <td>{user.id}</td>

                <td>{user.username}</td>
                <td>{user.email}</td>
                <td>{user.phone}</td>
                {/* <td>{user.password}</td> */}
                <td>{user.role}</td>
                <td>{user.speciality}</td>
                <td>{user.dateofsub}</td>


                <td>
                <Link to={`user/edit/${user.id}`}>
             <Button  variant="primary" className="btn-fill" type="submit" name="action">Edit</Button>
             </Link>
                </td>
                <td>
                <Button onClick={(e)=>this.delete(user.id ,e)} variant="danger" className="btn-fill" type="submit" name="action">
                  {/* <i class="material-icons">delete</i> */}
                  Delete
                </Button>
                </td>
              </tr>
              )
          }

                  </tbody>
                </Table>
              </Card.Body>
            </Card>
          </Col>
         
        </Row>
      </Container>
    </>
  );
}
}

export default UserManagement;



